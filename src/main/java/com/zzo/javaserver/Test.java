package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2001  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: Test.java 1297 2005-06-06 19:20:56Z swheat $
 */

import java.util.Iterator;

public class Test
{
        public static int count = 0;
	String i;

	String[] arr;

	public Test()
	{
		i = "Mark Rox";
	}

	public Test(String m)
	{
		i = m;
		arr = new String[5];
		for (int count = 0; count < arr.length; count++)
                {
			arr[count] = count+" String";
                }

	}

	public Test(String m, int j)
	{
		i = m;
	}

	public String get()
	{
		return i;
	}

        public int getCount()
        {
                return count;
        }
	public String[] getArr()
	{
		return arr;
	}

	public String get_string()
	{
		return get();
	}


	/*
        public static void main(String[] args)
        {
                try {
                JavaServer.start();
                }
                catch (java.io.IOException e)
                {
                        System.err.println("Shit howdy: "+e);
                }
                
                System.out.println("Howdy!");
                while(true)
                {
                        try{Thread.sleep(5000);}catch(Exception e){}
                }
        }
	*/

        public PTEST getPTEST()
        {
            return new PTEST();
        }

        private class PTEST implements Iterator
        {
            public Object next()
            {
                return "BLOT";
            }

            public boolean hasNext()
            {
                return false;
            }

            public void remove()
            {
                System.out.println("REMOVE");
            }
        }
}
		
