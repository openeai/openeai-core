package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000, 2001  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * This class is just used for Debug info
 * Nothing interesting here - just a way to turn debug messages
 * on & off & have them in one convenient place
 * @author Mark Trostler
 *
*/
/*
 * $Id: Debug.java 1298 2005-06-06 21:12:45Z swheat $
 */
public class Debug
{
	public static boolean debug = false; 

	/**
 	 * No instantiation for you!
	 */
	public Debug()
	{
		// You can't instantiate me!!!
	}

	/**
	 * Just do a println
	 * @param line The line you wanna print
	 */
	public static void println(String line)
	{
		if (debug)
		{
			System.out.println(System.currentTimeMillis()+": "+line);
			System.out.flush();
		}
	}

	/**
	 * Just do a print
	 * @param line The line you wanna print
	 */
	public static void print(String line)
	{
		if (debug)
		{
			System.out.print(line);
			System.out.flush();
		}
	}
}
