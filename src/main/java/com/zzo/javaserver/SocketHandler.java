package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000, 2001, 2004  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: SocketHandler.java 1299 2005-06-07 07:14:30Z swheat $
 */
import java.net.*;
import java.io.*;

/**
 * This is the sub-class of TCPServer that actually does something
 * with the client connexions - namely it reads in what port the client
 * wants events sent on & then opens a connexion to that port & then
 * passes the three open streams (1 read/write from the client Socket 
 * and one write only for events to Dealer where all the fun begins.
 *
 * It also checks for a shared secret between server & client
 *	if one exists
 *	Thanks Achim Settelmeier for the idea & initial implementation!
 *
 * @author Mark Ethan Trostler
 * @version 1.0
 * @see TCPServer
 * @see Dealer
 */
public class SocketHandler extends TCPServer
{
    /** 
     * This is the input stream from the client socket
     */
    public InputStream is = null;
    /** 
     * This is the output stream from the client socket
     */
    public OutputStream os = null;

    public String authSecret = "";

    // Default event port
    public final int default_event_port = 2001;

    /**
     * Called when a client comes 'a callin'
     * @param data The client socket
     * @see TCPServer
     */
    public void run(Socket data)
    {
	Dealer d = null;

	/* first get streams */
	try
	{
	    is = data.getInputStream();
	    os = data.getOutputStream();
	}
	catch (Exception e)
	{
	    System.out.println("Socket error: " + e);
	    return;
	}

	//
	// Then suck in line about what port to connect to 
	//	to send events
	//
	InputStreamReader isr = new InputStreamReader(is);
	BufferedReader br = new BufferedReader(isr);
	OutputStreamWriter osw = new OutputStreamWriter(os);

	String line = null;

	// Get & check secret
	PrintWriter pw = new PrintWriter(osw);

	logger.debug("Trying to get AUTH secret");
	try
	{
	    line = readLine(br);
	}
	catch (Exception e)
	{
	    logger.info("Couldn't read data from socket during authentication");
	    // Die off
	    return;
	}

	if (!line.startsWith("AUTH: "))
	{
	    logger.warn("Version skew from: "+data.getInetAddress());
	    pw.println("ERROR: Authentication required");
	    pw.flush();
	    return;
	}

	String pass = line.substring(5);
	pass = pass.trim();
	logger.debug("GOT AUTH secret: -"+pass+"-");
	if (!pass.equals(authSecret))
	{
	    logger.warn("Authentication failed from: "+data.getInetAddress());
	    pw.println("ERROR: Authentication failed");
	    pw.flush();
	    return;
	}

	logger.debug("Shipping OK...");

	pw.println("OK");
	pw.flush();

	// Now get event port
	int event_port;
	try
	{
	    line = readLine(br);
	    event_port = Integer.parseInt(line);
	}
	catch (Exception e)
	{
	    event_port = default_event_port;
	}

	logger.debug("Got event port: -"+event_port+"-");
	// Other side ships '-1' if they don't want events
	if (event_port > 0)
	{
	    // Now make a connexion to event_port
	    Socket event_socket = null;
	    OutputStream e_os = null;
	    try
	    {
		event_socket = new Socket(data.getInetAddress(),event_port);
		// We only need the output stream
		e_os = event_socket.getOutputStream();
	    }
	    catch (Exception e)
	    {
		logger.warn("Socket Exception: ", e);
		logger.warn("Tried to connect to: "+data.getInetAddress()+":"+event_port);
		return;
	    }

	    // Fire 'er up...
	    d = new Dealer(br, new OutputStreamWriter(os), new OutputStreamWriter(e_os));
	}
	else
	{
	    // They don't want events
	    d = new Dealer(br, new OutputStreamWriter(os), null);
	}

	d.setDaemon(true);
	shutdownables.add(d);
	d.start();
    }

    /**
     * Like Reader.readLine, but it throws EOFException instead of
     * returning null (at EOF). Fixes NullPointerException problem
     * when connecting to the socket and immediately shutting it down.
     */
    private String readLine(BufferedReader reader) throws IOException {
	String line = reader.readLine();
	if (line == null) {
	    throw new EOFException();
	}
	return line;
    }
}
