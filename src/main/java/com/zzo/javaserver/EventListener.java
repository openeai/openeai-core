package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000, 2001  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: EventListener.java 1299 2005-06-07 07:14:30Z swheat $
 */
import java.util.*;
import java.awt.event.*;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import org.openeai.OpenEaiObject;

/**
 * <pre>
 * This monster implements all conceivable events adaptors
 *	Registered-for events are then output to the output stream
 *
 * ADDING YER OWN EVENTS
 *	If you wanna add yer own Events to listen for you must edit
 *	'EventListener.java' to implement your event adapter interface
 *	and then add the appropriate functions with the one-line body
 *		<code>add_and_return(e);</code>
 *	Which will correctly output your event & the object that caused
 *	it to the event output stream.  Recompile that monkey & yer done!
 * </pre>
 *
 * @author Mark Ethan Trostler
 * @version 1.0
 * @see Dealer
 */
public class EventListener extends OpenEaiObject implements 
				ComponentListener,
				ContainerListener,
				FocusListener,
				KeyListener,
				MouseListener,
				MouseMotionListener,
				WindowListener,
				ActionListener,
				ItemListener,
				AdjustmentListener,
				TextListener
{
	// Handy dandy reference to Dealer object to get at object hash
	Dealer dealer;

	// Where we stash objects we're listening for events for keyed
	//	on the object itself
	Hashtable event_hash;

	// Stream to WRITE out events
	PrintWriter out;

	public EventListener() {}
	/**
	 * Alls we need is an output stream to send events to
	 * @param o PrintWriter stream to output events to
	 */
	public EventListener(PrintWriter o)
	{
		out = o;
		dealer = null;
		event_hash = new Hashtable();
	}

	/**
	 * Oh yeah, we need a Dealer too
	 * @param d Dealer
	 */
	public void setDealer(Dealer d)
	{
		dealer = d;
	}

	/**
	 * Called by Dealer when them some event (de)registration to be
	 *	done
	 * @param line EVT line
	 * @return String result of request
	 * @see Dealer 
	 */
	public String doListener(String line)
	{
		// Event registration
		//
		//	Should look like 
		//	"EVT java.awt.Frame^234("addComponentListener")
		//		or
		//	"EVT java.awt.Frame^234("removeComponentListener")
		//	This'll cause EVERY component event to be delivered
		//	to this client like:
		//	"EVE: java.awt.Frame^234#componentHidden(java.awt.event.ComponentEvent^345)"
		//

		// get rida EVT
		// Should prolly do all this krap in Dealer & just pass
		//	in an object & function name...
		//	Version 2.0 pehaps...
		line = line.substring(3);
		line = line.trim();

		int sep = line.indexOf("(");
		String variable = line.substring(0,sep);
		String func_name = line.substring(sep+2,line.length()-2);

		if (!dealer.object_hash.containsKey(variable))
			return "ERROR: Object "+variable+" does not exist";

		// Get out object to make the add listener call on...
		Object o = dealer.object_hash.get(variable);

		// Add it to our event hash stash
		event_hash.put(o,variable);

		logger.info("Doing event "+func_name+" for "+variable+":"
			+o.hashCode());

		// Now make an array out of our parameter list to invoke
		// the function...
		//	The sole parameter is 'this'
		// Basically we're just gonna call add<event>Listener or
		//	remove<event>Listener with a parameter of 'this'
		// 	to register ourselves for those/that event(s)
		Object[] obj_param_list = new Object[1];
		obj_param_list[0] = this;
		
		// Use Dealer 'getFunction' to find our function
		Method the_function = dealer.getFunction(o.getClass(), obj_param_list, func_name);
		if (the_function == null)
		{
			// Whoops!
			return "ERROR: Cannot find "+func_name + "(" + obj_param_list[0]+")";
		}


		// Call it! (listener calls just return null)
		try
		{
			the_function.invoke(o,obj_param_list);
		}
		catch (Exception e)
		{
			return "ERROR: "+e.toString();
		}
			
		return "OK";
	}

	/** 
	 * <pre>
	 * Called by all the little event implementation functions
	 * 	to output stuff to event output stream
	 * It'll figure out what object this event is associated with
	 * & send output to the event output stream of the form
 	 * EVE: &lt;DOM of event&gt &lt;DOM of object&gt
	 * </pre>
	 * @param o The event object
	 */
	public void add_and_return(EventObject o)
	{
		// Figure out what trew this event...
		String name = null;
		Object event_obj = o.getSource();

		// Look up object in keys hash which is indexed on the
		//	objects themselves...
		logger.info("Got event caused by "+event_obj.hashCode());
		if (event_hash.containsKey(event_obj))
		{
			name = (String)event_hash.get(event_obj);
			logger.info("Object in our hash "+name);

			// Stuff it into our hash & return ref to it
			String hash_value = o.getClass()+"^"+dealer.getObjectCount();
			// get rid of that pesky 'class' value
			hash_value = hash_value.substring(5);
			hash_value = hash_value.trim();

			// Add event object to Dealer's object hash
			dealer.putInHash(hash_value,o);

			// Ship event message
			logger.info("Shipping event: - EVE: "+hash_value+" "+name);
			out.println("EVE: "+hash_value+" "+name);
		}

		// If this thing wasn't in our event hash they musta
		//	BYE'd it & they're smokin' crack 'cuz the object
		//	is still around...
		// We'll just be nice & pass 'em nuthin'
		// In future should be smarter so when an object is BYE'd
		//	we remove ourself as a listener for whatever
		//	events they originally were listening for for this
		//	thang
	}

	/**
	 * This just tells EventListener that this object has been destroyed
	 *	so if we're listening for events for this thang forget
	 *	about it.
	 * @param o The object thats been destroyed
	 */
	public void remove_obj(Object o)
	{
		// Hopefully the object'll be destroyed so we won't get
		// anymore events for this thang anyways 'cuz I don't
		// really wanna hafta keep track of what events each &
		// every object is registered for....
		// So if we do get an event for this thang we'll just 
		// ignore it...
		// One day should be smart enough to remove ourselves
		//	from events but am lazy
/*
		if (event_hash.containsKey(o))
		{
			// So we _were_ listening for events from this
			//	guy...
			event_hash.remove(o);
		}
*/
	}

	///
	// 	MouseMotionListener Events
	///
	public void mouseDragged(MouseEvent e)
	{
		add_and_return(e);
	}
	
	public void mouseMoved(MouseEvent e)
	{
		add_and_return(e);
	}

	//
	//	Component Events
	//
	public void componentHidden(ComponentEvent e)
	{
		add_and_return(e);
	}

	public void componentMoved(ComponentEvent e)
	{
		add_and_return(e);
	}

	public void componentResized(ComponentEvent e)
	{
		add_and_return(e);
	}

	public void componentShown(ComponentEvent e)
	{
		add_and_return(e);
	}

	//
	//	Container Events
	//
	public void componentAdded(ContainerEvent e)
	{
		add_and_return(e);
	}

	public void componentRemoved(ContainerEvent e)
	{
		add_and_return(e);
	}

	//
	//	Focus Events
	//
	public void focusGained(FocusEvent e)
	{
		add_and_return(e);
	}

	public void focusLost(FocusEvent e)
	{
		add_and_return(e);
	}

	//
	//	Key Events
	//
	public void keyPressed(KeyEvent e)
	{
		add_and_return(e);
	}

	public void keyReleased(KeyEvent e)
	{
		add_and_return(e);
	}

	public void keyTyped(KeyEvent e)
	{
		add_and_return(e);
	}

	//
	//	Mouse Events
	//
	public void mouseClicked(MouseEvent e)
	{
		add_and_return(e);
	}
	public void mouseEntered(MouseEvent e)
	{
		add_and_return(e);
	}
	public void mouseExited(MouseEvent e)
	{
		add_and_return(e);
	}
	public void mousePressed(MouseEvent e)
	{
		add_and_return(e);
	}
	public void mouseReleased(MouseEvent e)
	{
		add_and_return(e);
	}

	//
	//	Window Events
	//
	public void windowActivated(WindowEvent e)
	{
		add_and_return(e);
	}
	public void windowClosed(WindowEvent e)
	{
		add_and_return(e);
	}
	public void windowClosing(WindowEvent e)
	{
		add_and_return(e);
	}
	public void windowDeactivated(WindowEvent e)
	{
		add_and_return(e);
	}
	public void windowDeiconified(WindowEvent e)
	{
		add_and_return(e);
	}
	public void windowIconified(WindowEvent e)
	{
		add_and_return(e);
	}
	public void windowOpened(WindowEvent e)
	{
		add_and_return(e);
	}

	//
	//	Action Events
	//
	public void actionPerformed(ActionEvent e)
	{
		add_and_return(e);
	}
	
	//
	//	Item Events
	//
	public void itemStateChanged(ItemEvent e)
	{
		add_and_return(e);
	}

	//
	//	Adjustment Events
	//
	public void adjustmentValueChanged(AdjustmentEvent e)
	{
		add_and_return(e);
	}

	//
	//	Text Events
	//
	public void textValueChanged(TextEvent e)
	{
		add_and_return(e);
	}
} // Ain't pretty but there it is
