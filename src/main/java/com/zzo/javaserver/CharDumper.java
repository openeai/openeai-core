package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000,2001  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: CharDumper.java 1297 2005-06-06 19:20:56Z swheat $
 */
public class CharDumper
{
	public static void dump(String s,String encaps)
	{
		try
		{
			printBytes(s.getBytes(encaps),encaps);
		}
		catch (Exception e)
		{
			System.err.println(encaps+" not supported");
		}
	}

	public static void dump(String s)
	{
		String encaps = System.getProperty("file.encoding");

		try
		{
			printBytes(s.getBytes(),encaps);
		}
		catch (Exception e)
		{
			System.err.println(encaps+" not supported");
		}
	}

	public static void printBytes(byte[] array, String name) 
	{
             for (int k = 0; k < array.length; k++) 
                 System.out.println(name + "[" + k + "] = " + "0x" +
                                    UnicodeFormatter.byteToHex(array[k]));
	}

	public static void print(String s)
	{
		System.out.println("STRING: "+s);
	}
}

