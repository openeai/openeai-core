package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2004  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * This class is just used for logging messages
 * Adaptor for different backend logging infrastructure.
 * @author Mark Trostler
 *
*/
/*
 * $Id: Log.java 1297 2005-06-06 19:20:56Z swheat $
 */
import java.lang.reflect.*;

public class Log
{
    // Generic logger object
    private static Object log = null;

    // Logging methods
    private static Method debug = null;
    private static Method info = null;
    private static Method warn = null;
    private static Method warnWithException = null;

    /**
     * No instantiation for you!
     */
    private Log()
    {
	// You can't instantiate me!!!
    }

    /**
     * Create generic log object
     *  use log4j if they got it - otherwise com.zzo.javaserver.Debug
     */
    private static void create() {

	Class loggerClass = null;
	try {
	    loggerClass = Class.forName("org.apache.log4j.Logger");
	    Method getLogger = loggerClass.getMethod("getLogger", 
		    new Class[] { Class.forName("java.lang.Class")});
	    log = getLogger.invoke(null, new Object[] { Log.class } );
	} catch (Exception e) {
	    log = new Debug();
	}

	// whatever the heck it is - don't matter
	loggerClass = log.getClass();

	try {
	    Class[] objArg = new Class[] {Class.forName("java.lang.Object")};
	    Class[] objArgTh = new Class[] { 
		Class.forName("java.lang.Object"),
		Class.forName("java.lang.Throwable")
	    };

	    debug = loggerClass.getMethod("debug", objArg);
	    info = loggerClass.getMethod("info", objArg);
	    warn = loggerClass.getMethod("warn", objArg);
	    warnWithException = loggerClass.getMethod("warn", objArgTh);
	} catch (Exception e) {
	    System.err.println("Unable to get logging functions");
	    log = null;
	}
    }

    /**
     * generic invoker
     */
    private static void dumpIt(Method m, Object[] args) 
    {
	try {
	    m.invoke(log, args);
	} catch (Exception e) {
	    // Sure would like to log this!
	    System.err.println("Unable to log message: " + args[0]);
	}
    }

    /**
     * Log a debug level message. 
     *
     * @param message The message you wanna print
     */
    public static void debug(Object message)
    {
	if (debug == null)
	    create();

	dumpIt(debug, new Object[] {message});
    }

    /**
     * Log an info level message. 
     *
     * @param message The message you wanna print
     */
    public static void info(Object message)
    {
	if (info == null)
	    create();

	dumpIt(info, new Object[] {message});
    }

    /**
     * Log a warning level message
     *
     * @param message The message you wanna print
     */
    public static void warn(Object message)
    {
	if (warn == null)
	    create();

	dumpIt(warn, new Object[] {message});
    }

    /**
     * Log a warning level message and associated exception
     *
     * @param message The message you wanna print
     */
    public static void warn(Object message, Throwable e)
    {
	if (warnWithException == null)
	    create();

	dumpIt(warnWithException, new Object[] {message, e});
    }
}

