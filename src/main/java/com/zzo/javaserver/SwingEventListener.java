package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000, 2001  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: SwingEventListener.java 1297 2005-06-06 19:20:56Z swheat $
 */
import javax.swing.event.*;
import java.io.PrintWriter;

public class SwingEventListener extends EventListener implements
			AncestorListener,
			CaretListener,
			CellEditorListener,
			ChangeListener,
			HyperlinkListener,
			InternalFrameListener,
			ListDataListener,
			ListSelectionListener,
			MenuDragMouseListener,
			MenuKeyListener,
			MenuListener,
			PopupMenuListener,
			TreeExpansionListener,
			TreeSelectionListener,
			TreeWillExpandListener
{
	
	/**
	 * Alls we need is an output stream to send events to
	 * @param o PrintWriter stream to output events to
	 */
	public SwingEventListener(PrintWriter o)
	{
		super(o);
	}

	// Ancestor events
	public void ancestorAdded(AncestorEvent e)
	{
		add_and_return(e);
	}
	public void ancestorMoved(AncestorEvent e)
	{
		add_and_return(e);
	}
	public void ancestorRemoved(AncestorEvent e)
	{
		add_and_return(e);
	}

	// Caret events
	public void caretUpdate(CaretEvent e)
	{
		add_and_return(e);
	}

	// Cell Editor events
	public void editingCanceled(ChangeEvent e)
	{
		add_and_return(e);
	}
	public void editingStopped(ChangeEvent e)
	{
		add_and_return(e);
	}

	// Change events
	public void stateChanged(ChangeEvent e)
	{
		add_and_return(e);
	}

	// Hyperlink listener
	public void internalFrameActivated(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void internalFrameClosed(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void internalFrameClosing(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void internalFrameDeactivated(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void internalFrameDeiconified(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void internalFrameIconified(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void internalFrameOpened(InternalFrameEvent e)
	{
		add_and_return(e);
	}
	public void hyperlinkUpdate(HyperlinkEvent e)
	{
		add_and_return(e);
	}

	// List Data events
	public void contentsChanged(ListDataEvent e)
	{
		add_and_return(e);
	}
	public void intervalAdded(ListDataEvent e)
	{
		add_and_return(e);
	}
	public void intervalRemoved(ListDataEvent e)
	{
		add_and_return(e);
	}
	
	// List Selection events
	public void valueChanged(ListSelectionEvent e)
	{
		add_and_return(e);
	}

	// Menu Drag Mouse events
	public void menuDragMouseDragged(MenuDragMouseEvent e)
	{
		add_and_return(e);
	}
	public void menuDragMouseEntered(MenuDragMouseEvent e)
	{
		add_and_return(e);
	}
	public void menuDragMouseExited(MenuDragMouseEvent e)
	{
		add_and_return(e);
	}
	public void menuDragMouseReleased(MenuDragMouseEvent e)
	{
		add_and_return(e);
	}

	// Menu Key Events
	public void menuKeyPressed(MenuKeyEvent e)
	{
		add_and_return(e);
	}
	public void menuKeyReleased(MenuKeyEvent e)
	{
		add_and_return(e);
	}
	public void menuKeyTyped(MenuKeyEvent e)
	{
		add_and_return(e);
	}

	// Menu events
	public void menuCanceled(MenuEvent e)
	{
		add_and_return(e);
	}
	public void menuDeselected(MenuEvent e)
	{
		add_and_return(e);
	}
	public void menuSelected(MenuEvent e)
	{
		add_and_return(e);
	}

	// Popup menu events
	public void popupMenuCanceled(PopupMenuEvent e)
	{
		add_and_return(e);
	}
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
	{
		add_and_return(e);
	}
	public void popupMenuWillBecomeVisible(PopupMenuEvent e)
	{
		add_and_return(e);
	}

	// Tree expansion events
	public void treeCollapsed(TreeExpansionEvent e)
	{
		add_and_return(e);
	}
	public void treeExpanded(TreeExpansionEvent e)
	{
		add_and_return(e);
	}

	// Tree selection event
	public void valueChanged(TreeSelectionEvent e)
	{
		add_and_return(e);
	}

	// Tree will expand events
	public void treeWillCollapse(TreeExpansionEvent e)
	{
		add_and_return(e);
	}
	public void treeWillExpand(TreeExpansionEvent e)
	{
		add_and_return(e);
	}
}

