package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000, 2001, 2003-2004  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: JavaServer.java 1299 2005-06-07 07:14:30Z swheat $
 */
import java.io.*;
import org.openeai.OpenEaiObject;

/**
 * <pre>
 * The start class for all this mess.
 *
 * It's just pretty much a shell that starts up SocketHandler.
 * It takes two optional command line arguments in any order:
 *
 *	An optional switch '--authfile=<filename>' specifying a filename
 *		whose first line contains a 'password' that a remote
 *		client must supply to connect to this server
 *		Thanks Achim Settelmeier for the idea & initial implementation!
 * 	An optional port which to start listening for client connexions
 *		(default is 2000)
 *
 * Oh, you can also use 'help' for a VERY imformative help message
 *
 * </pre>
 * @see SocketHandler
 */
public class JavaServer extends OpenEaiObject
{

    private static SocketHandler serv = null;

    // Default port
    public static final int default_port =  2000;

    /*
     * Assume anyone calling this method (& the one below it) are
     * providing their own main - since JavaServer's main calls the
     * 3 argument one
     */
    public static int start (int port) throws java.io.IOException
    {
	// No secret
	return start(port, "", false);
    }

    public static int start() throws java.io.IOException
    {
	// Default port
	return start(default_port);
    }

    /*
     * Start JavaServer with optional password
     */
    public static int start (int port, String secret, boolean standAlone) 
	throws java.io.IOException
    {
	logger.info("Server start {");
	if (serv == null) 
	{
	    logger.info("Server start - starting...");
	    serv = new SocketHandler();
	    if (secret != null)
		serv.authSecret = secret;
	    serv.startServer(port, standAlone);
	    logger.info("Server start }");
	    return 1;
	}

	logger.info("Server start }");
	return 0;
    }

    public static int stop ()
    {
	logger.info("Server stop {");
	if (serv != null) {
	    logger.info("Server stop - stopping...");
	    serv.stopServer();
	    serv = null;
	    logger.info("Server stop }");
	    return 1;
	}
	logger.info("Server stop }");
	return 0;
    }

    /**
     * Yer standard main
     * @param args Only one command line option is allowed and that's
     * the port number to start this monster on - default is 2000.  
     * Oh, you can type 'help' for a nice message too.
     */
    public static void main(String args[])
    {
	// default
	int port = default_port;

	String authSecret = "";	// default no secret

	if (args.length > 0)
	{
	    for (int i = 0; i < args.length; i++)
	    {
		if (args[i].equalsIgnoreCase("help") ||
			args[i].equalsIgnoreCase("-help") ||
			args[i].equalsIgnoreCase("--help"))
		{
		    // Spit out super-helpful usage message
		    System.out.println("java JavaServer [--authfile=<file>] [<port>]");
		    System.exit(0);
		}
		else if (args[i].startsWith("--authfile="))
		{
		    String authFile = args[i].substring(11);
		    // Read first line & use that as our
		    //	secret
		    try
		    {
			BufferedReader r = new BufferedReader(new FileReader(authFile));
			authSecret = r.readLine();
			r.close();
		    }
		    catch (Exception e)
		    {
			System.out.println("Can't read file "+authFile);
			System.exit(1);
		    }
		}
		else
		{
		    // Get port num
		    try
		    {
			port = Integer.parseInt(args[i]);
		    }
		    catch (Exception e)
		    {
			System.out.println("Unknown option/bad port number "
				+ args[i]);
			System.exit(1);
		    }
		}
	    }
	}

	/* 'true' for last arg since we're supplying the main */
	try {
	    start(port, authSecret, true);
	} catch (java.io.IOException e) {
	    logger.warn("Lacking in dice", e);
	}
    }
}
