package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000-2004  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import org.apache.log4j.*;

/**
 *
 * $Id: Dealer.java 1300 2005-06-07 07:44:39Z swheat $
 *
 * <pre>
 * This class takes one-line string commands to manipulate java objects
 *	and returns the results
 * What really makes this vaguely interesting is creating an interface
 *	in another language to talk to this guy... like the one provided
 *	that's in Perl5.  You can re-implement the one in Perl5 if ya
 *	don't like the syntax or better yet write an interface in another
 *	language!
 * So this further stuff is ONLY interesting to people who want to make
 *	their own front end for this madness - if yer just using someone
 *	else's (or my) front end to this stuff you can stop reading rite
 *	now & just read the dox provided w/yer chosen front end...
 * OKAY!!! There is where all the magic takes place!
 * It just needs two Writers (one to talk to the client and one to
 * send events to and one Reader (to read requests in from the client
 * and it'll do its magic!
 * First lets talk about java primitives and java objects:
 *
 *	First off all primitive types are passed using this notation:
 *		&lt;value&gt:&lt;type&gt
 *	(I consider java.lang.String a primitive type)
 *	So possible primitive types are:
 *		int, short, byte, char, long, float, double, boolean,
 *			and string
 *	And the way to specify 'em is:
 *		2309:int 
 *		23:short
 *		34:byte (base 10 only rite now - sorry!)
 *		32:char (decimal only - sorry - blame java!)
 *		234234:long
 *		3.43:float
 *		923.234:double
 *		true:boolean or false:boolean
 *		"This is a string":string
 *	You can also specify an encoding for a String using the ":string_<ENC>
 *		syntax like "This is a Unicode String":string_UTF8.
 *		see http://www.javasoft.com/products/jdk/1.1/docs/guide/intl/encoding.doc.html
 *		for the complete list of valid Java String encodings.
 *
 * SO here's what you can do:
 *	You can create a new java object
 *	You can call a method on a java object you created or on a static
 *		object
 *	You can get/set fields in an object you created or get static fields
 *	You can blow away objects you've created
 *	You can create arrays, get & set values, & get array lengths
 *	You can 'stringize' objects - usually to get actual values of
 *		primitives and strings
 *	You can register & unregister for events
 *
 * All commands you pass MUST begin with a 3 all capital letter command
 *	They are NEW, CAL, GET, SET, BYE, VAL, EVT
 * All responses are contained on one line and can be either:
 * All UNSUCCESSFUL command responses begin with the word "ERROR:"
 *	followed by either a stringification of the java exception that
 *	was thrown or an error message from Dealer itself - also most
 *	ERROR: lines will put the actual Exception object into the hash
 *	& the handle will be passed in the same line separated from the
 *	Exception message with the character sequence '%%%'.
 * All SUCCESSFUL command responses return either:
 * 	"NUL" if the operation did not return a value
 *			-- or --
 *	A &lt;DON&gt (see definition of &lt;DON&gt below)
 *			-- or --
 *	A stringified value requested via the 'VAL' command like "5"
 *
 * Now some definitions that we're gonna use a lot of throughout
 *
 * &lt;FQON&gt = Fully Qualified Java Object Name
 *	This just means something like "java.util.Vector" or "java.awt.Frame"
 *	This is how new java objects are created and make up a portion of
 *	the name of java object you create
 * &lt;DON&gt = Dealer Object Name
 *	This is name 'Dealer' or this monster gives to java objects it
 *	creates - either from a 'NEW' message from you or as the result
 *	of a function call ('CAL') or get ('GET') message or events.
 *	Whenever Dealer creates a java object for whatever reason its
 *	&lt;DON&gt looks like &lt;FQON&gt^&lt;unique ID&gt - Dealer assigns unique
 *	IDs so you don't gotta worry about them... 
 *	SO if you created a java.awt.Frame object it's &lt;DON&gt would look
 *	something like "java.awt.Frame^234"
 *	The client this uses an object's &lt;DON&gt when referencing it - say
 *	for method calls on that object or whatever.
 *	Note: Array references have '[L' and ';' 'wrapped' around the
 *	objects &lt;FQON&gt so they look like "[Ljava.awt.Font;^23"
 *	Basically whatever string Dealer passes back to you when you create
 *	a new object (either via a NEW call or from a method or GET call)
 *	just use that string to refer to that object in the future.
 * &lt;parameter list&gt = Parameter list
 *	Parameter lists are used when creating new objects and calling
 *	functions (including registering for events).
 *	They are a comma separated list of java primitives (using the
 *	aforementioned 'colon' notation (&lt;value&gtlttype&gt &lt;DON&gts of
 *	already existing java objects, NEW commands for objects created
 *	on-the-fly, and GET commands to get fields of either static
 *	or already existing java objects.
 *	Parameter lists may be empty also.
 *
 * Here's how the commands look:
 *
 *	The NEW command - this command creates a new java object
 *	Syntax: 
 *		NEW &lt;FQON&gt&lt;parameter list&gt
 *	Hopefully sorta like what you'd expect!
 *	This ends up looking like:
 *		NEW java.awt.Frame("This is the Frame's Title":string)
 *	   		-- or --
 *		NEW java.util.Vector(33:int)
 *			-- or --
 *		NEW java.awt.Dialog(java.awt.Frame^23,"Dialog's Title":string, true:boolean)
 *	To create a new array reference you must 'wrap' the object's
 *	&lt;FQON&gt with '[L' at the beginning and ';' at the end so
 *	that looks like:
 *		NEW [Ljava.net.Socket;(340)
 *	Which will return an &lt;DON&gt that refers to an array of 340
 *	java.net.Sockets...
 *	To create a new array with a primitive type use:
 *		NEW [L&lt;primitive type>;(5400)
 *	Where &lt;primtive type> can be int,char,float,double,short,
 *		long,boolean, or byte
 *	For instance NEW [Lfloat;(5400)
 *	Will create a float array of 5400 characters.
 *	(Yes I know the 'L' is only for 'real' objects but just roll with it
 *
 *	Note: Multi-dimensional arrays are barely supported so I would
 *		recommend NOT using them!
 *
 * 	Returns: 
 *		"ERROR:" message if there's something wrong
 * 		&lt;DON&gt which you then use to reference this object later
 *
 * 	The CAL command calls a method on an instantiated or static object
 *	Syntax:
 *		CAL &lt;DON&gt;%&lt;function name&gt;(&lt;parameter list&gt) for instantiated objects
 *			-- or --
 *		CAL &lt;FQON&gt;%&lt;function name&gt;(&lt;parameter list&gt;) for static objects
 *	Returns:
 *		"ERROR:" message if something went wrong
 *		"NUL" will be returned for void	function calls 
 *		&lt;DON&gt of the object that was returned.
 *
 *	This looks like:
 *		CAL java.awt.Frame^234%setSize(200:int,400:int)
 *			for an instantiated object
 *			-- or --
 *		CAL java.lang.Class%forName("java.lang.String":string)
 *			for a static method call
 *
 * 	The GET command gets a field, array element, or array length
 *	Syntax:
 *		GET &lt;DON&gt#&lt;field name&gt for instantiated objects
 *			-- or --
 *		GET &lt;FQON&gt#&lt;field name&gt for instantiated objects
 *			-- or --
 *		GET &lt;DON&gt#&lt;index&gt to get an array element
 *			-- or --
 *		GET &lt;DON&gt#LEN to get array length
 *
 *	Returns:
 *		"ERROR:" message if something went wrong
 *		&lt;DON&gt 
 *		In the case the array length command you'll just get a 
 *			plain integer - it will not be 'objectized'.
 *
 *	The SET command is used to set a field or array element
 *	Syntax:
 *		SET &lt;DON&gt#&lt;field name&gt(&lt;value&gt)
 *			for instantiated objects
 *			-- or --
 *		SET &lt;FQON&gt#&lt;field name&gt(&lt;value&gt)
 *			for static objects
 *			-- or --
 *		SET &lt;DON&gt#&lt;index&gt(&lt;value&gt)
 *			to set an array element
 *	Returns: 
 *		"ERROR:" if something went wrong or
 *		"NUL" if everything went right
 *
 *	The BYE command is used when you no longer need the object a
 *	&lt;DON&gt represents.
 *	Syntax:
 *		BYE &lt;DON&gt
 *	Which looks like:
 *		BYE java.lang.String^32
 *	Returns: 
 *		"ERROR:" if the object does not exist 
 *		"NUL" if it did
 *	You may no longer reference the &lt;DON&gt once you've BYE'd it
 *
 * 	The VAL command is used to 'stringify' a &lt;DON&gt
 *	This makes the most sense when you've got a primitive java object
 *	or string & you want its value although it can be used with any
 *	&lt;DON&gt - basically is just calls 'toString' on the object.
 *	Syntax:
 *		VAL &lt;DON&gt
 *	Which looks like
 *		VAL java.lang.String^2349
 *	Returns:
 *		 "ERROR:" on error or
 *		Stringified value if all went well
 *
 *	(By the way congrats if you've got this far!  it's really pretty
 *	simple, yah?  Thanks for reading the dox!)
 *
 *	EVENTS
 *
 *	Since events happen asynchronously they are returned thru a
 *	separate stream than control messages which are only returned
 *	in response to command requests. 
 *	Registering and unregistering for events happens thru the normal
 *	client control channel, only the events themselves are sent thru
 *	the event output stream.  A &lt;DON;&gt for the event object
 *	and the &lt;DOM&gt for the object for which the event occured
 *	are sent down the event output stream when an event for which
 *	you registered on that object occurs.  It makes sense believe me!
 *
 *	Registering for events Syntax:
 *		EVT &lt;DON&gt("add&lt;event type&gt;Listener")
 *	Valid &lt;event type&gt are:
 *		'Component', 'Container', 'Focus', 'Key', 'Mouse',
 *		'MouseMotion', 'Window', 'Action', 'Item', 'Adjustment',
 *		'Text'
 *	So this looks like:
 *		EVT java.awt.Frame^4("addComponenetListener")
 *		Now whenever a Component Event happens on java.awt.Frame^4
 *		it will be output to the event output stream
 *
 *	Unregistering for events Syntax:
 *		EVT &lt;DON&gt("remove&lt;event type&gt;Listener")
 *	Valid &lt;event type&gt are:
 *		'Component', 'Container', 'Focus', 'Key', 'Mouse',
 *		'MouseMotion', 'Window', 'Action', 'Item', 'Adjustment',
 *		'Text'
 *	So this looks like:
 *		EVT java.awt.Frame^4("removeComponenetListener")
 *
 * 	Note: Registering and Unregistering for events use the exact
 *	same syntax so you GOTTA make sure the 'add' or 'remove' part
 *	is in there (it won't find the correct listener function anways
 *	if it's not) BUT the point is that's the only difference between
 *	a register or unregister event command.
 *
 *	What Events looks like
 *	Once you've registered for events using the above syntax & that
 *	event happens a line will be output to the event output stream
 *	that looks like:
 *		EVE: &lt;DOM of event&gt &lt;DOM of object&gt 
 *
 *	EVT commands will return "OK" if all went well or an
 *	"ERROR:" string if not.
 *
 *	ADDING YER OWN EVENTS
 *	If you wanna add yer own Events to listen for you must edit
 *	'EventListener.java' to implement your event adapter interface
 *	and then add the appropriate functions with the one-line body
 *		'add_and_return(e);'
 *	Which will correctly output your event & the object that caused
 *	it to the event output stream.  Recompile that monkey & yer done!
 *	see EventListener for more details.
 *
 * CALLBACK object
 *	A new experimental feature allowing Java code to make
 *		calls into the remote client's space.
 *	This object simply passes String requests to the remote side
 *		& reads back (a possibly multi-lined) String response
 *		which will then get bundled up into our hash table &
 *		returned to the remote client
 *	Thanks Achim Settelmeier for bizarre idea & initial implementation!
 * </pre>
 *
 * @author Mark Ethan Trostler
 * @version 1.0
 * @see SocketHandler
 * @see EventListener
 * @see SwingEventListener
 * @see <a href="http://www.javasoft.com/products/jdk/1.1/docs/guide/intl/encoding.doc.html">Valid Java String encodings</a>
 *
 */ 
public class Dealer extends Thread implements Shutdownable
{
	/**
 	 * This is the output stream to the client
 	 */
	PrintWriter out;

	/**
 	 * This is the input stream from the client
 	 */
	BufferedReader in;

	/** determines if we should continue or not */
	boolean go;

	/**
	 * Hash of created java objects index by their &lt;DON&gt
	 */
	public Hashtable object_hash;

	/** unique ID for created objects */
	int object_count;

	/** the event listener */
	EventListener event_listener;

	/** the magical-mystical NULL token - used to pass in 'null' */
	public static final String NULL_TOKEN = "___NULL___";

	/** The Callback HASH KEY for sending requests to client */
	public String callbackObject;
  
  // Temporary private logger.
  private Category logger = org.openeai.OpenEaiObject.logger;
  
  // A dealer counter.
  private static int DEALER_COUNT = 0;

	/**
	 * Constructor that takes an in & out for the client control
	 * connexion and an out for the event output stream
	 * @param in Input client control reader
	 * @param out Output client control stream
	 * @param out2 Event output stream
	 */
	public Dealer(BufferedReader br, Writer out, Writer out2)
	{
		incrementDealerCount();
    logger.info("[Dealer-" + getDealerCount() + "] Constructing a new " +
      "dealer...");
    this.in = br;
		this.out = new PrintWriter(out);

		go = true;

		// Where we stash all our created objects
		object_hash = new Hashtable();

		// If they want events...
		if (out2 != null)
		{
			// Load a class to handle dealing w/events
			try
			{
				// Just seein' if Swing is available...
				Class c = Class.forName("javax.swing.JFrame");		
	
				// Try to load swing event listener class
				event_listener = new SwingEventListener(new PrintWriter(out2,true));
			}
			catch (Exception e)
			{
				// Nope - must not have swing loaded - use plain
				//	1.1 AWT events
				// erg make sure they've even got the AWT
				try {
				    Class c = Class.forName("java.awt.Frame");
				    event_listener = new EventListener(new PrintWriter(out2,true));
				} 
				catch (Exception e2)
				{
				    // Damnit Jim - no AWT - no events for you
				    event_listener = null;
				}
			}
		}
		else
			event_listener = null;

		// How each of 'em gets a unique name for our hash table
		object_count = 0;
    
    logger.info("[Dealer-" + getDealerCount() + "] Done constructing a new " +
      "dealer.");
	}

  private static void incrementDealerCount() {
    DEALER_COUNT++;
  }
  
  public int getDealerCount() {
    return DEALER_COUNT;
  }

	/**
	 * Pretty much just sit in loop & wait & service requests
	 */
	public void run()
	{
		// Give event dude some info
		if (event_listener != null)
			event_listener.setDealer(this);

		// Set up callbackObject & put in our object hash
		Callback o  = new Callback(this);
		callbackObject = o.getClass()+"^"+getObjectCount();
		callbackObject = callbackObject.substring(5);
		callbackObject = callbackObject.trim();
		putInHash(callbackObject,o);

		while(go)
		{
			StringBuffer line = new StringBuffer();
			String templine = null;
			try
			{
				// Suck it in until we hit ctrl-A starting
				//	a line
				boolean more = true;
				while (more)
				{
					templine = in.readLine();
					if (templine == null || templine.startsWith(""))
						more = false;
					else
						line.append(templine+"\n");
				}

				// get rid of last newline
				if (line != null && line.length() > 0)
				    line.setLength(line.length()-1);
					
				// & spit it out w/ctrl-A @ starting new line
				//	by itself to show we're done sending
				out.println(parse(line.toString())+"\n");
				if (out.checkError())
				{
					throw new IOException("PrintWriter error");
				}
			}
			catch (IOException e)
			{
				logger.info("Lost connexion!");
				go = false;
			}
		}

		// We're outta here
		try 
		{
			in.close();
			out.close();
		}
		catch (Exception e)
		{
			logger.info("Couldn't close sumthin'");
		}
	}

        /**
         * Used to shutdown this thread
         */
        public void shutdown() 
        {
                go = false;
                this.interrupt();
        }

	/*
	 * Allow outside Java program to 'register' a Java
	 *	object w/Dealer so a client can get a hold of
	 *	it...
	 * @param Object to register with JavaServer
	 * @returns a String handle that the client can use to
  	 *	grab this guy
	 */
	public String registerObject(Object o)
	{
		// Stuff it into our hash & return ref to it
		String hash_value = o.getClass()+"^"+getObjectCount();
		// get rid of that pesky 'class' value
		hash_value = hash_value.substring(5);
		hash_value = hash_value.trim();
		putInHash(hash_value,o);
		return hash_value;
	}

	/**
	 * Takes a line & attempts to parse it
	 * @param line The line from the client
	 * @return a String that's output to the client
	 */
	public String parse(String line)
	{
		// not much there!
		if (line == null)
			return null;

		// a NEW request?
		if (line.startsWith("NEW"))
		{
			// Try to instantiate new object
			Object o = null;
			try
			{
				logger.debug("Creating new object: "+line);
				o = createNewObject(line);
				logger.info("Created new object: "+o);
			}
			catch (Exception e)
			{
				return makeErrorResponse(e);
			}

			// Stuff it into our hash & return ref to it
			String hash_value = o.getClass()+"^"+getObjectCount();
			// get rid of that pesky 'class' value
			hash_value = hash_value.substring(5);
			hash_value = hash_value.trim();
			putInHash(hash_value,o);
			return hash_value;
		}
		// A method call?
		if (line.startsWith("CAL"))
		{
			// Function call
			// Ken be static call or object call
			// Looks like java.awt.Frame^2343%show(true:boolean)
			// & we'll create a handle to the returned object
			// 	if any
			// OR CAL java.util.Hashtable^4%get("a":string)
			//	& then it'll just create a new variable
			//	ref for whatever the function returns

			String hash_value = null;

			// Get rid of 'CAL'
			line = line.substring(3);
			line = line.trim();

			// Go 4 it!
			Object o = null;
			try
			{
				o = callFunction(line);
			}
			catch (Exception e)
			{
				return makeErrorResponse(e);
			}

			// Method returned sumthin'
			if (o != null)
			{
				// create a hash entry for this
				//	& return a ref to it...
				hash_value = o.getClass()+"^"+getObjectCount();
				// get rid of that pesky 'class' value
				hash_value = hash_value.substring(5);

				hash_value = hash_value.trim();
				putInHash(hash_value,o);
				return hash_value;
			}
			else
			{
				return "NUL";
			}
		}
		// Blow an object away?
		if (line.startsWith("BYE"))
		{
			// Blow away object
			// 	like BYE java.awt.Frame^0
			line = line.substring(3);
			line = line.trim();

			if (object_hash.containsKey(line))
			{
				Object o = object_hash.get(line);
				object_hash.remove(line);
				if (event_listener != null)
					event_listener.remove_obj(o);
			}
			else
				return "ERROR: Object "+line+" does not exist";

			return "NUL";
		}
		// Get a field?
		if (line.startsWith("GET"))
		{
			// Get a field - static or instantiated
			// GET java.awt.Font#BOLD
			// or GET java.util.Vector^23#elementCount
			// This'll return a object reference...
			// OR GET [Ljava.lang.String;^23#12 to get an
			//	array element
			// OR GET [Ljava.lang.String;^23#LEN to get an
			//	array's length
			// Get rid of GET
			line = line.substring(3);
			line = line.trim();

			String ret_val = null;

			Object o = null;
			try
			{
				o = makeIntoObject(line);
			}
			catch (Exception e)
			{
				return makeErrorResponse(e);
			}

			if (line.endsWith("LEN"))
			{
				// Array length - just return
				//	a stringified int
				return (String)o;
			}

			// Check to see if the return value is 'null'
			if (o == null)
				return "NUL";

			// create a hash entry for this
			//	& return a ref to it...
			ret_val = o.getClass()+"^"+getObjectCount();
			// get rid of that pesky 'class' value
			ret_val = ret_val.substring(5);
			
			ret_val = ret_val.trim();
			putInHash(ret_val,o);
			return ret_val;
		}

		// Set a field or array
		// Looks like SET java.lang.String^23#field(<something>)
		// OR	      SET [Ljava.lang.String^29#3(<something>)
		// OR	      SET java.lang.StaticClass#static_field(<something>)
		if (line.startsWith("SET"))
		{
			// Get rid of SET
			line = line.substring(3);
			line = line.trim();

			try
			{
				// Stupid name I know but I wanted it
				// to be analogous to 'makeIntoObject'
				setIntoObject(line);
			}
			catch (Exception e)
			{
				return makeErrorResponse(e);
			}

			return "NUL";
		}

		// Get a value for an object
		//	Works best on a primitive or String!
		if (line.startsWith("VAL"))
		{
			// looks like VAL java.lang.String^234
			// 	or VAL java.lang.Integer^23

			// Get rid of VAL
			line = line.substring(3);
			line = line.trim();

			if (!object_hash.containsKey(line))
				return "ERROR: "+line+" not in hash!";

			return object_hash.get(line).toString();
		}

		//
		// Event registration
		//
		//	Should look like 
		//	"EVT java.awt.Frame^234("addComponentListener")
		//	This'll cause EVERY component event to be delivered
		//	to this client like:
		//	"EVE: java.awt.Frame^234#componentHidden(java.awt.event.ComponentEvent^345)"
		//
		if (line.startsWith("EVT"))
		{
			if (event_listener != null)
				return event_listener.doListener(line);
			else
				return "ERROR: No event port";
		}
	
		if (line.startsWith("BYTE"))
		{
			// Got some form of byte-level input
			// So first line of form 'BYTE <FQCN> <INFO>'
			// 	so to create raw Strings it'd look like
			//	BYTE java.lang.String <ENCAPS> <byte len>
			//	And then the next 'line' would consist of
			//	<byte len> bytes to pass to String's 
			//	2-parameter constructor

			// Get rid of 'BYTE'
			line = line.substring(4);
			line = line.trim();
			
			int sep = line.indexOf(" ");
			String what = line.substring(0,sep);
			String rest = line.substring(sep+1);

			what = what.trim();
			rest = rest.trim();

			try
			{
				return doByteRequest(what,rest);
			}
			catch (Exception e)
			{
				return makeErrorResponse(e);
			}
		}

		// Request for callback object?
		if (line.startsWith("BCK"))
		{
			return callbackObject;
		}

		// shouldn't even get here!
		return "ERROR: Unknown command "+line;
	}

	public String callback(String sendString)
	{
		// Ship to remote client
		out.println("CALLBACK "+sendString+"\n");
		out.flush();

		StringBuffer line = new StringBuffer();
		String templine = null;

		// Collect response
		try
		{
			// Suck in lines until ctrl-A starting a line
			boolean more = true;
			while(more)
			{
				templine = in.readLine();
				if (templine == null || templine.startsWith(""))
					more = false;
				else
					line.append(templine+"\n");
			}

			// Get rid of last newline
			if (line != null && line.length() > 0)
			    line.setLength(line.length()-1);
		}
		catch (IOException e)
		{
			logger.info("Lost connexion!");
			go = false;
		}

		return (line.toString());
	}	

	/**
	 * Verify a line starts with what it's supposed to & then
	 * 	peels the 3 letter command off
	 *	Ain't really being used...
	 */
	String stripAndCheck(String line, String what) throws Exception
	{
		// First make sure there's a 'CAL' token there...
		if (!line.startsWith(what))
			throw new Exception("Don't start w/"+what);

		// Strip it off!
		line = line.substring(3);
		line = line.trim();

		return line;
	}

	/**
	 * Call a method 
	 */
	Object callFunction(String func) throws Exception
	{
		// Of form 'java.awt.Frame^23434%show(true:boolean)
		// OR 'java.lang.Class%forName("java.lang.String":string)
		//	for static function calls...

		// Okay - figure out what the heck we're tying to create
		//	& make sure it's legit...
		int separator = func.indexOf("%");

		// Malformed monkey
		if (separator < 0)
			throw new Exception("No separator in "+func);

		// Here's our DON
		String object = func.substring(0,separator);

		int first_paren = func.indexOf("(");

		// Malformed monkey
		if (first_paren < 0)
			throw new Exception("No first paren in "+func);

		// Suck out function name between the '%' and '('
		String function_name = func.substring(separator+1,first_paren);
		function_name = function_name.trim();

		// Get parameter list & strip off them parentheses
		String parameter_list = func.substring(first_paren+1,func.length()-1);
		parameter_list = parameter_list.trim();

		Object o = null;
		Class object_class = null;

		logger.debug("IN CAL with "+object);

		// Make sure we got it
		if (object.indexOf("^") > 0)
		{
			// It's one that should already exist
			if (object_hash.containsKey(object))
			{
				o = object_hash.get(object);
				object_class = o.getClass();
			}
			else
				throw new Exception("Object "+object+" not in our hash!");
		}
		else
		{
			// Static function call
			object_class = Class.forName(object);
		}

		logger.debug("found object class: "+object_class);
		logger.debug("Parameter list = "+parameter_list);

		// Gather our objects from parameter list...
		// This'll trow an Exception if something is wrong
		Object[] parameter_objects = parseArgs(parameter_list);

		// Get a Method object for this function
		Method the_function = getFunction(object_class, parameter_objects, function_name);

		logger.debug("WANTING "+function_name+" on "+object+":"+o+" with "+parameter_objects);

		if (the_function == null)
			throw new Exception("Can't find function "+function_name);
		logger.info("Calling function "+function_name+" on "+object+":"+o+" with "+parameter_objects);

		// 'o' will be 'null' for static function calls...
		//	we're returning whatever the function returns...
		Object ret_val = null;

		try
		{
			ret_val = the_function.invoke(o,parameter_objects);
		}
		catch (InvocationTargetException e)
		{
                        logger.warn("Exception in invoked method", e);
			throw convertITE(e);
		}
		catch (Exception e)
		{
                        logger.warn("Exception invoking method", e);
			throw e;
		}

        if (ret_val != null)
		    logger.info("Got back: "+ret_val.toString());

		return ret_val;
			
	}

	/**
	 * This method searches for a matching Method from a function name
	 * a parameter list and a Class object
	 * @param object_class The Class object for the object we're trying
	 *	to to get the function fer
	 * @param parameters The array of objectized parameters we want 
	 * 	this function to take
	 * @param function_name The name of the function we wanna call
	 * @return A Method object that represents the function we wanna 
	 *	call OR null if a Method was not found
	 */
	public Method getFunction(Class object_class, Object[] parameters, String function_name)
	{
		logger.debug("Looking for "+function_name+" with " +parameters);

		// Find our function!
		// Gonna hafta cycle thru each method one-by-one
		Method the_function = matchMethod(parameters, function_name,
                                                  object_class.getMethods());
                if (the_function == null) {
                  the_function
                    = matchMethod(parameters, function_name,
                                  object_class.getDeclaredMethods());
                }

		logger.debug("returning: "+the_function);
                if (the_function == null) {
                  return null;
                }

                logger.debug("Mods: "+Modifier.toString(the_function.getModifiers()));
		return the_function;
	}

  /**********************************************************************
   * Helper method to determine if a given method with specific signature
   * exists in an array of methods.
   *
   * @param parameters The parameter types for the desired method.
   * @param methodName The name of the desired method.
   * @param methods    The Methods to search
   *
   * @return The desired Method or null if no matching Method is found.
   **/
  private Method matchMethod(Object[] parameters, String methodName,
                             Method[] methods) {
    for (int i = 0; i < methods.length; i++) {
      Class[] types = methods[i].getParameterTypes();
			// This'll check if this object list matches
			//	this type list
			// Again java ain't that brite so I gotta do it
			//	myself
      if (typeMatch(parameters,types)) {
				// now check function name!!!
        logger.debug(methods[i].getName()+" vs. "+methodName);
        if (methods[i].getName().equals(methodName)) {
          logger.debug("FOUND A MATCH!");
          if (!methods[i].isAccessible()) {
            methods[i].setAccessible(true);
          }
					// We have a winner!
          return methods[i];
				}
			}
		}

    return null;
  }
   
  /**********************************************************************
   * Helper method to find a named field within a class.  First try to
   * access it as a PUBLIC field (possibly inherited) and secondly as
   * a protected or private field (but not inherited).
   *
   * @param clazz      The Class that contains the given field
   * @param name       The name of the desired field
   *
   * @return The desired Field
   *
   * @throws NoSuchFieldException if a field with the specified name
   * is not found.
   **/
  private Field matchField(Class clazz, String name)
    throws NoSuchFieldException {
    Field f;
    try {
      f = clazz.getField(name);
    } catch (NoSuchFieldException e) {
      f = clazz.getDeclaredField(name);
    }
    if (!f.isAccessible()) {
      f.setAccessible(true);
    }
    return f;
	}
		
	/* this is a fun one */
	Object createNewObject(String obj) throws Exception
	{
		//////////////////////////////////////////////////////
		// Trying to create a new object from something that
		// looks like "NEW java.util.Vector(5:int,6:int)"
		// 	or even to use an existing object
		//	"NEW java.awt.Dialog(java.awt.Frame^2349)"
		//	or even 
		//	"NEW java.awt.FontMetrics(NEW java.awt.Font("helvetica:string,java.awt.Font#BOLD,8:int))"
		//	to use static variables and instantiated vars
		// 	to create new objects on the fly that'll just get
		//		consumed - just like a function call...
		//	Use java.awt.Frame^2349#FIELDNAME to get actual
		//		instantiated field
		//	OR can create a NEW array like
		//	NEW [Ljava.awt.Frame;(3(,3,4.5))
		//		numbers in parens are for optional
		//		additional dimensions...
		//////////////////////////////////////////////////////

		obj = stripAndCheck(obj,"NEW");

		// Okay - figure out what the heck we're tying to create
		//	& make sure it's legit...
		int first_paren = obj.indexOf("(");

		// Malformed monkey
		if (first_paren < 0)
			throw new Exception("No first paren in "+obj);

		// FQON of object
		String object_to_create = obj.substring(0,first_paren);

		// Get parameter list & strip off them parentheses
		String parameter_list = obj.substring(first_paren+1,obj.length()-1);
		parameter_list = parameter_list.trim();

		if (object_to_create.startsWith("["))
		{
			// It's a freakin' array!
			// Pass object name & indicie list
			return createArray(object_to_create,parameter_list);
		}

		// Make sure this class exists
		Class new_class = null;
		try
		{
			new_class = Class.forName(object_to_create);
		}
		catch (ClassNotFoundException e)
		{
			throw new Exception("Class "+object_to_create+" don't exist!");
		}

		// This'll trow an Exception if something is wrong
		//	if one of the parameter type is screwey that is
		Object[] parameter_objects = parseArgs(parameter_list);
		
		logger.debug("Args");
		for (int i = 0; i < parameter_objects.length; i++)
		{
			logger.debug("Parameter Object "+i+" is "+parameter_objects[i]);
		}

		// Okay - so we've got a Object-ized parameter list & an
		//	object - just gotta find the rite constructor
		//	& call it!
		// java ain't that brite so i gotta do it myself
		Constructor[] constructors = new_class.getConstructors();
		Constructor the_constructor = null;
		for (int i = 0; i < constructors.length; i++)
		{
			Class[] types = constructors[i].getParameterTypes();
			// This'll check if this object list matches
			//	this type list
			if (typeMatch(parameter_objects,types))
			{
				the_constructor = constructors[i];
				break;
			}
		}

		if (the_constructor == null) {
                  StringBuffer err
                    = new StringBuffer("No constructor found: ");
                  err.append(object_to_create);
                  err.append("(");
                  for (int i = 0; i < parameter_objects.length; i++) {
                    err.append(parameter_objects[i]);
                    if (i != parameter_objects.length - 1) {
                      err.append(", ");
                    }
                  }
                  err.append(")");
                  throw new Exception(err.toString());
                }

		logger.debug("Found a constructor");

		// Now actually call that bad boy to create our new object
		//	AND I'm spent...
        Object o;
        try {
            o = the_constructor.newInstance(parameter_objects);
        }
        catch (InvocationTargetException e)
        {
            // Unwrap Exception thrown by underlying constructor
                logger.warn("Exception in constructor", e);
                throw convertITE(e);
        }
        catch (Exception e)
        {
                logger.warn("Exception invoking constructor", e);
                throw e;
        }

        return o;
	}

	//
	// This takes an object name of the form '[L(object};'
	//	and a comma separated list of indicies & returns a newly
	//	created array of objects with indicie dimensions...
	// Reflection support for multi-dimensional arrays is pretty
	//	krappy so I'll letcha create 'em but yer on yer own
	//	accessing 'em...
	//
	Object createArray(String object, String indicies) throws Exception
	{
		// First strip off '[L' and ';' nonsense
		object = object.substring(2,object.length()-1);

		// Now turn indicies into an array of ints...
		StringTokenizer st = new StringTokenizer(indicies,",");

		// This'll tell me how many indicies there are...
		int length = st.countTokens();

		int[] indicie_array = new int[length];
		int i = 0;

		// Blow thru each argument...
		while(st.hasMoreTokens())
		{
			String index = st.nextToken();
			// This'll trow an Exception if there's some
			//	funny stuff here...
			indicie_array[i++] = Integer.parseInt(index);
		}

		// Check if they wanna create a primitive array...
		if (object.indexOf(".") < 0)
		{
			// Want a primitive array
			if (object.equalsIgnoreCase("byte"))
			{
				byte[] arr = new byte[indicie_array[0]];
				return arr;
			}
			if (object.equalsIgnoreCase("int"))
			{
				int[] arr = new int[indicie_array[0]];
				return arr;
			}
			if (object.equalsIgnoreCase("short"))
			{
				short[] arr = new short[indicie_array[0]];
				return arr;
			}
			if (object.equalsIgnoreCase("long"))
			{
				long[] arr = new long[indicie_array[0]];
				return arr;
			}
			if (object.equalsIgnoreCase("boolean"))
			{
				boolean[] arr = new boolean[indicie_array[0]];
				return arr;
			}
			if (object.equalsIgnoreCase("double"))
			{
				double[] arr = new double[indicie_array[0]];
				return arr;
			}
			if (object.equalsIgnoreCase("float"))
			{
				float[] arr = new float[indicie_array[0]];
				return arr;
			}
		}
		else
		{	
			// Hokay - almost thar... new Class instance
			Class class_obj = Class.forName(object);

			// Les do it...
			return Array.newInstance(class_obj,indicie_array);
		}

		return null;
	}
			
	// This is HUGE - it blows thru an entire argument list &
	//	returns a array of objects - very key
	Object[] parseArgs(String args) throws Exception
	{
		// 'args' is a comma separated list of values
		// 	that we're magically gonna turn into an
		//	array of Objects for a function or constructor
		//	call...
		Vector objects = new Vector();

		// Separated by ctrl-A's
		StringTokenizer st = new StringTokenizer(args,"");

		// Blow thru each argument...
		while(st.hasMoreTokens())
		{
			String arg = st.nextToken();
			arg = arg.trim();
			// This does all the real work
			Object retval = makeIntoObject(arg);

			objects.addElement(retval);
		}

		// Now take our Vector & turn it into an array
		Object[] obj_array = new Object[objects.size()];
		objects.copyInto(obj_array);

		return obj_array;
	}

	// This is MASSIVE!
	//	Take a string & converts it into the requisite object
	Object makeIntoObject(String arg) throws Exception
	{
		// Take this String & return the object associated
		//	with it...
		Object o;

		logger.debug("Objectifying "+arg);

		// Is the argument null?
		//	Return magical-special NULL token
		if (arg.equalsIgnoreCase(NULL_TOKEN))
			return null;

		// Do we gotta create a new primitive for this monkey?
		//	or 'is this a primitive'?

		if (arg.indexOf(":") > 0)
			return createPrimitive(arg);

		// An field in an object (static or instantiated)
		if (arg.indexOf("#") > 0)
			return getField(arg);

		// Maybe they're trying to create a new object?
		if (arg.startsWith("NEW"))
			return createNewObject(arg);

		// Nope - guess it's already in our hash...
		// 	of form 'java.awt.Frame^12231'
		if (object_hash.containsKey(arg))
		{
			o = object_hash.get(arg);
/*
			if (o.isArray())
			{
				// do some special processing on it:
				//  namely if this is a primitive array
				//	them 'primitizie' it...
				// So now it's impossible to have and array
				//	of 'Character' 'cuz this will convert
				//	it to an array of 'char'	
				o = extractArray(o);
			}
*/
			return o;
		}
		else
			throw new Exception("Object "+arg+" not in our hash!");
	}


	// Used for setting fields & arrays
	//	line looks like
	//	java.lang.String^234#<field>(<value>)
	//		for instantiated objects
	//	[Ljava.lang.String^234;#<index>(<value>)
	//		for instantiated arrays
	//	java.lang.String#<field>(<value>)
	//		for static objects
	//		(unclear when'd ya ever need that last one!)
	//	Don't return nuthin' BUT will throw an Exception if
	//		something bad happens
	void setIntoObject(String arg) throws Exception
	{
		int sep = arg.indexOf("#");
		// An field in an object (static or instantiated)
		if (sep < 0)
			throw new Exception("Missing '#' in SET");
		
		String obj = arg.substring(0,sep);
		obj = obj.trim();

		// Now 'rest is <field>(<value>)
		//	or	<index>(<value>)
		String rest = arg.substring(sep+1);

		int paren = rest.indexOf("(");
		if (paren < 0)
			throw new Exception("Malformed SET: "+arg);

		// This is either a field name or integer index
		String field_or_index = rest.substring(0,paren);

		// There's only 1 parameter value
		String param = rest.substring(paren+1,rest.length()-1);
		Object param_object = makeIntoObject(param);

		// Now get our object & class
		// This looks JUST LIKE 'getField'
		Object o = null;
		Class object_class = null;

		// It's an real object... (vs. being a static field)
		if (obj.indexOf("^") > 0)
		{
			if (object_hash.containsKey(obj))
			{
				o = object_hash.get(obj);
				object_class = o.getClass();
			}
			else
				throw new Exception("Object "+obj+" doesn't exist!");
		}
		else
			object_class = Class.forName(obj);

		// Are we setting an element in an array?
		if (obj.startsWith("["))
		{
			// It's an array!
			//	Set param_object @ index in 'o'
			Array.set(o,Integer.parseInt(field_or_index),param_object);
			return;
		}

		// Get field
		Field f = matchField(object_class, field_or_index);

		// If it's a static field the object 'o' will be null
		f.set(o,param_object);	
	}
			
	/**
	 * Takes a <value>:<primitive type> thang & returns an Objectified
	 *	version of it
	 * Throws an Exception if that value ain't of that type
	 */
	Object createPrimitive(String primitive) throws Exception
	{
		// We gotta split this thing on the ':'
		String type, value;
		int index = primitive.lastIndexOf(":");

		value = primitive.substring(0,index);
		type = primitive.substring(index+1);

		// Blow thru all the types & see what we got

		if (type.equalsIgnoreCase("int"))
		{
			int num;
			num = Integer.parseInt(value);
			return new Integer(num);
		}
		if (type.equalsIgnoreCase("short"))
		{
			short num;
			num = Short.parseShort(value);

			return new Short(num);
		}
		if (type.equalsIgnoreCase("byte"))
		{
			byte num;
			num = Byte.parseByte(value);

			return new Byte(num);
		}
		if (type.equalsIgnoreCase("char"))
		{
			char num;
			num = (char)value.charAt(0);

			return new Character(num);
		}
		if (type.equalsIgnoreCase("long"))
		{
			long num;
			num = Long.parseLong(value);

			return new Long(num);
		}
		if (type.equalsIgnoreCase("float"))
		{
			return new Float(value);
		}
		if (type.equalsIgnoreCase("double"))
		{
			return new Double(value);
		}
		if (type.equalsIgnoreCase("boolean"))
		{
                        return new Boolean(value);
		}
		if (type.startsWith("string"))
		{
			// Get rid of quotes
			value = value.substring(1);
			String encoding;
			int enc = type.indexOf("_");
			if (enc > 0)
			{
				// A string w/some encoding...
				encoding = type.substring(enc+1);
				return new String(value.substring(0,value.length()-1).getBytes(),encoding);
			}
				
			// Else it's a 'normal' string
			return new String(value.substring(0,value.length()-1));
		}

		throw new Exception("I don't know the primitive type "+type);
	}

	// Handles GETs...
	Object getField(String object) throws Exception
	{
		// We've got something of the form
		//	java.awt.Font*BOLD & we wanna return
		//	the object associated w/this monster...
		int split = object.indexOf("#");

		String obj = object.substring(0,split);
		String field = object.substring(split+1);

		Object o = null;
		Class object_class = null;

		// It's an real object... (vs. being a static field)
		if (obj.indexOf("^") > 0)
		{
			if (object_hash.containsKey(obj))
			{
				o = object_hash.get(obj);
				object_class = o.getClass();
			}
			else
				throw new Exception("Object "+obj+" doesn't exist!");
		}
		else
			object_class = Class.forName(obj);

		// Maybe alls they want is an array length?
		if (obj.startsWith("[") && field.equalsIgnoreCase("len"))
		{
			// They just want this array's length...
			int len = Array.getLength(o);
			return Integer.toString(len);
		}

		// Want an element outta the array...
		if (obj.startsWith("["))
		{
			// It's an array!
			//	Just want an array element
			//	Return object @ index
			return Array.get(o,Integer.parseInt(field));
		}

		//
		// Any of these following 2 functions can trow an Exception
		//

		// Get field
		Field f = matchField(object_class, field);

		// If it's a static field the object 'o' will be null
		return f.get(o);	
	}

	// Return TRUE is these objects match these types
	// Since java ain't smart about matching primitives & objectified
	//	versions of them even tho using the 'invoke' & other
	//	reflection function calls work w/the objectified versions
	//	i gotta figure out if they match myself
	boolean typeMatch(Object[] objects, Class[] types)
	{
		// Gotta be same length first of all
		if (objects.length != types.length)
			return false;

		logger.debug("typeMatch: here we go... "+objects.length);

		// Okay - start blowing thru one by one
		for (int i = 0; i < objects.length; i++)
		{
			// Just check to see if this object can be
			//	converted into something of that type...

			// If the argument is null & the type of this
			//	arguemnt is NOT a primitive type then
			//	it's legit - otherwise bogus
			if (objects[i] == null)
			{
				if (types[i].isPrimitive())
					return false;

				continue;	// Move along 
			}

			Class parameter_class = objects[i].getClass();
			logger.debug(i+": "+parameter_class+" vs. "+types[i]);

			// First make sure primitives are handled rite
			if (checkPrimitives(parameter_class,types[i]))
				continue;

			// Now check real objects
			if (!types[i].isAssignableFrom(parameter_class))
			{
				logger.debug("not assignable from...");
				return false;
			}
		}

		// Lookin' good
		return true;
	}

	// The only time we wanna do this check is if one of these things
	//	is a primitive & the other ain't
	//  	like java.lang.Integer vs. int

	boolean checkPrimitives(Class c1, Class c2)
	{
		Class prim = null;
		Class obj = null;

		logger.debug("CheckPrim: "+prim+" vs. "+obj);

		if (c1.isArray() && c2.isArray())
			return true;

		// Make sure one of 'em is a primitive or array
		if (!c1.isPrimitive() && !c2.isPrimitive())
		{
			logger.debug("Neither is prim!");
			return false;
		}
		
		// If they're both primitives let the normal 
		//	'isAssignableFrom' function handle 'em...
		if (c1.isPrimitive() && c2.isPrimitive())
			return false;
		
		// Figure out what's what
		if (c1.isPrimitive())
		{
			prim = c1;
			obj = c2;
		}
		else
		{
			prim = c2;
			obj = c1;
		}


		String prim_name = prim.toString();
		String obj_name = obj.toString();

		logger.debug(prim_name +" vs. "+obj_name);
		// Now gotta go thru primitives one by one lookin'
		//	for a match...

		// Check integer
		if (prim_name.equals("int") && 
			obj_name.equals("class java.lang.Integer"))
				return true;

		// Check boolean
		if (prim_name.equals("boolean") && 
			obj_name.equals("class java.lang.Boolean"))
				return true;

		// Check double
		if (prim_name.equals("double") && 
			obj_name.equals("class java.lang.Double"))
				return true;

		// Check float
		if (prim_name.equals("float") && 
			obj_name.equals("class java.lang.Float"))
				return true;

		// Check char
		if (prim_name.equals("char") && 
			obj_name.equals("class java.lang.Character"))
				return true;

		// Check byte
		if (prim_name.equals("byte") && 
			obj_name.equals("class java.lang.Byte"))
				return true;

		// Check short
		if (prim_name.equals("short") && 
			obj_name.equals("class java.lang.Short"))
				return true;

		// Check long
		if (prim_name.equals("long") && 
			obj_name.equals("class java.lang.Long"))
				return true;

		// Check void
		if (prim_name.equals("void") && 
			obj_name.equals("class java.lang.Void"))
				return true;

		// No go
		return false;
	}

	/**
	 * Just a centralized place to add objects to our hash
	 * 	Note I keep 2 hashes - one indexed by DOMs and 
	 * 	the other by the objects themselves - used for
	 *	event tracking.
	 * @param key String &lt;DOM&gt that represents this object
 	 * @param value The actual Java object that this &lt;DOM&gt represents
	*/
	public synchronized String putInHash(String key, Object value)
	{
		// get rid of that pesky 'class' value
		if (key.startsWith("class"))
			key = key.substring(5);
		key = key.trim();
		object_hash.put(key,value);
		return key;
	}

	/**
	 * Just a central synchronzied place to get/update our
	 * unique ID for created java objects
	 * @return Unique integer ID used for creating DOMs
	 */
	public synchronized int getObjectCount()
	{
		return object_count++;
	}

	/**
	 * This will take an array of Objectized-primitives like
	 *	Character or Byte and return a 'primitized' version
	 *	of it, like an array of char or byte
	 */
	public Object extractArray(Object o)
	{
		/*
		Class c = o.getClass();
		int length = o.length;
		*/
		return null;
	}

	/**
	 * This function is sorta the clearing house for BYTE requests
	 */
	public String doByteRequest(String object, String info) throws Exception
	{
		// The only one we support now
		if (object.equals("java.lang.String"))
		{
			logger.debug("IN do Byte Request!\n");
			// info field is '<encaps> <byte len>'
			int sep = info.indexOf(" ");
			String encaps = info.substring(0,sep);
			encaps = encaps.trim();
			String s_byte_len = info.substring(sep+1);
			int byte_len = Integer.parseInt(s_byte_len);
			logger.debug("Got encaps of "+encaps+" and len of "+byte_len);

			out.println("GIMMIE");
			out.flush();

			logger.debug("Waiting to read...\n");

			String int_line = in.readLine();
			int_line = int_line.trim();

			logger.debug("Just read in line "+int_line);

			StringTokenizer st = new StringTokenizer(int_line," ");

			// This'll tell me how many bytes there are...
			int length = st.countTokens();
			if (length != byte_len)
			{
				logger.debug("Different number of bytes! "+length+" vs. "+byte_len);
			}
	
			byte[] b_array = new byte[length];
			int i = 0;
	
			// Blow thru each argument...
			while(st.hasMoreTokens())
			{
				String index = st.nextToken();
				index = index.trim();
				// This'll trow an Exception if there's some
				//	funny stuff here...

				logger.debug("Reading : "+index);

				try {
					b_array[i++] = (byte)Integer.parseInt(index);
				}
				catch (Exception e)
				{
					logger.warn("ERROR: ", e);
					logger.warn("TOK: "+index);
					throw(e);
				}
			}
			
			String new_string = new String(b_array,encaps);
            // create a hash entry for this
            //      & return a ref to it...
            String ret_val = new_string.getClass()+"^"+getObjectCount();

            ret_val = putInHash(ret_val,new_string);
            return ret_val;
        }

       return "ERROR: BYTE request on "+object+" no supported";
    }

public static void printBytes(byte[] array, String name) {
             for (int k = 0; k < array.length; k++) 
                 System.out.println(name + "[" + k + "] = " + "0x" +
                                    UnicodeFormatter.byteToHex(array[k]));
	}

public static void printChars(char[] array, String name) {
             for (int k = 0; k < array.length; k++) 
                 System.out.println(name + "[" + k + "] = " + "0x" + array[k]);
	}

  /**********************************************************************
   * Extract the Exception from an InvocationTargetException
   **/
  private static Exception convertITE(InvocationTargetException e) {
    Throwable t = e.getCause();
    if (t instanceof Exception) {
      return (Exception) t;
    }
    return e;
  }

	/**
	 * Formulates a correct error response when
	 *	an Exception was thrown as a result of some
	 *	remote call
	 **/
	public String makeErrorResponse(Exception e)
	{
                // If this is an InvocationTargetException, extract
                // the interesting part
                if (e instanceof InvocationTargetException) {
                  e = convertITE((InvocationTargetException) e);
                }
          
                logger.warn("Error invoking Java", e);

		// Stuff it into our hash & return ref to it
		String hash_value = e.getClass()+"^"+getObjectCount();
		// get rid of that pesky 'class' value
		hash_value = hash_value.substring(5);
		hash_value = hash_value.trim();
		putInHash(hash_value,e);

		// Format our error string
		return "ERROR: "+e.toString()+"%%%"+hash_value;
	}

} // class

/*
 * $Revision: 1300 $
 * $Log
 */
