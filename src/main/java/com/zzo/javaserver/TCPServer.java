package com.zzo.javaserver;

/*
 * Open interface to JVM
 * Copyright (C) 2000, 2001, 2004  Mark Ethan Trostler ZZO Associates
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/*
 * $Id: TCPServer.java 1299 2005-06-07 07:14:30Z swheat $
 */
import java.net.*;
import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.openeai.OpenEaiObject;

/**
 * This is the base class for a TCP Server.
 *
 * Shamelessly copied from 'Java Network Programming'
 *	by Merlin and Conrad Hughes
 *	   Michael Shoffner
 *	   Maria Winslow
 *	(c) 1997 Manning Publications Co.
 *
 *	This is pretty standard stuff.
 */
public class TCPServer extends OpenEaiObject implements Cloneable, Runnable, Shutdownable 
{
    /**
     * Count of how many TCPServer threads have been created.
     */
    private static int threadCount = 0;

    /**
     * Thread in which this server operates
     *
     * @field runner
     * @see java.lang.Thread
     *
     */
    public Thread runner = null;
    /**
     * The server socket
     */
    public ServerSocket server = null;
    /**
     * The client socket
     */
    public Socket data = null;

    /** 
     * The list of shutdownable objects that we should shutdown
     * on stopServer 
     */ 
    protected static List shutdownables = new LinkedList();

    /**
     * This function will start the server in a new Thread.
     * Only different 'standAlone makes it to daemon-ize (or not) the
     *	initial thread
     * @param port	Integer port number
     * @param standAlone    Is JavaServer running the show?	
     */
    public synchronized void startServer(int port, boolean standAlone) 
	throws IOException
    {
	if (runner == null)
	{
	    logger.debug("Starting server");
	    server = new ServerSocket(port);
	    runner = new Thread(this, "JavaServer TCPServer #" + threadCount++);
	    if (!standAlone)
		runner.setDaemon(true);
	    runner.start();
	    shutdownables.add(this);
	} 
    }

    /**
     * This function will stop the server. 
     * 
     */
    public void stopServer()
    {
	if (server != null)	
	{
	    try
	    {
		server.close();
		server = null;

		for (Iterator i = shutdownables.iterator(); 
			i.hasNext(); ) {
		    Shutdownable s = (Shutdownable) i.next();
		    s.shutdown();
		}
	    }
	    catch (IOException e)
	    {
		;
	    }
	}
    }

    /**
     * Used to shutdown this thread
     */
    public void shutdown() 
    {
	logger.debug("Server shutdown");
	server = null;
	runner.interrupt();
    }

    /**
     * This function checks the server socket;
     * needs to be sync'd otherwise race conditions
     * may occur
     */
    public void checkSocket()
	throws java.io.IOException, java.lang.CloneNotSupportedException
	{
	    logger.debug("checkSocket: " + this + " {");
	    if (server != null)
	    {
		/* 
		 * suck in connexions & spawn 'em off
		 */
		Socket datasocket = null;
		try {
		    datasocket = server.accept();
		} catch(IOException e) {
		    logger.warn("Socket barf", e);
		    shutdown();
		    return;
		} 

		logger.debug("got connexion");
		TCPServer newSocket = (TCPServer) clone();
		newSocket.server = null;
		newSocket.data = datasocket;
		newSocket.runner = new Thread(newSocket, 
			"Javaserver TCPServer #" + threadCount++);
		logger.debug("checkSocket: " + this + " created new socket " 
			+ newSocket);
		newSocket.runner.setDaemon(true);
		newSocket.runner.start();
		shutdownables.add(newSocket);
	    }
	    logger.debug("checkSocket: " + this + " }");
	}

    /**
     * Accepts connexions & spawns a new thread to deal w/'em
     * @see #run(java.net.Socket)
     *
     */
    public void run()
    {
	logger.debug("run: " + this + " {");
	if (server != null)
	{
	    while(server != null)
	    {
		try
		{
		    checkSocket();
		}
		catch (Exception e)
		{
		    logger.warn("Something bad happened ", e);
		}
	    }
	}
	else
	{
	    run(data);
	}
	logger.debug("run: " + this + " }");
    }


    /**
     * Implemented by sub-class
     * @param data client socket
     */
    public void run(Socket data)
    {
    }
}
