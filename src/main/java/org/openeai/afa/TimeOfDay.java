/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

import org.openeai.*;

/**
 * This class is used to determine when on a given day a Schedule should execute.
 * By hour and minute.  The ScheduleRuntime object contains a list of these objects
 * that are used to determine time(s) of day in which the Schedule should run.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class TimeOfDay extends OpenEaiObject {

  private int m_hour;
  private int m_minute;
  private String m_timeZone = "";

  /**
   * Constructor
   */
  public TimeOfDay() {
  }

/**
 * Sets the hour of the day in which the Schedule should run.
 * <P>
 * @param hour int hour of the day (24 hour basis)
 */
  public void setHour(int hour) {
    m_hour = hour;
  }
/**
 * Returns the hour of the day in which the Schedule should run.
 * <P>
 * @return int hour of the day (24 hour basis)
 */
  public int getHour() {
    return m_hour;
  }

/**
 * Sets the minute of the hour in which the Schedule should run.
 * <P>
 * @param minute int minute of the hour (60 minute basis)
 */
  public void setMinute(int minute) {
    m_minute = minute;
  }
/**
 * Returns the minute of the hour in which the Schedule should run.
 * <P>
 * @return int minute of the hour (60 minute basis)
 */
  public int getMinute() {
    return m_minute;
  }
}

