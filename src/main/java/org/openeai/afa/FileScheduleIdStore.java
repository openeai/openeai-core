/*******************************************************************************
 $Source$
 $Revision: 2748 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

import java.io.*;
import org.openeai.config.AppConfig;
import org.openeai.config.PropertyConfig;

/**
 * This maintains a list of all Schedules that have been executed by a given ScheduledApp.
 * The Schedule is responsible for clearing the list of executed schedules when the day changes (12am).
 * As a Schedule is executed, its Schedule Id is added to this store so the Schedule won't execute more than
 * once during a 24 hour period.  Additionally, this store is persisted to a configurable
 * location so the store survives application starting and stopping.
 * <P>
 * This implementation, supports storing and loading schedule ids from the 
 * file system.  The configuration information for the repository is specified in 
 * the ScheduledAppConfig element.
 *<P>
 * @author      Tod Jackson (tod@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see Schedule
 * @see DbScheduleIdStore
 * @see ScheduledApp
 * @see org.openeai.config.ScheduledAppConfig
 */
public class FileScheduleIdStore extends ScheduleIdStoreImpl implements ScheduleIdStore {

//  private Vector m_scheduleIds = new Vector();
  private String m_path = null;

  /**
   * Constructor
   */
  public FileScheduleIdStore() {
  }

  public final void init(AppConfig aConfig) throws ScheduleIdStoreException {
    setAppConfig(aConfig);
    try {
      PropertyConfig pConfig = new PropertyConfig();
      pConfig = (PropertyConfig)getAppConfig().getObjectByType(pConfig.getClass().getName());
      setPath(pConfig.getProperties().getProperty("ScheduleIdPath",null));
      load();
    }
    catch (Exception e) {
      String errMessage = "Exception loading the FileSchedulIdStore.  Exception: " + e.getMessage();
      logger.fatal(errMessage, e);
      throw new ScheduleIdStoreException(errMessage, e);
    }
  }
  
/**
 * Loads any previously ran schedule ids into memory when the application starts.  This is
 * how the store survives application starting and stopping.
 * <P>
 */
  public final void load() throws ScheduleIdStoreException {
    // load all schedule ids from persistent store
    if (getPath() != null) {
      // load them from the file system
      File aFile = new File(getPath());
      File[] fileList = aFile.listFiles();
      if (fileList != null) {
        logger.debug("There are " + fileList.length + " files.");
        for (int i=0; i<fileList.length; i++) {
          File aScheduleIdFile = fileList[i];
          if (aScheduleIdFile.isFile()) {
            logger.info("Adding previously run ScheduleId: " + aScheduleIdFile.getName());
            add(aScheduleIdFile.getName());
          }
        }
      }
      else {
        logger.info("No existing schedule ids to load.");
      }
    }
  }

  /**
   * Sets the path to which schedule ids will be written.
   * @param path String path name
   */
  public final void setPath(String path) {
    if (path == null) {
      m_path = "ScheduleIds";
    }
    else {
      m_path = "";
    }
  }
  /**
   * Returns the path to which schedule ids will be written.
   * @return String path name
   */
  public final String getPath() {
    return m_path;
  }

  /**
   * Removes all schedule ids from the repository that have an id containing the pattern passed in.
   * @param schedulePattern String schedulePattern (normally, the app name and the day name)
   */
  public final void removeSchedulesLike(String schedulePattern) throws ScheduleIdStoreException {
    boolean tryAgain = true;
    boolean foundMatch = false;
    while (tryAgain) {
      foundMatch = false;
      for (int i=0; i<getScheduleIds().size(); i++) {
        String scheduleId = (String)getScheduleIds().get(i);
        if (scheduleId.indexOf(schedulePattern) != -1) {
          foundMatch = true;
          logger.info("Removing " + scheduleId + " from the Schedule Id Repository.");
    		  File schedFile = new File(getPath() + "/" + scheduleId);
          schedFile.delete();
          getScheduleIds().remove(i);
        }
      }
      tryAgain = foundMatch;
    }
  }
  /**
   * Removes a specific schedule id from the repository that has an id equal to the schedule id passed in.
   * @param scheduleId (normally, the app name and the day name and the time (hour/minute))
   */
  public final void remove(String scheduleId) throws ScheduleIdStoreException {
    int index = getScheduleIds().indexOf(scheduleId);
    if (index != -1) {
      // remove the schedule id from the vector of "executed" schedules so we'll
      // execute it again if we're suppose to execute it on this day.
      logger.info("Removing " + scheduleId + " from the Schedule Id Repository.");
  		File schedFile = new File(getPath() + "/" + scheduleId);
      schedFile.delete();
      getScheduleIds().remove(index);
    }
  }

  /**
   * Determines if a particular Schedule Id exists in this store.
   * @return boolean true if the schedule id exists, false if not.
   */
  /*
  public final boolean contains(String scheduleId) throws ScheduleIdStoreException {
    if (m_scheduleIds.contains(scheduleId)) {
      return true;
    }
    else {
      return false;
    }
  }
  */

  /**
   * Adds a Schedule Id to this store.  Also, persists the schedule id to the 
   * file system (in this case) so it will be available when/if the application
   * is restarted.
   * @param scheduleId String the schedule id to be added to the store.
   */
  public final void add(String scheduleId) throws ScheduleIdStoreException {
    File store = new File(getPath());
    if (store.exists() == false) {
      store.mkdirs();
    }

    try {
  		new FileOutputStream(getPath() + "/" + scheduleId);
      getScheduleIds().add(scheduleId);
    }
    catch (Exception e) {
      throw new ScheduleIdStoreException(e.getMessage(), e);
    }
  }
}

