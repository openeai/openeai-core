/*******************************************************************************
 $Source$
 $Revision: 845 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

import java.util.Vector;

import org.apache.log4j.Logger;
import org.openeai.config.AppConfig;

public class ScheduleIdStoreImpl extends org.openeai.OpenEaiObject {
    public static Logger logger = Logger.getLogger(ScheduleIdStoreImpl.class);
      private Vector m_scheduleIds = new Vector();
  private AppConfig m_appConfig = null;

  public ScheduleIdStoreImpl() {
      logger.info("init");
  }
  
  /**
   * Determines if a particular Schedule Id exists in this store.  This method 
   * only checks what's in memory.  If there is another persistent store that may 
   * be written to, that store would have to be queried as well.
   * <P>
   * @return boolean true if the schedule id exists, false if not.
   */
  public boolean contains(String scheduleId) throws ScheduleIdStoreException {
    if (getScheduleIds().contains(scheduleId)) {
      return true;
    }
    else {
      return false;
    }
  }

  protected void setAppConfig(AppConfig aConfig) {
    m_appConfig = aConfig;
  }
  protected AppConfig getAppConfig() {
    return m_appConfig;
  }
  protected void setScheduleIds(Vector v) {
    m_scheduleIds = v;
  }
  protected Vector getScheduleIds() {
    return m_scheduleIds;
  }
}