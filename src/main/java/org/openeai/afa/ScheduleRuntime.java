/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

import org.openeai.*;
import org.openeai.xml.*;
import org.jdom.Element;
import org.jdom.Attribute;
import java.util.ArrayList;

/**
 * This is a wrapper class that helps a Schedule determine if it should execute
 * the commands that Schedule manages.  Basically, by day and time(s).  Each Schedule
 * that gets configured has a list of these as specified in the application's deployment
 * document.
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 */
public class ScheduleRuntime extends OpenEaiObject {

  private String m_day = "";
  private java.util.List m_times = null;

  /**
   * Constructor
   */
  public ScheduleRuntime(Element eConfig) {
    setDay(eConfig.getChild("Day").getAttribute("name").getValue());
    java.util.List eTimes = eConfig.getChild("Times").getChildren("Time");
    for (int i=0; i<eTimes.size(); i++) {
      Element eTime = (Element)eTimes.get(i);
      TimeOfDay timeOfDay = new TimeOfDay();
      timeOfDay.setHour(Integer.parseInt(eTime.getChild("Hour").getText()));
      timeOfDay.setMinute(Integer.parseInt(eTime.getChild("Minute").getText()));
      addTime(timeOfDay);
    }
  }

/**
  * Sets the day of the week that the Schedule should be executed.  Set based on
  * information found in the Config document.
  *
  * @param day String the day of the week the schedule should execute.
  */
  public final void setDay(String day) {
    m_day = day;
  }
/**
  * Returns the day of the week that the Schedule should be executed.  Set based on
  * information found in the Config document.
  *
  * @return  String the day of the week the schedule should execute.
  */
  public final String getDay() {
    return m_day;
  }

/**
  * Returns a list of TimeOfDay objects indicating when during a particular day
  * the Schedule should run.
  *
  * @return  ArrayList of TimeOfDay objects.
  */
  public final java.util.List getTimes() {
    return m_times;
  }
  private void addTime(TimeOfDay timeObj) {
    if (m_times == null) {
      setTimes(new ArrayList());
    }
    m_times.add(timeObj);
  }
  private void setTimes(java.util.List times) {
    m_times = times;
  }
}

