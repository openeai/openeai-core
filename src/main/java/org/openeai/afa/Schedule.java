/*******************************************************************************
 $Source$
 $Revision: 3968 $
 *******************************************************************************/

/**********************************************************************
 This file is part of the OpenEAI Application Foundation or
 OpenEAI Message Object API created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to build commercial integration software or to implement
 integrations for your enterprise, visit http://www.OpenEai.org/licensing.
 */

package org.openeai.afa;

// General
import java.util.*;
import java.text.*;
import java.lang.reflect.*;

import org.openeai.*;
import org.openeai.config.CommandConfig;
import org.openeai.config.ScheduleConfig;
import org.openeai.loggingutils.MailService;
import org.openeai.loggingutils.MailService;

/**
 * This is a class that wraps the execution details of a set of commands. It
 * keeps track of when a particular command(s) should execute. The ScheduledApp
 * class will use these objects to determine when commands associated to this
 * Schedule should run. Then the ScheduledApp will execute the command(s)
 * associated with this Schedule.
 * <P>
 * The Schedule object allows a flexible mechanism to associate certain times,
 * days or intervals at which the ScheduledCommands associated to this schedule
 * should run. By querying this information the ScheduledApp component can
 * determine if the ScheduledCommands associated to this Schedule should run and
 * if so, execute them.
 * 
 * @author Tod Jackson (tod@openeai.org)
 * @author Steve Wheat (steve@openeai.org)
 * @version 4.0 - 27 April 2005
 */
public class Schedule extends OpenEaiObject {

    private String m_name = "";
    private HashMap m_commands = new HashMap();
    private boolean m_immediate = false;
    private java.util.List m_runtimes = null;
    private ScheduleIdStore m_scheduleIdStore = null;
    private Hashtable m_days = new Hashtable();
    private Hashtable m_dayNames = new Hashtable();
    private int m_currentScheduleDay = 999; // Day schedule was started, or last
                                            // day schedule was executed.
    private MailService m_mailService = null;
    public static final String SUNDAY = "Sunday";
    public static final String MONDAY = "Monday";
    public static final String TUESDAY = "Tuesday";
    public static final String WEDNESDAY = "Wednesday";
    public static final String THURSDAY = "Thursday";
    public static final String FRIDAY = "Friday";
    public static final String SATURDAY = "Saturday";

    /**
     * Constructs the Schedule object with a ScheduleConfig object which wraps
     * the Schedules information found in the ScheduledApp's deployment
     * document.
     */
    public Schedule(ScheduleConfig sConfig) throws InstantiationException {
        setName(sConfig.getName());
        setImmediate(sConfig.isImmediate());
        setScheduleRuntimes(sConfig.getScheduleRuntimes());
        setMailService(sConfig.getMailService());
        Iterator keys = sConfig.getCommandConfigs().keySet().iterator();
        while (keys.hasNext()) {
            CommandConfig cConfig = (CommandConfig) sConfig.getCommandConfigs().get(keys.next());
            try {
                addCommand(cConfig.getName(), cConfig);
            } catch (Exception e) {
                e.printStackTrace();
                throw new InstantiationException(e.getMessage());
            }
        }

        m_days.put(SUNDAY, new Integer(Calendar.SUNDAY));
        m_days.put(MONDAY, new Integer(Calendar.MONDAY));
        m_days.put(TUESDAY, new Integer(Calendar.TUESDAY));
        m_days.put(WEDNESDAY, new Integer(Calendar.WEDNESDAY));
        m_days.put(THURSDAY, new Integer(Calendar.THURSDAY));
        m_days.put(FRIDAY, new Integer(Calendar.FRIDAY));
        m_days.put(SATURDAY, new Integer(Calendar.SATURDAY));

        m_dayNames.put(new Integer(Calendar.SUNDAY), SUNDAY);
        m_dayNames.put(new Integer(Calendar.MONDAY), MONDAY);
        m_dayNames.put(new Integer(Calendar.TUESDAY), TUESDAY);
        m_dayNames.put(new Integer(Calendar.WEDNESDAY), WEDNESDAY);
        m_dayNames.put(new Integer(Calendar.THURSDAY), THURSDAY);
        m_dayNames.put(new Integer(Calendar.FRIDAY), FRIDAY);
        m_dayNames.put(new Integer(Calendar.SATURDAY), SATURDAY);

        Calendar now = Calendar.getInstance();
        m_currentScheduleDay = now.get(Calendar.DAY_OF_WEEK);

        /*
         * FileScheduleIdStore sidstore = new FileScheduleIdStore(); // if the
         * 'ScheduleIdPath' property doesn't exist, the ScheduleIdStore will //
         * default it to 'ScheduleIds'
         * sidstore.setPath(sConfig.getProperties().getProperty
         * ("ScheduleIdPath",null)); try { sidstore.load(); } catch (Exception
         * e) {
         * logger.fatal("Exception loading the SchedulIdStore.  Exception: " +
         * e.getMessage(), e); } setScheduleIdStore(sidstore);
         */
        // if sConfig has a DbConnectionPool in it, we'll use a different
        // constructor
        // which will take create a db connection pool for the store, otherwise,
        // it will use the file system to determine what has run already...
    }

    public void stop() {
        Iterator it = m_commands.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            ScheduledCommand s = (ScheduledCommand) m_commands.get(key);
            try {
                logger.info("Shutting down ScheduledCommand '" + key + "'");
                s.shutdown();
            } catch (Exception e) {
                logger.warn("Error shutting down ScheduledCommand '" + key + "'", e);
            }
        }
        logger.info("All ScheduledCommands have been shutdown.");
    }

    public void setScheduleIdStore(ScheduleIdStore store) {
        m_scheduleIdStore = store;
    }

    public ScheduleIdStore getScheduleIdStore() {
        return m_scheduleIdStore;
    }

    /**
     * Adds a command to this schedule. This will be performed when AppConfig is
     * instantiating and configuring the ScheduledApp that contains this
     * schedule. This method will instantiate the command by passing the
     * CommandConfig object and link that command to a name. Then when it is
     * determined that the Schedule is suppose to run, it will be able to return
     * the command(s) associated to this Schedule for execution.
     * 
     * @param cConfig
     *            CommandConfig the CommandConfig object that AppConfig has made
     *            available and is used instantiate the command associated to
     *            this Schedule.
     */
    public final void addCommand(String name, CommandConfig cConfig) throws InstantiationException, NoSuchMethodException {
        logger.debug("Adding a CommandConfig named: " + name);
        String className = cConfig.getClassName();
        try {
            logger.info("Instantiating Scheduled Command: " + name + "->" + className);
            Class[] parms = { cConfig.getClass() };
            java.lang.Class obj = java.lang.Class.forName(className);
            try {
                Constructor c = obj.getConstructor(parms);
                Object[] o = { cConfig };
                try {
                    m_commands.put(name.toLowerCase(), c.newInstance(o));
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.fatal(e.getMessage(), e);
                    throw new InstantiationException(e.getMessage());
                }
            } catch (NoSuchMethodException ne) {
                logger.fatal(ne.getMessage(), ne);
            }
        } catch (Exception e1) {
            logger.fatal(e1.getMessage(), e1);
            throw new InstantiationException(e1.getMessage());
        }
    }

    /**
     * Returns a ScheduledCommand associated to this schedule by name. This will
     * be called by the ScheduledApp when it determines that this schedule
     * should execute. It will retrieve command(s) from this Schedule and call
     * their 'execute' methods.
     * 
     * @param name
     *            String command name.
     * @return ScheduledCommand an implementation of the ScheduledCommand
     *         interface.
     */
    public final ScheduledCommand getCommand(String name) {
        logger.debug("Looking for a CommandConfig named: " + name);
        return (ScheduledCommand) m_commands.get(name);
    }

    /**
     * Sets all ScheduledCommands associated to this schedule. This is called
     * when the Schedule is initialized.
     * 
     * @param commands
     *            HashMap of ScheduledCommands.
     */
    public final void setCommands(HashMap commands) {
        logger.debug("Adding " + commands.size() + " command configs.");
        m_commands = commands;
    }

    /**
     * Returns all ScheduledCommands associated to this schedule.
     * 
     * @return HashMap of ScheduledCommands.
     */
    public final HashMap getCommands() {
        return m_commands;
    }

    /**
     * Sets the Schedule name according to information found in the config
     * document for the application being configured.
     * 
     * @param name
     *            String the Schedule name
     */
    public final void setName(String name) {
        m_name = name;
    }

    /**
     * Returns the Schedule name according to information found in the config
     * document for the application being configured.
     * 
     * @return String the Schedule name
     */
    public final String getName() {
        return m_name;
    }

    /**
     * Returns a List that contains ScheduleRuntime objects associated to this
     * Schedule. These will be used to determine if it's time to run this
     * schedule.
     * 
     * @return java.util.List
     */
    public final java.util.List getScheduleRuntimes() {
        return m_runtimes;
    }

    private void addScheduleRuntime(ScheduleRuntime rTime) {
        if (m_runtimes == null) {
            setScheduleRuntimes(new ArrayList());
        }
        m_runtimes.add(rTime);
    }

    private void setScheduleRuntimes(java.util.List rTimes) {
        m_runtimes = rTimes;
    }

    private void setImmediate(boolean immediate) {
        m_immediate = immediate;
    }

    /**
     * This method sets the MailService object that will be associated to the
     * Schedule as specified in the deployment document. This MailService object
     * will be used by the ScheduleApp foundation to notify someone if a
     * 'daemon' type application (ScheduledCommand) throws an exception during
     * execution.
     * 
     * @param m
     *            the mail service object to associate to this schedule.
     */
    @Override
    public void setMailService(MailService m) {
        m_mailService = m;
    }

    /**
     * This method returns the MailService object that will be associated to the
     * Schedule as specified in the deployment document. This MailService object
     * will be used by the ScheduleApp foundation to notify someone if a
     * 'daemon' type application (ScheduledCommand) throws an exception during
     * execution.
     * 
     * @return MailService the mail service object.
     */
    @Override
    public MailService getMailService() {
        return m_mailService;
    }

    /**
     * Determines if the ScheduledCommands associated to this Schedule should
     * run. This is determined by a number of factors all configured via the
     * ScheduledApp's deployment document.
     * <P>
     * Factors that determine when a Schedule should be ran include the
     * Schedule's 'runtime'. The Schedule object contains a list of 'runtimes'
     * associated to this Schedule that tell it the Days and Times that this
     * Schedule should be ran. When a Schedule has been executed, it will update
     * its ScheduleIdStore indicating that the Schedule has been run. This store
     * is checked to ensure a Schedule is not executed more than once in a given
     * day. This store is persisted to a configurable location to survive the
     * application stopping and starting. When the day changes (midnight is
     * reached) the Schedule will remove any Schedule id's from the store for
     * the new day so they will again be executed.
     * 
     * @return boolean, true if the ScheduledCommands associated to this
     *         Schedule should run, false if not.
     * @throws ScheduleIdStoreException
     *             if errors occur determining if the Schedule should be ran.
     * @see ScheduleRuntime ScheduleRuntime
     * @see ScheduleIdStore ScheduleIdStore
     */
    public final boolean shouldRun() throws ScheduleIdStoreException {
        java.util.List rTimes = getScheduleRuntimes();
        if (rTimes == null) {
            return false;
        }
        logger.debug("ScheduleRuntimes size is: " + rTimes.size());
        for (int i = 0; i < rTimes.size(); i++) {
            ScheduleRuntime sRuntime = (ScheduleRuntime) rTimes.get(i);
            java.util.List times = sRuntime.getTimes();
            if (times == null) {
                continue;
            }
            logger.debug("Times size is: " + times.size());
            for (int j = 0; j < times.size(); j++) {
                TimeOfDay tod = (TimeOfDay) times.get(j);

                Calendar calendar = new GregorianCalendar();
                java.util.Date currentDate = new java.util.Date();
                calendar.setTime(currentDate);

                String year = Integer.toString(calendar.get(Calendar.YEAR));
                String month = Integer.toString(calendar.get(Calendar.MONTH) + 1);
                String dayOfWeek = Integer.toString(calendar.get(Calendar.DAY_OF_WEEK));
                int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                String dayOfMonth = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                String hour = Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
                String minute = Integer.toString(calendar.get(Calendar.MINUTE));
                String second = Integer.toString(calendar.get(Calendar.SECOND));
                String subSecond = Integer.toString(calendar.get(Calendar.MILLISECOND));
                String timeZone = TimeZone.getDefault().getDisplayName();

                String scheduleId = getName() + "-" + sRuntime.getDay() + "-" + Integer.toString(tod.getHour()) + "-"
                        + Integer.toString(tod.getMinute());
                logger.debug("Checking if ScheduleId " + scheduleId + " should run.  ScheduleId: " + scheduleId);
                if (iDayOfWeek != m_currentScheduleDay) {
                    // the day has changed, since the schedule was last
                    // executed.
                    m_currentScheduleDay = iDayOfWeek;

                    // remove all the schedule ids from the vector of "executed"
                    // schedules
                    // for this app on this day so we'll
                    // execute it again if we're suppose to execute it on this
                    // day.
                    Integer dayNum = new Integer(m_currentScheduleDay);
                    String sCurrentDayName = (String) m_dayNames.get(dayNum);
                    logger.info("Day of week has changed.  Removing all schedules like " + getName() + "-" + sCurrentDayName
                            + " from ScheduleId repository.");
                    getScheduleIdStore().removeSchedulesLike(getName() + "-" + sCurrentDayName);
                }

                java.util.Date scheduleDate = new java.util.Date();
                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss SSS");

                String sDate = month + "-" + dayOfMonth + "-" + year + " " + Integer.toString(tod.getHour()) + ":"
                        + Integer.toString(tod.getMinute()) + ":00 000";

                try {
                    scheduleDate = formatter.parse(sDate);
                } catch (Exception e) {
                    // throw new InvalidFormatException(e.getMessage());
                }

                Integer sdow = (Integer) m_days.get(sRuntime.getDay());
                int scheduleDayOfWeek = sdow.intValue();
                logger.debug("Schedule Day of Week: " + scheduleDayOfWeek + ".  System 'dow' is " + dayOfWeek);
                logger.debug("Schedule Date: " + scheduleDate + ".  System Date: " + currentDate);
                if (scheduleDayOfWeek == Integer.parseInt(dayOfWeek)) {
                    if (currentDate.after(scheduleDate)) {

                        // it might be time to execute the schedule

                        if (getScheduleIdStore().contains(scheduleId) == false) {
                            getScheduleIdStore().add(scheduleId);
                            logger.info("Schedule " + scheduleId + " needs to run.");
                            return true;
                        } else {
                            logger.info("Schedule " + scheduleId + " has already been run.");
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Tells the caller if this Schedule is suppose to run immediately.
     * 
     * @return boolean
     */
    public final boolean isImmediate() {
        return m_immediate;
    }
}
