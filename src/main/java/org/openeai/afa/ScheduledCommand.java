/*******************************************************************************
 $Source$
 $Revision: 992 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

/**
 * This is the interface that all ScheduledCommands must implement.  It defines
 * one method (execute) that they must implement.  It is in this execute method
 * that the specific business logic for the command is coded.  The ScheduledApp component
 * will execute these commands when the Schedule they are associated to is met.
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see ScheduledCommandImpl
 * @see ScheduledApp
 */
public interface ScheduledCommand {

	/**
	 * The execute method that must be implemented by all ScheduledCommand implementations is where the specific
   * business logic associated to the ScheduledCommand is implemented.  The the ScheculedApp component determines
   * that the ScheduledCommand implementation must be executed, it will call this method and the business logic
   * associated to that ScheduledCommand will be executed.
   *
   * @return int generally anything other than -1 indicates success to the ScheduledApp which calls this method.
   * @throws ScheduledCommandException if errors occur executing the business logic associated to the ScheduledCommand.
	 */
  public int execute() throws ScheduledCommandException;
  public void shutdown() throws ScheduledCommandException;
}

 
