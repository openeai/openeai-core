/*******************************************************************************
 $Source$
 $Revision: 885 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

import org.openeai.config.AppConfig;
import org.openeai.dbpool.EnterpriseConnectionPool;
import java.sql.*;

/**
 * This maintains a list of all Schedules that have been executed by a given ScheduledApp.
 * The Schedule is responsible for clearing the list of executed schedules when the day changes (12am).
 * As a Schedule is executed, its Schedule Id is added to this store so the Schedule won't execute more than
 * once during a 24 hour period.  Additionally, this store is persisted to a configurable
 * location so the store survives application starting and stopping.
 * <P>
 * This implementation, supports storing and loading schedule ids from a database.  The configuration information 
 * for the repository is specified in the ScheduledAppConfig element.
 *<P>
 * @author      Tod Jackson (tod@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see Schedule
 * @see FileScheduleIdStore
 * @see ScheduledApp
 * @see org.openeai.config.ScheduledAppConfig
 */
public class DbScheduleIdStore extends ScheduleIdStoreImpl implements ScheduleIdStore {
  private EnterpriseConnectionPool m_connPool = null;

  public DbScheduleIdStore() {
  }
  
  public final void init(AppConfig aConfig) throws ScheduleIdStoreException {
    setAppConfig(aConfig);
    try {
      m_connPool = (EnterpriseConnectionPool)getAppConfig().getObjectByType("org.openeai.dbpool.EnterpriseConnectionPool");
      load();
    }
    catch (Exception e) {
      String errMessage = "Exception loading the DbSchedulIdStore.  Exception: " + e.getMessage();
      logger.fatal(errMessage, e);
      throw new ScheduleIdStoreException(errMessage, e);
    }
  }
  
/**
 * Loads any previously ran schedule ids into memory when the application starts.  This is
 * how the store survives application starting and stopping.  This ScheduleIdStore loads 
 * previously ran schedules from a database structure (T_PROCESSED_SCHEDULES).
 * <P>
 */
  public final void load() throws ScheduleIdStoreException {
    // load all schedule ids from persistent store

		PreparedStatement queryStmt = null;
		String queryString = "SELECT PROCESSED_SCHEDULE_ID " +
											 "FROM T_PROCESSED_SCHEDULES";

		logger.info("Retrieving existing schedules from repository.");
		try {
  		java.sql.Connection conn = m_connPool.getConnection();
			queryStmt = conn.prepareStatement(queryString);
			queryStmt.clearParameters();
			ResultSet results = queryStmt.executeQuery();
      while (results.next()) {
				String scheduleId = results.getString(1);
        logger.info("Found existing schedule '" + scheduleId + "'");
        getScheduleIds().add(scheduleId);
      }
			queryStmt.close();
		}
		catch (Exception e) {
      if (queryStmt != null) {
  			try { queryStmt.close(); } catch (SQLException sqle) { }
      }
			throw new ScheduleIdStoreException(e.getMessage(), e);
		}
  }

  /**
   * Removes all schedule ids from the repository that have an id containing the pattern passed in.
   * @param schedulePattern String schedulePattern (normally, the app name and the day name)
   */
  public final void removeSchedulesLike(String schedulePattern) throws ScheduleIdStoreException {
    boolean tryAgain = true;
    boolean foundMatch = false;
    while (tryAgain) {
      foundMatch = false;
      for (int i=0; i<getScheduleIds().size(); i++) {
        String scheduleId = (String)getScheduleIds().get(i);
        if (scheduleId.indexOf(schedulePattern) != -1) {
          foundMatch = true;
          logger.info("Removing " + scheduleId + " from the Schedule Id Repository.");
          remove(scheduleId);
          getScheduleIds().remove(i);
        }
      }
      tryAgain = foundMatch;
    }
  }

  /**
   * Removes a specific schedule id from the repository that has an id equal to the schedule id passed in.
   * @param scheduleId (normally, the app name and the day name and the time (hour/minute))
   */
  public final void remove(String scheduleId) throws ScheduleIdStoreException {
    int index = getScheduleIds().indexOf(scheduleId);
    if (index == -1) {
      return;
    }

		PreparedStatement deleteStmt = null;
		String deleteString = "DELETE FROM T_PROCESSED_SCHEDULES WHERE PROCESSED_SCHEDULE_ID = ?";

		try {
  		java.sql.Connection conn = m_connPool.getConnection();
			deleteStmt = conn.prepareStatement(deleteString); 
			deleteStmt.clearParameters();
			deleteStmt.setString(1, scheduleId);
			logger.info("Attempting to delete the schedule id '" + scheduleId + "'");
			int deleteRc = deleteStmt.executeUpdate();
      if (deleteRc != 1) {
        logger.info("No records were found matching the ScheduleId '" + scheduleId + "' passed in.");
      }
      else {
        logger.info("Deleted " + deleteRc + " ScheduleIds '" + scheduleId + "'");
      }
      deleteStmt.close();
      getScheduleIds().remove(index);
		}
		catch (Exception e) {
      if (deleteStmt != null) {
  			try { deleteStmt.close(); } catch (SQLException sqle) { }
      }
			logger.fatal(e.getMessage(), e);
			throw new ScheduleIdStoreException(e.getMessage(), e);
		}
  }

  /**
   * Adds a Schedule Id to this store.  Also, persists the schedule id to the 
   * database (in this case) so it will be available when/if the application
   * is restarted.
   * @param scheduleId String the schedule id to be added to the store.
   */
  public final void add(String scheduleId) throws ScheduleIdStoreException {
		PreparedStatement insertStmt = null;
		String insertString = "INSERT INTO T_PROCESSED_SCHEDULES (PROCESSED_SCHEDULE_ID, " +
                          "CREATE_DATE, CREATE_USER) " +
													"VALUES (?,?,?)";

		try {
  		java.sql.Connection conn = m_connPool.getConnection();
			insertStmt = conn.prepareStatement(insertString);
			insertStmt.clearParameters();
			insertStmt.setString(1, scheduleId);
      insertStmt.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
      insertStmt.setString(3, conn.getMetaData().getUserName());
			int insertRc = insertStmt.executeUpdate();
			logger.info("Inserted the '" + scheduleId + "' ScheduleId.");
			insertStmt.close();
      getScheduleIds().add(scheduleId);
		}
		catch (Exception e) {
      if (insertStmt != null) {
  			try { insertStmt.close(); } catch (SQLException sqle) { }
      }
			logger.fatal(e.getMessage(), e);
			throw new ScheduleIdStoreException(e.getMessage(), e);
		}
  }
  
  /**
   * Determines if a particular Schedule Id exists in this store.  This method 
   * checks what's in memory in addition to what's been persisted in the database.
   * If the schedule isn't in memory but it is in the persistent store, it will add 
   * the id to the in-memory list of schedule ids.  If there are multiple instances 
   * running that use this repository, only one of them will execute the schedule.  
   * The other instance will always determine that the schedule has been executed 
   * by the other process.
   * <P>
   * @return boolean true if the schedule id exists, false if not.
   */
  public boolean contains(String scheduleId) throws ScheduleIdStoreException {
    if (getScheduleIds().contains(scheduleId)) {
      return true;
    }
    else {
      // need to check the db...
      boolean foundSchedule = false;
      PreparedStatement queryStmt = null;
      String queryString = "SELECT PROCESSED_SCHEDULE_ID " +
                         "FROM T_PROCESSED_SCHEDULES WHERE PROCESSED_SCHEDULE_ID = ?";

//      logger.info("Retrieving existing schedules from repository.");
      try {
        java.sql.Connection conn = m_connPool.getConnection();
        queryStmt = conn.prepareStatement(queryString);
        queryStmt.clearParameters();
        queryStmt.setString(1, scheduleId);
        ResultSet results = queryStmt.executeQuery();
        while (results.next()) {
          foundSchedule = true;
          String dbScheduleId = results.getString(1);
          getScheduleIds().add(scheduleId);
          logger.info("Found existing schedule '" + scheduleId + "'");
        }
        queryStmt.close();
      }
      catch (Exception e) {
        if (queryStmt != null) {
          try { queryStmt.close(); } catch (SQLException sqle) { }
        }
        throw new ScheduleIdStoreException(e.getMessage(), e);
      }
      return foundSchedule;
    }
  }
}
