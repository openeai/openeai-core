/*******************************************************************************
 $Source$
 $Revision: 838 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.afa;

//import java.util.*;
//import java.io.*;
//import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;

public interface ScheduleIdStore  {
  public void load() throws ScheduleIdStoreException;
  public void removeSchedulesLike(String schedulePattern) throws ScheduleIdStoreException;
  public void remove(String scheduleId) throws ScheduleIdStoreException;
  public boolean contains(String scheduleId) throws ScheduleIdStoreException;
  public void add(String scheduleId) throws ScheduleIdStoreException;
  public void init(AppConfig aConfig) throws ScheduleIdStoreException;
}