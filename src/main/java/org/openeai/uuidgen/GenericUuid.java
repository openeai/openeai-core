/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.uuidgen;

import java.net.*;
import java.io.*;

import org.openeai.uuid.*;
import org.openeai.*;

/**
 * This class is used to either request a UUID (RandomUuid) from the URL specified or generate
 * one locally if errors occur requesting one remotely.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see org.openeai.uuid.RandomUuid
  * @see org.openeai.uuid.Uuid
 */
public class GenericUuid extends OpenEaiObject {

	private String m_url = "";
	private String m_id = "";

  /**
   * Constructor
   */
  public GenericUuid() {
  }
	public GenericUuid(String url) throws IOException {
		if (url != null) {
			m_url = url;
			setId(requestId(m_url));
		}
	}

  /**
  * Returns the URL to be used when requesting a UUID.
  *<P>
  * @return String the url
  */
	public String getUrl() {
		return m_url;
	}
  /**
  * Sets the URL to be used when requesting a UUID.
  *<P>
  * @param url String the url
  */
	public void setUrl(String url) {
		if (url == null) {
			m_url = "";
		}
		else {
			m_url = url;
		}
	}

  /**
  * Returns the UUID currently contained within this object.
  *<P>
  * @return String the UUID
  */
	public String getId() {
		return m_id;
	}
  /**
  * Sets the UUID currently contained within this object.
  *<P>
  * @return String the UUID
  */
	public void setId(String id) {
		if (id == null) {
			m_id = "";
		}
		else {
			m_id = id;
		}
	}

  /**
  * Requests a RandomUuid from the supplied URL.
  *<P>
  * A UUID will be generated locally if one of the following criteria are met:
  * <ul>
  * <li>if no URL is supplied (null or empty string)
  * <li>if the URL is "localhost"
  * <li>if errors occur connecting to the supplied URL.
  *<P>
  * @return String the UUID
  * @param url String the URL from which to request the UUID.
  */
	public String requestId(String url) throws MalformedURLException, IOException {
		if (url == "" || url.length() == 0) {
			// Generate one right here
			logger.debug("No UUID Gen service URL, generating a UUID locally...");
			return new RandomUuid().toString();
		}
    else if (url.equalsIgnoreCase("localhost")) {
			logger.debug("'localhost' was specified as the URL, generating a UUID locally...");
			return new RandomUuid().toString();
    }
		else {
			URL aUrl = new URL(url);
			logger.debug("Requesting uuid from service...");
			try {
				URLConnection urlConn = aUrl.openConnection();
				InputStream iStream = urlConn.getInputStream();
				logger.debug("Got uuid...");
				byte[] buf = new byte[256];
				iStream.read(buf,0,buf.length);
				return new String(buf).trim();
			}
			catch (Exception e) {
				logger.warn("GenericUuid.requestId: Exception caught: " + url + " : " + e.getMessage());
				logger.warn("Generating UUID locally since there were errors connecting to service");
				return new RandomUuid().toString();
			}
		}
	}
}

 
