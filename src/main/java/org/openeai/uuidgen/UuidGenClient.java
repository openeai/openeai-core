/*******************************************************************************
 $Source$
 $Revision: 735 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI sample, reference implementation,
and deployment management suite created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to implement integrations for your enterprise, visit
http://www.OpenEai.org/licensing.
*/

package org.openeai.uuidgen;

import java.net.*;
import java.io.*;

import org.openeai.*;

/**
 * Test UuidGen client that can invoke the UuidGen servlet.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class UuidGenClient extends OpenEaiObject {

  private static int m_iterations = 1;

  /**
   * Constructor
   */
  public UuidGenClient() {
    String path = "http://127.0.0.1:8080/examples/servlet/edu.uillinois.aits.uuidgen.UuidGen";
    /*
    addLog4jProperty("log4j.rootCategory", "INFO, A1");
    addLog4jProperty("log4j.category.org.openeai.uuidgen", "WARN, A1");
    addLog4jProperty("log4j.appender.A1", "org.apache.log4j.FileAppender");
    addLog4jProperty("log4j.appender.A1.File", "System.out");
    addLog4jProperty("log4j.appender.A1.layout", "org.apache.log4j.PatternLayout");
    addLog4jProperty("log4j.appender.A1.layout.ConversionPattern", "%-4d{ISO8601} %-5p [%t] %37c %3x - %m%n");
    */
    initializeLog4j();
    logger.info("Requesting " + m_iterations + " UUID's from UUIDGen service");
    String prevUuid = "";
    for (int i=0; i<m_iterations; i++) {
      try {
        URL aUrl = new URL(path);
        logger.info("Requesting uuid...");
        URLConnection urlConn = aUrl.openConnection();
        InputStream iStream = urlConn.getInputStream();
        logger.info("Got uuid...");
        byte[] buf = new byte[256];
        iStream.read(buf,0,buf.length);
        String currentUuid = new String(buf).trim();
        if (currentUuid != prevUuid) {
          String response = "Unique UUID from Servlet: " + currentUuid;
          logger.info(response);
          prevUuid = currentUuid;
        }
        else {
          String response = "NON-UNIQUE UUID from Servlet: " + currentUuid;
          logger.warn(response);
          prevUuid = currentUuid;
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * main
   * @param args
   */
  public static void main(String[] args) {
		if (args.length == 1) {
      m_iterations = Integer.parseInt(args[0]);
		}
    UuidGenClient uuidGenClient = new UuidGenClient();
  }
}

