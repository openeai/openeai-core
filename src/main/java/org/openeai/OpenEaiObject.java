/*******************************************************************************
 $Source$
 $Revision: 4000 $
 *******************************************************************************/

/**********************************************************************
 This file is part of the OpenEAI Application Foundation or
 OpenEAI Message Object API created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to build commercial integration software or to implement
 integrations for your enterprise, visit http://www.OpenEai.org/licensing.
 */

package org.openeai;

import java.util.*;

//import org.openeai.enterprise.config.*;
import org.openeai.loggingutils.*;
// log4J
import org.apache.log4j.*;

/**
 * The parent of all OpenEAI related objects. This class makes several
 * components that are part of OpenEAI frameworks available to descendants.
 * <P>
 * 
 * @author Tod Jackson (tod@openeai.org)
 * @author Steve Wheat (steve@openeai.org)
 * @version 3.0 - 28 January 2003
 */
public class OpenEaiObject {

    private MailService m_mailSvc = null;
    private Properties m_props = null;

    private String appName = "";
    private boolean debug = false;
    private String mailHost = "";
    private String fromAddr = "";
    private String toAddr = "";

    // log4j logger
    public static Category logger = Logger.getLogger(OpenEaiObject.class);
    private Properties log4jProps = null;

    // throwing out permission denied exception for System.out. gwang28 8/4/2015
    // static method to create default logger
    // static {
    // // set a default logger
    // logger = Category.getRoot();
    //
    // Properties lProps = new Properties();
    // lProps.put("log4j.rootCategory", "INFO, stdout");
    // lProps.put("log4j.appender.stdout", "org.apache.log4j.FileAppender");
    // lProps.put("log4j.appender.stdout.File", "System.out");
    // lProps.put("log4j.appender.stdout.layout",
    // "org.apache.log4j.PatternLayout");
    // lProps.put("log4j.appender.stdout.layout.ConversionPattern",
    // "%-4d{ISO8601} %-5p [%t] %3x - %m%n");
    // PropertyConfigurator.configure(lProps);
    // }

    /**
     * Constructor
     */
    public OpenEaiObject() {
    }

    // START - Methods for configuring log4j logger
    private void configureLog4j(Properties props) {
        PropertyConfigurator.configure(props);
    }

    private void configureLog4j(String fileName) {
        PropertyConfigurator.configure(fileName);
    }

    private void configureLog4j() {
        if (log4jProps == null) {
            log4jProps = new Properties();
        }
        PropertyConfigurator.configure(log4jProps);
    }

    /**
     * Initializes the static Log4J logger used by all decendants of
     * OpenEaiObject with properties contained in the log4j properties object in
     * this object. Properties are added to the log4j properties object via the
     * addLog4jProperty method.
     * 
     * @return void
     * 
     */
    public void initializeLog4j() {
        configureLog4j();
//        if (getClass().getPackage() == null) {
//            logger = Category.getInstance(getClass().getName().substring(0, getClass().getName().lastIndexOf('.')));
//        } else {
//            logger = Category.getInstance(getClass().getPackage().toString());
//        }
    }

    /**
     * Initializes the static Log4J logger used by all decendants of
     * OpenEaiObject with properties found in the file name passed in (a
     * properties file).
     * 
     * @param fileName
     *            String name of properties file.
     * 
     */
    public void initializeLog4j(String fileName) {
        configureLog4j(fileName);
        //logger = Category.getInstance(getClass().getName().substring(0, getClass().getName().lastIndexOf('.')));
    }

    /**
     * Initializes the static Log4J logger used by all decendants of
     * OpenEaiObject with properties found in the Properties object passed in.
     * 
     * @param props
     *            Properties pre-configured properties object containing Log4J
     *            properties.
     * 
     */
    public void initializeLog4j(Properties props) {
        //logger = Category.getInstance(getClass().getName().substring(0, getClass().getName().lastIndexOf('.')));
        // logger = Category.getInstance(getClass().getName());
        configureLog4j(props);
        // setLog4jProperties(props);
    }

    /*
     * public void setLog4jProperties(Properties props) { log4jProps = props; }
     * public Properties getLog4jProperties() { return log4jProps; }
     */

    /**
     * Adds an individual Property (name/value) to this object that will be used
     * by the default initializeLog4j() method.
     * 
     * @param String
     *            key name of property being added.
     * @param String
     *            value associated to property being added.
     * 
     */
    /*
     * public void addLog4jProperty(String key, String value) { if (log4jProps
     * == null) { log4jProps = new Properties(); } log4jProps.put(key, value); }
     */
    /**
     * Returns log4j properties as an Object array.
     * 
     * @return Object[] an object array or log4jproperties.
     * 
     */
    /*
     * public Object[] getLog4jProperties() { if (log4jProps == null) {
     * log4jProps = new Properties(); } return new
     * ArrayList(log4jProps.values()).toArray(); }
     */
    // END - Methods for configuring log4j logger

    /**
     * Allows decendants of OpenEaiObject to support default Clone behavior.
     * Note, this may not work for all objects.
     * 
     * @return Object a clone of this object as defined by Object.
     * 
     */
    /*
     * public Object clone() throws CloneNotSupportedException { return
     * super.clone(); }
     */

    public void setDebug(boolean in) {
        debug = in;
    }

    public boolean getDebug() {
        return debug;
    }

    /**
     * Sets the mail host associated to the mail service in this object.
     * 
     * @param in
     *            String name of the mail host.
     * 
     */
    public void setMailHost(String in) {
        mailHost = in;
    }

    /**
     * Returns the mail host associated to the mail service in this object.
     * 
     * @return String name of the mail host.
     * 
     */
    public String getMailHost() {
        return mailHost;
    }

    /**
     * Sets the from email address associated to the mail service in this
     * object.
     * 
     * @param in
     *            String from address (from whom the email messages are being
     *            sent).
     * 
     */
    public void setFromAddr(String in) {
        fromAddr = in;
    }

    /**
     * Returns the from email address associated to the mail service in this
     * object.
     * 
     * @return String from address (from whom the email messages are being
     *         sent).
     * 
     */
    public String getFromAddr() {
        return fromAddr;
    }

    /**
     * Sets the to email address associated to the mail service in this object.
     * This is the email address that the email will be sent to.
     * 
     * @param in
     *            String to address (who the message will be sent to).
     * 
     */
    public void setToAddr(String in) {
        toAddr = in;
    }

    /**
     * Returns the to email address associated to the mail service in this
     * object. This is the email address that the email will be sent to.
     * 
     * @return String to address (who the message will be sent to).
     * 
     */
    public String getToAddr() {
        return toAddr;
    }

    /**
     * Sets the app name associated to this object.
     * 
     * @param in
     *            String
     * 
     */
    public void setAppName(String in) {
        appName = in;
    }

    /**
     * Returns the app name associated to this object.
     * 
     * @return String
     * 
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Sets the MailService to be used within this instance of the class. The
     * MailService object is used to send email.
     * 
     * @param in
     *            MailService the MailService to use within this instance.
     * 
     * @return void
     * 
     */
    public void setMailService(MailService in) {
        m_mailSvc = in;
    }

    /**
     * Returns the MailService to be used within this instance of the class. The
     * MailService object is used to send email.
     * 
     * @return MailService
     * 
     */
    public MailService getMailService() {
        if (m_mailSvc == null) {
            m_mailSvc = new MailService();
        }
        return m_mailSvc;
    }

    /**
     * Sets the properties to be used within this instance of the class.
     * 
     * @param in
     *            Properties the Properties to use within this instance.
     * 
     * @return void
     * 
     */
    public void setProperties(Properties in) {
        m_props = in;
    }

    /**
     * Returns the Properties to used within this instance of the class.
     * 
     * @return Properties
     * 
     */
    public Properties getProperties() {
        if (m_props == null) {
            m_props = new Properties();
        }
        return m_props;
    }
}
