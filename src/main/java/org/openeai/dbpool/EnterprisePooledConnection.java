/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.dbpool;

import java.sql.*;

/**
 * This is the actual java.sql.Connection that is stored in the EnterpriseConnectionPool.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class EnterprisePooledConnection extends EnterpriseDbConnectionObject {

  private Connection m_connection = null;
  private boolean m_inuse = false;
  private long m_connectionId = 0;

  /**
   * Constructor
   */
  public EnterprisePooledConnection(String poolName, String driverName, String connString,
    String connUserId, String connUserPassword, String verifyString) throws SQLException {

    setPoolName(poolName);
    setDriverName(driverName);
    setConnectString(connString);
    setConnectUserId(connUserId);
    setConnectPassword(connUserPassword);
    setVerificationQueryString(verifyString);
    initializeConnection();
  }


  /**
   * Sets a flag indicating whether or not this connection is in use.
   *<P>
   * @param inUse boolean 
   */
  public synchronized void setInuse(boolean inUse) {
    m_inuse = inUse;
  }
  /**
   * Returns a flag indicating whether or not this connection is in use.
   *<P>
   * @return boolean 
   */
  public boolean isInuse() {
    return m_inuse;
  }

  public void setConnectionId(long id) {
    m_connectionId = id;
  }
  public long getConnectionId() {
    return m_connectionId;
  }

  /**
   * Returns the java.sql.Connection object associated to this pooled connection.
   * Before returning the connection though, it verifies that the connection is still in tact
   * by testing the isClosed method of the connection as well as invoking any verification
   * string associated to the connection.  If either of these two operations cannot be successfully
   * completed, it will attempt to re-establish the connection with the database before returning.
   * If the connection cannot be successfully re-established, it will throw an exception.
   *<P>
   * @return java.sql.Connection
   * @throws java.sql.SQLException if errors occur re-establishing the connection with the database.
   */
  public Connection getConnection() throws SQLException {
    if (m_connection.isClosed()) {
      // Try to re-initialize the connection and return it.
      logger.warn(getPoolName() + " - Database connection has been lost.  Trying to re-establish connection with the database...");
      initializeConnection();
      logger.info(getPoolName() + " - Database connection was successfully re-established.");
    }
    else {
      // Since not all JDBC drivers support isClosed() (Oracle, Microsoft), we have to test it this way also.
      // Since it's up to the vendor as to when the connection is actually used, the safest way
      // to verify a connection is to actually try and use it by executing something against the db.
      // The 'VerificationQueryString' should be a simple (quick) query against something in the db
      // that will force the connection to hit the db.  Only then, will we know for sure that it's
      // a usable connection.  If the connection isn't usable, we'll attempt to re-initialize it and
      // return it.  If it can't be re-established, an exception will be thrown.
      if (getVerificationQueryString() != null && getVerificationQueryString().length() != 0) {
    		Statement getStmt = null;
    		try {
          getStmt = m_connection.createStatement();
          boolean retVal = getStmt.execute(getVerificationQueryString());
          getStmt.close();
    		}
	    	catch (Exception e) {
          if (getStmt != null) {
            getStmt.close();
          }
          logger.warn(getPoolName() + " - Database connection has been lost.  Exception: " + e.getMessage() +
            "  Trying to re-establish connection with the database...");
          initializeConnection();
          logger.info(getPoolName() + " - Database connection was successfully re-established.");
  		  }
      }
    }
    setInuse(true);
    logger.debug("[EnterprisePooledConnection] returning connection: " + getConnectionId() + " to caller.");
    return m_connection;
  }
  /**
   * Sets the java.sql.Connection object associated to this pooled connection object.
   * This connection will be established when the EnterprsieConnectionPool instantiates
   * this pooled connection object passing all the necessary information to establish the 
   * connection.
   *<P>
   * @param conn java.sql.Connection
   */
  public void setConnection(Connection conn) {
    m_connection = conn;
  }

  private void initializeConnection() throws SQLException {
    try {
      Driver myDriver = (Driver)Class.forName(getDriverName()).newInstance();
	   	DriverManager.registerDriver(myDriver);
      setConnection(DriverManager.getConnection(getConnectString(), getConnectUserId(), getConnectPassword()));
    }
    catch(Exception e) {
      String errMessage = getPoolName() + " - Error initializing database connection.  Exception: " + e.getMessage();
      throw new SQLException(errMessage);
    }
  }
}

