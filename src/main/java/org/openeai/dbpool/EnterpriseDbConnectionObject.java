/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.dbpool;

import org.openeai.*;
import java.sql.*;

/**
  * A class that is stored in the EnterpriseConnectionPool.  This
  * class contains all the information the pool needs to instantiate
  * a java.sql.Connection object that it stores in the pool.  This is a convenience
  * class that allows both the EnterpriseConnectionPool and the EnterprisePooledConnection
  * to inherit some convenience methods that they both share.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public abstract class EnterpriseDbConnectionObject extends OpenEaiObject {
  private String m_poolName = "";
  private String m_driverName = "";
  private String m_connectString = "";
  private String m_connectUserId = "";
  private String m_connectPassword = "";
  private String m_verificationQueryString = "";

  /**
   * Sets the name of the pool to which this object is associated.
   * <P>
   * @param poolName String the name of the pool
   */
  public void setPoolName(String poolName) {
    if (poolName == null || poolName.length() == 0) {
      m_poolName = "Unknown Pool Name";
    }
    m_poolName = poolName;
  }
  /**
   * Returns the name of the pool to which this object is associated.
   *<P>
   * @return String the name of the pool.
   */
  public String getPoolName() {
    return m_poolName;
  }

  /**
   * Sets the database driver name that should be used to establish a connection.
   * <P>
   * Example: com.sybase.jdbc2.jdbc.SybDriver
   * <P>
   * @param driverName String the database driver name
   * @throws NullPointerException if the driver name is null or empty.
   * @see java.sql.Connection
   */
  public void setDriverName(String driverName) throws NullPointerException {
    if (driverName == null || driverName.length() == 0) {
      throw new NullPointerException(getPoolName() + " - No Database Driver specified, can't initialize DB Pool.");
    }
    m_driverName = driverName;
  }
  /**
   * Returns the database driver name that should be used to establish a connection.
   * <P>
   * @return String the database driver name
   */
  public String getDriverName() {
    return m_driverName;
  }

  /**
   * Sets the verification string that will be used to test a connection prior to returning
   * if from the pool.  This information is specified in the DbConnectionPoolConfig Element/Object.
   * Since not all JDBC drivers support the isClosed method, this extra check is needed to ensure
   * the connection is still in tact.  If no verification string is specified, the pool will simply
   * use the isClosed method to determine if the connection is in tact prior to returning it.
   * <P>
   * Examples of verification strings:
   * <ul>
   * <li>Sybase - select getdate()
   * <li>MS SQL Server - select getdate()
   * </ul>
   * <P>
   * If the driver specified is an Oracle driver, the foundation will default to a 
   * robust verification string that tests the connection ("declare x number; begin x:=1; end;").
   * However, this can be overridden if the information is specified in the configuration document.
   *<P>
   * @param verifyString String the string to use to verify the connection.
   */
  public void setVerificationQueryString(String verifyString) {
    if (verifyString == null || verifyString.length() == 0) {
      logger.warn(getPoolName() + " - No verification string, won't be able to check connection except by isClosed.");
      verifyString = "";
    }
    m_verificationQueryString = verifyString;
  }
  /**
   * Returns the verification string associated to this connection.
   *<P>
   * @return String the verification string.
   */
  public String getVerificationQueryString() {
    return m_verificationQueryString;
  }

  /**
   * Sets the connect string associated to this connection as specified in the configuration document.
   * <P>
   * Example: jdbc:sybase:Tds:ahost.or.ip.com:5000
   * <P>
   * @param connectString String connection string.
   * @see java.sql.Connection
   */
  public void setConnectString(String connectString) throws NullPointerException {
    if (connectString == null || connectString.length() == 0) {
      throw new NullPointerException(getPoolName() + " - No Database Connect String specified, can't initialize DB Pool.");
    }
    m_connectString = connectString;
  }
  /**
   * Returns the connection string associated to this connection.
   * <P>
   * @return String
   * @see java.sql.Connection
   */
  public String getConnectString() {
    return m_connectString;
  }

  /**
   * Sets the database user id to be used to establish the connection.
   * <P>
   * @param connectUserId String the database user id.
   */
  public void setConnectUserId(String connectUserId) throws NullPointerException {
    if (connectUserId == null || connectUserId.length() == 0) {
      throw new NullPointerException(getPoolName() + " - No Database User Id String specified, can't initialize DB Pool.");
    }
    m_connectUserId = connectUserId;
  }
  /**
   * Returns the database user id to be used to establish the connection.
   *<P>
   * @return String
   */
  public String getConnectUserId() {
    return m_connectUserId;
  }

  /**
   * Sets the password for the databse user to be used to establish the connection.
   * <P>
   * @param connectPassword String the password of the database user.
   */
  public void setConnectPassword(String connectPassword) throws NullPointerException {
    if (connectPassword == null || connectPassword.length() == 0) {
      throw new NullPointerException(getPoolName() + " - No Database User Password String specified, can't initialize DB Pool.");
    }
    m_connectPassword = connectPassword;
  }
  /**
   * Returns the password for the databse user to be used to establish the connection.
   * <P>
   * @return String the password of the database user.
   */
  public String getConnectPassword() {
    return m_connectPassword;
  }
}

