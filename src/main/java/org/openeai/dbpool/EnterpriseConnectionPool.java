/*******************************************************************************
 $Source$
 $Revision: 2361 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.dbpool;

import org.openeai.OpenEaiObject;
import org.openeai.config.DbConnectionPoolConfig;

import java.util.*;

import java.sql.*;


/**
 * The Enterprise Database Connection pool class.  This class acts as a container
 * for Database connections.  It provides a mechanism for retrieving those connections
 * as well as initialization and shutdown methods.  The actual java.sql.Connection
 * objects are wrapped by the EnterprisePooledConnection.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the DbConnectionPoolConfig
 * Element associated to this pool.
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the pool</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbDriverName</TD>
 * <TD>yes</TD>
 * <TD>JDBC Database driver name</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbConnectString</TD>
 * <TD>yes</TD>
 * <TD>JDBC URL that is used to connect to the database</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbConnectUserId</TD>
 * <TD>yes</TD>
 * <TD>User id to connect as</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbConnectPassword</TD>
 * <TD>yes</TD>
 * <TD>password associated to the user id</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbPoolSize</TD>
 * <TD>yes</TD>
 * <TD>Initial size of the pool</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbPoolMaxSize</TD>
 * <TD>no</TD>
 * <TD>Maximum number of connections that may be created by this pool.
 * if this parameter is left out or if zero is specified, the pool
 * will not restrict how large the pool may get at a given time.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbVerificationString</TD>
 * <TD>no</TD>
 * <TD>Some SQL string that can be used to test a connection prior to returning
 * it from the pool.  This should be a highly efficient statement.  If this
 * is not specified, ONLY the Connection.isClosed() method will be used.
 * If the dbDriverName is an Oracle driver a default verification string
 * will be used in addition to the Connection.isClosed</TD>
 * </TR>
 * </TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see EnterpriseDbConnectionObject
 * @see EnterprisePooledConnection
 * @see org.openeai.config.DbConnectionPoolConfig
 */
public class EnterpriseConnectionPool extends EnterpriseDbConnectionObject {

  private java.util.List m_pooledConnections = 
    Collections.synchronizedList(new ArrayList());
  private int m_connectionIndex = 0;
  private int m_connectionsInuse = 0;
  private long m_newConnectionCount = 0;
  private String m_poolName = "";
  private int m_poolSize = 1; // Default
  private int m_initialSize = 1; // Default
  private int m_maxPoolSize = 0; // the maximum size we want to allow 
  // this connection pool to grow.

  /**
   * Constructor
   */
  public EnterpriseConnectionPool(String poolName) {
    setPoolName(poolName);
  }

  public EnterpriseConnectionPool(String poolName, 
                                  Properties poolProps) throws SQLException {
    setPoolName(poolName);
    setProperties(poolProps);
    initializePool(getProperties());
  }

  /**
   * This is the constructor that AppConfig uses when it instantiates an EnterpriseConnectionPool
   * based on information found in an application's deployment XML document.
   */
  public EnterpriseConnectionPool(DbConnectionPoolConfig dbConfig) throws SQLException {
    this(dbConfig.getName(), dbConfig.getProperties());
  }


  /**
   * Returns the name of the pool as specified in the deployment document for the application.
   *<P>
   * @return String the pool's name.
   */
  public String getPoolName() {
    return m_poolName;
  }

  /**
   * Sets the name of the pool.
   *<P>
   * @param poolName String the name of the pool.
   */
  public void setPoolName(String poolName) {
    m_poolName = poolName;
  }

  /**
  * Iterates through the pool and closes all connections.
  *
  * @throws - SQLException if it can't close a connection.
  */
  public void closePool() throws SQLException {
    for (int i = 0; i < getConnections().size(); i++) {
      logger.info(getPoolName() + " - Closing connection " + i);
      getConnection(i).getConnection().close();
    }
  }

  /**
  * Sets the initial size of the pool.  This is specified in the deployment document
  * for the application being configured and is passed to this object via the DbConnectionPoolConfig
  * object that gets built from that configuration information.  If the initial size passed in
  * is less than or equal to zero, the default initial size is 1.
  *<P>
  * @param initialSize the pool's intitial size.  If the number passed in is less than or equal to zero, the
  * initial size will be set to one (1).
  * @throws - SQLException if it can't close a connection.
  */
  private void setInitialSize(int initialSize) {
    if (initialSize > 0) {
      m_initialSize = initialSize;
    }
    else {
      logger.warn("No initial pool sized specified, defaulting to 1.");
      m_initialSize = 1;
    }
  }

  /**
   * Returns the initial size for th pool.
   *<P>
   * @return int the initial size of the pool.
   */
  private int getInitialSize() {
    return m_initialSize;
  }

  /**
  * Sets the size of the pool (the number of connections that should be created and stored in the pool).
  * This is specified in the deployment document for the application being configured and is
  * passed to this object via the DbConnectionPoolConfig
  * object that gets built from that configuration information.  If the size passed in
  * is less than or equal to zero, the default size is 1.
  *<P>
  * @param poolSize int the pool size.  If the number passed in is less than or equal to zero, the
  * initial size will be set to one (1).
  */
  public void setPoolSize(int poolSize) {
    if (poolSize > 0) {
      m_poolSize = poolSize;
    }
    else {
      logger.debug("No maximum pool sized specified, defaulting to initial size of 1");
      m_poolSize = 1;
    }
  }

  /**
   * Returns the size for the pool.
   *<P>
   * @return int the size of the pool.
   */
  public int getPoolSize() {
    return m_poolSize;
  }

  /**
  * Sets the maxiumum size of the pool (the maximum number of connections that should be created and stored in the pool).
  * This is specified in the deployment document for the application being configured and is
  * passed to this object via the DbConnectionPoolConfig
  * object that gets built from that configuration information.  If no maximum
  * size is specified, the pool will grow based on how busy the application using
  * this pool is.  There will always be at least the number of initial connections
  * created.  As exclusive connections are retrieved there may be more connections
  * added to the pool.  Exclusive connections will be closed and released when
  * the user of the pool is done with the connection and calls the releaseExclusiveConnection
  * method.
  *<P>
  * @param maxSize int the maximum pool size.
  */
  public void setMaxPoolSize(int maxSize) {
    m_maxPoolSize = maxSize;
  }

  /**
   * Returns the maximum size of the pool.
   *<P>
   * @return int the maximum size of the pool.
   */
  public int getMaxPoolSize() {
    return m_maxPoolSize;
  }

  /**
  * Based on properties, establishes a configurable number of pre-established
  * connections to a database and adds them to the Pool.  These connections
  * can then be retrieved from the pool via the getConnection() method.  The
  * properties are all generally retrieved from the DbConnectionPoolConfig object
  * used to construct this object.
  *
  * @param props Properties properties object created from the Configuration document.
  * @throws - SQLException if it has any problems establishing connections given the properties passed in.
  */
  public void initializePool(Properties props) throws SQLException {
    try {
      // initialize internal connection 'pool'
      setDriverName(props.getProperty("dbDriverName", ""));
      setConnectString(props.getProperty("dbConnectString", ""));
      setConnectUserId(props.getProperty("dbConnectUserId", ""));
      setConnectPassword(props.getProperty("dbConnectPassword", ""));
      setPoolSize(Integer.parseInt(props.getProperty("dbPoolSize", "1")));
      setMaxPoolSize(Integer.parseInt(props.getProperty("dbPoolMaxSize", 
                                                        "0")));
      setVerificationQueryString(props.getProperty("dbVerificationString", 
                                                   ""));
      if (getVerificationQueryString().length() == 0 && 
          getDriverName().toLowerCase().indexOf("oracle") != -1) {

        // specify a default query string for Oracle connections
        setVerificationQueryString("declare x number; begin x:=1; end;");
      }

      for (int i = 0; i < getPoolSize(); i++) {
        logger.info(getPoolName() + " - Adding DB Connection " + i);
        //        addConnection(getPoolName(), getDriverName(), getConnectString(), getConnectUserId(), getConnectPassword(), getVerificationQueryString());
        EnterprisePooledConnection p = 
          new EnterprisePooledConnection(getPoolName(), getDriverName(), 
                                         getConnectString(), 
                                         getConnectUserId(), 
                                         getConnectPassword(), 
                                         getVerificationQueryString());
        p.setInuse(false);
        p.setConnectionId(i);
        getConnections().add(p);
      }
      setInitialSize(getPoolSize());
    }
    catch (Exception e) {
      String errMsg = 
        "Error initializing '" + getPoolName() + "' DB Connection pool.  Exception: " + 
        e.getMessage();
      logger.fatal(errMsg, e);
      throw new SQLException(errMsg);
    }
  }

  private void addConnection(String poolName, String driverName, 
                             String connString, String connUserId, 
                             String connUserPassword, 
                             String verifyString) throws SQLException {
    getConnections().add(new EnterprisePooledConnection(poolName, driverName, 
                                                        connString, connUserId, 
                                                        connUserPassword, 
                                                        verifyString));
  }

  /**
  * Returns a pre-established Database connection from the pool.
  * Prior to returning the connection, it checks its state and re-establishes
  * a clean connection if the connection has been broken for some reason.
  *<P>
  * NOTE:  This method should only be used if the connection is going to be used
  * in non-transacted mode (autocommit=true).  If the connection will be
  * used for multiple transactions in a single logical unit of work, you
  * should use getExclusiveConnection.  This method returns a connection
  * regardless of whether or not it is in use so it could be possible that two
  * threads attempt to use a connection returned from this method at the same
  * time.  This would NOT be good.
  *
  * @return - Connection pre-established.
  * @throws - SQLException if it can't obtain a clean connection.
  */
  public synchronized Connection getConnection() throws SQLException {
    ArrayList errorMessages = new ArrayList();

    for (int i = 0; i < getConnections().size(); i++) {
      if (getConnectionIndex() >= getConnections().size() - 1) {
        setConnectionIndex(0);
      }
      else {
        m_connectionIndex++;
      }
      try {
        // Normally, this is as far as we'll need to go...
        EnterprisePooledConnection pConnection = 
          (EnterprisePooledConnection)getConnection(getConnectionIndex());
        if (pConnection != null) {
          logger.debug("[EnterpriseConnectionPool] Returnning connection from pool at index " + 
                       getConnectionIndex());
          return pConnection.getConnection();
        }
      }
      catch (SQLException e) {
        String errMsg = 
          "Couldn't get connection from pool.  Trying next connection in pool.  Exception: " + 
          e.getMessage();
        errorMessages.add(errMsg);
        logger.warn(errMsg);
        m_connectionIndex++;
      }
    }
    // If we get here, it means we could not get a connection from the pool, so we'll throw an exception.

    // Or, maybe we need to try and establish new connections.......

    String exceptionString = 
      "Could not obtain a usable connection from the Database Connection Pool.  Exceptions were:\n";
    for (int i = 0; i < errorMessages.size(); i++) {
      String errMsg = (String)errorMessages.get(i);
      exceptionString = 
          exceptionString + "[Exception " + i + "] " + errMsg + "\n";
    }
    throw new SQLException(exceptionString);
  }

  /**
  * Returns a pre-established, AVAILABLE pooled connection object from the pool.
  * If an avaliable pooled connection cannot be obtained, it will create a new
  * one and return it unless the maximum number of connections for the pool has
  * been reached (maxPoolSize).  After the calling application is finished with the pooled
  * connect, it will release that connection making it available for others.
  *<P>
  * This method ensures that the connection being returned is not being used and
  * will mark the connection as "in-use" so no one else will use the connection.
  *<P>
  * @return EnterprisePooledConnection which contains a pre-established Connection.
  * @throws SQLException if it can't obtain an available connection.
  * @see EnterpriseConnectionPool#releaseExclusiveConnection
  */
  public EnterprisePooledConnection getExclusiveConnection() throws SQLException {
    if (getMaxPoolSize() > 0) {
      if (getConnections().size() >= getMaxPoolSize()) {
        // have to wait until a connection's available, can't create any more
        boolean keepChecking = true;
        int cntr=0;
        while (keepChecking) {
          EnterprisePooledConnection pConnection = findAvailableConnection();
          if (pConnection != null) {
            keepChecking = false;
            return pConnection;
          }
          try {
            if (cntr >= 6) {
              logger.warn("[EnterpriseConnectionPool.getExclusiveConnection] " + 
                "- Unable to obtain an exclusive connection from the " + 
                this.getPoolName() + " db pool.  'maxPoolSize' (" + getMaxPoolSize() + ") " +
                "has been reached, must wait for an available connection.  " + 
                "If this message continues, the 'maxPoolSize' property may need to be increased.");
              cntr = 0;
            }
            else {
              cntr++;
            }
            Thread.sleep(500);
          }
          catch (Exception e) { /* nop */ }
        }
      }
      else {
        // we can still create another Exclusive connection if we can't find
        // an available one already in the pool.
        EnterprisePooledConnection pConnection = findAvailableConnection();
        if (pConnection != null) {
          return pConnection;
        }

        // If we get here, it means we could not get a connection from the pool, 
        // but the maxPoolSize has not been reached so we need to establish a new connection...
        return newExclusiveConnection();
      }
    }

    // We will look for an available connection, if none exists, we'll create
    // a new one because they don't care about the maximum connections in the
    // pool.
    EnterprisePooledConnection pConnection = findAvailableConnection();
    if (pConnection != null) {
      return pConnection;
    }

    // If we get here, it means we could not get a connection from the pool, 
    // and no maxPoolSize has been specified so we need to establish a new connection...
    return newExclusiveConnection();
  }

  /**
  * Creates and returns a new EnterprisePooledConnection.
  **/
  private EnterprisePooledConnection newExclusiveConnection() throws SQLException {
    logger.debug("Creating new DB Connection because all others are busy.");
    EnterprisePooledConnection p = 
      new EnterprisePooledConnection(getPoolName(), getDriverName(), 
                                     getConnectString(), getConnectUserId(), 
                                     getConnectPassword(), 
                                     getVerificationQueryString());
    long connectionId = incrementNewConnectionCount();
    p.setInuse(true);
    p.setConnectionId(100 + connectionId);
    getConnections().add(p);
    logger.debug("[EnterpriseConnectionPool] Returnning NEW exclusive connection: " + 
                 p.getConnectionId() + " from pool.");
    return p;
  }

  /**
  * Returns an available EnterprisePooledConnection if one exists, null if not.
  **/
  private EnterprisePooledConnection findAvailableConnection() {
    for (int i = 0; i < getConnections().size(); i++) {
      EnterprisePooledConnection pConnection = getConnection(i);
      if (pConnection != null) {
        if (pConnection.isInuse() == false) {
          pConnection.setInuse(true);
          logger.debug("[EnterpriseConnectionPool] Returnning exclusive connection: " + 
                       pConnection.getConnectionId() + " from pool.");
          return pConnection;
        }
      }
    }
    return null;
  }

  private synchronized long incrementNewConnectionCount() {
    m_newConnectionCount++;
    return m_newConnectionCount;
  }

  /**
  * Releases a pooled connection back to the pool.  This means, the connection
  * being released will again be available for use by subsequent calls.
  *<P>
  * If the connection being released, was not one of the original connections
  * created by the pool, it will be closed unless the maximum pool size property
  * is greater than zero and the actual pool size is less than that maximum size.
  *<P>
  * @param p EnterprisePooledConnection to release.
  * @throws SQLException if errors occur closing the connection (if appropriate)
  * @see EnterpriseConnectionPool#getExclusiveConnection
  */
  public synchronized void releaseExclusiveConnection(EnterprisePooledConnection p) throws SQLException {
    logger.debug("EnterpriseConnectionPool:release:  pool size is now: " + 
                 getConnections().size());
    for (int i = 0; i < getConnections().size(); i++) {
      EnterprisePooledConnection pConnection = getConnection(i);
      if (pConnection != null) {
        if (pConnection.getConnectionId() == p.getConnectionId()) {

          logger.debug("[EnterpriseConnectionPool] releasing exclusive connection: " + 
                       pConnection.getConnectionId());

          if (pConnection.getConnectionId() > getInitialSize()) {
            // Only want to close the connection if the max pool size is greater
            // than zero and the connection was created after initialization.
            if (getMaxPoolSize() > 0) {
              if (pConnection.getConnectionId() > 100) {
                logger.debug("[EnterpriseConnectionPool.releaseExclusiveConnection] - Pooled Connection " + 
                             pConnection.getConnectionId() + 
                             " was not one of the original connections, so I'm closing it.");
                try {
                  pConnection.getConnection().close();
                  if (getConnections().remove(pConnection) == false) {
                    logger.warn("[EnterpriseConnectionPool.releaseExclusiveConnection] - unable to remove " + 
                      pConnection.getConnectionId() + " from the list of connections.");
                  }
                }
                catch (Exception e) {
                  throw new SQLException(e.getMessage());
                }
              }
              else {
                logger.debug("[EnterpriseConnectionPool.releaseExclusiveConnection] - Setting Pooled Connection " + 
                             pConnection.getConnectionId() + 
                             " inUse to false.");
                pConnection.setInuse(false);
              }
            }
            else {
              // if no maxPoolSize was specified, we'll always clean up the
              // connection if it wasn't one of the original connections created.
              logger.debug("Pooled Connection " + 
                           pConnection.getConnectionId() + 
                           " was not one of the original connections, so I'm closing it.");
              try {
                pConnection.getConnection().close();
                if (getConnections().remove(pConnection) == false) {
                  logger.warn("[EnterpriseConnectionPool.releaseExclusiveConnection] - unable to remove " + 
                    pConnection.getConnectionId() + " from the list of connections.");
                }
              }
              catch (Exception e) {
                throw new SQLException(e.getMessage());
              }
            }
          }
          else {
            pConnection.setInuse(false);
          }

          return;
        }
      }
    }
  }

  private EnterprisePooledConnection getConnection(int index) {
    // if index is >= getConnections().size() return null;
    // this is possible if the pool was configured with an initial size
    // that is smaller than the  maximum pool size because other threads
    // could be releasing connections back to the pool that were dyanimcally
    // allocated and are therefore removed from the pool when released.

    if (index < getConnections().size() && index > getInitialSize()) {
      return null;
    }
    return (EnterprisePooledConnection)getConnections().get(index);
  }

  private void setConnections(java.util.List vConnections) {
    m_pooledConnections = vConnections;
  }

  private java.util.List getConnections() {
    return m_pooledConnections;
  }

  private void setConnectionIndex(int index) {
    m_connectionIndex = index;
  }

  private int getConnectionIndex() {
    return m_connectionIndex;
  }

  private int getConnectionsInuseCount() {
    return m_connectionsInuse;
  }

  /**
	 * This Thread will sleep for a specified period of time and then wake up
   * and check to see if there are any connections not in use that were not
   * part of the original pool allocation.  If they are found, they will be closed
   * and removed from the pool.
   *
   * The thread is started when the pool is started the first time.
	 *
   * @author Tod Jackson
	 *
	 */
  /*
	private class MonitorPool implements java.lang.Runnable {
    private int m_sleepInterval = 30000;   // thirty seconds default

		public MonitorPool(int sleepInterval) {
      m_sleepInterval = sleepInterval;
		}
    public MonitorPool() {
    }

    public void run() {
      // sleep for m_sleepInterval
      // wake up, check for any connections not in use that
      // were not part of the original pool allocation.
      // if they're not busy, close and remove them.
      boolean stayAlive = true;
      while(stayAlive) {
        try {
          Thread.sleep(m_sleepInterval);
        }
        catch (Exception e) {
          logger.debug("Error sleeping...");
        }

        // wake up and check for connections that are not in use
        // that were not part of the original pool allocation.
        for (int i=0; i<getConnections().size(); i++) {
          EnterprisePooledConnection pConnection = getConnection(i);
          if (pConnection != null) {
            if (pConnection.isInuse() == false) {
              if (pConnection.getConnectionId() > getInitialSize()) {
                logger.info("Pooled Connection " + i + " was not one of the original connections, so I'm closing it.");
                try {
                  pConnection.getConnection().close();
                }
                catch (Exception e) {
                  logger.warn("Excpeiton closing unused connection.  Exception: " + e.getMessage());
                }
                getConnections().remove(i);
              }
              return;
            }
          }
        }
      }
    }
  }
  */
}

