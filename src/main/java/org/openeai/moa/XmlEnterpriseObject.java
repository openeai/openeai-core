/*******************************************************************************
 $Source$
 $Revision: 1243 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa;

import java.util.HashMap;
import org.jdom.Document;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.config.EnterpriseFields;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.layouts.*;

/**
 * Placeholder for future interface..
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public interface XmlEnterpriseObject extends EnterpriseObject {

	/**
	 * Using the currently assigned input layout manager, this method
   * takes the input passed in and builds the object from that input.
   * For example, if the data passed in is a BasicPerson XML Element, and
   * the current inputLayoutManager is the XmlLayout manager, it will ask
   * the layout manager to build the BasicPerson object from that element.
   * The layout manager will then take the data in the element and put it into
   * the appropriate instance variables (via object getter/setter methods).  While
   * it's doing that, the input data will be validated based on business rules
   * found in the EnterpriseFields object (built from EnterpriseObjects.xml)
   * <P>
   * Additionally, if the data passed in is "application specific" data (like codes)
   * this data will be converted to Enterprise Values based on Translation information
   * also obtained from the EnterpriseObjects.xml document.
	 *<P>
   * This method (along with buildOutputFromObject and the Layout Manager infrastructure)
   * allows us to generalize the building our message objects so we don't have to
   * put all of that logic in the objects themselves.  We just implement new Layout Managers
   * and associate those layouts to the objects.  All objects have an Xml Layout Manager
   * associated to them that is derived from the EnterpriseObjects.xml document.
   *<P>
	 * @param input Object the data that will be used to build the object.
   * e.g. - Element, String etc.
	 *<P>
   * @throws EnterpriseLayoutException.  If an error occurs building the object
   * from the data passed in.
	 */
	public void buildObjectFromInput(Object input) throws EnterpriseLayoutException;
	/**
	 * Using the currently assigned output layout manager, this method
   * uses the current contents of the object and builds an output object as
   * implemented by the layout manager.
   * For example, if the object is a BasicPerson, and
   * the current outputLayoutManager is the XmlLayout manager, it will ask
   * the layout manager to build the BasicPerson XML Element from BasicPerson object.
   * The layout manager will then retrieve the data from the BasicPerson object
   * via object getter methods and build an Element from the data.  While
   * it's doing that, the object will be validated based on business rules
   * found in the EnterpriseFields object (built from EnterpriseObjects.xml)
	 *<P>
   * This method (along with buildInputFromObject and the Layout Manager infrastructure)
   * allows us to generalize the serialization our message objects so we don't have to
   * put all of that logic in the objects themselves.  We just implement new Layout Managers
   * and associate those layouts to the objects.  All objects have an Xml Layout Manager
   * associated to them that is derived from the EnterpriseObjects.xml document.
   *<P>
	 * @return  Object the data that will be built from the object.  The calling applicatons
   * will cast this object to the appropriate thing. e.g. - Element, String etc.
	 *<P>
   * @throws EnterpriseLayoutException.  If an error occurs building the output
   * from the object.
	 */
	public Object buildOutputFromObject() throws EnterpriseLayoutException;
	/**
	 * Using the currently assigned output layout manager, this method
   * uses the current contents of the object and builds an output object as
   * implemented by the layout manager for the specified application name.
   * For example, if the object is a BasicPerson, and
   * the current outputLayoutManager is the ExtractLayout manager, it will ask
   * the layout manager to build an extract line with application specific values
   * using the contents of the BasicPerson object.  This allows us to "reverse translate"
   * our enterprise values to application specific values (like codes etc.)
   * The information related to these translations can be found in the EnterpriseObjects.xml document.
	 *<P>
   * This method (along with buildInputFromObject and the Layout Manager infrastructure)
   * allows us to generalize the serialization our message objects so we don't have to
   * put all of that logic in the objects themselves.  We just implement new Layout Managers
   * and associate those layouts to the objects.  All objects have an Xml Layout Manager
   * associated to them that is derived from the EnterpriseObjects.xml document.
   *<P>
	 * @return  Object the data that will be built from the object.  The calling applicatons
   * will cast this object to the appropriate thing. e.g. - Element, String etc.
	 *<P>
   * @throws EnterpriseLayoutException.  If an error occurs building the output
   * from the object.
	 */
	public Object buildOutputFromObject(String appName) throws EnterpriseLayoutException;
	/**
	 * This method allows application developers to build an object from
   * a XML String representation of an object that was previously converted
   * to an XML String via the toXmlString() method.  This method basically
   * uses the "xml" inputLayoutManager to build the object from an Element that
   * gets created out of the String passed in.  This is so developers don't have to
   * convert the String to an element themselves (as a convenience).  They just
   * have to pass the String and this method takes care of converting it to an Element
   * and asking the layout manager to build the object from that element.
	 *<P>
	 * @param theString String the XML String that will be used to build the object.
	 *<P>
   * @throws XmlEnterpriseObjectException.  If an error occurs building the object
   * from the data passed in.
	 */
	public void buildObjectFromXmlString(String theString) throws XmlEnterpriseObjectException;

	/**
	 * Performs a recursive field for field copy of the current Object and returns the result.
	 * Any object found that supports the "clone" method is cloned.  Since all simple fields within
   * an XmlEnterpriseObjectImpl are ultimately Strings, the resulting object is in fact a new object
   * because the copy is recursive.
	 *
	 * @return  XmlEnterpriseObject a "deep" copy of the object.
	 *
	 */
	public Object clone() throws CloneNotSupportedException;
  
	/**
	 * Returns the EnterpriseFields object associated with this object.
	 * For information on the EnterpriseFields object, refer to the JavaDoc
	 * for the org.openeai.config package.
	 *<P>
   * @return EnterpriseFields the EnterpriseFields object that will be used by this 
   * object to validate/format data passed to setter methods on this object.
   * @see org.openeai.config.EnterpriseFields
	 *
	 */
	public EnterpriseFields getEnterpriseFields();
	/**
	 * Sets the EnterpriseFields object associated with this object.
	 * For information on the EnterpriseFields object, refer to the JavaDoc
	 * for the org.openeai.config package.  This is called
	 * when the object is initialized.  The EnterpriseFields object is built
	 * based on information found in the EnterpriseObjects document.
   *<P>
   * This information is specified in the MessageObjectConfig XML Element in the deployment
   * documents and is passed to this object during initialization by the MessageObjectConfig
   * Java object.
   *<P>
   * @param fields EnterpriseFields the EnterpriseFields object that will be used by this 
   * object to validate/format data passed to setter methods on this object.
   * @see org.openeai.config.EnterpriseFields
	 */
	public void setEnterpriseFields(EnterpriseFields fields);
  
  /**
  * Sets the test id associated to this object.  This is used during testing to correlate
  * a message sent by a particular object to a message consumed by a gateway.  This is 
  * related to the OpenEAI Test Suite foundation.
  *<P>
  * @param tId TestId the test id object associated to this object.
  */
  public void setTestId(TestId tId);
  /**
  * Returns the test id associated to this object.  This is used during testing to correlate
  * a message sent by a particular object to a message consumed by a gateway.  This is 
  * related to the OpenEAI Test Suite foundation.
  *<P>
  * @return TestId the test id object associated to this object.
  */
  public TestId getTestId();
  
	/**
   * Sets the baseline object associated to this object.  The baseline object
   * is populated by all objects when that object performs a Query action
   * via the JmsEnterpriseObject.query method.  Then,
   * when updates are performed on the object and an Update-Request/Sync is made on behalf
   * of the object (via the JmsEnterpriseObject.update/updateSync method, it uses the baseline
   * object to populate the DataArea/BaselineData element in the update document.
   * The baseline object remains untouched during the processing on the main object.
	 *<P>
	 * @param baseline XmlEnterpriseObject the baseline object (e.g. - BasicPerson) that is an
   * image of the object at the time the Query action was performed.
	 */
	public void setBaseline(XmlEnterpriseObject baseline);
	/**
   * Returns the baseline object associated to this object.  The baseline object
   * is populated by all objects when that object performs a Query action
   * via the JmsEnterpriseObject.query method.  Then,
   * when updates are performed on the object and an Update-Request/Sync is made on behalf
   * of the object (via the JmsEnterpriseObject.update/updateSync method, it uses the baseline
   * object to populate the DataArea/BaselineData element in the update document.
   * The baseline object remains untouched during the processing on the main object.
	 *<P>
	 * @return XmlEnterpriseObject the baseline object (e.g. - BasicPerson) that is an
   * image of the object at the time of the Query.
	 */
	public XmlEnterpriseObject getBaseline();
  
	/**
	 * Returns this object's currently assigned Input Layout manager.  If no
   * current input layout manager exists, it returns the XmlLayout manager that all
   * objects must have.
	 *<P>
	 * @return EnterpriseLayoutManager the currently assigned input layout manager.
   * if no input layout manager has been assigned, it returns the XmlLayout manager.
	 */
	public EnterpriseLayoutManager getInputLayoutManager();
	/**
	 * Returns a HashMap containing all Input Layout managers associated to
   * this object.  These are all the input layout managers that may be used by
   * this object.  Since objects may support several different layout managers
   * that are used to build themselves, this is necessary.
	 *<P>
	 * @return HashMap the list of Input Layout managers that may be used by this
   * object.
	 */
	public HashMap getInputLayoutManagers();
	/**
	 * Returns an Input Layout manager for this object of a specified type.  (e.g. - "xml", "extract").
   * If no layout manager of the specified type exists, it returns null.
	 *<P>
	 * @param type String the type of input layout manager ("xml", "extract" etc.).
	 *<P>
	 * @return EnterpriseLayoutManager the input layout manager that is the type
   * specified as a parm.
	 */
	public EnterpriseLayoutManager getInputLayoutManager(String type);
	public void setInputLayoutManagers(HashMap iManagers);
	public void addInputLayoutManager(String type, EnterpriseLayoutManager iLayout);
	public void setInputLayoutManager(EnterpriseLayoutManager elm);
  
	/**
	 * Returns a HashMap containing all Output Layout managers associated to
   * this object.  These are all the output layout managers that may be used by
   * this object.  Since objects may support several different layout managers
   * that are used to serialize themselves, this is necessary.
	 *<P>
	 * @return HashMap the list of Output Layout managers that may be used by this
   * object.
	 */
	public HashMap getOutputLayoutManagers();
	/**
	 * Returns this object's currently assigned Output Layout manager.  If no
   * current output layout manager exists, it returns the XmlLayout manager that all
   * objects must have.
	 *<P>
	 * @return EnterpriseLayoutManager the currently assigned output layout manager.
   * if no output layout manager has been assigned, it returns the XmlLayout manager.
	 */
	public EnterpriseLayoutManager getOutputLayoutManager();
	/**
	 * Returns an Output Layout manager for this object of a specified type.  (e.g. - "xml", "extract").
   * If no layout manager of the specified type exists, it returns null.
	 *<P>
	 * @return String the type of output layout manager ("xml", "extract" etc.).
	 *<P>
	 * @return EnterpriseLayoutManager the output layout manager that is the type
   * specified as a parm.
	 */
	public EnterpriseLayoutManager getOutputLayoutManager(String type);
	public void setOutputLayoutManagers(HashMap oManagers);
	public void addOutputLayoutManager(String type, EnterpriseLayoutManager oLayout);
	public void setOutputLayoutManager(EnterpriseLayoutManager elm);

	/**
	 * Recursively checks the contents of the object and true if it contains any data.  Otherwise,
   * it returns false.  For example, if the BasicPerson/Name/FirstName variable
   * has data in it, this method will return true.  If no instance variables, or child
   * objects have data in them, it returns false.
	 *<P>
	 * @return boolean an indicator specifying whether or not the object is empty.
	 *<P>
	 * @throws XmlEnterpriseObjectException if any errors occur when determining
	 * if the object empty.
	 */
	public boolean isEmpty() throws XmlEnterpriseObjectException;
	/**
	 * Checks to see if this object is a "Date/Datetime" object.  This is because Dates and Datetimes
   * contain special constructors that indicate the type of Date/Datetime being built and
   * several other foundation components need to determine if an object being dealt with
   * is one of those Date/Datetime objects.
	 *
	 * @return  boolean
	 *
	 */
  public boolean isDate();
	/**
	 * A convenience method that can be used to compare two Xml aware objects (XmlEnterpriseObjectImpl).
   * This method converts the current object and the object passed in to an XML String
   * via the toXmlString() method.  Then it does a String comparison on those two strings.
   * Note, this method IS case sensitive.
	 *<P>
	 * @return  boolean
	 *<P>
   * @throws XmlEnterpriseObjectException.  If an error occurs serializing the
   * object to a String.  This is usually due to invalid data (formats etc.) determined
   * from the object's current rules as specified in EnterpriseFields.
	 */
  public boolean equals(XmlEnterpriseObject xeo) throws XmlEnterpriseObjectException;
  /**
  * Returns a comma separated String containing all the data currently stored in this object.
  *<P>
  * Format for returned data:
  * <P>
  * FieldName1=some value, FieldName2=field2 data etc.
  * <P>
  * @return String
  */
  public String toString();
	/**
	 * A convenience method that can be used simply by application developers
   * to retreive the contents of the object as an XML String.  Basically,
   * this is a fully valid XML Element just represented as a String.  This
   * data can then be used to persist the entire object, and/or build an
   * Element from that String which can be used to build the object from an
   * input (an XML Element).
	 *<P>
	 * @return  String the XML representation of the object as a String.
	 *<P>
   * @throws XmlEnterpriseObjectException.  If an error occurs serializing the
   * object to a String.  This is usually due to invalid data (formats etc.) determined
   * from the object's current rules as specified in EnterpriseFields.
	 */
	public String toXmlString() throws XmlEnterpriseObjectException;

  /**
   * goes through all the key fields on the xeo passed in and creates a string containing all the
   * values from those key fields.  This string is used by other methods to determine if there are
   * matching new/baseline xeos with the same key information.  That allows us to determine
   * if a transaction should be an insert, update, delete or ignored.
   * <P>
   * @return String the combined key value
   * <P>
   * @throws XmlEnterpriseObjectException
  **/
  public String getCombinedKeyValue() throws XmlEnterpriseObjectException;
  
  /**
  * Returns the current value from the field name passed in.  Convenience
  * method to allow "reflective" use of XmlEnterpriseObjects.  Assumes
  * the getter method being called does not take any parameters.
  *<P>
  * @return Object the value from the field 
  * @param fieldName String the name of the field from which to retrieve data
  * @throws XmlEnterpriseObjectException if the field name passed in does not exist 
  * or if any other errors occur calling the getter method associated to the field.
  */
	public Object getValueFromObject(String fieldName) throws XmlEnterpriseObjectException;
  /**
  * Returns the current value from the field name passed in.  Convenience
  * method to allow "reflective" use of XmlEnterpriseObjects.  Allows for
  * parameters.  This is especially neccessary when retrieving an individual
  * object from a list of objects contained within the parent object.
  *<P>
  * @return Object the value from the field 
  * @param fieldName String the name of the field from which to retrieve data
  * @param parms Object[] any parameters that need to be passed to the getter method being called.
  * @param parmTypes Class[] the paramater types associated to any parameters.
  * @throws XmlEnterpriseObjectException if the field name passed in does not exist 
  * or if any other errors occur calling the getter method associated to the field.
  */
	public Object getValueFromObject(String fieldName, Object[] parms, Class[] parmTypes)
  	throws XmlEnterpriseObjectException;

	/**
	 * Returns the "primed" Create Document associated with this object.
	 * This document is used as a baseline for create message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed create document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
	 *
	 * @return  Document the primed create document. Returns null if the document is null.
	 *
	 */
	public Document getCreateDoc();
  
	/**
  * Sets the "primed" create document associated with this object.  This is called
  * during object initialization and is set based on information in the deployment 
  * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
  * @param doc  org.jdom.Document the "primed" create Document
  *
  */
	public void setCreateDoc(Document doc);
  
	/**
	 * Returns the "primed" CreateSync Document associated with this object.
	 * This document is used as a baseline for CreateSync message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed create sync document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
	 *
	 * @return  Document the primed CreateSync document. Returns null if the document is null.
	 *
	 */
	public Document getCreateSyncDoc();

	/**
	 * Sets the "primed" create sync document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" create sync Document
	 *
	 */
	public void setCreateSyncDoc(Document doc);

	/**
	 * Returns the "primed" Delete Document associated with this object.
	 * This document is used as a baseline for delete message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed delete document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed delete document. Returns null if the document is null.
	 *
	 */
	public Document getDeleteDoc();
  
	/**
	 * Sets the "primed" delete document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" delete Document
	 *
	 */
	public void setDeleteDoc(Document doc);
  
	/**
	 * Returns the "primed" DeleteSync Document associated with this object.
	 * This document is used as a baseline for DeleteSync message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed delete sync document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed DeleteSync document. Returns null if the document is null.
	 *
	 */
	public Document getDeleteSyncDoc();
  
	/**
	 * Sets the "primed" delete sync document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" delete sync Document
	 *
	 */
	public void setDeleteSyncDoc(Document doc);
  
	/**
	 * Returns the "primed" generate Document associated with this object.
	 * This document is used as a baseline for generate message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed generate document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed generate document. Returns null if the document is null.
	 *
	 */
	public Document getGenerateDoc();
  
	/**
	 * Sets the "primed" generate document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" generate Document
	 *
	 */
	public void setGenerateDoc(Document doc);

	/**
	 * Sets the "primed" generate sync document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" generate sync Document
	 *
	 */
	public void setGenerateSyncDoc(Document doc);

	/**
	 * Returns the "primed" GenerateSync Document associated with this object.
	 * This document is used as a baseline for GenerateSync message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed generate sync document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed GenerateSync document. Returns null if the document is null.
	 *
	 */
	public Document getGenerateSyncDoc();
  
	/**
	 * Returns the "primed" Update Document associated with this object.
	 * This document is used as a baseline for update message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed update document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed update document.
	 *
	 */
	public Document getUpdateDoc();
  
	/**
	 * Sets the "primed" update document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" update Document
	 *
	 */
	public void setUpdateDoc(Document doc);

	/**
	 * Returns the "primed" UpdateSync Document associated with this object.
	 * This document is used as a baseline for UpdateSync message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed update sync document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed UpdateSync document.
	 *
	 */
	public Document getUpdateSyncDoc();
  
	/**
	 * Sets the "primed" update sync document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" update sync Document
	 *
	 */
	public void setUpdateSyncDoc(Document doc);
  
	/**
	 * Returns the "primed" Provide Document associated with this object.
	 * This document can be used by gateways who consume Query-Requests for this 
   * object as a baseline for Provide-Reply messages.  This way, the 
   * gateway doesn't have to support any complex mapping in the gateway itself.  Although 
   * some gateways may still choose to do it that way.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed provide document.
	 *
	 */
	public Document getProvideDoc();
  
	/**
	 * Sets the "primed" provide document associated with this object.  This is called
	 * during object initialization and is set based on information in the Deployment 
	 * descriptor.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" provide Document
	 *
	 */
	public void setProvideDoc(Document doc);

	/**
	 * Returns the "primed" Response Document associated with this object.
	 * This document can be used by gateways who consume Create, Delete and Update Requests for this 
   * object as a baseline for Response-Reply messages.  This way, the 
   * gateway doesn't have to support any complex mapping in the gateway itself.  Although 
   * some gateways may still choose to do it that way.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed provide document.
	 *
	 */
	public Document getResponseDoc();
  
	/**
	 * Sets the "primed" response document associated with this object.  This is called
	 * during object initialization and is set based on information in the Deployment 
	 * descriptor.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" response Document
	 *
	 */
	public void setResponseDoc(Document doc);
  
	/**
	 * Returns the "primed" Query Document associated with this object.
	 * This document is used as a baseline for Query message production.
	 * This is valuable because we can populate the primed document with information
	 * that will not change and the message production logic doesn't have to worry
	 * about filling that information in prior to sending the message.
	 * Clients should never need to call this method directly unless they just want to 
   * know if the object has a primed query document associated to it.
   *<P>
   * If the document is not initialized because of deferred initialization, the
   * document will be initialized in this method and then it will be returned.  This
   * way, document initialization will only ever have to happen once and it can
   * be configured to do it when an application starts or when the document is
   * first requested.
   *<P>
	 * @return  Document the primed Query document. Returns null if the document is null.
	 *
	 */
	public Document getQueryDoc();

	/**
	 * Sets the "primed" query document associated with this object.  This is called
	 * during object initialization and is set based on information in the MessagingEnterprise
	 * document.  Clients should never need to call this method directly unless they want 
  * to override the primed document that was associated to the object when it was 
  * initialized.
  *<P>
  * This information is specified in the MessageObjectConfig XML Element in the deployment
  * documents and is passed to this object during initialization by the MessageObjectConfig
  * Java object.
  *<P>
	 * @param doc  org.jdom.Document the "primed" query Document
	 *
	 */
	public void setQueryDoc(Document doc);
  
  /**
  * This method sets the EnterpriseFields and XML layout manager information on the child object 
  * passed in to be that of the current object.  This is a convenience method that can
  * be used by Parent objects when returning a child object from a "getter" method.  Since
  * the child objects will not be initialized with this information at start up, this 
  * eliminates the need to application developers to do this programmatically since
  * it can be done automatically in the getter methods.
  *<P>
  * Example:  The BasicPerson object has a child object in it called Name.  The
  * name object is another XmlEnterpriseObjectImpl.  When the getName() method is
  * called on BasicPerson, this method is called to "give" the Name object being returned
  * the EnterpriseFields and XmlLayout manager associated to the BasicPerson.  Since
  * the BasicPerson's EnterpriseObjects XML document will also have to include the Name
  * object's definition, the Name object returned by the BasicPerson.getName() method will
  * have what it needs to function properly and no action will be required by the developers.
  *<P>
  * @param childXeo XmlEnterpriseObjectImpl the child object being initialized.
  */
  public void initializeChild(XmlEnterpriseObject childXeo);
  
	public boolean getValidation();
}

 
