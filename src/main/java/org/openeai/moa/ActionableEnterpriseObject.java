package org.openeai.moa;

import java.util.List;
import java.util.Map;

import org.openeai.transport.*;
import org.openeai.moa.objects.resources.*;

public interface ActionableEnterpriseObject extends XmlEnterpriseObject {
	/**
	 * Query message production.  Builds an XML document (using the primed query document
	 * as a "template") out of the current contents of the keyObject passed in.
	 * While building the XML Query message, it will validate contents of the keyObject checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param keyObject XmlEnterpriseObject to use as retreival arguments in the Query.
   * The contents of this object are inserted into the Query document prior to
   * sending the query request.  Under normal conditions, this object is
   * actually a LightweightPerson object since that's the most common query object used.
	 *<P>
	 * NOTE:  This method should be over-ridden by lower level classes if the keyObject
	 * isn't a LightweightPerson.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the update-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  java.util.List this is a list of XmlEnterpriseObject objects
   * (like BasicPerson, BasicEmployee etc.) provided by the authoritative source
   * (the application consuming the query request).
	 *<P>
	 * @throws EnterpriseObjectQueryException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public java.util.List query(XmlEnterpriseObject keyObject, RequestService producer)
	throws EnterpriseObjectQueryException;
  
	/**
	 * Create message production.  Builds an XML document (using the primed create document
	 * as a "template") out of the current contents of the object.  While building the
	 * XML Create document, it will validate contents of the object checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the create-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  XmlEnterpriseObject a generic response
	 * which will indicate success or failure.  If failure, any error information will
	 * be included in that result.  The object returned is actually a
	 * org.openeai.moa.objects.Result object.
	 *<P>
	 * @throws EnterpriseObjectCreateException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public XmlEnterpriseObject create(RequestService producer)
	throws EnterpriseObjectCreateException;
  
	/**
	 * Create Sync message production.  Builds an XML document (using the primed
	 * create sync document as a baseline) out of the current contents of the object.
	 * While building the XML Create Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *
	 * @param producer   org.openeai.jms.producer.PubSubProducer to
	 * use to publish the message.
	 *
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
	public void createSync(SyncService producer)
	throws EnterpriseObjectSyncException;
  
	/**
	 * Delete message production.  Builds an XML document (using the primed delete document
	 * as a "template") out of the current contents of the object.  While building the
	 * XML Delete document, it will validate contents of the object checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param deleteAction String the delete action ('delete' or 'purge')
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the delete-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  XmlEnterpriseObject a generic response
	 * which will indicate success or failure.  If failure, any error information will
	 * be included in that result.  The object returned is actually a
	 * org.openeai.moa.objects.Result object.
	 *<P>
	 * @throws EnterpriseObjectDeleteException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public XmlEnterpriseObject delete(String deleteAction, RequestService producer)
	throws EnterpriseObjectDeleteException;
  
	/**
	 * Delete Sync message production.  Builds an XML document (using the primed
	 * delete sync document as a baseline) out of the current contents of the object.
	 * While building the XML Delete Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *<P>
	 * @param deleteAction String delete action ('purge' or 'delete')
	 *<P>
	 * @param producer PubSubProducer a pre-configured and started PubSubProducer
   * which will be used to send the delete-sync message to the appropriate destination
   * (via the PubSubProducer.publishMessage method).
	 *<P>
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
	public void deleteSync(String deleteAction, SyncService producer)
	throws EnterpriseObjectSyncException;
  
	/**
	 * Generate message production.  Builds an XML document (using the primed generate document
	 * as a "template") out of the current contents of the object.  While building the
	 * XML Generate document, it will validate contents of the object checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the update-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @param keyObject XmlEnterpriseObject to use as generation "seed" data
   * (like an UnknownPerson for an InstitutionalIdentity-Generate-Request).
	 *<P>
	 * @return  java.util.List this is a list of XmlEnterpriseObject objects generated by the authoritative source
	 * (the application consuming the request to generate).  Typically, this will
	 * be only one object (like an InstitutionalIdentity).
	 *<P>
	 * @throws EnterpriseObjectGenerateException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public java.util.List generate(XmlEnterpriseObject keyObject, RequestService producer)
	throws EnterpriseObjectGenerateException;
  
	/**
	 * Generate Sync message production.  Builds an XML document (using the primed
	 * generate sync document as a "template") out of the current contents of the object.
	 * While building the XML generate Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *<P>
	 * @param producer PubSubProducer a pre-configured and started PubSubProducer
   * which will be used to send the delete-sync message to the appropriate destination
   * (via the PubSubProducer.publishMessage method).
	 *<P>
	 * @param keyObject XmlEnterpriseObject to use as generation "seed" data
   * (like an UnknownPerson for an InstitutionalIdentity-Generate-Sync).
	 *<P>
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
//	public void generateSync(SyncService producer, XmlEnterpriseObject keyObject)
//	throws EnterpriseObjectSyncException;
  
	/**
	 * Update message production.  Builds an XML document (using the primed update document
	 * as a "template") out of the current contents of the object (this).  It will use the
	 * current contents of the object as the NewData portion of the message and it will
	 * use this object's "baseline" object as the Baseline portion of the message.  The "baseline"
	 * object is set when this object was "queried" for previously.
	 * While building the XML Update message, it will validate contents checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the update-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  XmlEnterpriseObject (Result) this will indicate the success or failure
	 * of the Update request.  In an error condition, this will include any error
	 * information.
	 *<P>
	 * @throws EnterpriseObjectUpdateException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
  public XmlEnterpriseObject update(RequestService producer)
  throws EnterpriseObjectUpdateException;
  
	/**
	 * Update Sync message production.  Builds an XML document (using the primed
	 * update sync document as a "template") out of the current contents of the object.
	 * It will use the current contents of the object as the NewData portion of the message and it will
	 * use the m_baseline object as the Baseline portion of the message.  The m_baseline
	 * object is set when this object was "queried" for previously.
	 * While building the XML Update Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *<P>
	 * @param producer PubSubProducer a pre-configured and started PubSubProducer
   * which will be used to send the delete-sync message to the appropriate destination
   * (via the PubSubProducer.publishMessage method).
	 *<P>
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
	public void updateSync(SyncService producer)
	throws EnterpriseObjectSyncException;
  
	/**
	 * Returns the Command name associated with this object.  This is a
	 * core part of all messages (a property on the JMS message) and no message can be produced
	 * if either this hasn't been set.  However, this gets set when the Message Object is
	 * initialized via the init method and the MessageObjectConfig object.  The Command Name
	 * is set based on contents of the MessagingEnterprise XML document which is used to initialize all
	 * message objects.  No client application should ever need to call this method
	 * directly.  This is information that will be used by a consumer of the message
	 * to determine which "command" to execute when it receives the message.  For more information
	 * on Commands and the University JMS Consumer framework refer to the JavaDoc for the
	 * org.openeai.jms.consumer package.
	 *<P>
	 * @return  String message name associated with this object.
	 *
	 */
	public String getCommandName();
	/**
	 * Sets the Command name associated with this object.  This is a
	 * core part of all messages (a property on the JMS message) and no message can be produced
	 * if this hasn't been set.  However, this gets set when the Message Object is
	 * initialized via the init method and the MessageObjectConfig object.  The Command Name
	 * is set based on contents of the application Deployment configuration document which is used to initialize all
	 * message objects.  No client application should ever need to call this method
	 * directly.  This is information that will be used by a consumer of the message
	 * to determine which "command" to execute when it receives the message.  For more information
	 * on Commands and the University JMS Consumer framework refer to the JavaDoc for the
	 * org.openeai.jms.consumer package.
	 *<P>
   * @param name String the name of the command as specified in the configuration document.
	 */
	public void setCommandName(String name);
  
	/**
	 * Returns the Authentication object associated with this object.  This is a
	 * core part of all messages (in the ControlArea) and no message can be produced
	 * if this hasn't been set.  However, this gets set when the Message Object is
	 * initialized via the init method and the MessageObjectConfig object.  The
	 * information contained in the Authentication object is set based on contents
	 * of the MessagingEnterprise XML document which is used to initialize all
	 * message objects.  No client application should ever need to call this method
	 * directly.
	 *<P>
	 * @return  Authentication the authentication object associated with this object.
	 *
	 */
	public Authentication getAuthentication();
	/**
	 * Sets the Authentication object associated with this object.  This is a
	 * core part of all messages (in the ControlArea) and no message can be produced
	 * if this hasn't been set.  However, this gets set when the Message Object is
	 * initialized via the init method and the MessageObjectConfig object.  The
	 * information contained in the Authentication object is set based on contents
	 * of the MessagingEnterprise XML document which is used to initialize all
	 * message objects.  No client application should ever need to call this method
	 * directly.
	 *<P>
	 * @param auth Authentication
	 *
	 */
	public void setAuthentication(Authentication auth);
  
	/**
	 * Returns the MessageId object associated with this object.  This is a
	 * core part of all messages (in the ControlArea) and no message can be produced
	 * if this hasn't been set.  However, this gets set when the Message Object is
	 * initialized via the init method and the MessageObjectConfig object.  The
	 * information contained in the MessageId object is set based on contents
	 * of the MessagingEnterprise XML document which is used to initialize all
	 * message objects.  No client application should ever need to call this method
	 * directly.
	 *<P>
	 * @return  MessageId the MessageId object associated with this object.
	 *
	 */
	public MessageId getMessageId();
	/**
	 * Sets the MessageId object associated with this object.  This is a
	 * core part of all messages (in the ControlArea) and no message can be produced
	 * if this hasn't been set.  However, this gets set when the Message Object is
	 * initialized via the init method and the MessageObjectConfig object.  The
	 * information contained in the MessageId object is set based on contents
	 * of the MessagingEnterprise XML document which is used to initialize all
	 * message objects.  No client application should ever need to call this method
	 * directly.
	 *<P>
   * @param msgId MessageId
	 *
	 */
	public void setMessageId(MessageId msgId);

  /**
  * Helper method that returns this object as an XmlEnterpriseObject.
  *<P>
  * @return org.openeai.moa.XmlEnterpriseObject.  This object cast to an XmlEnterpriseObject.
  **/
  public XmlEnterpriseObject getXmlEnterpriseObject();
  /**
  * Returns a List containing the last errors encountered by this object during 
  * a Request action (create, query, generate, update, delete).  When an error
  * occurs during one of those actions, an exception is thrown.  Therefore, the
  * calling application doesn't have access to the errror objects in the result.
  * This convenience method gives them access to those Error objects so they
  * may use them accordingly in their application instead of having to parse
  * the exception message etc.
  * <P>
  * @return java.util.List the list of errors that were saved the last time this
  * object had an error performing a request action.
  **/
  public java.util.List getLastErrors();
 
  /**
   * 
   * Compare each field between baseline and newData and put it to the map if they are not equal to each other.
   * throws XmlEnterpriseObjectException if baselinen or newdata is null
   * 
   * @param baselineKeyToValue
   * @param newDataKeyToValue
   * @return a String which diff between baseline and new object in the format
   * fieldName:baseline=>new;fieldName2:baseline=>new
   */
  public String diff(Map baselineKeyToValue,Map newDataKeyToValue) throws XmlEnterpriseObjectException;
}