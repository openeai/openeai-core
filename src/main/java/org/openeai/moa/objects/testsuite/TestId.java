/*******************************************************************************
 $Source$
 $Revision: 844 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.testsuite;

import org.openeai.moa.*;
import org.openeai.config.*;

/**
 * A Class that wraps the TestId element as specified in
 * the OpenEAI protocol.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class TestId extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject, org.openeai.PubliclyCloneable {
  private String m_testSuiteName = "";
  private String m_testSeriesNumber = "";
  private String m_testCaseNumber = "";
  private String m_testStepNumber = "";

  public TestId() {
  }
  public TestId(MessageObjectConfig mConfig) throws EnterpriseObjectException {
    init(mConfig);
  }
  
  public String getTestSuiteName() {
    return m_testSuiteName;
  }
  public void setTestSuiteName(String name) throws EnterpriseFieldException {
    m_testSuiteName = getEnterpriseValue("TestSuiteName", name);
  }

  public String getTestSeriesNumber() {
    return m_testSeriesNumber;
  }
  public void setTestSeriesNumber(String number) throws EnterpriseFieldException {
    m_testSeriesNumber = getEnterpriseValue("TestSeriesNumber", number);
  }

  public String getTestCaseNumber() {
    return m_testCaseNumber;
  }
  public void setTestCaseNumber(String number) throws EnterpriseFieldException {
    m_testCaseNumber = getEnterpriseValue("TestCaseNumber", number);
  }

  public String getTestStepNumber() {
    return m_testStepNumber;
  }
  public void setTestStepNumber(String number) throws EnterpriseFieldException {
    m_testStepNumber = getEnterpriseValue("TestStepNumber", number);
  }

  public String toString() {
    StringBuffer sBuf = new StringBuffer();
    sBuf.append( "TestSuiteName-" + getTestSuiteName() );
    if (getTestSeriesNumber().length() > 0) {
      sBuf.append( "-TestSeriesNumber-" + getTestSeriesNumber() );
    }
    if (getTestCaseNumber().length() > 0) {
      sBuf.append( "-TestCaseNumber-" + getTestCaseNumber() );
    }
    if (getTestStepNumber().length() > 0) {
      sBuf.append( "-TestStepNumber-" + getTestStepNumber() );
    }
    return sBuf.toString();
  }
}
