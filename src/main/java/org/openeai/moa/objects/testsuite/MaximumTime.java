/*******************************************************************************
 $Source$
 $Revision: 845 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.testsuite;

import org.openeai.moa.*;
import org.openeai.moa.objects.resources.*;
import org.openeai.config.*;

public class MaximumTime extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject, org.openeai.PubliclyCloneable {
  private String m_value = null;    // required=false
  private String m_testStepId = null;

  public MaximumTime() {
  }
  public MaximumTime(MessageObjectConfig mConfig) throws EnterpriseObjectException {
    init(mConfig);
  }
  
  /**
  * Returns the Value instance variable
  * associated to this RequestTimings object.
  * <P>
  * @return String
  **/
  public String getValue() {
    return m_value;
  }


  /**
  * Convenience method that returns the reverse translated
  * application value for the application name passed in.
  * <P>
  * @param String the name of the application for who the reverse
  * translations should be performed.
  * @return String
  * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
  * the current contents of the Value instance variable
  * to the Application Value associated to the application name passed in.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public String getValue( String appMaximumTime ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appMaximumTime, "Value", getValue() );
  }


  /**
  * Sets the Value instance variable
  * associated to this RequestTimings object to the value passed in.
  * Uses the EnterpriseFields object associated to the RequestTimings
  * to verify the data passed in meets the rules specified in
  * the Enterprise Objects document for the RequestTimings
  * If the data passed in does not meet those rules and it
  * cannot be Translated/Scrubbed to meet those rules, an
  * exception will be thrown.
  * <P>
  * @param String
  * @throws org.openeai.config.EnterpriseFieldException if the value passed in
  * does not meet the business rules for the field as specified
  * in the Enterprise Objects document for this object.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public void setValue( String aValue ) throws org.openeai.config.EnterpriseFieldException {
    m_value = getEnterpriseValue( "Value", aValue );
  }

  /**
  * Returns the TestStepId instance variable
  * associated to this RequestTimings object.
  * <P>
  * @return String
  **/
  public String getTestStepId() {
    return m_testStepId;
  }


  /**
  * Convenience method that returns the reverse translated
  * application value for the application name passed in.
  * <P>
  * @param String the name of the application for who the reverse
  * translations should be performed.
  * @return String
  * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
  * the current contents of the TestStepId instance variable
  * to the Application Value associated to the application name passed in.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public String getTestStepId( String appMaximumTime ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appMaximumTime, "TestStepId", getTestStepId() );
  }


  /**
  * Sets the TestStepId instance variable
  * associated to this RequestTimings object to the value passed in.
  * Uses the EnterpriseFields object associated to the RequestTimings
  * to verify the data passed in meets the rules specified in
  * the Enterprise Objects document for the RequestTimings
  * If the data passed in does not meet those rules and it
  * cannot be Translated/Scrubbed to meet those rules, an
  * exception will be thrown.
  * <P>
  * @param String
  * @throws org.openeai.config.EnterpriseFieldException if the value passed in
  * does not meet the business rules for the field as specified
  * in the Enterprise Objects document for this object.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public void setTestStepId( String aTestStepId ) throws org.openeai.config.EnterpriseFieldException {
    m_testStepId = getEnterpriseValue( "TestStepId", aTestStepId );
  }

}