/*******************************************************************************
 $Source$
 $Revision: 844 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.testsuite;

import org.openeai.moa.*;
import org.openeai.moa.objects.resources.*;
import org.openeai.config.*;

/**
 * A Class that wraps the TestSuiteSummary element as specified in
 * the OpenEAI TestSuiteSummary definition.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class TestSuiteSummary extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject, org.openeai.PubliclyCloneable {
  private Datetime m_requestReplyStartDatetime = null;
  private Datetime m_requestReplyEndDatetime = null;
  private Datetime m_syncConsumptionStartDatetime = null;
  private Datetime m_syncConsumptionEndDatetime = null;
  private TestSeriesSummary m_testSeriesSummary = null;
  private TestCaseSummary m_testCaseSummary = null;
  private TestStepSummary m_testStepSummary = null;
  private java.util.List m_requestTimings = null;    // required=false
  
  public TestSuiteSummary() {
  }
  public TestSuiteSummary(MessageObjectConfig mConfig) throws EnterpriseObjectException {
    init(mConfig);
  }

  public void setRequestReplyStartDatetime(Datetime time) {
    m_requestReplyStartDatetime = time;
  }
  public Datetime getRequestReplyStartDatetime() {
    return m_requestReplyStartDatetime;
  }
  
  public void setRequestReplyEndDatetime(Datetime time) {
    m_requestReplyEndDatetime = time;
  }
  public Datetime getRequestReplyEndDatetime() {
    return m_requestReplyEndDatetime;
  }

  public void setSyncConsumptionStartDatetime(Datetime time) {
    m_syncConsumptionStartDatetime = time;
  }
  public Datetime getSyncConsumptionStartDatetime() {
    return m_syncConsumptionStartDatetime;
  }
  
  public void setSyncConsumptionEndDatetime(Datetime time) {
    m_syncConsumptionEndDatetime = time;
  }
  public Datetime getSyncConsumptionEndDatetime() {
    return m_syncConsumptionEndDatetime;
  }

  public void setTestSeriesSummary(TestSeriesSummary sum) {
    m_testSeriesSummary = sum;
  }
  public TestSeriesSummary getTestSeriesSummary() {
    return m_testSeriesSummary;
  }
  
  public void setTestCaseSummary(TestCaseSummary sum) {
    m_testCaseSummary = sum;
  }
  public TestCaseSummary getTestCaseSummary() {
    return m_testCaseSummary;
  }
  
  public void setTestStepSummary(TestStepSummary sum) {
    m_testStepSummary = sum;
  }
  public TestStepSummary getTestStepSummary() {
    return m_testStepSummary;
  }
  
  /**
  * Convenience method that returns an initialized child
  * RequestTimings object.  This object is
  * initialized with the layout managers and EnterpriseFields
  * objects currently associated to the BasicPerson object.
  * <P>
  * @return RequestTimings
  **/
  public RequestTimings newRequestTimings() {
    RequestTimings aRequestTimings = new RequestTimings();
    initializeChild( aRequestTimings );
    return aRequestTimings;
  }


  /**
  * Convenience method that returns the number of RequestTimings
  * objects in this BasicPerson.  i.e. - the 'size' of the List of
  * RequestTimings objects.
  * <P>
  * @return int
  **/
  public int getRequestTimingsLength() {
    if (m_requestTimings == null) { setRequestTimings( new java.util.ArrayList() ); }
    return m_requestTimings.size();
  }


  /**
  * Returns an initialized RequestTimings object
  * associated to this BasicPerson object from the List
  * of RequestTimings's at the index passed in.
  * If the List is null, this method will return a null.
  * If the index passed in is greater than or equal to the size of the
  * RequestTimings List, null will be returned.
  * <P>
  * @param int the index from which to get the object.
  * @return RequestTimings
  **/
  public RequestTimings getRequestTimings( int index ) {
    if (m_requestTimings == null) { return null; }
    if (index >= m_requestTimings.size()) { return null; }
    RequestTimings aRequestTimings = ( RequestTimings )m_requestTimings.get( index );
    if (aRequestTimings != null) {
      initializeChild( aRequestTimings );
    }
    return aRequestTimings;
  }


  /**
  * Returns the List of RequestTimings objects associated to this BasicPerson
  * If the List is null, this method will create a new List and return it.
  * <P>
  * @return java.util.List
  **/
  public java.util.List getRequestTimings() {
    if (m_requestTimings == null) { setRequestTimings( new java.util.ArrayList() ); }
    return m_requestTimings;
  }


  /**
  * Sets an RequestTimings object
  * associated to this BasicPerson object in its List
  * of RequestTimings's at the index passed in.
  * If the List is null, this method will create a new List and add the object passed in to the end.
  * If the index passed in is greater than or equal to the size of the
  * RequestTimings List, the object will be added to the end of the List.
  * <P>
  * @param int the index at which to add the object.
  * @param RequestTimings
  **/
  public void setRequestTimings( int index, RequestTimings aRequestTimings ) {
    if (m_requestTimings == null) {
      setRequestTimings( new java.util.ArrayList() );
      addRequestTimings( aRequestTimings );
      return;
    }
    if ( index >= m_requestTimings.size() ) {
      addRequestTimings( aRequestTimings );
      return;
    }
    if (aRequestTimings == null) {
      m_requestTimings.set( index, new RequestTimings());
    }
    else {
      m_requestTimings.set( index, aRequestTimings );
    }
  }


  /**
  * Sets the List of RequestTimings objects associated to this BasicPerson
  * If the List is null, this method will create a new List.
  * <P>
  * @param java.util.List
  **/
  public void setRequestTimings( java.util.List lRequestTimings ) {
    if (lRequestTimings == null) { lRequestTimings = new java.util.ArrayList(); }
    m_requestTimings = lRequestTimings;
  }


  /**
  * Convenience method that adds a RequestTimings to the end of the
  * List of RequestTimings objects.  If the RequestTimings List is null
  * this method will initialize it before adding the RequestTimings passed in.
  * <P>
  * @param RequestTimings
  **/
  public void addRequestTimings( RequestTimings aRequestTimings ) {
    if (m_requestTimings == null) { setRequestTimings( new java.util.ArrayList() ); }
    m_requestTimings.add( aRequestTimings );
  }
}
