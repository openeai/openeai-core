/*******************************************************************************
 $Source$
 $Revision: 691 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.testsuite;

import org.openeai.moa.*;
import org.openeai.config.*;

/**
 * A Class that wraps the TestStatus element as specified in
 * the OpenEAI TestSuite definition.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class Failure extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject {
  private java.util.List m_reason = null;

  public Failure() {
  }
  public Failure(MessageObjectConfig mConfig) throws EnterpriseObjectException {
    init(mConfig);
  }
  
  // The following methods (6 total) MUST be present for any repeating field...
  public int getReasonLength() {
    if (m_reason == null) {
      return 0;
    }
    return m_reason.size();
  }
  public String getReason(int index) {
    String aReason = (String)m_reason.get(index);
    return (String)m_reason.get(index);
  }
  public java.util.List getReason() {
    return m_reason;
  }
  public void setReason(int index, String aReason) {
    m_reason.set(index, aReason);
  }
  public void setReason(java.util.List aReason) {
    m_reason = aReason;
  }
  public void addReason(String aReason) throws EnterpriseFieldException {
    if (m_reason == null) {
      setReason(new java.util.ArrayList());
    }
    m_reason.add(aReason);
  }
}
