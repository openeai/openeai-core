/*******************************************************************************
 $Source$
 $Revision: 845 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.testsuite;

import org.openeai.moa.*;
import org.openeai.moa.objects.resources.*;
import org.openeai.config.*;

public class RequestTimings extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject, org.openeai.PubliclyCloneable {
  private String m_averageTime = null;    // required=false
  private String m_messageAction = null;
  private String m_numberOfRequests = null;
  private MaximumTime m_maximumTime = null;
  private MinimumTime m_minimumTime = null;

  public RequestTimings() {
  }
  public RequestTimings(MessageObjectConfig mConfig) throws EnterpriseObjectException {
    init(mConfig);
  }
  
  /**
  * Returns the AverageTime instance variable
  * associated to this RequestTimings object.
  * <P>
  * @return String
  **/
  public String getAverageTime() {
    return m_averageTime;
  }


  /**
  * Convenience method that returns the reverse translated
  * application value for the application name passed in.
  * <P>
  * @param String the name of the application for who the reverse
  * translations should be performed.
  * @return String
  * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
  * the current contents of the AverageTime instance variable
  * to the Application Value associated to the application name passed in.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public String getAverageTime( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "AverageTime", getAverageTime() );
  }


  /**
  * Sets the AverageTime instance variable
  * associated to this RequestTimings object to the value passed in.
  * Uses the EnterpriseFields object associated to the RequestTimings
  * to verify the data passed in meets the rules specified in
  * the Enterprise Objects document for the RequestTimings
  * If the data passed in does not meet those rules and it
  * cannot be Translated/Scrubbed to meet those rules, an
  * exception will be thrown.
  * <P>
  * @param String
  * @throws org.openeai.config.EnterpriseFieldException if the value passed in
  * does not meet the business rules for the field as specified
  * in the Enterprise Objects document for this object.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public void setAverageTime( String aAverageTime ) throws org.openeai.config.EnterpriseFieldException {
    m_averageTime = getEnterpriseValue( "AverageTime", aAverageTime );
  }

  /**
  * Returns the NumberOfRequests instance variable
  * associated to this RequestTimings object.
  * <P>
  * @return String
  **/
  public String getNumberOfRequests() {
    return m_numberOfRequests;
  }


  /**
  * Convenience method that returns the reverse translated
  * application value for the application name passed in.
  * <P>
  * @param String the name of the application for who the reverse
  * translations should be performed.
  * @return String
  * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
  * the current contents of the NumberOfRequests instance variable
  * to the Application Value associated to the application name passed in.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public String getNumberOfRequests( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "NumberOfRequests", getNumberOfRequests() );
  }


  /**
  * Sets the NumberOfRequests instance variable
  * associated to this RequestTimings object to the value passed in.
  * Uses the EnterpriseFields object associated to the RequestTimings
  * to verify the data passed in meets the rules specified in
  * the Enterprise Objects document for the RequestTimings
  * If the data passed in does not meet those rules and it
  * cannot be Translated/Scrubbed to meet those rules, an
  * exception will be thrown.
  * <P>
  * @param String
  * @throws org.openeai.config.EnterpriseFieldException if the value passed in
  * does not meet the business rules for the field as specified
  * in the Enterprise Objects document for this object.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public void setNumberOfRequests( String aNumberOfRequests ) throws org.openeai.config.EnterpriseFieldException {
    m_numberOfRequests = getEnterpriseValue( "NumberOfRequests", aNumberOfRequests );
  }

  /**
  * Returns the MessageAction instance variable
  * associated to this RequestTimings object.
  * <P>
  * @return String
  **/
  public String getMessageAction() {
    return m_messageAction;
  }


  /**
  * Convenience method that returns the reverse translated
  * application value for the application name passed in.
  * <P>
  * @param String the name of the application for who the reverse
  * translations should be performed.
  * @return String
  * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
  * the current contents of the MessageAction instance variable
  * to the Application Value associated to the application name passed in.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public String getMessageAction( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MessageAction", getMessageAction() );
  }


  /**
  * Sets the MessageAction instance variable
  * associated to this RequestTimings object to the value passed in.
  * Uses the EnterpriseFields object associated to the RequestTimings
  * to verify the data passed in meets the rules specified in
  * the Enterprise Objects document for the RequestTimings
  * If the data passed in does not meet those rules and it
  * cannot be Translated/Scrubbed to meet those rules, an
  * exception will be thrown.
  * <P>
  * @param String
  * @throws org.openeai.config.EnterpriseFieldException if the value passed in
  * does not meet the business rules for the field as specified
  * in the Enterprise Objects document for this object.
  * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
  **/
  public void setMessageAction( String aMessageAction ) throws org.openeai.config.EnterpriseFieldException {
    m_messageAction = getEnterpriseValue( "MessageAction", aMessageAction );
  }

  /**
  * Returns the MaximumTime instance variable
  * associated to this BasicPerson object.
  * <P>
  * @return MaximumTime
  **/
  public MaximumTime getMaximumTime() {
    return m_maximumTime;
  }


  /**
  * Sets the MaximumTime instance variable
  * associated to this BasicPerson object to the value passed in.
  * <P>
  * @param MaximumTime
  **/
  public void setMaximumTime( MaximumTime aMaximumTime ) {
    m_maximumTime = aMaximumTime;
  }


  /**
  * Convenience method that returns an initialized child
  * MaximumTime object.  This object is
  * initialized with the layout managers and EnterpriseFields
  * objects currently associated to the BasicPerson object.
  * <P>
  * @return MaximumTime
  **/
  public MaximumTime newMaximumTime() {
    MaximumTime aMaximumTime = new MaximumTime();
    initializeChild( aMaximumTime );
    return aMaximumTime;
  }

  /**
  * Returns the MinimumTime instance variable
  * associated to this BasicPerson object.
  * <P>
  * @return MinimumTime
  **/
  public MinimumTime getMinimumTime() {
    return m_minimumTime;
  }


  /**
  * Sets the MinimumTime instance variable
  * associated to this BasicPerson object to the value passed in.
  * <P>
  * @param MinimumTime
  **/
  public void setMinimumTime( MinimumTime aMinimumTime ) {
    m_minimumTime = aMinimumTime;
  }


  /**
  * Convenience method that returns an initialized child
  * MinimumTime object.  This object is
  * initialized with the layout managers and EnterpriseFields
  * objects currently associated to the BasicPerson object.
  * <P>
  * @return MinimumTime
  **/
  public MinimumTime newMinimumTime() {
    MinimumTime aMinimumTime = new MinimumTime();
    initializeChild( aMinimumTime );
    return aMinimumTime;
  }

}