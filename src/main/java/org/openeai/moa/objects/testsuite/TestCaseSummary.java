/*******************************************************************************
 $Source$
 $Revision: 844 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.testsuite;

import org.openeai.moa.*;
import org.openeai.config.*;

/**
 * A Class that wraps the TestSuiteSummary element as specified in
 * the OpenEAI TestSuiteSummary definition.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class TestCaseSummary extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject, org.openeai.PubliclyCloneable {
  private String m_totalCases = "";
  private String m_passedCases = "";
  private String m_failedCases = "";

  public TestCaseSummary() {
  }
  public TestCaseSummary(MessageObjectConfig mConfig) throws EnterpriseObjectException {
    init(mConfig);
  }

  public void setTotalCases(String total) throws EnterpriseFieldException {
    m_totalCases = getEnterpriseValue("TotalCases", total);
  }
  public String getTotalCases() {
    return m_totalCases;
  }
  
  public void setPassedCases(String passed) throws EnterpriseFieldException {
    m_passedCases = getEnterpriseValue("PassedCases", passed);
  }
  public String getPassedCases() {
    return m_passedCases;
  }
  
  public void setFailedCases(String failed) throws EnterpriseFieldException {
    m_failedCases = getEnterpriseValue("FailedCases", failed);
  }
  public String getFailedCases() {
    return m_failedCases;
  }
}
