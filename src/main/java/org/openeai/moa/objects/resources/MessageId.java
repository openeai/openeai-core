/*******************************************************************************
 $Source$
 $Revision: 885 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.resources;

import org.jdom.Element;

import org.openeai.moa.*;
import org.openeai.config.*;

/**
 * A Class that wraps the MessageId element as specified in
 * the OpenEAI protocol.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class MessageId
extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject {

  private String m_senderAppId = "";
  private String m_producerId = "";
  private String m_messageSeq = "";

  /**
   * Constructor
   */
  public MessageId() {
  }
  public MessageId(Element messageId) throws EnterpriseFieldException {
    if (messageId != null) {
      try {
        buildObjectFromInput(messageId);
      }
      catch (Exception e) {
        String errMsg = "Exception building MessageId object from input.  Exception: " + e.getMessage();
        logger.fatal(errMsg);
        throw new EnterpriseFieldException(errMsg, e);
      }
    }
  }

  public String getSenderAppId() {
    return m_senderAppId;
  }
  public void setSenderAppId(String appId) {
    if (appId == null) {
      m_senderAppId = "";
    }
    else {
      m_senderAppId = appId;
    }
  }

  public String getProducerId() {
    return m_producerId;
  }
  public void setProducerId(String id) {
    if (id == null) {
      m_producerId = "";
    }
    else {
      m_producerId = id;
    }
  }

  public String getMessageSeq() {
    return m_messageSeq;
  }
  public void setMessageSeq(String seq) {
    if (seq == null) {
      m_messageSeq = "";
    }
    else {
      m_messageSeq = seq;
    }
  }

  public String toString() {
    return getSenderAppId() + "-" + getProducerId() + "-" + getMessageSeq();
  }
}
