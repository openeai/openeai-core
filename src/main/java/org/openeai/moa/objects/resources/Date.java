/*******************************************************************************
 $Source$
 $Revision: 3980 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org).

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.moa.objects.resources;

import org.openeai.moa.*;
import org.openeai.config.*;

import java.util.*;
import java.text.*;

/**
 * A Class that wraps the Date element as specified in
 * the OpenEAI protocol.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class Date extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject, org.openeai.PubliclyCloneable {

  private String m_type;

  private String m_day = "";
  private String m_month = "";
  private String m_year = "";


  /**
   * Default constructor for a Date.
   */
  private Date() { }


  /** 
   * Constructor for a Date of a specific type.
   * @param type the type of the date (e.g., TerminationDate) 
   **/
  public Date( String type ) {
      if (type == null || type.trim().length() == 0) m_type = "Date";
      else                                           m_type = type;
  }


  /** 
   * Returns the type for this date.
   **/
  public String getType() {
      return m_type;
  }


  /**
   * Returns the day component of this date.
   **/
  public String getDay() {
      if (m_day == null || m_day.equals( "" )) {
          try {
              String value = getEnterpriseValue( "Day", "" );
              logger.debug( "Returning the enterprise value " + value + " for Day. " + getType() );
              return value;
          } catch (Exception e) {
              // We're just returning the a blank because
              // if we get here, it means the Day field
              // for this date object doesn't have a
              // translator/scrubber/formatter associated
              // with it.  Because of that, we have to
              // let whatever method is calling this method deal with
              // the exception because it may be an Optional Date object.
              return m_day;
          }
      } else {
          return m_day;
      }
  }


  /**
   * Sets the day component of this date.
   * @param day the new value of the day component for this date
   **/
  public void setDay( String day ) throws EnterpriseFieldException {
      m_day = getEnterpriseValue( "Day", day );
  }


  /**
   * Returns the month component of this date.
   **/
  public String getMonth() {
      if (m_month == null || m_month.equals( "" )) {
          try {
              String value = getEnterpriseValue( "Month", "" );
              logger.debug( "Returning the enterprise value " + value + " for Month. " + getType() );
              return value;
          } catch (Exception e) {
              // We're just returning the a blank because
              // if we get here, it means the Month field
              // for this date object doesn't have a
              // translator/scrubber/formatter associated
              // with it.  Because of that, we have to
              // let whatever method is calling this method deal with
              // the exception because it may be an Optional Date object.
              return m_month;
          }
      } else {
          return m_month;
      }
  }


  /**
   * Sets the month component of this date.
   * @param month the new month component for this date
   **/
  public void setMonth( String month ) throws EnterpriseFieldException {
      m_month = getEnterpriseValue( "Month", month );
  }


  /**
   * Returns the year component of this date.
   **/
  public String getYear() {
      if (m_year == null || m_year.equals( "" )) {
          try {
              String value = getEnterpriseValue( "Year", "" );
              logger.debug( "Returning the enterprise value " + value + " for Year. " + getType() );
              return value;
          } catch (Exception e) {
              // We're just returning the a blank because
              // if we get here, it means the Year field
              // for this date object doesn't have a
              // translator/scrubber/formatter associated
              // with it.  Because of that, we have to
              // let whatever method is calling this method deal with
              // the exception because it may be an Optional Date object.
              return m_year;
          }
      } else {
          return m_year;
      }
  }


  /**
   * Sets the year component of this date.
   * @param year the new year component for this date
   **/
  public void setYear( String year ) throws EnterpriseFieldException {
      m_year = getEnterpriseValue( "Year", year );
  }


  /**
   * Returns a string representation of this date.
   **/
  public String toString() {
      return new String( m_type + ": " + m_month + "/" + m_day + "/" + m_year );
  }


  /**
   * Returns the state of this date object as a new java.util.Date instance.
   **/
  public java.util.Date toDate() throws InvalidFormatException {
      if (getMonth() == null || getMonth().length() == 0) {
          return null;
      }

      java.util.Date d = new java.util.Date();
      try {
          d = new SimpleDateFormat( "MM/dd/yyyy" ).parse(getMonth() + "/" + getDay() + "/" +  getYear());
      } catch (Exception e) {
          throw new InvalidFormatException( e.getMessage(), e );
      }
      return d;
  }

	public final Object clone() throws CloneNotSupportedException {
    String dateType = getType();
    String className = getClass().getName();
//    XmlEnterpriseObject newDate = null;
    org.openeai.moa.objects.resources.Date newDate = null;
    Class[] parms = {String.class};
    try {
      java.lang.Class obj = java.lang.Class.forName(className);
      java.lang.reflect.Constructor c = obj.getConstructor(parms);
      Object[] o = {dateType};
//      newDate = (XmlEnterpriseObject)c.newInstance(o);
      newDate = (org.openeai.moa.objects.resources.Date)c.newInstance(o);

//      String saveMonth = getMonth();
//      String saveDay = getDay();
//      String saveYear = getYear();
      newDate.setMonth(getMonth());
      newDate.setDay(getDay());
      newDate.setYear(getYear());
      if (getEnterpriseFields() != null) {
        newDate.setEnterpriseFields((org.openeai.config.EnterpriseFields)getEnterpriseFields().clone());
      }
      newDate.setInputLayoutManager(getInputLayoutManager());
      newDate.setInputLayoutManagers(getInputLayoutManagers());
      newDate.setOutputLayoutManager(getOutputLayoutManager());
      newDate.setOutputLayoutManagers(getOutputLayoutManagers());
    }
    catch (Exception e) {
      logger.fatal("Error instantiating a " + className);
      logger.fatal(e.getMessage(), e);
      throw new CloneNotSupportedException(e.getMessage());
    }

    return newDate;
  }
//	/**
//	 * Convenience factory method to get a Openeai Date object out of a java.util.Date object, which is the reverse of toDate()
//	 * @param date
//	 * @return
//	 * @throws EnterpriseFieldException
//	 */
//	public static Date newInstance(java.util.Date date) throws EnterpriseFieldException{
//	    Date newDate=new Date();
//	    newDate.setYear(String.valueOf(date.getYear()));
//	    newDate.setMonth(String.valueOf(date.getMonth()));
//	    newDate.setDay(String.valueOf(date.getDay()));
//    return newDate;
//	}
	public void update(java.util.Date date) throws EnterpriseFieldException{
	    Calendar calendar = new GregorianCalendar();
	    calendar.setTime(date);
	    setYear(Integer.toString(calendar.get(Calendar.YEAR)));
	    setMonth(Integer.toString(calendar.get(Calendar.MONTH)+1));
	    setDay(Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
	}

	public int hashCode(){
	    return toString().hashCode();
	}
    public boolean equals(Object o){
        return equalsHelper(o);
    }	
    public boolean equals(Date o) {
        return equalsHelper(o);
    }
    
    public boolean equals(XmlEnterpriseObject o) {
        return equalsHelper(o);
    }
    private boolean equalsHelper(Object o){
        if(o!=null && o instanceof Date){
            Date d=(Date)o;
            return this.toString().equals(d.toString());
        }
        return false;
    }

}
