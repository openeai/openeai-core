/*******************************************************************************
 $Source$
 $Revision: 691 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.moa.objects.resources;

import org.openeai.moa.*;

/**
 * A Class that wraps the ControlAreaReply element as specified in
 * the OpenEAI protocol.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class ControlAreaReply 
extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject {

  private String m_messageCategory = "";
  private String m_messageObject = "";
  private String m_messageAction = "";
  private String m_messageRelease = "";
  private String m_messagePriority = "";
  private String m_messageType = "";
  private SourceInfo m_sourceInfo = new SourceInfo();
  private TargetInfo m_targetInfo = new TargetInfo();
  private Sender m_sender = new Sender();
  private Datetime m_datetime = null;
  private Result m_result = null;

  /**
   * Constructor
   */
  public ControlAreaReply() {
  }

  /** This one is a little "special" since it's the value we'll be using
      as the "timestamp" in the actual message we're sending.  Therefore, we
      always want to just get the current timestamp from the system at this point.
  */
  public Datetime getDatetime() {
    return new Datetime();
  }
  /** Not sure we should even have a setter method for the Datetime object?
  */

  public void setResult(Result result) {
    m_result = result;
  }
  public Result getResult() {
    return m_result;
  }

  public Sender getSender() {
    return m_sender;
  }
  public void setSender(Sender sender) {
    if (sender == null) {
      m_sender = new Sender();
    }
    else {
      m_sender = sender;
    }
  }

  public SourceInfo getSourceInfo() {
    return m_sourceInfo;
  }
  public void setSourceInfo(SourceInfo sourceInfo) {
    if (sourceInfo == null) {
      m_sourceInfo = new SourceInfo();
    }
    else {
      m_sourceInfo = sourceInfo;
    }
  }

  public TargetInfo getTargetInfo() {
    return m_targetInfo;
  }
  public void setTargetInfo(TargetInfo targetInfo) {
    if (targetInfo == null) {
      m_targetInfo = new TargetInfo();
    }
    else {
      m_targetInfo = targetInfo;
    }
  }

  public String getMessageCategory() {
    return m_messageCategory;
  }
  public void setMessageCategory(String category) {
    if (category == null) {
      m_messageCategory = "";
    }
    else {
      m_messageCategory = category;
    }
  }

  public String getMessageObject() {
    return m_messageObject;
  }
  public void setMessageObject(String object) {
    if (object == null) {
      m_messageObject = "";
    }
    else {
      m_messageObject = object;
    }
  }

  public String getMessageAction() {
    return m_messageAction;
  }
  public void setMessageAction(String action) {
    if (action == null) {
      m_messageAction = "";
    }
    else {
      m_messageAction = action;
    }
  }

  public String getMessageRelease() {
    return m_messageRelease;
  }
  public void setMessageRelease(String release) {
    if (release == null) {
      m_messageRelease = "";
    }
    else {
      m_messageRelease = release;
    }
  }

  public String getMessagePriority() {
    return m_messagePriority;
  }
  public void setMessagePriority(String priority) {
    if (priority == null) {
      m_messagePriority = "";
    }
    else {
      m_messagePriority = priority;
    }
  }

  public String getMessageType() {
    return m_messageType;
  }
  public void setMessageType(String type) {
    if (type == null) {
      m_messageType = "";
    }
    else {
      m_messageType = type;
    }
  }
}

