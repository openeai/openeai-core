/*******************************************************************************
 $Source$
 $Revision: 691 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.moa.objects.resources;

import org.openeai.moa.*;

/**
 * A Class that wraps the Result element as specified in
 * the OpenEAI protocol.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class Result
extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject {

  private String m_action = "";
  private String m_status = "";
  private java.util.List m_error = null;
  private ProcessedMessageId m_processedMessageId = new ProcessedMessageId();

  /**
   * Constructor
   */
  public Result() {
  }

  public int getErrorLength() {
    if (m_error == null) {
      return 0;
    }
    return m_error.size();
  }
  public java.util.List getError() {
    return m_error;
  }
  public Error getError(int index) {
    Error anError = (Error)m_error.get(index);
    if (anError == null) {
      return new Error();
    }
    return anError;
  }
  public void setError(java.util.List error) {
    m_error = error;
  }
  public void setError(int index, Error err) {
    if (err == null) {
      m_error.set(index, new Error());
    }
    else {
      m_error.set(index, err);
    }
  }
  public void addError(Error error) {
    if (m_error == null) {
      setError(new java.util.ArrayList());
    }
    m_error.add(error);
  }

  public void setAction(String act) {
    m_action = act;
  }
  public String getAction() {
    return m_action;
  }

  public void setStatus(String stat) {
    m_status = stat;
  }
  public String getStatus() {
    return m_status;
  }

  public ProcessedMessageId getProcessedMessageId() {
    return m_processedMessageId;
  }
  public void setProcessedMessageId(ProcessedMessageId msgId) {
    if (msgId == null) {
      m_processedMessageId =
      new ProcessedMessageId();
    }
    else {
      m_processedMessageId = msgId;
    }
  }
}
