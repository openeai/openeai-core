/*******************************************************************************
 $Source$
 $Revision: 3980 $
 *******************************************************************************/

/**********************************************************************
 This file is part of the OpenEAI Application Foundation or
 OpenEAI Message Object API created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org).

 Copyright (C) 2002 The OpenEAI Software Foundation

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to build commercial integration software or to implement
 integrations for your enterprise, visit http://www.OpenEai.org/licensing.
 ***********************************************************************/

package org.openeai.moa.objects.resources;

import org.jdom.Element;

import java.util.*;
import java.text.*;

import org.openeai.moa.*;
import org.openeai.config.*;
import org.openeai.layouts.EnterpriseLayoutException;

/**
 * A Class that wraps the Datetime element as specified in the OpenEAI protocol.
 * <P>
 * 
 * @author Tod Jackson (tod@openeai.org)
 * @author Steve Wheat (steve@openeai.org)
 * @author George Wangt (george.wang@emory.edu)
 * @version 3.0 - 28 January 2003
 */
public class Datetime extends XmlEnterpriseObjectImpl implements XmlEnterpriseObject {

    private String m_type;

    private String m_year = "";
    private String m_month = "";
    private String m_day = "";
    private String m_hour = "";
    private String m_minute = "";
    private String m_second = "";
    private String m_subSecond = "";
    private String m_timezone = "";

    /**
     * Constructor - use the element passed in to build the Datetime object.
     */
    public Datetime(Element eDatetime) throws EnterpriseLayoutException {
        m_type = eDatetime.getName();
        buildObjectFromInput(eDatetime);
    }

    /**
     * Constructor - get the current system time and populate this object
     * appropriately.
     */
    public Datetime() {
        m_type = "Datetime";
        update(new GregorianCalendar());
    }

    /**
     * Constructor - use the Date passed in to populate this object
     * appropriately.
     */
    public Datetime(java.util.Date aDate) {
        m_type = "Datetime";
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        update(calendar);
    }

    /**
     * Constructor - set the Datetime 'type' to be that which is passed in and
     * then instantiate a normal Datetime object. This is needed because the
     * Datetime element as specified in the OpenEAI protocol is a defined
     * "domain" just like the Date element. Therefore, it may be necessary to
     * instantiate a Datetime object of a certain type depending on the element
     * that corresponds to this Datetime object in a message definition. This is
     * to avoid having to maintain multiple versions of the Datetime object, one
     * for each element of type Datetime. Example, the StartDatetime element is
     * a Datetime but we don't have to maintain a StartDatetime Java object.
     * Developers just instantiate a Datetime object and pass the
     * "StartDatetime" type to this constructor.
     */
    public Datetime(String type) {
        if (type == null || type.trim().length() == 0) {
            m_type = "Datetime";
        } else {
            m_type = type;
        }
        update(new GregorianCalendar());
    }

    private void setTimezoneToDefault() {
        setTimezone(TimeZone.getDefault().getID());
    }

    /**
     * Constructor - set the Datetime 'type' to be that which is passed in (for
     * the same reasons as outlined in the String-only constructor) and then
     * instantiate a Datetime object initial values indicative of the
     * java.util.Date passed in. This is needed, because one frequently wants to
     * instatiate a date with some other value than the current system time.
     */
    public Datetime(String type, java.util.Date date) {
        if (type == null || type.trim().length() == 0) {
            m_type = "Datetime";
        } else {
            m_type = type;
        }

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        update(calendar);
    }

    /**
     * Constructor - set the Datetime 'type' to be that which is passed in (for
     * the same reasons as outlined in the String-only constructor) and then
     * instantiate a Datetime object initial values indicative of the long time
     * in milliseconds passed in. This is needed, because one frequently wants
     * to instatiate a date with some other value than the current system time.
     */
    public Datetime(String type, long time) {
        if (type == null || type.trim().length() == 0) {
            m_type = "Datetime";
        } else {
            m_type = type;
        }

        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(time);
        update(calendar);
    }

    /**
     * Returns the type of Datetime element this is. The type is defined during
     * construction.
     */
    public String getType() {
        return m_type;
    }

    public String getYear() {
        return m_year;
    }

    public void setYear(String year) {
        if (year == null) {
            m_year = "";
        } else {
            m_year = year;
        }
    }

    public String getMonth() {
        return m_month;
    }

    public void setMonth(String month) {
        if (month == null) {
            m_month = "";
        } else {
            m_month = month;
        }
    }

    public String getDay() {
        return m_day;
    }

    public void setDay(String day) {
        if (day == null) {
            m_day = "";
        } else {
            m_day = day;
        }
    }

    public String getHour() {
        return m_hour;
    }

    public void setHour(String hour) {
        if (hour == null) {
            m_hour = "";
        } else {
            m_hour = hour;
        }
    }

    public String getMinute() {
        return m_minute;
    }

    public void setMinute(String minute) {
        if (minute == null) {
            m_minute = minute;
        } else {
            m_minute = minute;
        }
    }

    public String getSecond() {
        return m_second;
    }

    public void setSecond(String second) {
        if (second == null) {
            m_second = "";
        } else {
            m_second = second;
        }
    }

    public String getSubSecond() {
        return m_subSecond;
    }

    public void setSubSecond(String subSecond) {
        if (subSecond == null) {
            m_subSecond = "";
        } else {
            m_subSecond = subSecond;
        }
    }

    public String getTimezone() {
        return m_timezone;
    }

    public void setTimezone(String timezone) {
        if (timezone == null) {
            m_timezone = "";
        } else {
            m_timezone = timezone;
        }
    }

    /**
     * Creates a String object out of the current contents of the object and
     * returns a concatenated string representing that information.
     * <P>
     * 
     * @return String the concatenated string created from the contents of this
     *         object (YY-MM-DD HH:MM:SS:SSS GMT).
     */
    public String toString() {
        return getYear() + "-" + getMonth() + "-" + getDay() + " " + getHour() + ":" + getMinute() + ":" + getSecond()
                + ":" + getSubSecond() + " " + getTimezone();
    }

    public final Object clone() throws CloneNotSupportedException {
        String dateType = getType();
        String className = getClass().getName();
        org.openeai.moa.objects.resources.Datetime newDatetime = null;
        Class[] parms = { String.class };
        try {
            java.lang.Class obj = java.lang.Class.forName(className);
            java.lang.reflect.Constructor c = obj.getConstructor(parms);
            Object[] o = { dateType };
            newDatetime = (org.openeai.moa.objects.resources.Datetime) c.newInstance(o);

            newDatetime.setMonth(getMonth());
            newDatetime.setDay(getDay());
            newDatetime.setYear(getYear());
            newDatetime.setHour(getHour());
            newDatetime.setMinute(getMinute());
            newDatetime.setSecond(getSecond());
            newDatetime.setSubSecond(getSubSecond());
            newDatetime.setTimezone(getTimezone());
            if (getEnterpriseFields() != null) {
                newDatetime.setEnterpriseFields((org.openeai.config.EnterpriseFields) getEnterpriseFields().clone());
            }
            newDatetime.setInputLayoutManager(getInputLayoutManager());
            newDatetime.setInputLayoutManagers(getInputLayoutManagers());
            newDatetime.setOutputLayoutManager(getOutputLayoutManager());
            newDatetime.setOutputLayoutManagers(getOutputLayoutManagers());
        } catch (Exception e) {
            logger.fatal("Error instantiating a " + className);
            logger.fatal(e.getMessage(), e);
            throw new CloneNotSupportedException(e.getMessage());
        }

        return newDatetime;
    }

    public Calendar toCalendar() {
        Calendar c = new GregorianCalendar(TimeZone.getTimeZone(getTimezone()));
        if(m_month==null||m_month.length()==0)
            return null;
        c.set(Integer.parseInt(m_year), Integer.parseInt(m_month) - 1, Integer.parseInt(m_day),
                Integer.parseInt(m_hour), Integer.parseInt(m_minute), Integer.parseInt(m_second));
        if (m_subSecond != null && m_subSecond.length() > 0)
            c.set(Calendar.MILLISECOND, Integer.parseInt(m_subSecond));
        c.getTime();
        return c;
    }

    /**
     * be careful about the input of the calendar.  Here is the example from java doc:
     * // create a GregorianCalendar with the Pacific Daylight time zone // and
     * the current date and time 
     * Calendar calendar = new GregorianCalendar(pdt);
     * Date trialTime = new Date(); 
     * calendar.setTime(trialTime);
     * 
     * Please also see com.openii.openeai.toolkit.rdbms.persistence.hibernate.TimeZoneDatetimeType.nullSafeGet(...) and 
     * org.openeai.utils.moa.mapper.DozerCalendarConverter
     * 
     * 
     * @param calendar
     */
    public void update(Calendar calendar) {
        setYear(Integer.toString(calendar.get(Calendar.YEAR)));
        setMonth(Integer.toString(calendar.get(Calendar.MONTH) + 1));
        setDay(Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
        setHour(Integer.toString(calendar.get(Calendar.HOUR_OF_DAY)));
        setMinute(Integer.toString(calendar.get(Calendar.MINUTE)));
        setSecond(Integer.toString(calendar.get(Calendar.SECOND)));
        setSubSecond(Integer.toString(calendar.get(Calendar.MILLISECOND)));
        setTimezone(calendar.getTimeZone().getID());
    }


    public int hashCode() {
        return (getType() + toString()).hashCode();
    }
    @Override
    public boolean equals(Object o){
        return equalsHelper(o);
    }   

    public boolean equals(Datetime o) {
        return equalsHelper(o);
    }
    public boolean equals(XmlEnterpriseObject o) {
        return equalsHelper(o);
    }
    private boolean equalsHelper(Object o){
        if (o != null && o instanceof Datetime) {
            Datetime dt = (Datetime) o;
            return this.toString().equals(dt.toString()) && this.getType().equals(dt.getType());
        }
        return false;
    }

}
