/******************************************
 Copyright 2010 Emory University.  All Rights Reserved.
 *******************************************/

package org.openeai.moa.jmsobjects.coremessaging.v1_0;


/**

 * A class representing RouterData information.
 * <P>
 * This file was automatically generated by the OpenEAI MOA Generation Application on Sun Dec 22 22:25:18 EST 2019
 * <P>
 * Note: manual changes to this file will be overwritten any time these
 * files are re-generated.  Any necessary manual modifications should be made carefully.
 * <P>
 * @version     v1_0 - Sun Dec 22 22:25:19 EST 2019
 **/
public class RouterData
        extends org.openeai.moa.ActionableEnterpriseObjectBase
        implements org.openeai.moa.XmlEnterpriseObject,
        org.openeai.PubliclyCloneable,
        java.io.Serializable,
        org.openeai.moa.ActionableEnterpriseObject {

  // instance variables
  private String m_rowId = null;    // required=false
  private String m_senderAppId = null;    // required=false
  private String m_msgId = null;    // required=false
  private String m_targetMessageId = null;    // required=false
  private String m_targetAppId = null;    // required=false
  private String m_msgCategory = null;    // required=false
  private String m_msgObject = null;    // required=false
  private String m_msgAction = null;    // required=false
  private String m_msgType = null;    // required=false
  private String m_msgRelease = null;    // required=false
  private String m_createUser = null;    // required=true
  private org.openeai.moa.objects.resources.Date m_createDate = null;    // required=true
  private String m_modUser = null;    // required=false
  private org.openeai.moa.objects.resources.Date m_modDate = null;    // required=false

  /**
   * Constructor(s).
   **/
  public RouterData()  { }
  public RouterData(org.openeai.config.MessageObjectConfig mConfig) throws org.openeai.moa.EnterpriseObjectException { init(mConfig); }

  // getter/setter methods

  /**
   * Returns the RowId instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getRowId() {
    return m_rowId;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the RowId instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getRowId( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "RowId", getRowId() );
  }


  /**
   * Sets the RowId instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aRowId
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setRowId( String aRowId ) throws org.openeai.config.EnterpriseFieldException {
    m_rowId = getEnterpriseValue( "RowId", aRowId );
  }


  /**
   * Returns the SenderAppId instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getSenderAppId() {
    return m_senderAppId;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the SenderAppId instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getSenderAppId( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "SenderAppId", getSenderAppId() );
  }


  /**
   * Sets the SenderAppId instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aSenderAppId
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setSenderAppId( String aSenderAppId ) throws org.openeai.config.EnterpriseFieldException {
    m_senderAppId = getEnterpriseValue( "SenderAppId", aSenderAppId );
  }


  /**
   * Returns the MsgId instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getMsgId() {
    return m_msgId;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the MsgId instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getMsgId( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MsgId", getMsgId() );
  }


  /**
   * Sets the MsgId instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aMsgId
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setMsgId( String aMsgId ) throws org.openeai.config.EnterpriseFieldException {
    m_msgId = getEnterpriseValue( "MsgId", aMsgId );
  }


  /**
   * Returns the TargetMessageId instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getTargetMessageId() {
    return m_targetMessageId;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the TargetMessageId instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getTargetMessageId( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "TargetMessageId", getTargetMessageId() );
  }


  /**
   * Sets the TargetMessageId instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aTargetMessageId
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setTargetMessageId( String aTargetMessageId ) throws org.openeai.config.EnterpriseFieldException {
    m_targetMessageId = getEnterpriseValue( "TargetMessageId", aTargetMessageId );
  }


  /**
   * Returns the TargetAppId instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getTargetAppId() {
    return m_targetAppId;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the TargetAppId instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getTargetAppId( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "TargetAppId", getTargetAppId() );
  }


  /**
   * Sets the TargetAppId instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aTargetAppId
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setTargetAppId( String aTargetAppId ) throws org.openeai.config.EnterpriseFieldException {
    m_targetAppId = getEnterpriseValue( "TargetAppId", aTargetAppId );
  }


  /**
   * Returns the MsgCategory instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getMsgCategory() {
    return m_msgCategory;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the MsgCategory instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getMsgCategory( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MsgCategory", getMsgCategory() );
  }


  /**
   * Sets the MsgCategory instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aMsgCategory
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setMsgCategory( String aMsgCategory ) throws org.openeai.config.EnterpriseFieldException {
    m_msgCategory = getEnterpriseValue( "MsgCategory", aMsgCategory );
  }


  /**
   * Returns the MsgObject instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getMsgObject() {
    return m_msgObject;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the MsgObject instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getMsgObject( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MsgObject", getMsgObject() );
  }


  /**
   * Sets the MsgObject instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aMsgObject
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setMsgObject( String aMsgObject ) throws org.openeai.config.EnterpriseFieldException {
    m_msgObject = getEnterpriseValue( "MsgObject", aMsgObject );
  }


  /**
   * Returns the MsgAction instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getMsgAction() {
    return m_msgAction;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the MsgAction instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getMsgAction( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MsgAction", getMsgAction() );
  }


  /**
   * Sets the MsgAction instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aMsgAction
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setMsgAction( String aMsgAction ) throws org.openeai.config.EnterpriseFieldException {
    m_msgAction = getEnterpriseValue( "MsgAction", aMsgAction );
  }


  /**
   * Returns the MsgType instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getMsgType() {
    return m_msgType;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the MsgType instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getMsgType( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MsgType", getMsgType() );
  }


  /**
   * Sets the MsgType instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aMsgType
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setMsgType( String aMsgType ) throws org.openeai.config.EnterpriseFieldException {
    m_msgType = getEnterpriseValue( "MsgType", aMsgType );
  }


  /**
   * Returns the MsgRelease instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getMsgRelease() {
    return m_msgRelease;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the MsgRelease instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getMsgRelease( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "MsgRelease", getMsgRelease() );
  }


  /**
   * Sets the MsgRelease instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aMsgRelease
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setMsgRelease( String aMsgRelease ) throws org.openeai.config.EnterpriseFieldException {
    m_msgRelease = getEnterpriseValue( "MsgRelease", aMsgRelease );
  }


  /**
   * Returns the CreateUser instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getCreateUser() {
    return m_createUser;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the CreateUser instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getCreateUser( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "CreateUser", getCreateUser() );
  }


  /**
   * Sets the CreateUser instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aCreateUser
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setCreateUser( String aCreateUser ) throws org.openeai.config.EnterpriseFieldException {
    m_createUser = getEnterpriseValue( "CreateUser", aCreateUser );
  }


  /**
   * Returns the CreateDate instance variable
   * associated to this RouterData object.
   * <P>
   * @return org.openeai.moa.objects.resources.v1_0.Date
   **/
  public org.openeai.moa.objects.resources.Date getCreateDate() {
    if (m_createDate != null) {
      initializeChild( m_createDate );
    }
    return m_createDate;
  }


  public void setCreateDate(org.openeai.moa.objects.resources.Date pDate) {
    if(pDate==null) {m_createDate= null;return;}
    try {
      if(m_createDate==null) m_createDate=newCreateDate();
      m_createDate.update(pDate.toDate());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  /**
   * Convenience method that returns an initialized child
   * Date object for the CreateDate field.  This object is
   * initialized with the layout managers and EnterpriseFields
   * objects currently associated to the RouterData object.
   * <P>
   * @return Date
   **/
  public org.openeai.moa.objects.resources.Date newCreateDate() {
    org.openeai.moa.objects.resources.Date aCreateDate = new org.openeai.moa.objects.resources.Date("CreateDate");
    initializeChild( aCreateDate );
    return aCreateDate;
  }


  /**
   * Returns the ModUser instance variable
   * associated to this RouterData object.
   * <P>
   * @return String
   **/
  public String getModUser() {
    return m_modUser;
  }


  /**
   * Convenience method that returns the reverse translated
   * application value for the application name passed in.
   * <P>
   * @param appName the name of the application for who the reverse
   * translations should be performed.
   * @return String
   * @throws org.openeai.config.EnterpriseFieldException if errors occur converting
   * the current contents of the ModUser instance variable
   * to the Application Value associated to the application name passed in.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public String getModUser( String appName ) throws org.openeai.config.EnterpriseFieldException {
    return getApplicationValue( appName, "ModUser", getModUser() );
  }


  /**
   * Sets the ModUser instance variable
   * associated to this RouterData object to the value passed in.
   * Uses the EnterpriseFields object associated to the RouterData
   * to verify the data passed in meets the rules specified in
   * the Enterprise Objects document for the RouterData
   * If the data passed in does not meet those rules and it
   * cannot be Translated/Scrubbed to meet those rules, an
   * exception will be thrown.
   * <P>
   * @param aModUser
   * @throws org.openeai.config.EnterpriseFieldException if the value passed in
   * does not meet the business rules for the field as specified
   * in the Enterprise Objects document for this object.
   * @see org.openeai.config.EnterpriseFields org.openeai.config.EnterpriseFields
   **/
  public void setModUser( String aModUser ) throws org.openeai.config.EnterpriseFieldException {
    m_modUser = getEnterpriseValue( "ModUser", aModUser );
  }


  /**
   * Returns the ModDate instance variable
   * associated to this RouterData object.
   * <P>
   * @return org.openeai.moa.objects.resources.v1_0.Date
   **/
  public org.openeai.moa.objects.resources.Date getModDate() {
    if (m_modDate != null) {
      initializeChild( m_modDate );
    }
    return m_modDate;
  }


  public void setModDate(org.openeai.moa.objects.resources.Date pDate) {
    if(pDate==null) {m_modDate= null;return;}
    try {
      if(m_modDate==null) m_modDate=newModDate();
      m_modDate.update(pDate.toDate());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  /**
   * Convenience method that returns an initialized child
   * Date object for the ModDate field.  This object is
   * initialized with the layout managers and EnterpriseFields
   * objects currently associated to the RouterData object.
   * <P>
   * @return Date
   **/
  public org.openeai.moa.objects.resources.Date newModDate() {
    org.openeai.moa.objects.resources.Date aModDate = new org.openeai.moa.objects.resources.Date("ModDate");
    initializeChild( aModDate );
    return aModDate;
  }


  /**
   * Since the RouterData does not support the Generate action, this builGenerateMessage implementation
   * simply throws an EnterpriseObjectGenerateException if anyone attempts to call the RouterData's generate method.
   * <P>
   * NOTE:  All ActionableEnterpriseObjects must provide an implementation for this method since it is defined as abstract in ActionableEnterpriseObjectBase.
   * @param genDoc org.jdom.Document the 'primed' generate document associated to this RouterData
   * @param keyObject org.openeai.moa.XmlEnterpriseObject the key object used to build the generate message.
   * @throws org.openeai.moa.EnterpriseObjectGenerateException
   * @throws org.jdom.JDOMException
   **/
  protected void buildGenerateMessage( org.jdom.Document genDoc, org.openeai.moa.XmlEnterpriseObject keyObject )
          throws org.jdom.JDOMException, org.openeai.moa.EnterpriseObjectGenerateException {
    throw new org.openeai.moa.EnterpriseObjectGenerateException( "The RouterData object does not support the Generate action!");
  }


  /**
   * Since the Query-Request message defined for the RouterData specifies a 'query object' other than LightweightPerson
   * it must override the default buildQueryMessage method it inherits from ActionableEnterpriseObjectBase
   * This method will take the primed query document and the key object passed in and build the
   * RouterData-Query-Request message that will be enqueued when the query method is called.
   * <P>
   * @param doc the 'primed' update document associated to this RouterData
   * @param keyObject the key object used to build the query message.
   * @throws org.openeai.moa.EnterpriseObjectQueryException
   * @throws org.jdom.JDOMException
   **/
  protected void buildQueryMessage(org.jdom.Document doc, org.openeai.moa.XmlEnterpriseObject keyObject)
          throws org.jdom.JDOMException, org.openeai.moa.EnterpriseObjectQueryException {

    org.jdom.Element controlArea = getControlArea(doc.getRootElement());
    org.jdom.Element dataArea = doc.getRootElement().getChild("DataArea" );
    String className = keyObject.getClass().getName();
    String keyObjectName = className.substring(className.lastIndexOf('.') + 1);
    org.jdom.Element eKeyObject = null;
    try {
      if (keyObject.getEnterpriseFields().getFieldsForObject( keyObjectName ) == null ||
              keyObject.getEnterpriseFields().getFieldsForObject( keyObjectName ).size() == 0) {

        keyObject.setInputLayoutManager( getInputLayoutManager( "xml" ) );
        keyObject.setOutputLayoutManager( getOutputLayoutManager( "xml" ) );
        keyObject.setEnterpriseFields( getEnterpriseFields() );
      }
      eKeyObject = ( org.jdom.Element ) keyObject.buildOutputFromObject();
    }
    catch (Exception e) {
      String errMessage = "[RouterData] Exception building query document.  Exception: " + e.getMessage();
      logger.fatal( errMessage );
      throw new org.openeai.moa.EnterpriseObjectQueryException( errMessage, e );
    }

    java.util.List children = dataArea.getChildren();
    for (int i=0; i<children.size(); i++) {
      org.jdom.Element eChild = (org.jdom.Element)children.get(i);
      String childName = eChild.getName();
      dataArea.removeChild(childName);
    }

    dataArea.addContent( eKeyObject );

    try {
      setControlArea( controlArea );
    }
    catch (Exception e) {
      String errMessage = "[RouterData] Exception setting ControlArea of query document.  Exception: " + e.getMessage();
      logger.fatal( errMessage );
      throw new org.openeai.moa.EnterpriseObjectQueryException( errMessage, e );
    }
  }


}
