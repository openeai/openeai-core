/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.layouts;

import org.jdom.Element;
import org.jdom.Document;
import org.openeai.moa.XmlEnterpriseObject;

/**
 * The interface that specifies what methods all EnterpriseLayoutManagers must implement.
 * The implementations of this interface (layout managers) are associated to a message
 * object through an application's configuration document when that object is initialized
 * by AppConfig.  A message object may have multiple layout managers associated to it and
 * they can be switched between at runtime.  It is these layout managers that provide
 * the logic to build the object from different types of inputs as well as serialize
 * the object to different types of outputs.
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see XmlEnterpriseObject
 */
public interface EnterpriseLayoutManager {
	/**
   * Method used to initialize the LayoutMangager implementation with information found in the EnterpriseObjects
   * document associated to the object being initialized.  The layoutmanager implementation retrieves information from
   * the EnterpriseObjects document to tell it how the object should be built from an input and serialized to an
   * output.
   *<P>
   * @param layoutManagerName String, the name of the layout associated to the object that should be located and used.  This layout
   * specifies the details regarding the type of layout being used.  The layout manager implementation will use this
   * information to determine how to build and serialize the object using the layout manager implementation.  The contents
   * of this layout may include a specific layout definition or it may simply be a reference to another Document that 
   * contains that information.  That is up to the layout manager implementation.
   * @param layoutDoc Document, the JDOM Document that contains the layout information (the EnterpriseObject Document)
	 *
   * @throws EnterpriseLayoutException if any errors occur initializing the layout manager implementation.
	 */
  public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException;
	/**
   * Method used to build an object from an input.  An input can be any type of object and it is up to the 
   * layout manager implementation to specify what allowable inputs are.  For example, the OpenEAI software foundation
   * includes an XmlLayout manager that all objects use to build themselves from and serialize themselves to XML.  This
   * is one implementation of an EnterpriseLayoutManager that expects the input passed to this method to be an XML Element (JDOM Element)
   * Other examples, are ExtractLayouts that expect the input to be a String and so on.  Those Layout Manager implementations
   * then take the input given to them and build the Java object from that input.  How they do this is completely up to the implementors
   * of the layout manager.
   *<P>
   * All XmlEnterpriseObject objects inherit a buildObjectFromInput method that makes a call to the Layout manager implementation
   * as specified in the configuration document for that object.
   *<P>
   * @param input Object, the input object that should be used to build the object from.
   * @param anXmlEnterpriseObject XmlEnterpriseObject, the XmlEnterpriseObject that is to be built from the input.
	 *
   * @throws EnterpriseLayoutException if any errors occur building the object from the input.
	 */
  public void buildObjectFromInput(Object input, XmlEnterpriseObject anXmlEnterpriseObject) throws EnterpriseLayoutException;

	/**
   * Method used to serialize an object to some output.  An output can be any type of object and it is up to the 
   * layout manager implementation to specify what output will be.  It is also up to the application developer using that 
   * layout manager to cast the returned serialized version of the object to the appropriate type of object that they're expecting.
   * For example, the OpenEAI software foundation includes an XmlLayout manager that all objects use to build themselves 
   * from and serialize themselves to XML.  This is one implementation of an EnterpriseLayoutManager that expects 
   * the output returned from this method to be an XML Element (JDOM Element).
   *<P>
   * All XmlEnterpriseObject objects inherit a buildOutputFromObject method that makes a call to the Layout manager implementation
   * as specified in the configuration document for that object.
   *<P>
   * @param anXmlEnterpriseObject XmlEnterpriseObject, the XmlEnterpriseObject that is to be serialized.
   * @return Object the serialized representation of the object.
	 *
   * @throws EnterpriseLayoutException if any errors occur building the object from the input.
	 */
  public Object buildOutputFromObject(XmlEnterpriseObject anXmlEnterpriseObject) throws EnterpriseLayoutException;

	/**
   * Method used to serialize an object to some output for a particular application.  An output can be any type of object and it is up to the 
   * layout manager implementation to specify what output will be.  It is also up to the application developer using that 
   * layout manager to cast the returned serialized version of the object to the appropriate type of object that they're expecting.
   * For example, the OpenEAI software foundation includes an XmlLayout manager that all objects use to build themselves 
   * from and serialize themselves to XML.  This is one implementation of an EnterpriseLayoutManager that expects 
   * the output returned from this method to be an XML Element (JDOM Element).
   *<P>
   * This method differs from the buildOutputFromObject(XmlEnterpriseObject) method in that it takes the current 'Enterprise Values'
   * contained within the object and converts them to 'Application Values' that are appropriate for the application name passed in.
   * The rules for these reverse translations are found in the EnterpriseObjects document associated to the object in the application's
   * configuration document.
   *<P>
   * All XmlEnterpriseObject objects inherit a buildOutputFromObject(String appName) method that makes a call to the Layout manager implementation
   * as specified in the configuration document for that object.
   *<P>
   * @param anXmlEnterpriseObject XmlEnterpriseObject, the XmlEnterpriseObject that is to be serialized.
   * @param appName the name of the application as specified in the translations section of the Enterprise Object document 
   * whos application values should be used in the conversion from Enterprise Values to Application Values.
   * @return Object the serialized representation of the object.
	 *
   * @throws EnterpriseLayoutException if any errors occur building the object from the input.
	 */
  public Object buildOutputFromObject(XmlEnterpriseObject anXmlEnterpriseObject, String appName) throws EnterpriseLayoutException;

  /**
   * Sets the name associated to the layout manager.
   *<P>
   * @param name String the layout manager name.
   */
  public void setLayoutManagerName(String name);
  /**
   * Returns the name associated to the layout manager.
   *<P>
   * @return String the layout manager name.
   */
	public String getLayoutManagerName();
  /**
   * Returns the layout manager's root element from the EnterpriseObjects document
   * associated to the MessageObject that was configured.
   *<P>
   * @return Element
   */
	public Element getLayoutRoot(); 
  /**
   * Sets the EnterpriseObjects document uri associated to this EnterpriseFields object.
   * This document contains the rules that will be use to build the EnterpriseFields object.
   *
   * @param uri String URI to the EnterpriseObjects document.  Can be file system or web uri.
   */
	public void setEnterpriseObjectsUri(String uri);
  /**
   * Returns the EnterpriseObjects document uri associated to this EnterpriseFields object.
   * This document contains the rules that will be use to build the EnterpriseFields object.
   *
   * @return String URI to the EnterpriseObjects document.  Can be file system or web uri.
   */
	public String getEnterpriseObjectsUri();
}

 
