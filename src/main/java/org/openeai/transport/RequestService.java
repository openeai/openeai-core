package org.openeai.transport;

import org.openeai.moa.*;
import org.jdom.Document;

public interface RequestService  {
	/**
	 * Query message production.  Builds an XML document (using the primed query document
	 * as a "template") out of the current contents of the keyObject passed in.
	 * While building the XML Query message, it will validate contents of the keyObject checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param keyObject XmlEnterpriseObject to use as retreival arguments in the Query.
   * The contents of this object are inserted into the Query document prior to
   * sending the query request.  Under normal conditions, this object is
   * actually a LightweightPerson object since that's the most common query object used.
	 *<P>
	 * NOTE:  This method should be over-ridden by lower level classes if the keyObject
	 * isn't a LightweightPerson.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the update-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  java.util.List this is a list of XmlEnterpriseObject objects
   * (like BasicPerson, BasicEmployee etc.) provided by the authoritative source
   * (the application consuming the query request).
	 *<P>
	 * @throws EnterpriseObjectQueryException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public java.util.List query(XmlEnterpriseObject keyObject, ActionableEnterpriseObject theObject)
	throws TransportException;
  
	/**
	 * Create message production.  Builds an XML document (using the primed create document
	 * as a "template") out of the current contents of the object.  While building the
	 * XML Create document, it will validate contents of the object checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the create-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  XmlEnterpriseObject a generic response
	 * which will indicate success or failure.  If failure, any error information will
	 * be included in that result.  The object returned is actually a
	 * org.openeai.moa.objects.Result object.
	 *<P>
	 * @throws EnterpriseObjectCreateException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public XmlEnterpriseObject create(ActionableEnterpriseObject theObject)
	throws TransportException;
  
	/**
	 * Delete message production.  Builds an XML document (using the primed delete document
	 * as a "template") out of the current contents of the object.  While building the
	 * XML Delete document, it will validate contents of the object checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param deleteAction String the delete action ('delete' or 'purge')
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the delete-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  XmlEnterpriseObject a generic response
	 * which will indicate success or failure.  If failure, any error information will
	 * be included in that result.  The object returned is actually a
	 * org.openeai.moa.objects.Result object.
	 *<P>
	 * @throws EnterpriseObjectDeleteException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public XmlEnterpriseObject delete(String deleteAction, ActionableEnterpriseObject theObject)
	throws TransportException;
  
	/**
	 * Generate message production.  Builds an XML document (using the primed generate document
	 * as a "template") out of the current contents of the object.  While building the
	 * XML Generate document, it will validate contents of the object checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the update-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @param keyObject XmlEnterpriseObject to use as generation "seed" data
   * (like an UnknownPerson for an InstitutionalIdentity-Generate-Request).
	 *<P>
	 * @return  java.util.List this is a list of XmlEnterpriseObject objects generated by the authoritative source
	 * (the application consuming the request to generate).  Typically, this will
	 * be only one object (like an InstitutionalIdentity).
	 *<P>
	 * @throws EnterpriseObjectGenerateException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public java.util.List generate(XmlEnterpriseObject keyObject, ActionableEnterpriseObject theObject)
	throws TransportException;
  
	/**
	 * Update message production.  Builds an XML document (using the primed update document
	 * as a "template") out of the current contents of the object (this).  It will use the
	 * current contents of the object as the NewData portion of the message and it will
	 * use this object's "baseline" object as the Baseline portion of the message.  The "baseline"
	 * object is set when this object was "queried" for previously.
	 * While building the XML Update message, it will validate contents checking
	 * for any malformed, missing or invalid fields.  Field data values are also
	 * checked for validitity against the EnterpriseObjects document.  Uses the
   * producer passed in to send the XML document as a JMS request to the queue
   * connected to by the producer which then processes the response checking
   * for any errors in the response.
	 *<P>
	 * @param producer PointToPointProducer a pre-configured and started PointToPointProducer
   * which will be used to send the update-request message to the appropriate destination
   * and return the reply to this method (via the PointToPointProducer.produceRequest method).
	 *<P>
	 * @return  XmlEnterpriseObject (Result) this will indicate the success or failure
	 * of the Update request.  In an error condition, this will include any error
	 * information.
	 *<P>
	 * @throws EnterpriseObjectUpdateException if any errors occur when validating
	 * the contents of the object, if any errors occur while producing the request
	 * or if the object doesn't support the create action.  This exception will also
   * be thrown if the contents of the reply document contains an error.  That is, if
   * the consuming application had errors processing the request.  This exception will
   * include the Result information containing the error that occurred.
	 */
	public XmlEnterpriseObject update(ActionableEnterpriseObject theObject)
	throws TransportException;
  
	public Document produceRequest(ActionableEnterpriseObject theObject, Document doc) 
  throws TransportException;

	public ProducerId getProducerId(String dummyParm);
	public void setProducerId(ProducerId producerId);
	public int incrementMessageSequence();
}