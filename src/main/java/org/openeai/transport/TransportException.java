package org.openeai.transport;

import org.openeai.OpenEaiException;

/**
 * A TransportException class.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     4.0  - 11 January 2005
 */
public class TransportException extends OpenEaiException {
  /**
   * Constructor
   */
  public TransportException() {
    super();
  }

  public TransportException(String msg) {
    super(msg);
  }
  
  public TransportException(String msg, Throwable rootCause) {
    super(msg, rootCause);
  }
  
  public TransportException(Throwable rootCause) {
    super(rootCause);
  }
}