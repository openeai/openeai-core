package org.openeai.transport;

import org.openeai.moa.*;
import org.jdom.Document;

public interface SyncService  {
	/**
	 * Create Sync message production.  Builds an XML document (using the primed
	 * create sync document as a baseline) out of the current contents of the object.
	 * While building the XML Create Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *
	 * @param producer   org.openeai.jms.producer.PubSubProducer to
	 * use to publish the message.
	 *
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
	public void createSync(ActionableEnterpriseObject theObject)
	throws TransportException;
  
	/**
	 * Delete Sync message production.  Builds an XML document (using the primed
	 * delete sync document as a baseline) out of the current contents of the object.
	 * While building the XML Delete Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *<P>
	 * @param deleteAction String delete action ('purge' or 'delete')
	 *<P>
	 * @param producer PubSubProducer a pre-configured and started PubSubProducer
   * which will be used to send the delete-sync message to the appropriate destination
   * (via the PubSubProducer.publishMessage method).
	 *<P>
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
	public void deleteSync(String deleteAction, ActionableEnterpriseObject theObject)
	throws TransportException;
  
	/**
	 * Update Sync message production.  Builds an XML document (using the primed
	 * update sync document as a "template") out of the current contents of the object.
	 * It will use the current contents of the object as the NewData portion of the message and it will
	 * use the m_baseline object as the Baseline portion of the message.  The m_baseline
	 * object is set when this object was "queried" for previously.
	 * While building the XML Update Sync document, it will validate contents of
	 * the object checking for any malformed, missing or invalid fields.
	 * Field data values are also checked for validitity against the
	 * EnterpriseObjects document.  Publishes the XML document in a JMS message
	 * to the topic connected to by producer.
	 *<P>
	 * @param producer PubSubProducer a pre-configured and started PubSubProducer
   * which will be used to send the delete-sync message to the appropriate destination
   * (via the PubSubProducer.publishMessage method).
	 *<P>
	 * @throws EnterpriseObjectSyncException if any errors occur when validating
	 * the contents of the object.  Or, if any errors occur while publishing the message.
	 */
	public void updateSync(ActionableEnterpriseObject theObject)
	throws TransportException;
  
	public boolean publishMessage(ActionableEnterpriseObject theObject, Document doc) 
  throws TransportException;
  
	public ProducerId getProducerId(String dummyParm);
	public void setProducerId(ProducerId producerId);
	public int incrementMessageSequence();
}