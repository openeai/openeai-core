/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI sample, reference implementation,
and deployment management suite created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to implement integrations for your enterprise, visit
http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.scrubbers;

import java.util.*;
import java.text.*;

/**
 * A general CaseConverter class that converts the first letter of a String to Upper case
 * an all subsequent letters to lower case.  These scrubbers are associated to a particular
 * field in the EnterpriseObjects documents.
 *<P>
 * Example 1:  TOD JACKSON becomes Tod Jackson
 *<P>
 * Example 2: tod jackson becomes Tod Jackson
 *<P>
 * Example 3: TODJACKSON becomes Todjackson
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class CaseConverter
extends EnterpriseScrubberImpl
implements EnterpriseScrubber {

	/**
	 * Constructor
	 */
	public CaseConverter() {
	}

	/**
	 * This is the scrub method that all EnterpriseScrubbers must implement.
   * This particular scrub method converts the first letter of each space delimited Strings
   * in the String passed in to Upper case an all subsequent letters of that String to lower case.
	 *<P>
   * @param value the String to be scrubbed.
   * @return String     the scrubbed value in 'mixed case'.
	 */
	public String scrub(String value) throws EnterpriseScrubberException {

		if (value == null || value.length() == 0) {
			return "";
		}

		// Go through all values (separated by a space) and convert the
		// first letter to upper case, all others to lower case.
		logger.debug("Value before scrubbing: " + value);
		StringTokenizer st = new StringTokenizer(value, " ");
		Vector objects = new Vector();
		StringBuffer sBuf = new StringBuffer();
		while (st.hasMoreTokens()) {
			String elem = st.nextToken();
			sBuf.append(elem.substring(0,1).toUpperCase());
			sBuf.append(elem.substring(1).toLowerCase());
			sBuf.append(" ");
		}
		String retVal = new String(sBuf).trim();
		logger.debug("Value after scrubbing: " + retVal);
		return retVal;
	}
}
