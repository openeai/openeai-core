/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI sample, reference implementation,
and deployment management suite created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to implement integrations for your enterprise, visit
http://www.OpenEai.org/licensing.
*/

package org.openeai.scrubbers;

/**
 * Scrubber that converts the incomming string to lower case. 
 *<P>
 * Example 1:  TOD JACKSON becomes tod jackson
 *<P>
 * Example 2: tod jackson becomes tod jackson
 *<P>
 * Example 3: TODJACKSON becomes todjackson
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class LowerCaseScrubber
extends EnterpriseScrubberImpl
implements EnterpriseScrubber {

  /**
   * Constructor
   */
  public LowerCaseScrubber() {
  }

  /**
   * Converts the string passed in to lower case.
   *<P>
   * @return String the "scrubbed" string.
   * @param value String the value to scrub.
   */
	public String scrub(String value) throws EnterpriseScrubberException {

		if (value == null || value.length() == 0) {
			return "";
		}
    else {
      return value.toLowerCase();
    }
  }
}

 
