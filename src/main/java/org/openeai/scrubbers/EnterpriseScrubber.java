/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.scrubbers;

/**
 * This is the interface which specifies the methods all OpenEAI EnterpriseScubber
 * objects must implement.
 * These scrubbers are associated to a particular field in the EnterpriseObjects document.
 * When a setter method is called for a particular field, the EnterpriseFields object associated
 * to that field, uses it's EnterpriseFormatter which may contain one or more EnterpriseScrubber implementations
 * that are then used to "scrub" the data being passed to the setter method according to the
 * rules of that scrubber implementation.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see org.openeai.config.EnterpriseFields
  * @see org.openeai.config.EnterpriseFormatter
 */
public interface EnterpriseScrubber {

	/**
   * Method used to "scrub" in input value and return some other version of that value.
   * Examples of scrubbers include scrubbers that convert a String to all Lower Case or to Mixed Case.
   *<P>
   * If a field has a scrubber implementation associated to it, this method will be called on that scrubber 
   * implementation when data is set on the field in an object's setter method for that field.  This is automatically
   * done when the 'getEnterpriseValue(...)' method is called in the setter method.  The value being set on the field,
   * is first run through the EnterpriseFields object that all XmlEnterpriseObjects inherit.  At that point,
   * the data will be scrubbed using the scrubber implementation associated to the field in the EnterpriseObjects document.
   *<P>
   * As mentioned below, depending on how many scrubbers a field has associated to it and in what order they're
   * specified, there may be more than one scrubber implementation executed on a field.
   *<P>
   * @param value String, the value to be scrubbed.
   * @return String the after it's been scrubbed.
	 *
   * @throws EnterpriseScrubberException if any errors occur scrubbing the input.
   * @see org.openeai.config.EnterpriseFields
   * @see org.openeai.config.EnterpriseFormatter
	 */
  public String scrub(String value) throws EnterpriseScrubberException;

	/**
   * Method is used to establish an order inwhich scrubbers should be executed if a field has more 
   * than one scrubber associated to it.
   *<P>
   * @param sequence int the order inwhich this scrubber should be executed.
   * @return void
	 */
  public void setSequence(int sequence);

	/**
   * Method is used by the EnterpriseFields object to determine in what order scrubbers should 
   * be executed if a field has more than one scrubber associated to it.
   *<P>
   * @return int the order inwhich this scrubber should be executed.
	 */
  public int getSequence();
}

 
