/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.xml;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.openeai.*;
import java.io.StringReader;

/**
 * This class is used by the XmlDocumentReader class when XML validation is falsed.
 * By using this class, we're able to avoid going to the Webserver to get the constraint
 * information altogether.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see XmlDocumentReader
 */
public class IgnoreDTDResolver extends OpenEaiObject implements EntityResolver {

   /**
   * Constructor
   */
  public IgnoreDTDResolver() {
  }

  /**
  * This method basically overrides the default behavior of going to the web server
  * to retrieve the constraint information for an XML document being read.
  *<P>
  * @return InputSource the "dummy" constraint information.
  * @param publicId String public id (not used)
  * @param systemId String system id (not used)
  */
  public InputSource resolveEntity (String publicId, String systemId) {
    logger.debug("Using the IgnoreDTDResolver...");
    StringReader reader = new StringReader("");
    return new InputSource(reader);
  }
}

 
