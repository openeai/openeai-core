/*******************************************************************************
 $Source$
 $Revision: 691 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.xml;

import org.openeai.OpenEaiObject;
import java.io.File;
import java.io.*;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;



/**
 * This class is used to perform XSLT transformations using Xalan.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class OpenEaiTransformer extends OpenEaiObject {

  public OpenEaiTransformer() {
  }

/*
  public static void transform(String xmlDoc, String outputURL)
  throws java.io.IOException,
         java.net.MalformedURLException,
         org.xml.sax.SAXException  {

    // Use XSLTProcessorFactory to instantiate an XSLTProcessor.
    XSLTProcessor processor = XSLTProcessorFactory.getProcessor();

    // Create the 3 objects the XSLTProcessor needs to perform the transformation.
    XSLTInputSource xmlSource = new XSLTInputSource(xmlDoc);
    XSLTInputSource xslSheet =  null;
    XSLTResultTarget xmlResult = new XSLTResultTarget(outputURL);

    // Perform the transformation.
    processor.process(xmlSource, xslSheet, xmlResult);
    processor.reset();

  }
*/

/*
  public static void transform(byte[] xmlDoc, String outputURL)
  throws java.io.IOException,
         java.net.MalformedURLException,
         org.xml.sax.SAXException  {

    // Use XSLTProcessorFactory to instantiate an XSLTProcessor.
    XSLTProcessor processor = XSLTProcessorFactory.getProcessor();

    // Create the 3 objects the XSLTProcessor needs to perform the transformation.
    ByteArrayInputStream iStream = new ByteArrayInputStream(xmlDoc);
    XSLTInputSource xmlSource = new XSLTInputSource(iStream);
    XSLTInputSource xslSheet =  null;
    XSLTResultTarget xmlResult = new XSLTResultTarget(outputURL);

    // Perform the transformation.
    processor.process(xmlSource, xslSheet, xmlResult);
    processor.reset();
  }
*/

  /**
  * Transorms the byte array (XML) passed in using the XSL URL passed in and 
  * sends the result of the transformation to the outputUrl passed in.
  *<P>
  * @return OutputStream the results of the transformation.
  * @throws IOException
  * @throws MalformeURLException
  * @throws SAXException
  */
  public static void transform(byte[] xmlDoc, String outputURL, String xslDocUri)
  throws java.io.IOException,
         java.net.MalformedURLException,
         org.xml.sax.SAXException  {

    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = null;
    try {
      transformer = tFactory.newTransformer(new StreamSource(xslDocUri));
//      transformer = tFactory.newTransformer(new StreamSource(new ByteArrayInputStream(xslDoc.getBytes())));
    	transformer.transform(
        new StreamSource(new ByteArrayInputStream(xmlDoc)), 
        new StreamResult(outputURL));
    }
    catch (Exception e) {
      throw new IOException(e.getMessage());
    }
  }

  /**
  * Transorms the byte array (XML) passed in using the XSL URL passed in and returns the result of the transformation.
  *<P>
  * @return OutputStream the results of the transformation.
  * @throws IOException
  * @throws MalformeURLException
  * @throws SAXException
  */
  public static OutputStream getTransformation(byte[] xmlDoc, String xslDocUri)
  throws java.io.IOException,
         java.net.MalformedURLException,
         org.xml.sax.SAXException  {

    ByteArrayInputStream iStream = new ByteArrayInputStream(xmlDoc);
    ByteArrayOutputStream boutArray = new ByteArrayOutputStream();

    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = null;
    try {
      transformer = tFactory.newTransformer(new StreamSource(xslDocUri));
    }
    catch (Exception e) {
      logger.fatal(e.getMessage(), e);
      throw new IOException(e.getMessage());
    }
    try {
    	transformer.transform(
        new StreamSource(iStream), 
        new StreamResult(boutArray));
      return boutArray;
    }
    catch (Exception e) {
      logger.fatal(e.getMessage(), e);
      throw new IOException(e.getMessage());
    }
  }
}
