/*******************************************************************************
 $Source$
 $Revision: 4158 $
 *******************************************************************************/

/**********************************************************************
 This file is part of the OpenEAI Application Foundation or
 OpenEAI Message Object API created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to build commercial integration software or to implement
 integrations for your enterprise, visit http://www.OpenEai.org/licensing.
 */

package org.openeai.xml;

import org.openeai.*;

import java.io.*;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.Document;

/**
 * This class is used to create a JDOM Document object from the contents of an
 * XML file that can be stored in a file (on a web server or local file system)
 * or passed in as an in-memory representation of that file (Reader,
 * InputStream).
 * <p>
 *
 * @author Tod Jackson (tod@openeai.org)
 * @author Steve Wheat (steve@openeai.org)
 * @version 3.0 - 28 January 2003
 */
public class XmlDocumentReader extends OpenEaiObject {
    public static final String DOC_URI_BASE = "docUriBase";
    public static final String OPENEAI_CLASSPATH_FIRST = "openeaiClasspathFirst";
    protected boolean openeaiClasspathFirst = false;
    private static Logger logger = Logger.getLogger(XmlDocumentReader.class);

    /**
     * Constructor
     */
    public XmlDocumentReader() {
        openeaiClasspathFirst = "true".equals(System.getProperty(OPENEAI_CLASSPATH_FIRST));
    }

    /**
     * Creates a JDOM Document from the specified XML file name.
     * <p>
     * Example 1: http://www.somexmlserver.com/xml/AnXmlFile.xml
     * <p>
     * Example 2: file://localhost/xmlfiles/AnXmlFile.xml
     * <p>
     * Example 3: c:\xmlfiles\AnXmlFile.xml
     * <p>
     * Example 4: /opt/xmlfiles/AnXmlFile.xml
     * <p>
     *
     * @param docUriPath It will load the resource in the following order:
     *                   <ol>
     *                   if docUriPath is an absolute file such as
     *                   http://www.emory.edu/YourEO.xml or file://xxx/yyy/YourEO.xml,
     *                   it will load it.
     *                   </ol>
     *                   <ol>
     *                   if there is a system property openeaiClasspathFirst set, to true it will load from the classPath first if it exists
     *                   </ol>
     *                   <ol>
     *                   if there is a system property docUriBase set, it will prefix
     *                   the docUriPath with the value of docUriBase and load it if
     *                   it exists.
     *                   </ol>
     *                   <ol>
     *                   load the resource as if docUriPath is a relative file path.
     *                   </ol>
     *                    <ol>
     *                   load the resource  from classpath.
     *                   </ol>
     * @param validate   boolean flag indicating whether or not this method should also
     *                   validate the contents of the document from an XML perspective
     *                   (true=validate, false=don't validate)
     * @return org.jdom.Document
     */
    public final Document initializeDocument(String docUriPath, boolean validate) throws XmlDocumentReaderException {
        logger.debug("docUriPath="+docUriPath);
        if (docUriPath == null || docUriPath.trim().length() == 0) {
            logger.debug("XmlDocumentReader: Null Document name passed in, can't initialize:docUriPath="+docUriPath);
            return null;
        }
        docUriPath = docUriPath.trim();
        String docUriBase = System.getProperty(DOC_URI_BASE);
        Document doc = null;
        try {
            SAXBuilder builder = new SAXBuilder(false);
            if (!validate) {
                logger.trace("Setting the EntityResolver to IgnoreDTDResolver:" + validate);
                builder.setEntityResolver(new IgnoreDTDResolver());
            }
            doc = initializeDocument(docUriPath, builder);
            logger.trace("XmlDocumentReader:" + docUriPath + ":successfully read!");
            if (validate) {
                XmlValidator xmlValidator = new XmlValidator();
                if (xmlValidator.isValid(doc) == false) {
                    throw new XmlDocumentReaderException("Document is not valid!");
                }
            } else {
                logger.trace("Validation is turned off.");
            }
        } catch (Exception e) {
            throw new XmlDocumentReaderException("File: docUriBase= " + docUriBase + ",docUriPath="+ docUriPath + ",message: " + e.getMessage(), e);
        }
        return doc;
    }

    protected Document initializeDocument(String docUriPath, SAXBuilder builder) throws JDOMException, XmlDocumentReaderException, IOException {
        docUriPath = docUriPath.trim();
        String docUriBase = System.getProperty(DOC_URI_BASE);
        if (isCompleteAddress(docUriPath)) {
            return buildDocFromCompleteAddress(docUriPath, builder);
        }
        logger.debug("openeaiClasspathFirst=" + openeaiClasspathFirst + "," +docUriPath+"="+ getClass().getResource("/" + docUriPath));
        if (openeaiClasspathFirst && getClass().getResource("/" + docUriPath) != null) {
            return getDocumentFromClasspathIfExist(docUriPath, builder);
        }
        logger.debug("isDocuUriBaseDefined=" + isDocUriBaseDefined(docUriBase));
        if (isDocUriBaseDefined(docUriBase)) {
            String docUrBasePlusPath = docUriBase + docUriPath;
            if (isCompleteAddress(docUrBasePlusPath))
                return buildDocFromCompleteAddress(docUrBasePlusPath, builder);
            else {
                return getDocumentFromFileOrClasspath(docUriPath, builder, docUrBasePlusPath);
            }
        }
        return getDocumentFromFileOrClasspath(docUriPath, builder);
    }

    protected Document getDocumentFromClasspathIfExist(String docUriPath, SAXBuilder builder) throws JDOMException, IOException {
        return builder.build(getClass().getClassLoader().getResourceAsStream(docUriPath));
    }

    private Document getDocumentFromFileOrClasspath(String docUriPath, SAXBuilder builder) throws JDOMException, IOException {
        Document doc;
        if (new File(docUriPath).exists())
            doc = builder.build(new File(docUriPath));
        else
            doc = getDocumentFromClasspathIfExist(docUriPath, builder);
        return doc;
    }

    private Document getDocumentFromFileOrClasspath(String docUriPath, SAXBuilder builder, String docUriBaseAndPath) throws JDOMException, IOException {
        Document doc;
        if (new File(docUriBaseAndPath).exists())
            doc = builder.build(new File(docUriBaseAndPath));
        else if (new File(docUriPath).exists())
            doc = builder.build(new File(docUriPath));
        else
            doc = getDocumentFromClasspathIfExist(docUriPath, builder);
        return doc;
    }

    private boolean isDocUriBaseDefined(String docUriBase) {
        // sometimes undefined docUriBase can have value of
        // '${docUriBase}'
        return docUriBase != null && docUriBase.trim().length() > 0 && !docUriBase.contains("docUriBase");
    }

    private boolean isCompleteAddress(String docUriString) {
        return docUriString.toLowerCase().indexOf("http") == 0 || docUriString.toLowerCase().indexOf("file") == 0;
    }

    protected static Document buildDocFromCompleteAddress(String urlString, SAXBuilder builder) throws XmlDocumentReaderException, JDOMException, IOException {
        logger.trace("Building document from " + urlString);
        Document doc;
        java.net.URL url = null;
        try {
            url = new java.net.URL(urlString);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new XmlDocumentReaderException(e.getMessage(), e);
        }
        // logger.info("Started - SAXBuilder.build for URL " +
        // fileName);
        doc = builder.build(url);
        return doc;
    }

    /**
     * Creates a JDOM Document from the supplied InputStream.
     * <p>
     *
     * @param is       InputStream contents of the XML file that has been stored in
     *                 the InputStream.
     * @param validate boolean flag indicating whether or not this method should also
     *                 validate the contents of the document from an XML perspective
     *                 (true=validate, false=don't validate)
     * @return org.jdom.Document
     */
    public final Document initializeDocument(InputStream is, boolean validate) throws XmlDocumentReaderException {

        if (is == null) {
            logger.trace("XmlDocumentReader: Null Document name passed in, can't initialize.");
            return null;
        }

        Document doc = null;
        try {
            SAXBuilder builder = new SAXBuilder(false);
            logger.trace("Building document from Reader");
            if (validate == false) {
                logger.trace("Setting the EntityResolver to IgnoreDTDResolver.");
                builder.setEntityResolver(new IgnoreDTDResolver());
            } else {
                logger.trace("Using the default EntityResolver.");
            }
            doc = builder.build(is);
            logger.trace("XmlDocumentReader: document successfully read!");
            if (validate) {
                XmlValidator xmlValidator = new XmlValidator();
                if (xmlValidator.isValid(doc) == false) {
                    throw new XmlDocumentReaderException("Document is not valid!");
                }
            } else {
                logger.trace("Validation is turned off.");
            }
        } catch (JDOMException e) {
            throw new XmlDocumentReaderException(e.getMessage(), e);
        } catch (Exception e) {
            throw new XmlDocumentReaderException(e.getMessage(), e);
        }
        return doc;
    }

    /**
     * Creates a JDOM Document from the supplied Reader.
     * <p>
     *
     * @param docAsReader Reader contents of the XML file that has been stored in the
     *                    Reader.
     * @param validate    boolean flag indicating whether or not this method should also
     *                    validate the contents of the document from an XML perspective
     *                    (true=validate, false=don't validate)
     * @return org.jdom.Document
     */
    public final Document initializeDocument(Reader docAsReader, boolean validate) throws XmlDocumentReaderException {

        if (docAsReader == null) {
            logger.trace("XmlDocumentReader: Null Document name passed in, can't initialize.");
            return null;
        }

        Document doc = null;
        try {
            SAXBuilder builder = new SAXBuilder(false);
            logger.trace("Building document from Reader");
            if (validate == false) {
                logger.trace("Setting the EntityResolver to IgnoreDTDResolver.");
                builder.setEntityResolver(new IgnoreDTDResolver());
            } else {
                logger.trace("Using the default EntityResolver.");
            }
            doc = builder.build(docAsReader);
            logger.trace("XmlDocumentReader: document successfully read!");
            if (validate) {
                XmlValidator xmlValidator = new XmlValidator();
                if (xmlValidator.isValid(doc) == false) {
                    throw new XmlDocumentReaderException("Document is not valid!");
                }
            } else {
                logger.trace("Validation is turned off.");
            }
        } catch (JDOMException e) {
            throw new XmlDocumentReaderException(e.getMessage(), e);
        } catch (Exception e) {
            throw new XmlDocumentReaderException(e.getMessage(), e);
        }
        return doc;
    }
}
