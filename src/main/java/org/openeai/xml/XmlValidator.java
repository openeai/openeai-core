/*******************************************************************************
 $Source$
 $Revision: 691 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.xml;

import org.openeai.*;

import java.io.*;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

/**
 * A convenience classed that can be used to validate an already created
 * JDOM Document object.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class XmlValidator extends OpenEaiObject {

  /**
   * Constructor
   */
  public XmlValidator() {
  }

  /**
  * Uses the Xerces SAXParser to validate an existing JDOM Document object.
  *<P>
  * @return boolean true if the document is valid, false if the document is not valid
  */
  public final synchronized boolean isValid(Document inDoc) {
    XMLOutputter xmlOut = new XMLOutputter();
    ByteArrayOutputStream bArray = new ByteArrayOutputStream();
    String parserName = "org.apache.xerces.parsers.SAXParser";
    try {
      xmlOut.output(inDoc, bArray);
    }
    catch (IOException e) {
      logger.fatal(e.getMessage(), e);
    }
    byte[] b = bArray.toByteArray();
    try {
      ParseErrorHandler errHandler = new ParseErrorHandler();
      errHandler.setExitOnError(false);
      XMLReader parser = (XMLReader)Class.forName(parserName).newInstance();
      parser.setFeature("http://xml.org/sax/features/validation",true);
      parser.setErrorHandler(errHandler);
      parser.parse(new InputSource(new ByteArrayInputStream(b)));
      if (errHandler.hasErrors()) {
        return false;
      }
      logger.debug("XML Document is valid!");
      return true;
    }
    catch (Exception e) {
      logger.fatal(e.getMessage(), e);
    }
    return false;
  }
}

 
