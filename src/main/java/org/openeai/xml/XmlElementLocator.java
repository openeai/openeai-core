/*******************************************************************************
 $Source$
 $Revision: 3666 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.xml;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Attribute;

import org.openeai.*;

/**
 * An XmlElementLocator class.
 * Contains helper methods to find elements within an Xml document.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class XmlElementLocator extends OpenEaiObject {
    private static Logger logger=Logger.getLogger(XmlElementLocator.class);
	/**
	 * Constructor
	 */
	public XmlElementLocator() {
	}

  /**
  * Convenience method that searches for and returns an Element that has a child Attribute
  * of "attrName" with a value of "attrValue".  If no Elements are found, returns null.
  *<P>
  * The search is NOT recursive meaning that it will NOT "drill" down into all child Elements of
  * the starting element passed in.
  *<P>
  * @return Element the element that meets the criteria specified by the 
  * Attribute name and value passed in, null if no Elements are found
  * @param e Element the Element at which to start the search.  This means the search will
  * be start from this Element and continue until there are no more children within this Element.
  * @param attrName String the name of the child Attribute that's being searched for.
  * @param attrValue String the value that's expected to exist in that Attribute.
  */
	public final Element getElementByAttributeNameValue(Element e, String attrName, String attrValue) {
		// Drills down the element (e) until it finds a child named "name"
		logger.trace("Looking for Element with an attribute named: " + 
				attrName + " and value of: " + attrValue + 
				" on the " + e.getName() + " element.");
		java.util.List eChildren1 = e.getChildren();
		logger.trace("there are " + eChildren1.size() + " child elements in the " + e.getName() + " element.");
		for (int i=0; i<eChildren1.size(); i++) {
			Element e1 = (Element)eChildren1.get(i);
			java.util.List aChildren = e1.getAttributes();
			logger.trace("e1 is a " + e1.getName() + " element with " + 
					aChildren.size() + " attributes.");
			for (int j=0; j<aChildren.size(); j++) {
				Attribute a1 = (Attribute)aChildren.get(j);
				logger.trace("a1's name is: " + a1.getName());
				if (a1.getName().equals(attrName)) {
					if (a1.getValue() != null && a1.getValue().trim().equals(attrValue)) {
						return e1;
					}
				}
			}
			Element e2 = matchItByAttributeNameValue(e1,attrName,attrValue);
			if (e2 != null) {
				return e2;
			}
		}
		return null;
	}
	private Element matchItByAttributeNameValue(Element e, String attrName, String attrValue) {
		if (e.getContentSize()>0) {
			java.util.List ec = e.getChildren();
			for (int i=0; i<ec.size(); i++) {
				Element e1 = (Element)ec.get(i);
				java.util.List ac = e1.getAttributes();
				for (int j=0; j<ac.size(); j++) {
					Attribute a1 = (Attribute)ac.get(j);
					if (a1.getName().equals(attrName)) {
						if (a1.getValue() != null && a1.getValue().trim().equals(attrValue)) {
							return e1;
						}
					}
				}
				Element e2 = matchItByAttributeNameValue(e1,attrName,attrValue);
				if (e2 == null) {
					return null;
				}
        if (e2 != null) {
	  			java.util.List ac2 = e2.getAttributes();
		  		for (int j=0; j<ac2.size(); j++) {
			  		Attribute a2 = (Attribute)ac2.get(j);
					  if (a2.getName().equals(attrName)) {
  						if (a2.getValue() != null && a2.getValue().trim().equals(attrValue)) {
	  						return e2;
		  				}
			  		}
  				}
        }
			}
		}
		return null;
	}

  /**
  * Convenience method that searches for and returns an Element that has a child Attribute
  * of "attrName" with a value of "attrValue".  The attribute value comparison IS case insensitive.
  * If no Elements are found, returns null.
  *<P>
  * The search is recursive meaning that it will "drill" down into all child Elements of
  * the starting element passed in.
  *<P>
  * @return Element the element that meets the criteria specified by the 
  * Attribute name and value passed in, null if no Elements are found
  * @param e Element the Element at which to start the search.  This means the search will
  * be start from this Element and continue until there are no more children within this Element.
  * @param attrName String the name of the child Attribute that's being searched for.
  * @param attrValue String the value that's expected to exist in that Attribute.
  */
	public final Element getElementByAttributeNameValueRecursive(Element e, String attrName, String attrValue) {
		// Drills down the element (e) until it finds a child named "name"
		logger.debug("Looking for Element with an attribute named: " + attrName + " and value of: " + attrValue);
		java.util.List eChildren1 = e.getChildren();
		for (int i=0; i<eChildren1.size(); i++) {
			Element e1 = (Element)eChildren1.get(i);
			java.util.List aChildren = e1.getAttributes();
			for (int j=0; j<aChildren.size(); j++) {
				Attribute a1 = (Attribute)aChildren.get(j);
				if (a1.getName().equals(attrName)) {
					if (a1.getValue() != null && a1.getValue().trim().equalsIgnoreCase(attrValue)) {
						return e1;
					}
				}
			}
			Element e2 = matchItByAttributeNameValueRecursive(e1,attrName,attrValue);
			if (e2 != null) {
				return e2;
			}
		}
		return null;
	}
	private Element matchItByAttributeNameValueRecursive(Element e, String attrName, String attrValue) {
		if (e.getContentSize()>0) {
			java.util.List ec = e.getChildren();
			for (int i=0; i<ec.size(); i++) {
				Element e1 = (Element)ec.get(i);
				java.util.List ac = e1.getAttributes();
				for (int j=0; j<ac.size(); j++) {
					Attribute a1 = (Attribute)ac.get(j);
					if (a1.getName().equals(attrName)) {
						if (a1.getValue() != null && a1.getValue().trim().equalsIgnoreCase(attrValue)) {
							return e1;
						}
					}
				}
				Element e2 = matchItByAttributeNameValueRecursive(e1,attrName,attrValue);
        if (e2 != null) {
	  			java.util.List ac2 = e2.getAttributes();
		  		for (int j=0; j<ac2.size(); j++) {
			  		Attribute a2 = (Attribute)ac2.get(j);
					  if (a2.getName().equals(attrName)) {
  						if (a2.getValue() != null && a2.getValue().trim().equalsIgnoreCase(attrValue)) {
	  						return e2;
		  				}
			  		}
  				}
        }
			}
		}
		return null;
	}

	/** Drills down the element (e) until it finds a child with attribute value of attrValue. Returns null if not found. */
	/** This method DOES NOT do a complete search of the document!! */
  /**
  * Convenience method that searches for and returns an Element that has any child Attributes
  * with a value of "attrValue".  If no Elements are found, returns null.
  *<P>
  * The search is recursive meaning that it will "drill" down into all child Elements of
  * the starting element passed in as well.
  *<P>
  * @return Element the element that meets the criteria specified by the 
  * Attribute value passed in, null if no Elements are found
  * @param e Element the Element at which to start the search.  This means the search will
  * be start from this Element and continue until there are no more children within this Element.
  * @param attrValue String the value being searched for in any child Attribute of the Element passed in.
  */
	public final Element getElementByAttributeValue(Element e, String attrValue) {
		// Drills down the element (e) until it finds a child named "name"
		java.util.List eChildren1 = e.getChildren();
		for (int i=0; i<eChildren1.size(); i++) {
			Element e1 = (Element)eChildren1.get(i);
			java.util.List aChildren = e1.getAttributes();
			for (int j=0; j<aChildren.size(); j++) {
				Attribute a1 = (Attribute)aChildren.get(j);
				if (a1.getValue() != null && a1.getValue().trim().equals(attrValue)) {
					return e1;
				}
			}
			Element e2 = matchItByAttributeValue(e1, attrValue);
			if (e2 != null) {
				return e2;
			}
		}
		return null;
	}
	private Element matchItByAttributeValue(Element e, String attrValue) {
		if (e.getContentSize()>0) {
			java.util.List ec = e.getChildren();
			for (int i=0; i<ec.size(); i++) {
				Element e1 = (Element)ec.get(i);
				java.util.List ac = e1.getAttributes();
				for (int j=0; j<ac.size(); j++) {
					Attribute a1 = (Attribute)ac.get(j);
					if (a1.getValue() != null && a1.getValue().trim().equals(attrValue)) {
						return e1;
					}
				}
				Element e2 = matchItByAttributeValue(e1,attrValue);
				if (e2 == null) {
          return null;
				}
				java.util.List ac2 = e2.getAttributes();
				for (int j=0; j<ac2.size(); j++) {
					Attribute a2 = (Attribute)ac2.get(j);
					if (a2.getValue() != null && a2.getValue().trim().equals(attrValue)) {
						return e2;
					}
				}
			}
		}
		return null;
	}

  /**
  * Convenience method that searches for and returns an Element that has a child Element
  * with a name that matches the name passed in.  If no Elements are found, returns null.
  *<P>
  * The search is recursive meaning that it will "drill" down into all child Elements of
  * the starting element passed in as well.
  *<P>
  * @return Element the element that meets the criteria specified by the 
  * Element name passed in, null if no Elements are found
  * @param e Element the Element at which to start the search.  This means the search will
  * be start from this Element and continue until there are no more children within this Element.
  * @param name String the name of the child Element that's being searched for.
  */
	public final Element getElementByName(Element e, String name) {
		logger.trace("Looking for Element named: " + name);
		return getElementByName_internal(e, name);
	}
	private Element getElementByName_internal(Element e, String name) {
		java.util.List eChildren1 = e.getChildren();
		for (int i=0; i<eChildren1.size(); i++) {
			Element e1 = (Element)eChildren1.get(i);

			if (e1.getName().equals(name)) {
				return e1;
			}

			Element e2 = getElementByName_internal(e1, name);
			if (e2 != null) {
				return e2;
			}
		}

		return null;
	}

  /**
  * Convenience method that searches for and returns an Element that has a child Element
  * of "name" with a value of "value".  If no Elements are found, returns null.
  *<P>
  * The search is recursive meaning that it will "drill" down into all child Elements of
  * the starting element passed in as well.
  *<P>
  * @return Element the element that meets the criteria specified by the 
  * Element name and value passed in, null if no Elements are found
  * @param e Element the Element at which to start the search.  This means the search will
  * be start from this Element and continue until there are no more children within this Element.
  * @param name String the name of the child Element that's being searched for.
  * @param value String the value that's expected to exist in that Element.
  */
	public final Element getElementByNameValue(Element e, String name, String value) {
		logger.debug("Looking for Element named: " + name + " and value of: " + value);
		return getElementByNameValue_internal(e, name, value);
	}
	private Element getElementByNameValue_internal(Element e, String name, String value) {
		java.util.List eChildren1 = e.getChildren();
		for (int i=0; i<eChildren1.size(); i++) {
			Element e1 = (Element)eChildren1.get(i);

			if (e1.getName().equals(name)) {
				if (e1.getText() != null && e1.getText().trim().equals(value)) {
					return e1;
				}
			}

			Element e2 = getElementByNameValue_internal(e1, name, value);
			if (e2 != null) {
				return e2;
			}
		}

		return null;
	}

  /**
  * Convenience method that searches for and returns an Element that has a child Element
  * with a value of "value".  If no Elements are found, returns null.
  *<P>
  * The search is recursive meaning that it will "drill" down into all child Elements of
  * the starting element passed in as well.
  *<P>
  * @return Element the element that meets the criteria specified by the 
  * Element value passed in, null if no Elements are found
  * @param e Element the Element at which to start the search.  This means the search will
  * be start from this Element and continue until there are no more children within this Element.
  * @param value String the value that's expected to exist in any child Element.
  */
	public final Element getElementByValue(Element e, String value) {
		logger.debug("Looking for Element with a value of: " + value);
		return getElementByValue_internal(e, value);
	}
	private Element getElementByValue_internal(Element e, String value) {
		java.util.List eChildren1 = e.getChildren();
		for (int i=0; i<eChildren1.size(); i++) {
			Element e1 = (Element)eChildren1.get(i);

			if (e1.getText() != null && e1.getText().trim().equals(value)) {
				return e1;
			}

			Element e2 = getElementByValue_internal(e1, value);
			if (e2 != null) {
				return e2;
			}
		}

		return null;
	}
}

