/*******************************************************************************
 $Source$
 $Revision: 1264 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.loggingutils;

import java.net.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import org.openeai.config.MailServiceConfig;

/**
 * MailService class.  Common re-usable class for sending email messages.
 * Uses JavaMail and Java Activation Frameworks
 *
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class MailService {
	private String m_mailHost=null;
	private InternetAddress[] m_toAddr;
	private InternetAddress m_fromAddr;
	private String m_subject=null;
	private String m_msgText=null;
	private Map<String,String> headerFields=new HashMap<>();

  /**
  * As AppConfig reads through an application's deployment document, it will build a
  * MailServiceConfig Java object and pass that object to this constructor.  Then
  * this MailService object will have all the information it needs to initialize itself.
  * <P>
  * @param config org.openeai.config.MailServiceConfig
  * @see org.openeai.config.MailServiceConfig
  * @see org.openeai.loggingutils.MailService
  **/
	public MailService(MailServiceConfig config) throws AddressException {
    setMailHost(config.getMailHost());
    setToAddr(config.getRecipientList());
    setRecipientList(config.getRecipientList());
    setFromAddr(config.getFromAddress());
    setFromAddress(config.getFromAddress());
    setSubject(config.getSubject());
    setMsgText(config.getMessageBody());
    setMessageBody(config.getMessageBody());
	}

	public MailService() {
	}

	public MailService(String toAddr, String fromAddr, String subject, String msg) throws AddressException {
		setFromAddr(fromAddr);
		setToAddr(toAddr);
		setSubject(subject);
		setMsgText(msg);
	}

	public MailService(String mailHost, String fromAddr) throws AddressException {
		setMailHost(mailHost);
		setFromAddr(fromAddr);
	}

	public MailService(String mailHost, String fromAddr, String toAddr) throws AddressException {
		setMailHost(mailHost);
		setFromAddr(fromAddr);
		setToAddr(toAddr);
	}

	public void addHeaderField(String name,String value){
		headerFields.put(name,value);
	}

/**
	 * Sends an email message to everyone included in the toAddr class variable.
	 *
	 * @return boolean True if successful, false if an error occurs
	 */
	public boolean sendMessage() {
		Properties props = new Properties();
		props.put("mail.smtp.host", getMailHost());
		Session session = Session.getDefaultInstance(props, null);
		// session.setDebug(true);
		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(getFromAddr());
			msg.setRecipients(Message.RecipientType.TO, getToAddr());
			msg.setSubject(getSubject());
			msg.setContent(getMsgText(), "text/plain");
			addHeaderFieldsToMessage(msg);
			Transport.send(msg);
		}
		catch (MessagingException mex) {
			mex.printStackTrace();
			return false;
		}
		return true;
	}

	private void addHeaderFieldsToMessage(Message msg) throws MessagingException {
		for(Map.Entry e:headerFields.entrySet()){
			msg.addHeader(e.getKey().toString(),e.getValue().toString());
		}
	}

	/**
	 * Sends an HTML email message to everyone included in the toAddr class variable.
	 *
	 * @return boolean True if successful, false if an error occurs
	 */
	public boolean sendHTMLMessage() {
		Properties props = new Properties();
		props.put("mail.smtp.host", getMailHost());
		Session session = Session.getDefaultInstance(props, null);
		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(getFromAddr());
			msg.setRecipients(Message.RecipientType.TO, getToAddr());
			msg.setSubject(getSubject());
			msg.setContent(getMsgText(), "text/html");
			addHeaderFieldsToMessage(msg);
			Transport.send(msg);
		}
		catch (MessagingException mex) {
			mex.printStackTrace();
			return false;
		}
		return true;
	}

/**
	 * Sets the mailHost class variable to the data passed in
	 *
	 * @param mailHost mail host set by the calling client
	 *
	 * @return void
	 */
	public void setMailHost(String mailHost) {
		m_mailHost = mailHost;
	}

/**
	 * Sets the toAddr class variable to the data passed in
	 *
	 * @param addr String coma separated list of email addresses to send messages to set by the client
	 *
	 * @return void
	 *
	 * @exception AddressException
   * @deprecated As of OpenEAI 4.0 release, use setRecipientList(String) instead.
	 */
	public void setToAddr(String addr) throws AddressException {
		m_toAddr = InternetAddress.parse(addr);
	}

/**
	 * Sets the toAddr variable to the data passed in
	 *
	 * @param addr String coma separated list of email addresses to send messages to set by the client
	 *
	 * @return void
	 *
	 * @exception AddressException
	 */
	public void setRecipientList(String addr) throws AddressException {
		m_toAddr = InternetAddress.parse(addr);
	}

/**
	 * Sets the fromAddr class variable to the data passed in
	 *
	 * @param fromAddr Address that messages are coming from
	 *
	 * @return void
	 *
	 * @exception AddressException
   * @deprecated As of OpenEAI 4.0 release, use setFromAddress() instead.
	 */
	public void setFromAddr(String fromAddr) throws AddressException {
		m_fromAddr = new InternetAddress(fromAddr);
	}

/**
	 * Sets the fromAddr variable to the data passed in
	 *
	 * @param fromAddr Address that messages are coming from
	 *
	 * @return void
	 *
	 * @exception AddressException
	 */
	public void setFromAddress(String fromAddr) throws AddressException {
		m_fromAddr = new InternetAddress(fromAddr);
	}

/**
	 * sets the subject class variable to data passed in from the client
	 *
	 * @param subject subject of the message passed from the client
	 *
	 * @return void
	 */
	public void setSubject(String subject) {
		m_subject = subject;
	}

/**
	 * sets the msgText class variable to data passed from the client.
	 *
	 * @param msg String message to be sent
	 *
	 * @return void
   * @deprecated As of OpenEAI 4.0 release, use setMessageBody(String) instead.
	 */
	public void setMsgText(String msg) {
		m_msgText = msg;
	}

/**
	 * sets the msgText variable to data passed from the client.
	 *
	 * @param msg String message to be sent
	 *
	 * @return void
	 */
	public void setMessageBody(String msg) {
		m_msgText = msg;
	}

/**
	 * returns the value of the mail host class variable
	 *
	 * @return  String.mail host
	 */
	public String getMailHost() {
		return m_mailHost;
	}

/**
	 * Returns the to address class variable
	 *
	 * @return InternetAddress[] email addresses of the recipients of a message
   * @deprecated As of OpenEAI 4.0 release, use getRecipientList() instead.
	 */
	public InternetAddress[] getToAddr() {
		return m_toAddr;
	}

/**
	 * Returns the to address class variable
	 *
	 * @return InternetAddress[] email addresses of the recipients of a message
	 */
	public InternetAddress[] getRecipientList() {
		return m_toAddr;
	}

/**
	* Returns a specific email address from the toAddr InternetAddress array
	*
	* @return InternetAddress an email address from the toAddr array
   * @deprecated As of OpenEAI 4.0 release, use getRecipientList(int) instead.
	*/
	public InternetAddress getToAddr(int index) {
		return m_toAddr[index];
	}

/**
	* Returns a specific email address from the toAddr InternetAddress array
	*
	* @return InternetAddress an email address from the toAddr array
	*/
	public InternetAddress getRecpient(int index) {
		return m_toAddr[index];
	}

/**
	 * returns the from address class variable
	 *
	 * @return InternetAddress email address of the sender of the message
   * @deprecated As of OpenEAI 4.0 release, use getFromAddress() instead.
	 */
	public InternetAddress getFromAddr() {
		return m_fromAddr;
	}

/**
	 * returns the from address class variable
	 *
	 * @return InternetAddress email address of the sender of the message
	 */
	public InternetAddress getFromAddress() {
		return m_fromAddr;
	}

/**
	 * returns the subject of the current message
	 *
	 * @return String subject of the email message
	 */
	public String getSubject() {
		return m_subject;
	}

/**
	 * returns the message text of the current message
	 *
	 * @return String message text
   * @deprecated As of OpenEAI 4.0 release, use getMessageBody() instead.
	 */
	public String getMsgText() {
		String msg = "";
		try {
			msg = m_msgText + "\n\nFrom Server: " + InetAddress.getLocalHost();
			if (System.getProperty("user.dir") != null && System.getProperty("user.dir").trim().length() > 0) {
				msg += ", Directory: " + System.getProperty("user.dir");
			}
			//msg = m_msgText + "\n\nFrom Server on: " + InetAddress.getLocalHost() + " at directory: " + System.getProperty("user.dir");
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
			msg = m_msgText;
		}
		return msg;
	}

/**
	 * returns the message body of the current message
	 *
	 * @return String message body
	 */
	public String getMessageBody() {
		String msg = "";
		try {
			msg = m_msgText + "\n\nFrom Server: " + InetAddress.getLocalHost();
			if (System.getProperty("user.dir") != null && System.getProperty("user.dir").trim().length() > 0) {
				msg += ", Directory: " + System.getProperty("user.dir");
			}
			//msg = m_msgText + "\n\nFrom Server on: " + InetAddress.getLocalHost() + " at directory: " + System.getProperty("user.dir");
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
			msg = m_msgText;
		}
		return msg;
	}
}

