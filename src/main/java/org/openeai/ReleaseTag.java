/*******************************************************************************
 $Source$
 $Revision: 2755 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to implement integrations for your enterprise, visit
http://www.OpenEai.org/licensing.
*/

package org.openeai;

/**
 * Used to echo the OpenEAI Copyright notice at the beginning of Gateways and Applications developed
 * with the OpenEAI software foundation code.
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     5.0 - 27 February 2010
 */
public class ReleaseTag extends Object {

  public final static String API_VERSION = "Release 5.0 beta build ####";

  /**
   * Constructor
   */
  public ReleaseTag() {
    System.out.println("\n");
    System.out.println("Copyright (c) 2001-2010 OpenEAI Software Foundation.");
    System.out.println("Created by Tod Jackson and Steve Wheat at the " +
    	"University of Illinois at Urbana-Champaign.");
    System.out.println("http://www.openeai.org");
    System.out.println(API_VERSION);
    System.out.println("\n\n");
  }
}

 
