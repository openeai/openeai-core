/*******************************************************************************
 $Source$
 $Revision: 691 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.uuid;

// RandomUuid.java - Version 4 UUID from the Draft spec (defacto standard of
//                   uuid/guid specs)

import java.security.*;

/**
 * Version 4 UUID from the Draft spec (defacto standard of uuid/guid specs).
 * <P>
  * @author      Chris Barton
  * @version     3.0  - 28 January 2003
 */
public class RandomUuid extends Uuid
{
    /**
    * Generates a "random" UUID.  Sets the "m_data" instance variable to that UUID.
    */
    protected void generateUuid()
    {   
        SecureRandom s = new SecureRandom();
        byte randomBuffer[] = new byte [16];
        s.nextBytes(randomBuffer);

        int uuid[] = new int [16];
        for(int i=0; i<16; i++)
            uuid[i] = randomBuffer[i] - Byte.MIN_VALUE;


        uuid[8] &= 0x3F;
        uuid[8] |= 0x80;            
        uuid[6] &= 0x0F;
        uuid[6] |= 0x40;

        StringBuffer result = new StringBuffer();

        // time_low
        for(int i=0; i<4; i++)
            result.append(hexByte(uuid[i]));

        // time_mid
        result.append("-");
        for(int i=4; i<6; i++)
            result.append(hexByte(uuid[i]));

        // time_high_and_version
        result.append("-");
        for(int i=6; i<8; i++)
            result.append(hexByte(uuid[i]));

        // clock_seq_high_and_reserved, clock_seq_low
        result.append("-");
        for(int i=8; i<10; i++)
            result.append(hexByte(uuid[i]));

        // node        
        result.append("-");
        for(int i=10; i<16; i++)
            result.append(hexByte(uuid[i]));

        m_data = result.toString();
    }

    public RandomUuid()
    {   
        generateUuid();
    }
}
