/*******************************************************************************
 $Source$
 $Revision: 883 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai;

import java.io.CharArrayWriter;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * The parent of all OpenEAI related exception.  This 
 * exception contains a root cuase exception.
 * All printStackTrace() methods are overridden to append
 * the root cause's stack trace to the normal stack trace.
 * <P>
  * @author      John Ellis (joellis@sct.com)
  * @version     3.0  - 28 January 2003
 *
 **/
public class OpenEaiException extends Exception {


    /**
     * Default constructor.
     **/
    public OpenEaiException() {
    }


    /**
     * Constructor.
     * @param message text descibing the exception
     **/
    public OpenEaiException(String message) {
        super(message);
    }


    /**
     * Constructor.
     * @param rootCause the Exception that caused this exception to be thrown
     **/
    public OpenEaiException(Throwable rootCause) {
        this.rootCause = rootCause;
    }


    /**
     * Constructor.
     * @param reason an text message describing the exception
     * @param parent the Exception that caused this exception to be thrown
     **/
    public OpenEaiException(String message, Throwable rootCause) {
        super(message);
        this.rootCause = rootCause;
    }


    /**
     * Prints this Exception and its backtrace to System.err.
     **/
    public void printStackTrace() { 
        synchronized (System.err) {
            super.printStackTrace();
            printParentStackTrace(System.err);
        }
    }


    /**
     * Prints this Exception and its backtrace to the 
     * specified print stream.
     */
    public void printStackTrace(java.io.PrintStream s) { 
        synchronized (s) {
            super.printStackTrace(s);
            printParentStackTrace(s);
        }
    }


    /**
     * Prints this Exception and its backtrace to the 
     * specified print writer.
     **/
    public void printStackTrace( java.io.PrintWriter s ) { 
        synchronized (s) {
            super.printStackTrace(s);
            printParentStackTrace(s);
        }
    }


//---------------------------- private members ---------------------------------

    private static final String NEWLINE = System.getProperty( "line.separator" );

    private Throwable rootCause;


    private void printParentStackTrace(PrintStream s) {
        s.println(getParentStackTrace());
    }


    private void printParentStackTrace(PrintWriter s) {
        s.println(getParentStackTrace());
    }


    /**
     * Returns a concatenated stack trace 
     **/
    private String getParentStackTrace() {
        StringBuffer sb = new StringBuffer();
        if (rootCause != null) {
            CharArrayWriter writer = new CharArrayWriter();
            rootCause.printStackTrace(new PrintWriter(writer));
            sb.append("With root cause:").append(NEWLINE);
            sb.append(writer.toString());
        } 
        return sb.toString();
    }

}
