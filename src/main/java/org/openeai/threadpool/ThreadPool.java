/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.threadpool;

// The interface to be implemented by the thread pool.
public interface ThreadPool {
/**
  * Adds a 'job' to the ThreadPool to be executed in a Thread.  If the maximum number
  * of threads for this pool has not been reached and an idle thread can't be found,
  * this method will create a new thread and run the job in that thread.
  * If it can't find an idle thread and it can't create a new one, it will check
  * to see if it is allowed to add the job to a Vector of "pending" jobs so the job will be ran when 
  * there is room in the pool.  If it is not allowed to add jobs to the Vector of pending
  * jobs, it will throw an exception.
  *<P>
  * @param job java.lang.Runnable the job to be added to the ThreadPool
  * @throws - ThreadPoolException if it has any problems adding the job to the ThreadPool.
  * @see #checkBeforeProcessing()
  */
	public void addJob(java.lang.Runnable job) throws ThreadPoolException;
  /**
   * Takes a "snapshot" of the threadpool at the current time and returns statistics.
   * @return Stats
   * @see Stats
   */
	public Stats getStats();
  /**
  * Returns the number of jobs that are currently in progress for this ThreadPool.
  * @return int the number of jobs in progress (available threads - idle threads)
  */
  public int getJobsInProgress();
  /**
  * Returns a flag indicating whether this ThreadPool should check itself before adding another
  * job to the pool.  Based on this flag, if the thread pool can't find an idle thread and it can't
  * create a new one because the maximum number of threads have already been allocated, it may or may not
  * add the job to the list of "pending" jobs.
  *<P>
  * This is used to verify that the ThreadPool can actually accept and immediately process
  * a job that's added via the addJob method.  This is to reduce the number of potential
  * jobs in memory in a "pending" state waiting for the ThreadPool to free up some idle threads.
  *<P>
  * @return boolean true=make sure there's room in the pool before adding to the list of pending jobs.
  */
  public boolean checkBeforeProcessing();
}
