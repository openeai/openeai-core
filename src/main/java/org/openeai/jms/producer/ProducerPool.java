/*******************************************************************************
 $Source$
 $Revision: 856 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.util.*;
import javax.jms.*;
import org.openeai.*;

/**
 * The ProducerPool is a class that contains a list of producers for use by an application or command.
 * It is very similar to a Database Connection pool in that it is filled with pre-configured and started
 * producers when the application starts up.  This is to allow developers to affect the performance of
 * an application by adding producers to a pool relative to the amount of processing and number of threads
 * present in the application.  Since OpenEAI producers are by default thread safe, it does not do anything
 * to limit the number of threads accessing a producer at the same time.  It simply makes the number of producers
 * producing to the same queue higher so their will be a smaller number of synchronizations occurring within
 * the producers (when a message is produced and Session synchronization occurrs).
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.config.ProducerConfig
 */
public class ProducerPool extends OpenEaiObject {
  private java.util.List m_producers = Collections.synchronizedList(new ArrayList());
  private int m_producerIndex = 0;
  private HashMap m_producerStatuses = new HashMap();
  private static String STARTED = "Started";
  private static String STOPPED = "Stopped";
  private ArrayList m_inUseProducers = new ArrayList();

  /**
   * Constructor
   */
  public ProducerPool() {
  }
  
  public ProducerPool(java.util.List vProducers) {
    if (vProducers == null) {
      setProducers(Collections.synchronizedList(new ArrayList()));
    }
    else {
      setProducers(vProducers);
    }
  }

  /**
  * Sets the list of OpenEAI producers to what's passed in.
  * <P>
  * @param vProducers java.util.List
  **/
  public final void setProducers(java.util.List vProducers) {
    m_producers = vProducers;
  }
  /**
  * Returns the list of OpenEAI producers associated to this pool.
  * <P>
  * @return java.util.List
  **/
  public final java.util.List getProducers() {
    return m_producers;
  }

  /**
  * Returns an OpenEAI producer from the list of producers associated to this
  * pool from the index specified.  This is a convenience method.
  * <P>
  * @return Object that must be cast to the appropriate type of producer (PointToPoint
  * or PubSub Producer).  
  **/
  private Object getProducer(int index) {
    return getProducers().get(index);
  }

  /**
  * Returns the size of the pool.
  * <P>
  * @return int the size of the pool.
  **/
  public final int size() {
    return getProducers().size();
  }

  private final void setProducerIndex(int index) {
    m_producerIndex = index;
  }
  private final int getProducerIndex() {
    return m_producerIndex;
  }
  private void incrementProducerIndex() {
    m_producerIndex++;
  }

  /**
  * Returns the next producer from the pool.  This is determined simply by maintaining
  * and counter indicating the last producer returned.  Since producers are thread safe
  * it does not attempt to ensure that only one request is using a given producer at 
  * a time.
  * <P>
  * If a producer is not started, it will not be returned.
  * <P>
  * @return MessageProducer either a PointToPoint or PubSubProducer
  * @throws JMSException if no started producers can be found in the pool.
  **/
  public final synchronized MessageProducer getProducer() throws JMSException {
  	if (getProducerIndex() >= getProducers().size() - 1) {
	  	setProducerIndex(0);
		}
		else {
		  incrementProducerIndex();
		}
    logger.debug("ProducerPool:  returning producer from index " + getProducerIndex());
    MessageProducer aProducer = (MessageProducer)getProducer(getProducerIndex());
    if (aProducer.isStarted()) {
      m_producerStatuses.put(Integer.toString(getProducerIndex()), STARTED);
      return aProducer;
    }
    else {
      logger.warn("Producer I was attempting to return is not started, can't return it.  Returning next available producer.");
      m_producerStatuses.put(Integer.toString(getProducerIndex()), STOPPED);
      if (m_producerStatuses.values().contains("Started")) {
        return getProducer();
      }
      else {
        logger.fatal("No 'Started' producers exist in the ProducerPool.");
        throw new JMSException("No 'Started' producers exist in the ProducerPool.");
      }
    }
  }

  /**
  * Returns an EXCLUSIVE producer from the pool.  This is most often required when 
  * a high-volume gateway or application needs to use a producer pool filled with 
  * PubSubProducers that will be transacted.  In this situation, you want to make sure
  * you've got an exclusive hold on the producer you're using to publish the message with 
  * because when you perform a "commit" on the producer, you'll be committing everything 
  * published with that producer.  If you have multiple threads using that producer who 
  * have all published messages but not committed them, they will all be committed when 
  * any of those threads performs a commit on the producer (much like a database connection).
  * <P>
  * By obtaining an exclusive producer, you're ensuring that the thread requesting the 
  * producer from the pool is the only thread that should be using that producer at that 
  * time and therefore anything committed on that producer will be related to the messages 
  * published by that thread only.
  * <P>
  * Generally, this method SHOULD NOT be called if the pool contains PointToPointProducers because 
  * there is no need to obtain an exclusive producer in that case.
  * <P>
  * When the getExclusiveProducer method is called, a flag is set so that any subsequent 
  * retrievals from the pool will not return the same producer until the producer has 
  * been released back to the pool via the releaseProducer(MessageProducer) method.
  * <P>
  * This method DOES NOT attempt to create a new producer if there are none available in the 
  * pool.  Instead, it "blocks" until one becomes available.  Because of this, it is critical 
  * that if using this method, you promptly release the producer back to the pool when you're 
  * done with it.  Otherwise, all producers will become unavailable and it will be impossible 
  * to retrieve any producers from this particular pool resulting in an endless loop.
  * <P>
  * NOTE:  Applications using the <b>getExclusiveProducer</b> method on a ProducerPool should 
  * NOT use the <b>getProducerMethod</b> on that same pool.  This will confuse things and may 
  * result in un-expected behavior since the getProducer method doesn't care about multiple 
  * threads accessing the same producer.
  * <P>
  * @return MessageProducer either a PointToPoint or PubSubProducer
  * @throws JMSException if no started producers can be found in the pool.
  * @see #releaseProducer(MessageProducer)
  * @see #getProducer()
  **/
  public final MessageProducer getExclusiveProducer() throws JMSException {
    // see if producer is in use (by index)
    boolean cont = true;
    
    // should this be a "for" loop so we don't get into an endless loop?????
    while (cont) {
      for (int i=0; i<getProducers().size(); i++) {
        if (m_inUseProducers.contains(Integer.toString(i))) {
          // skip to the next one
          logger.debug("Producer " + i + " is already in-use.  Continuing to next producer.");
          continue;
        }
        else {
          // add the current index to the list and return the producer
          MessageProducer aProducer = (MessageProducer)getProducer(i);
          if (aProducer.isStarted()) {
            synchronized (m_producerStatuses) {
              m_producerStatuses.put(Integer.toString(i), STARTED);
            }
            synchronized (m_inUseProducers) {
              m_inUseProducers.add(Integer.toString(i));
            }
            logger.debug("Returning exclusive producer " + i + " from the pool");
            return aProducer;
          }
          else {
            logger.warn("Producer I was attempting to return is not started, can't return it.  Returning next available producer.");
            m_producerStatuses.put(Integer.toString(i), STOPPED);
            if (m_producerStatuses.values().contains("Started")) {
              return getExclusiveProducer();
            }
            else {
              logger.fatal("No 'Started' producers exist in the ProducerPool.");
              throw new JMSException("No 'Started' producers exist in the ProducerPool.");
            }
          }
        }
      }
      // start - experimenting when starting another producer...
      /*
        Note, if we decide to do this there are other changes that need to be made 
        based on the way it's currently coded:
          - MessageProducer needs to be abstract and have two new methods (stop and start)
          - PubSub and PointToPoint producers need to implement the start and stop methods 
            by calling the appropriate start/stop Publisher and start/stop Prodcer methods.
      */
      /*
      try {
        MessageProducer obj = (MessageProducer)getProducer(0);
        String className = obj.getClass().getName();
        MessageProducer newProducer = (MessageProducer)Class.forName(className).newInstance();
        Properties props = obj.getProperties();
        logger.info("=========  Starting a new producer with properties: " + props);
        newProducer.init(props);
        logger.info("connection factory name for new producer: " + newProducer.getConnectionFactoryName());
        newProducer.start();
        if (newProducer instanceof PubSubProducer) {
          PubSubProducer newPubSub = (PubSubProducer)newProducer;
  
          if (newPubSub.getLoggingProducer() != null) {
            if (newPubSub.getLoggingProducer().isStarted()) {
              PubSubProducer newLoggingProducer = new PubSubProducer();
              Properties loggingProperties = newPubSub.getLoggingProducer().getProperties();
              newLoggingProducer.init(loggingProperties);
              newLoggingProducer.startPublisher();
              newPubSub.setLoggingProducer(newLoggingProducer);
            }
          }
          logger.info("============== returning newly created PubSubProducer ==============");
          return newPubSub;
        }
        else {
          return newProducer;
        }
      }
      catch (Exception e) {
        logger.fatal("Exception starting new PubSubProducer.", e);
        throw new JMSException(e.getMessage());
      }
      */
      // end - experimenting when starting another producer...
      logger.debug("No available producers have been found yet.  Re-starting the loop...");
    }
    // if we get here, something's wrong
    String msg = "No available producers could be retrieved from the pool.  " + 
      "This is bad!  Please make sure all producers retrieved via this method " + 
      "are being released appropriately via the 'releaseProducer' method";
    logger.fatal(msg);
    throw new JMSException(msg);
  }
  /**
  * Release a previously retrieved Exclusive producer to the pool.  This method 
  * should be called promptly after you're finished with the exclusive producer 
  * you previously rertrieved (via the getExclusiveProducer method).  If you fail 
  * to release the producer, all producers in this pool will become unavailable 
  * and your application will find itself in an endless "hung" situation once 
  * all producers have been retrieved exclusively.
  * <P>
  * @param producer the MessageProducer to release back to the pool.
  **/
  public final void releaseProducer(MessageProducer producer) {
    for (int i=0; i<getProducers().size(); i++) {
      MessageProducer prd = (MessageProducer)getProducer(i);
      if (prd.equals(producer)) {
        logger.debug("releasing producer " + i + " back to the pool.");
        synchronized (m_inUseProducers) {
          m_inUseProducers.remove(Integer.toString(i));
        }
        return;
      }
    }    
    logger.warn("did not find a producer matching the one passed in to remove!");
    /*
      The following should be uncommented if we decide to go with creating a new
      producer if the pool is 'busy'.
    */
//    logger.warn("did not find a producer matching the one passed in to remove, just stopping the producer!");
//    producer.stop();
  }
}

