/*******************************************************************************
 $Source$
 $Revision: 3553 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.io.*;
import java.util.*;

// Java Messaging Service
import javax.jms.*;

// JNDI Stuff
import javax.naming.*;
import javax.naming.directory.*;

import org.openeai.*;
import org.openeai.loggingutils.*;
import org.openeai.transport.ProducerId;

/**
 * The ancestor class for PubSub and PointToPoint producers.  These are the counterparts
 * to OpenEAI MessageConsumer.  These classes (PubSub and PointToPoint Producers) produce messages to
 * and potentially handle responses from OpenEAI MessageConsumers (PointToPoint and PubSub
 * Producers)
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.config.ProducerConfig
 * @see org.openeai.jms.consumer.MessageConsumer
 */
public abstract class MessageProducer
extends OpenEaiObject {

	protected HashMap m_messages;
	private String m_userName;
	private String m_password;
	private String m_providerUrl;
	private String m_initCtxFactory;
  private String m_producerName = "";
	private String m_securityPrincipal = "";
	private String m_securityCredentials = "";
  private String m_defaultCommandName = "";
  private String m_acknowledgementMode = "";
  private String m_deliveryMode = "";
	private String m_producerStatus = STOPPED;

  protected static String STOPPED = "Stopped";
  protected static String STOPPING = "Stopping";
  protected static String STARTED = "Started";
  protected static String PERSISTENT_DELIVERY = "PERSISTENT";
  protected static String NON_PERSISTENT_DELIVERY = "NON-PERSISTENT";
  public static String MESSAGE_ID = "MESSAGE_ID";
  public static String COMMAND_NAME = "COMMAND_NAME";
  public static String MESSAGE_NAME = "MESSAGE_NAME";

	private String m_connectionFactoryName = null;
	private String m_destinationName = null;

	private ProducerId m_producerId = null;
  private org.openeai.jms.producer.ProducerId m_oldProducerId = null;
	private String m_producerIdUrl = "";
	private int m_messageSeq = 0;
	private boolean m_startOnInitialization = false;	// default
	private boolean m_transacted = false;	// default

	public MessageProducer() {
	}

  /**
  * Sets the name of this producers as specified in its configuration
  * information.
  * <P>
  * @param name String name of the producer.
  **/
	public final void setProducerName(String name) {
		m_producerName = name;
	}
  /**
  * Returns the name of this producers as specified in its configuration
  * information.
  * <P>
  * @return String name of the producer.
  **/
	public final String getProducerName() {
		return m_producerName;
	}

	public final void setAcknowledgementMode(String mode) {
		m_acknowledgementMode = mode;
	}
	public final String getAcknowledgementMode() {
		return m_acknowledgementMode;
	}

	public final void setDeliveryMode(String mode) {
		m_deliveryMode = mode;
	}
	public final String getDeliveryMode() {
		return m_deliveryMode;
	}

	public final void setTransacted(boolean trans) {
		m_transacted = trans;
	}
	public final boolean getTransacted() {
		return m_transacted;
	}

	public final void setDefaultCommandName(String defaultCommand) {
		m_defaultCommandName = defaultCommand;
	}
	public final String getDefaultCommandName() {
		return m_defaultCommandName;
	}

  /**
  * Sets the LDAP security principal this producer should use when connecting
  * to the directory server to retrieve JMS administered objects 
  * (ConnectionFactories and Destinations).  This is specified in the Producers's
  * configuration element (ProducerConfig).
  * <P>
  * @param principal String the LDAP principal
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setSecurityPrincipal(String principal) {
		m_securityPrincipal = principal;
	}
  /**
  * Returns the LDAP security principal this producer should use when connecting
  * to the directory server to retrieve JMS administered objects 
  * (ConnectionFactories and Destinations).  This is specified in the Producers's
  * configuration element (ProducerConfig).
  * <P>
  * @return String the LDAP principal
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getSecurityPrincipal() {
		return m_securityPrincipal;
	}

  /**
  * Sets the LDAP security credentials (password) this producer should use when connecting
  * to the directory server to retrieve JMS administered objects 
  * (ConnectionFactories and Destinations).  This is specified in the Producers's
  * configuration element (ProducerConfig).
  * <P>
  * @param credentials String the LDAP credentials
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setSecurityCredentials(String credentials) {
		m_securityCredentials = credentials;
	}
  /**
  * Returns the LDAP security credentials (password) this producer should use when connecting
  * to the directory server to retrieve JMS administered objects 
  * (ConnectionFactories and Destinations).  This is specified in the Producers's
  * configuration element (ProducerConfig).
  * <P>
  * @return String the LDAP credentials
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getSecurityCredentials() {
		return m_securityCredentials;
	}

  /**
  * Returns the ProducerId object associated to this producer. 
  * <P>
  * @return org.openeai.jms.producer.ProducerId old producer id
  * @deprecated as of OpenEAI 4.0 use org.openeai.transport.ProducerId instead
  **/
	public org.openeai.jms.producer.ProducerId getProducerId() {
		if (m_oldProducerId == null) {
			m_oldProducerId = new org.openeai.jms.producer.ProducerId();
		}
    m_oldProducerId.setId(m_producerId.getId());
    return m_oldProducerId;
	}
  /**
  * Sets the ProducerId object associated to this producer. 
  * <P>
  * @param org.openeai.jms.producer.ProducerId old producer id
  * @deprecated as of OpenEAI 4.0 use org.openeai.transport.ProducerId instead
  **/
	public void setProducerId(org.openeai.jms.producer.ProducerId producerId) {
		if (producerId == null) {
			m_oldProducerId = new org.openeai.jms.producer.ProducerId();
		}
		else {
			m_oldProducerId = producerId;
		}
    m_producerId = new org.openeai.transport.ProducerId();
    m_producerId.setId(m_oldProducerId.getId());
	}

  /**
  * Returns the ProducerId object associated to this producer. 
  * <P>
  * NOTE:  This is a temporary method that will be deprecated in the next release
  * of the foundation.  The method is being 
  * @return org.openeai.transport.ProducerId the initialized producer id object 
  * associated to this producer.
  * @param dummyParm a parameter used just to make this method declaration different 
  * than the old deprecated method.  Once the old getProducerId() method is completely 
  * removed, this method will be deprecated and a new getProducerId() method will be
  * used inplace of the old getProducerId() method.  Finally, this method will 
  * be completely removed in future releases (tentatively Release 6.0).
  **/
	public org.openeai.transport.ProducerId getProducerId(String dummyParm) {
		return m_producerId;
	}
  /**
  * Sets the ProducerId object associated to this producer. 
  * <P>
  * @param org.openeai.transport.ProducerId old producer id
  **/
	public void setProducerId(org.openeai.transport.ProducerId producerId) {
		if (producerId == null) {
			m_producerId = new org.openeai.transport.ProducerId();
		}
		else {
			m_producerId = producerId;
		}
	}

  /**
  * Returns a boolean indicating whether or not this producer should be started 
  * (connected to the broker and ready to produce messages) when it is initialized.
  * This is specified in the ProducerConfig Element (startOnInitialization Attribute) 
  * of the deployment document for the application being started.
  * <P>
  * @return boolean
  * @see org.openeai.config.ProducerConfig
  **/
	public final boolean getStartOnInitialization() {
		return m_startOnInitialization;
	}
  /**
  * Sets a boolean indicating whether or not this producer should be started 
  * (connected to the broker and ready to produce messages) when it is initialized.
  * This is specified in the ProducerConfig Element (startOnInitialization Attribute) 
  * of the deployment document for the application being started.
  * <P>
  * @return boolean
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setStartOnInitialization(boolean start) {
		m_startOnInitialization = start;
	}

	public final String getProducerIdUrl() {
		return m_producerIdUrl;
	}
	public final void setProducerIdUrl(String url) {
		if (url == null) {
			m_producerIdUrl = "";
		}
		else {
			m_producerIdUrl = url;
		}
	}

	public final synchronized int incrementMessageSequence() {
    setMessageSeq(getMessageSeq() + 1);
    return getMessageSeq();
	}
	public final void setMessageSeq(int seq) {
		m_messageSeq = seq;
	}
	public final int getMessageSeq() {
		return m_messageSeq;
	}

	public final String getProducerStatus() {
		return m_producerStatus;
	}
	public final void setProducerStatus(String status) {
		m_producerStatus = status;
	}
	public final boolean isStarted() {
		if (getProducerStatus().equals(STARTED)) {
			return true;
		}
		else {
			return false;
		}
	}

  public abstract boolean start() throws JMSException;
  public abstract void stop();

  /**
  * Returns the connection factory name (QueueConnectionFactory or TopicConnectionFactory)
  * that this producer should retrieve and use to initialize its connection to the
  * broker with.
  * <P>
  * See JMS Specification for a description of Connection Factories.
  * <P>
  * @return String name of the Connection Factory.
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getConnectionFactoryName() {
		return m_connectionFactoryName;
	}
  /**
  * Sets the connection factory name (QueueConnectionFactory or TopicConnectionFactory)
  * that this producer should retrieve and use to initialize its connection to the
  * broker with.
  * <P>
  * See JMS Specification for a description of Connection Factories.
  * <P>
  * @param name String name of the Connection Factory.
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setConnectionFactoryName(String name) {
		m_connectionFactoryName = name;
	}

  /**
  * Returns the destination name (Queue or Topic) that this Producer should connect
  * to and consume from.
  * <P>
  * See JMS Specification for a description of Destinations.
  * <P>
  * @return String the destination (Queue or Topic) name.
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getDestinationName() {
		return m_destinationName;
	}
  /**
  * Sets the destination name (Queue or Topic) that this Producer should connect
  * to and consume from.
  * <P>
  * See JMS Specification for a description of Destinations.
  * <P>
  * @param name String the destination (Queue or Topic) name.
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setDestinationName(String name) {
		m_destinationName = name;
	}

  /**
  * Sets the broker user name that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Producers and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ProducerConfig
  * Element in the deployment document.
  * <P>
  * @param in String broker user name
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setUserName(String in) {
		m_userName = in;
	}
  /**
  * Returns the broker user name that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Producers and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ProducerConfig
  * Element in the deployment document.
  * <P>
  * @return  String broker user name
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getUserName() {
		return m_userName;
	}

  /**
  * Sets the broker user password that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Producer and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ProducerConfig
  * Element in the deployment document.
  * <P>
  * @param in String broker user name
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setPassword(String in) {
		m_password = in;
	}
  /**
  * Sets the broker user password that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Producer and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ProducerConfig
  * Element in the deployment document.
  * <P>
  * @return String broker user name
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getPassword() {
		return m_password;
	}

  /**
  * Sets the location where the administered objects can be retrieved from.  
  * To maintain broker independance, this should reference a location in a 
  * Directory server or some other JNDI resource.
  * <P>
  * @param in String the URL that resolves to the location of the JMS Administered Objects
  * (ConnectionFactories and Destinations).
  * @see org.openeai.config.ProducerConfig
  **/
	public final void setProviderUrl(String in) {
		m_providerUrl = in;
	}
  /**
  * Returns the location where the administered objects can be retrieved from.  
  * To maintain broker independance, this should reference a location in a 
  * Directory server or some other JNDI resource.
  * <P>
  * @return String the URL that resolves to the location of the JMS Administered Objects
  * (ConnectionFactories and Destinations).
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getProviderUrl() {
		return m_providerUrl;
	}

  /**
  * Sets the class name of the JNDI context factory that will be used to retrieve the JMS
  * administered objects from a Directory Server or wherever the administered
  * objects are stored.
  * <P>
  * Typically, this is com.sun.jndi.ldap.LdapCtxFactory.
  * <P>
  * @param in String initial context factory class name.
  * @see org.openeai.config.ConsumerConfig
  **/
	public final void setInitialContextFactory(String in) {
		m_initCtxFactory = in;
	}
  /**
  * Returns the class name of the JNDI context factory that will be used to retrieve the JMS
  * administered objects from a Directory Server or wherever the administered
  * objects are stored.
  * <P>
  * Typically, this is com.sun.jndi.ldap.LdapCtxFactory.
  * <P>
  * @return String initial context factory class name.
  * @see org.openeai.config.ProducerConfig
  **/
	public final String getInitialContextFactory() {
		return m_initCtxFactory;
	}

  /**
  * Establishes and returns an InitialContext to be used by the calling application to
  * retrieve ConnectionFactories and Destination objects via JNDI.
  * @return        DirContext the InitialContext object established
  **/
	protected synchronized DirContext getInitialContext() throws NamingException {
		DirContext ic = null;
		Hashtable env = new Hashtable(5, 0.75f);
		logger.debug("Getting initial context");

    boolean useSsl = false;
    String tempProviderUrl = getProviderUrl();
    if (getProviderUrl().indexOf("ldaps:") != -1) {
      useSsl = true;
      // Replace the ldaps with ldap
      tempProviderUrl = "ldap:" + getProviderUrl().substring(getProviderUrl().indexOf(":") + 1);
//      setProviderUrl(newProviderUrl);
      logger.debug("ProviderUrl was changed to " + tempProviderUrl);
    }

		env.put(Context.INITIAL_CONTEXT_FACTORY, getInitialContextFactory());
		logger.debug("Set initCtxFactory to " + getInitialContextFactory());
		env.put(Context.PROVIDER_URL, tempProviderUrl);
		logger.debug("Set providerUrl to " + tempProviderUrl);

    if (useSsl) {
      // Bind via ssl
 	  	logger.debug("Setting SECURITY_PROTOCOL to ssl");
      env.put(Context.SECURITY_PROTOCOL, "ssl");
    }

		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, getSecurityPrincipal());
		env.put(Context.SECURITY_CREDENTIALS, getSecurityCredentials());

		logger.info("Creating a new InitialContext object (ie - connecting to the DirectoryServer to retrieve Administered Object)...");

    Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
 		ic = new InitialDirContext(env);
		logger.info("Created the InitialContext object...");
		return ic;
	}

	private void initializeProperties(Properties props) throws IOException {
		try {
			setProperties(props);

			// Mail Service properties
			setMailHost(props.getProperty("mailHost"));
			setFromAddr(props.getProperty("fromAddr"));
			setToAddr(props.getProperty("toAddr"));
			if (getMailHost() != null && getFromAddr() != null && getToAddr() != null) {
				setMailService(new MailService(getMailHost(), getFromAddr(), getToAddr()));	// For emailing
			}

			setDebug(new Boolean(props.getProperty("debug", "false")).booleanValue());
			setStartOnInitialization(new Boolean(props.getProperty("startOnInitialization","false")).booleanValue());

			setUserName(props.getProperty("Username", ""));
			setPassword(props.getProperty("Password", ""));

			setProducerName(props.getProperty("name",""));
			setInitialContextFactory(props.getProperty("InitialContextFactory", ""));
			setProviderUrl(props.getProperty("ProviderURL", ""));
			setProducerIdUrl(props.getProperty("ProducerIdURL",""));
			setSecurityPrincipal(props.getProperty("SecurityPrincipal", ""));
			setSecurityCredentials(props.getProperty("SecurityCredentials", ""));
			setConnectionFactoryName(props.getProperty("ConnectionFactoryName",""));
			setDestinationName(props.getProperty("DestinationName"));
      setDefaultCommandName(props.getProperty("DefaultCommandName",""));
      setAcknowledgementMode(props.getProperty("acknowledgementMode","AUTO_ACKNOWLEDGE"));
      setDeliveryMode(props.getProperty("deliveryMode","PERSISTENT"));
			setTransacted(new Boolean(props.getProperty("transacted", "false")).booleanValue());
		}
    catch (Exception e) {
      logger.fatal(e.getMessage(), e);
      throw new IOException(e.getMessage());
    }
	}

  /**
  * Takes the Properties object contained within the ProducerConfig object passed
  * to the constructor of this Producer and sets all the appropriate instance variables
  * on this producer during initialization.
  * <P>
  * @param props Properties the Java properties object retrieved from the ProducerConfig
  * object built from the ProducerConfig Element in the gateway's deployment document.
  * @throws IOException if errors occur populating the instance variables from the 
  * Properties object.
  **/
	protected void init(Properties props) throws IOException {
		initializeProperties(props);
	}
}
