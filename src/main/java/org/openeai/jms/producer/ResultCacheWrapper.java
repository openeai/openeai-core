/*******************************************************************************
 $Source$
 $Revision: 898 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2013 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.util.Properties;

import org.openeai.*;


/**
 * An wrapper for implementing a singleton request cache. It is necessary for
 * all instances of a producer in a producer pool to use the same result cache 
 * for optimal results. Using this wrapper and the factory functor each 
 * producer can get a reference to the same result cache.
 * <P>
 * @author      Steve Wheat (swheat@openeai.org)
 * @version     1.0  -  20 April 2013
 */
final public class ResultCacheWrapper extends OpenEaiObject {

  private static ResultCacheFactoryFunctor _factory = null;
  private static ResultCache _instance = null;

  private static ResultCache makeInstance(String className, MessageProducer mp,
    Properties initProps) throws ResultCacheException {
    ResultCache resultCache = null;
    try {
      resultCache = (ResultCache)Class.forName(className).newInstance();
    }
    catch(ClassNotFoundException cnfe) {
      // An error occurred locating the result cache class specified.
      // Log it and throw an exception. Note, the message of a ClassNotFound
      // exception is not very clear, so provide the user with a little more
      // information.
      String errMsg = "An error occurred locating the ResultCache " +
        "implementation class spcified. ClassNotFoundException: " +
        cnfe.getMessage();
      logger.fatal("[ResultCacheWrapper.makeInstance] " + errMsg);
      throw new ResultCacheException(errMsg);
    }
    catch(InstantiationException ie) {
      // An error occurred instantiating a ResultCache. Log it and throw
      // an exception.
      String errMsg = "An error occurred instantiating a ResultCache.  " +
        "InstantiationException: " + ie.getMessage();
      logger.fatal("[ResultCacheWrapper.makeInstance] " + errMsg);
      throw new ResultCacheException(errMsg);
    }
    catch(IllegalAccessException iae) {
      // An error occurred instantiating a ResultCache. Log it and throw
      // and exception.
      String errMsg = "An error occurred instantiating a ResultCache. " +
        "IllegalAccessException: " + iae.getMessage();
      logger.fatal("[ResultCacheWrapper.makeInstance] " + errMsg);
      throw new ResultCacheException(errMsg);
    }
    resultCache.init(mp, initProps);
    return resultCache;
  }

  public static synchronized ResultCache getInstance(String className, 
    MessageProducer mp, Properties initProps) throws ResultCacheException {
    if (null == _instance) {
      _instance = (null == _factory) ? makeInstance(className, mp, initProps) :
      _factory.makeInstance();
    }
    return _instance;
  }

  public static synchronized void setFactory(ResultCacheFactoryFunctor
    factory) {
    _factory = factory;
  }

  public static synchronized void setInstnace(ResultCache instance) {
    _instance = instance;
  }
}