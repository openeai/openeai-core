/*******************************************************************************
 $Source$
 $Revision: 1315 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.io.*;
import javax.jms.*;
import org.openeai.*;

/**
 * This is a class that wraps a JMS TemporaryQueue and QueueReceiver that listens for responses
 * sent on the TemporaryQueue.  It is instantiated and stored in the TemporaryQueuePool for use
 * by PointToPointProducers.  These objects are created when the producer is initialized and retrieved
 * from the pool by the producer's QueueRequestor when requests are produced.  This is done so message
 * production doesn't have to be slowed down by the creation of the temporary queues each time a request
 * is produced.  The TempQueuePool is responsible for determining if a PooledTempQueue is available
 * and if not, it will create a new one for use by the current request.  This way, there will never be
 * two threads attempting to use the same TempoaryQueue at the same time.
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.jms.producer.QueueRequestor
 * @see org.openeai.jms.producer.PointToPointProducer
 * @see org.openeai.jms.producer.TempQueuePool
 */
public class PooledTempQueue extends OpenEaiObject {

  private boolean m_inUse = false;
  private String m_queueId = "";
  private TemporaryQueue m_tempQueue = null;
  private QueueReceiver m_receiver = null;

  /**
   * Constructor, creates a TemporaryQueue and QueueReceiver that will be 
   * used when the OpenEAI QueueRequestor produces a request.  It will 
   * retrieve this PooledTempQueue from it's TempQueuePool and use the resources
   * wrapped by this class (the TemporaryQueue and QueueReceiver).
   */
  public PooledTempQueue(QueueSession session) throws JMSException {
    setTemporaryQueue(session.createTemporaryQueue());
    setQueueId(getTemporaryQueue().getQueueName());
    setQueueReceiver(session.createReceiver(getTemporaryQueue()));
    setInUse(false);
  }

  /**
  * Sets the Queue Id associated to this PooledTempQueue.  This is the TemporaryQueue's
  * name and is set when this object is constructed after the TemporaryQueue has
  * been created..
  * <P>
  * @param id String the queue id
  **/
  public final void setQueueId(String id) {
    m_queueId = id;
  }
  /**
  * Returns the Queue Id associated to this PooledTempQueue.  This is the TemporaryQueue's
  * name and is set when this object is constructed after the TemporaryQueue has
  * been created..
  * <P>
  * @return String the queue id
  **/
  public final String getQueueId() {
    return m_queueId;
  }

  /**
  * Returns true if this PooledTempQueue object is in use false if it's not.
  * This way, the TempQueuePool can keep track of PooledTempQueues that are 
  * in use by other requests being sent and create a new JMS resource if necessary.
  * <P>
  * @return boolean true if this object is in use, false if it's not.
  **/
  public final boolean inUse() {
    return m_inUse;
  }
  /**
  * Sets the inUse flag associated to this PooledTempQueue.
  * This way, the TempQueuePool can keep track of PooledTempQueues that are 
  * in use by other requests being sent and create a new JMS resource if necessary.
  * <P>
  * This is set to true each time a PooledTempQueue is retreived from the TempQueuePool 
  * and set to false when the PooledTempQueue is released.  These objects are retrieved
  * and released from the TempQueuePool by the OpenEAI QueueRequestor when requests
  * are produced.
  * <P>
  * @param inUse boolean true if this object is in use, false if it's not.
  **/
  public final void setInUse(boolean inUse) {
    m_inUse = inUse;
  }

  /**
  * Sets the JMS TempoaryQueue object associated to the PooledTempQueue.  This
  * is called when the PooledTempQueue is constructed and it creates this JMS resource.
  * <P>
  * @param tempQueue javax.jms.TempoaryQueue
  **/
  public final void setTemporaryQueue(TemporaryQueue tempQueue) {
    m_tempQueue = tempQueue;
  }
  /**
  * Returns the JMS TempoaryQueue object associated to the PooledTempQueue.  This
  * is called when the QueueRequestor retrieves one of these PooledTempQueues from
  * the TempQueuePool for request message production.
  * <P>
  * @return javax.jms.TempoaryQueue
  **/
  public final TemporaryQueue getTemporaryQueue() {
    return m_tempQueue;
  }

  /**
  * Sets the JMS QueueReceiver object associated to the PooledTempQueue.  This
  * is called when the PooledTempQueue is constructed and it creates this JMS resource.
  * <P>
  * @param receiver javax.jms.QueueReceiver
  **/
  public final void setQueueReceiver(QueueReceiver receiver) {
    m_receiver = receiver;
  }
  /**
  * Returns the JMS QueueReceiver object associated to the PooledTempQueue.  This
  * is called when the QueueRequestor retrieves one of these PooledTempQueues from
  * the TempQueuePool for request message production.
  * <P>
  * @return javax.jms.QueueReceiver
  **/
  public final QueueReceiver getQueueReceiver() {
    return m_receiver;
  }

  /**
  * Frees up the resources allocated on behalf of the JMS resources.  Deletes the
  * TempoaryQueue and QueueReceiver objects associated to this PooledTempQueue.
  * <P>
  * @throws JMSException if errors occur deleting and closing the JMS resources.
  **/
  public final void delete() throws JMSException {
    String exceptionMessage = "";
    String exceptionStackTrace = "";
    String exceptionToThrow = "";
    int exceptionNumber = 0;
    boolean exceptionOccurred = false;

    if (m_receiver != null) {
      try {
        m_receiver.close();
      }
      catch (NullPointerException npe) { }
      catch (Exception e) {
        logger.fatal("Exception deleting QueueReceiver in PooledTempQueue " + getQueueId());
        logger.fatal(e.getMessage(), e);
        exceptionNumber++;
        exceptionOccurred = true;
        exceptionMessage = e.getMessage();
        ByteArrayOutputStream bw = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(bw, true);
        e.printStackTrace(pw);
        exceptionStackTrace = bw.toString();
        exceptionToThrow = exceptionToThrow + "Exception Message [" + exceptionNumber + "] " + exceptionMessage + "\n";
        exceptionToThrow = exceptionToThrow + "Exception Stack [" + exceptionNumber + "] " + exceptionStackTrace + "\n";
      }
    }
    if (m_tempQueue != null) {
      try {
        m_tempQueue.delete();
      }
      catch (NullPointerException npe) { }
      catch (Exception e) {
        logger.fatal("Exception deleting TempoaryQueue in PooledTempQueue " + getQueueId());
        logger.fatal(e.getMessage(), e);
        exceptionNumber++;
        exceptionOccurred = true;
        exceptionMessage = e.getMessage();
        ByteArrayOutputStream bw = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(bw, true);
        e.printStackTrace(pw);
        exceptionStackTrace = bw.toString();
        exceptionToThrow = exceptionToThrow + "Exception Message [" + exceptionNumber + "] " + exceptionMessage + "\n";
        exceptionToThrow = exceptionToThrow + "Exception Stack [" + exceptionNumber + "] " + exceptionStackTrace + "\n";
      }
    }

    if (exceptionOccurred) {
      throw new JMSException(exceptionToThrow);
    }
  }
}

