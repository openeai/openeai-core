/*******************************************************************************
 $Source$
 $Revision: 898 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2013 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.uuid.RandomUuid;

/**
 * A simple implementation of ResultCache. Frequently, applications
 * that perform requests repeat these requests in rapid succession. Such
 * applications benefit from caching results and this can be accomplished
 * by the message producer. This cache is intended to be used by a message
 * producer or request service implementation.
 * <P>
 * @author      Steve Wheat (steve@openeai.org)
 * @version     5.0  - 20 April 2013
 */
public class SimpleResultCache extends OpenEaiObject implements
        ResultCache {
	private String m_cacheId = new RandomUuid().toString();
    private static Logger logger = Logger.getLogger(SimpleResultCache.class);
    private String LOGTAG = "[SimpleRequestCache-" + getCacheId() + "] ";
    private Hashtable<String, CachedResult> m_cache = new Hashtable();
    private long m_requestCount = 0;
    private long m_hitCount = 0;
    private long m_missCount = 0;
    private long m_expiredCount = 0;
    private MessageProducer m_producer = null;
    private boolean m_verbose = true;

    /**
     * @see ResultCache.java
     */
    public void init(MessageProducer mp, Properties props) throws ResultCacheException {
        logger.info(LOGTAG + "Initializing...");

        // Set the cache properties 
        // Get the value of the verbose property 
    	if (getProperties().getProperty("verboseCache", "false")
          .equalsIgnoreCase("true")) {
          
          setVerbose(true);
    	}
    	logger.info(LOGTAG + "The value of verboseCache is: " + getVerbose());
        logger.info(LOGTAG + "Initialization complete.");
        
        // Set the producer with which this cache is associated.
        setProducer(mp);
        
    }
    
    public synchronized List getResult(String key) {
    	// Update request count;
    	m_requestCount++;
    	if (getVerbose()) logger.info(LOGTAG + "cache request number: " + m_requestCount);
    	
    	CachedResult cr = m_cache.get(key);
    	if (cr != null) {
    		if (cr.isAlive()) {
    			if (getVerbose()) logger.info(LOGTAG + "Found result in cache " 
    				+ "for key: " + key);
    			m_hitCount++;
    			return cr.getResult();
    		}
    		// Otherwise, remove it from the cache and return null.
    		else {
    			if (getVerbose()) logger.info(LOGTAG + "Found expired result in"
    				+ " cache for key: " + key); 
    			m_expiredCount++;
    			m_cache.remove(key);
    			return null;
    		}
    	}
    	else {
    		if (getVerbose()) logger.info(LOGTAG + "No result found in cache " 
    			+ "for key: " + key);
    		m_missCount++;
    		return null;		
    	}
    }
    
    public synchronized void addResult(String producerName, XmlEnterpriseObject querySpec, 
    	ActionableEnterpriseObject object, List result, long timeToLive) throws
    	ResultCacheException {
    	
    	if (getVerbose())logger.info(LOGTAG + "Adding result to cache.");
    	CachedResult cr = new CachedResult(producerName, querySpec, object, result, timeToLive);
    	m_cache.put(cr.getKey(), cr);
    	if (getVerbose()) logger.info(LOGTAG + "\n" + getCacheStats());
    	
    }

	private boolean getVerbose() {
		return m_verbose;
	}

	private void setVerbose(boolean verbose) {
		m_verbose = verbose;
		
	}
	
	private long getRequestCount() {
		return m_requestCount;
	}
	
	private long getHitCount() {
		return m_hitCount;
	}
	
	private long getMissCount() {
		return m_missCount;
	}
	
	private long getExpiredCount() {
		return m_expiredCount;
	}
	
	private float getHitScore() {
		return getPercentage(getHitCount(), getRequestCount());
	}
	
	private float getMissScore() {
		return getPercentage(getMissCount(), getRequestCount());
	}
	
	private float getExpiredScore() {
		return getPercentage(getExpiredCount(), getRequestCount());
	}
	
	private String getCacheStats() {
		String stats = "         Cache Size: " + m_cache.size() + " results\n" +
					   "    Cache Hit Score: " + getHitScore() + " percent\n" +
					   "   Cache Miss Score: " + getMissScore() + " percent\n" +
				       "Cache Expired Score: " + getExpiredScore() + " percent\n" +
				       "    Cache Hit Count: " + getHitCount() + "\n" +
					   "   Cache Miss Count: " + getMissCount() + "\n" +
				       "Cache Expired Count: " + getExpiredCount() + "\n" +
				       "Cache Request Count: " + getRequestCount() + "\n";
		return stats;
					   
 	}
	
	/**
	 * Returns a percentage.
	 */
	public static float getPercentage(long part, long total) {
	    float proportion = ((float) part) / ((float) total);
	    return proportion * 100;
	}
	
	private String getCacheId() {
		return m_cacheId;
	}
	
	private void setProducer(MessageProducer mp) {
		m_producer = mp;
	}
	
	public String buildKey(String producerName, ActionableEnterpriseObject
		aeo, XmlEnterpriseObject querySpec) {
		
		String xmlString = null;
		try {
			xmlString =  querySpec.toXmlString();
		}
		catch (XmlEnterpriseObjectException xeoe) {
			String errMsg = "An error occurred converting the query spec " +
			  "to XML format. The exception is: " + xeoe.getMessage();
			logger.error(LOGTAG + errMsg);
		}
		
		String key = "<ProducerName>" + producerName + "</ProducerName>" +
                "<ObjectClassName>" + aeo.getClass().getName() + "</ObjectClassName>" +
			     "<QuerySpec>" + xmlString + "</QuerySpec>";
		return key;
		
	}
	
	
	private class CachedResult {		
		private String m_producerName = null;
		private XmlEnterpriseObject m_querySpec = null;
		private String m_key = null;
		private ActionableEnterpriseObject m_object = null;
		private List m_result = null;
		private long m_timeCached = 0;
		private long m_timeToLive = 0;
		
		CachedResult(String producerName, XmlEnterpriseObject querySpec, ActionableEnterpriseObject
			object, List result, long timeToLive) {
			m_producerName = producerName;
			m_querySpec = querySpec;
			m_object = object;
			m_result = result;
			m_timeCached = System.currentTimeMillis();
			m_timeToLive = timeToLive;
			if (getVerbose()) logger.info(LOGTAG + "New CachedResult: " + toString());													
		}
		
		private String getProducerName() {
			return m_producerName;
		}
		
		private XmlEnterpriseObject getQuerySpec() {
			return m_querySpec;
		}
		
		private String getQuerySpecXmlString() {
			String xmlString = null;
			try {
				xmlString =  m_querySpec.toXmlString();
			}
			catch (XmlEnterpriseObjectException xeoe) {
				String errMsg = "An error occurred converting the query spec " +
				  "to XML format. The exception is: " + xeoe.getMessage();
				logger.error(LOGTAG + errMsg);
			}
			return xmlString;
		}
		
		private String getKey() {
			String key = "<ProducerName>" + getProducerName() + "</ProducerName>" +
	                 "<ObjectClassName>" + getObject().getClass().getName() + "</ObjectClassName>" +
				     "<QuerySpec>" + getQuerySpecXmlString() + "</QuerySpec>";
			return key;
		}
		
		private ActionableEnterpriseObject getObject() {
			return m_object;
		}
		
		private String getObjectXmlString() {
			String xmlString = null;
			try {
				xmlString =  m_object.toXmlString();
			}
			catch (XmlEnterpriseObjectException xeoe) {
				String errMsg = "An error occurred converting the message " +
				  "object to XML format. The exception is: " + xeoe.getMessage();
				logger.error(LOGTAG + errMsg);
			}
			return xmlString;
		}
		
		private List getResult() {
			return m_result;
		}
		
		private long getTimeCached() {
			return m_timeCached;
		}
		
		private long getTimeToLive() {
			return m_timeToLive;
		}
		
		private long getAge() {
			return System.currentTimeMillis() - getTimeCached();
		}
		
		private boolean isAlive() {
			if (getAge() < getTimeToLive()) return true;
			else return false;
		}
		
		public String toString() {
			String cachedResult = "<ProducerName>" + getProducerName() + "</ProducerName>" +
								  "<QuerySpec>" + getQuerySpecXmlString() + "</QuerySpec>" +
								  "<Key>" + getKey() + "</Key>" +
								  "<Object>" + getObject().getClass().getName() + " (content suppressed)</Object>" +
								  "<Result>suppressed</Result>" +
								  "<CachedTime>" + getTimeCached() + "</CachedTime>" +
								  "<TimeToLive>" + getTimeToLive() + "</TimeToLive>";
			return cachedResult;
		}
								  
		
	}
 
}
