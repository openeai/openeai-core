/*******************************************************************************
 $Source$
 $Revision: 898 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2013 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.util.List;
import java.util.Properties;

import org.openeai.config.AppConfig;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;

/**
 * Defines an interface for a result cache. Frequently, applications
 * that perform requests repeat these requests in rapid succession. Such
 * applications benefit from caching results and this can be accomplished
 * by the message producer. This cache is intended to be used by a message
 * producer or request service implementation.
 * <P>
 * @author      Steve Wheat (steve@openeai.org)
 * @version     5.0  - 20 April 2013
 */
public interface ResultCache {

 /**
   *
   * @return Key, a key specifically generated during this setting of the lock,
   * which must be used to release the lock.
   * @throws LockAlreadySetException if the lock is already set.
   * @throws LockException with details of the error setting the lock.
   * <P>
   * Sets the lock.
   *
   */
	public void init(MessageProducer mp, Properties props) throws ResultCacheException;	
	
 /**
   *
   * @return Key, a key specifically generated during this setting of the lock,
   * which must be used to release the lock.
   * @throws LockAlreadySetException if the lock is already set.
   * @throws LockException with details of the error setting the lock.
   * <P>
   * Sets the lock.
   *
   */
	public List getResult(String request) throws ResultCacheException;
	
 /**
   *
   * @return Key, a key specifically generated during this setting of the lock,
   * which must be used to release the lock.
   * @throws LockAlreadySetException if the lock is already set.
   * @throws LockException with details of the error setting the lock.
   * <P>
   * Sets the lock.
   *
   */
	public String buildKey(String producerName, ActionableEnterpriseObject aeo,
		XmlEnterpriseObject querySpec) throws ResultCacheException;	
		
  /**
   * 
   * @param Key, a key generated when the lock was set. This key must be used
   * to release the lock. This helps ensure that only the agent who set the 
   * lock can release it. 
   * @throws LockException with details of the error releasing the lock.
   * <P>
   * Releases the lock.
	 */
	public void addResult(String producerName, XmlEnterpriseObject querySpec, 
		ActionableEnterpriseObject object, List result, long timeToLive) throws
		ResultCacheException;   
}