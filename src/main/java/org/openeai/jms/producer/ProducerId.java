/*******************************************************************************
 $Source$
 $Revision: 1240 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.io.IOException;

import org.openeai.uuidgen.*;
import org.openeai.*;

/**
 * This object is used by the PointToPoint and PubSub producers to uniquely
 * identify each producer.  The ProducerId is populated with a GenericUuid which
 * requests a UUID remotely or generates one locally if a remote request cannot be
 * successfully generated.  This occurs when the producer is first initialized.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see org.openeai.uuidgen.GenericUuid
  * @deprecated As of OpenEAI version 4.0, replaced by {@link #org.openeai.transport.ProducerId}
 */
public class ProducerId extends OpenEaiObject {

  private GenericUuid m_genericUuid = new GenericUuid();
	private String m_url = "";
	private String m_id = "";

	/**
	 * Constructor
	 */
	public ProducerId() {
	}
  /**
  * This constructor automatically attempts to generate a GenericUuid using the 
  * URL passed in and populates itself with that UUID.
  */
	public ProducerId(String url) throws IOException {
		if (url != null) {
			setUrl(url);
      setId(m_genericUuid.requestId(getUrl()));
		}
	}

  /**
  * Returns the URL from which the UUID will be requested.
  * <P>
  * @return String
  */
	public String getUrl() {
		return m_url;
	}
  /**
  * Sets the URL from which the UUID will be requested.
  * <P>
  * @return String
  */
	public void setUrl(String url) {
		if (url == null) {
			m_url = "";
		}
		else {
			m_url = url;
		}
	}

  /**
  * Returns the UUID associated to this object.
  *<P>
  * @return String the UUID that was generated for this object.
  */
	public String getId() {
		return m_id;
	}
  /**
  * Sets the UUID associated to this object.
  *<P>
  * @param id String the UUID that was generated for this object.
  */
	public void setId(String id) {
		if (id == null) {
			m_id = "";
		}
		else {
			m_id = id;
		}
	}

}

