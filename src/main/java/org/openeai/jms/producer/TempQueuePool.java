/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.producer;

import java.io.*;
import java.util.*;
import javax.jms.*;
import org.openeai.*;

/**
 * The TempQueuePool contains a list of PooledTempQueue objects that will be used by a PointToPointProducer's
 * QueueRequestor when a request is produced.  The TempQueuePool is responsible for determining if a PooledTempQueue
 * is in use and if so, allocate another one for temporary use.
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see PooledTempQueue
 * @see PointToPointProducer
 * @see QueueRequestor
 */
public class TempQueuePool extends OpenEaiObject {

  private QueueSession   m_session = null;     // The queue session the queue belongs to.
  private int m_poolSize = 0;
  private Hashtable m_pooledTempQueues = new Hashtable();

  /**
  * This constructor is used by the PointToPointProducer to create this TempQueuePool
  * and set it on the QueueRequestor associated to the producer.  The QueueRequestor
  * will then use this pool to retrieve pre-established TemporaryQueues and QueueReceivers
  * with which it will send requests on behalf of the producer.
  * <P>
  * @param size int the size of the pool
  * @param session javax.jms.QueueSession the QueueSession associated to the Producer creating
  * this pool that will be used to create TemporaryQueues that are added to the pool
  * via the PooledTempQueue object.
  * @see QueueRequestor
  * @see PooledTempQueue
  **/
  public TempQueuePool(int size, QueueSession session) throws JMSException {
    setPoolSize(size);
    setQueueSession(session);
    initializePool();
  }

  /**
  * Sets the size of the pool.
  * <P>
  * @param size int the size of the pool
  **/  
  public void setPoolSize(int size) {
    m_poolSize = size;
  }
  /**
  * Returns the size of the pool.
  * <P>
  * @return int the size of the pool
  **/  
  public int getPoolSize() {
    return m_poolSize;
  }

  private void setQueueSession(QueueSession session) {
    m_session = session;
  }
  private QueueSession getQueueSession() {
    return m_session;
  }

  private Hashtable getPooledTempQueues() {
    return m_pooledTempQueues;
  }
  private void addPooledTempQueue(PooledTempQueue pq) {
    getPooledTempQueues().put(pq.getQueueId(), pq);
  }
  /**
  * Returns a PooledTempQueue that contains a pre-established TempoaryQueue and
  * QueueReceiver.  If no available PooledTempQueues exist in the pool at the time
  * of the request, a new one will be created.  Then, when that resource is released
  * back to the pool, it will be cleaned up so the pool size will remain constant
  * over time.
  * <P>
  * The QueueRequestor object calls this method to retreive a PooledTempQueue from 
  * the pool when it produces a request on behalf of a PointToPointProducer.
  * <P>
  * @return PooleTempQueue either an existing PooledTempQueue from the pool if one
  * is available or a newly created one if an available one does not exist.
  * @throws JMSException if an error occurs creating a new PooledTempQueue if an 
  * available one can't be retrieved from the pool.
  **/
  public synchronized PooledTempQueue getPooledTempQueue() throws JMSException {
    Enumeration keys = getPooledTempQueues().keys();
    while (keys.hasMoreElements()) {
      String keyName = (String)keys.nextElement();
      PooledTempQueue pq = (PooledTempQueue)getPooledTempQueues().get(keyName);
      if (pq.inUse() == false) {
        pq.setInUse(true);
        logger.debug("Returning TempQueue named: " + pq.getQueueId() + " from pool...");
        return pq;
      }
    }

    // If we get down here, we need to create another temp queue and return it
    // because everything in our pool is busy.
    logger.warn("TempQueuePool is busy, returning new TemporaryQueue and QueueReceiver.");
    return new PooledTempQueue(getQueueSession());
  }
  /**
  * Releases a PooledTempQueue back to the pool making it available for retrieval 
  * again.  If the PooledTempQueue passed in was not originally retrieved from the pool 
  * (because it was created dynamically) it will be deleted altogether.
  * <P>
  * If the PooledTempQueue passed in was originally retrieved from the pool, its 
  * status will be set back to "not in use" so it may be retrieved from the pool 
  * by subsequent calls.
  * <P>
  * The OpenEAI QueueRequestor object calls this method when it's finished sending
  * a request and has consumed the response.  If a request times out, the QueueRequestor
  * will call the reinitializePooledTempQueue method.
  * <P>
  * @param pq PooledTempQueue the PooledTempQueue to be released.
  * @throws JMSException if an error occurs deleting the resources allocated to 
  * a PooledTempQueue that was not originally a part of the pool.
  * @see #reinitializePooledTempQueue
  **/
  public void releasePooledTempQueue(PooledTempQueue pq) throws JMSException {
    if (getPooledTempQueues().containsKey(pq.getQueueId())) {
      PooledTempQueue pq2 = (PooledTempQueue)getPooledTempQueues().get(pq.getQueueId());
      pq2.setInUse(false);
      logger.debug("Making PooledTempQueue " + pq.getQueueId() + " available in the pool.");
    }
    else {
      // It's not a PooledTempQueue that we're maintaining in our pool, so we
      // just need to clean it up (delete and close).
      logger.info("PooledTempQueue " + pq.getQueueId() + " isn't in the Pool, just cleaning it up.");
      pq.delete();
    }
  }

  /**
   * This will be called by the QueueRequestor if a Timeout occurs during a "request".  If a timeout occurs
   * we need to remove the PooledTempQueue from our pool and delete it (close and delete the
   * temporary queue and receiver associated to the PooledTempQueue.  Then, we need to
   * add a new PooledTempQueue to our pool.  If the PooleTempQueue passed in wasn't a member
   * of our pool to begin with, we'll just delete it.
   * <P>
   * We have to do this because if we send requests to gateways that are down, the messages
   * sit on the Queue until the next time the gateway is brought up.  At that time, the gateway
   * will see that it has requests to process and it will process and respond to those requests.
   * By that time, the TempQueuePool may be in use by another request and we don't want the gateway
   * to mistakenly respond to those requests using the request sent by the original request that timed
   * out.  Instead, we want the gateway to receive a "queue not found" error at that time.  By removing
   * the PooledTempQueue from the pool and deleting it, we're ensuring that any PooledTempQueue used
   * when a timeout occurred isn't used again when the gateway starts processing messages again.
   * <P>
   * @param pq PooledTempQueue the PooledTempQueue to be re-initialized.
   * @throws JMSException if any errors occur re-initiallizing the PooledTempQueue.
   */
  public synchronized void reinitializePooledTempQueue(PooledTempQueue pq) throws JMSException {
    if (getPooledTempQueues().containsKey(pq.getQueueId())) {
      getPooledTempQueues().remove(pq.getQueueId());
      pq.delete();
      addPooledTempQueue(new PooledTempQueue(getQueueSession()));
      logger.debug("Making PooledTempQueue " + pq.getQueueId() + " available in the pool.");
    }
    else {
      // It's not a PooledTempQueue that we're maintaining in our pool, so we
      // just need to clean it up (delete and close).
      logger.info("PooledTempQueue " + pq.getQueueId() + " isn't in the Pool, just cleaning it up.");
      pq.delete();
    }
  }

  /**
  * Releases all JMS resources allocated to all PooledTempQueues in this pool.  This
  * includes deleting the TemporaryQueues and closing the QueueReceivers associated
  * to each PooledTempQueue.
  * <P>
  * @throws JMSException if errors occur.  However, it will continue to iterate through
  * all PooledTempQueue objects closing them all and only throw the exception after 
  * all resources that could be released are.  It will save up any exceptions that occur
  * during the cleanup process and include those messages in the exception that it throws.
  **/
  public synchronized void close() throws JMSException {
    Enumeration keys = getPooledTempQueues().keys();
    String exceptionMessage = "";
    String exceptionStackTrace = "";
    String exceptionToThrow = "";
    int exceptionNumber = 0;
    boolean exceptionOccurred = false;
    while (keys.hasMoreElements()) {
      String keyName = (String)keys.nextElement();
      PooledTempQueue pq = (PooledTempQueue)getPooledTempQueues().get(keyName);
      try {
        pq.delete();
      }
      catch (Exception e) {
        logger.fatal("Exception deleting PooledTempQueue " + keyName + " building exception message an continuing to next PooledTempQueue.");
        exceptionNumber++;
        exceptionOccurred = true;
        exceptionMessage = e.getMessage();
        ByteArrayOutputStream bw = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(bw, true);
        e.printStackTrace(pw);
        exceptionStackTrace = bw.toString();
        exceptionToThrow = exceptionToThrow + "Exception Message [" + exceptionNumber + "] " + exceptionMessage + "\n";
        exceptionToThrow = exceptionToThrow + "Exception Stack [" + exceptionNumber + "] " + exceptionStackTrace + "\n";
      }
    }

    if (exceptionOccurred) {
      throw new JMSException(exceptionToThrow);
    }
  }

  private void initializePool() throws JMSException {
    logger.info("Initializing " + getPoolSize() + " TemporaryQueues and adding them to the pool.");
    for (int i=0; i<getPoolSize(); i++) {
      logger.debug("Adding PooledTempQueue " + i);
      addPooledTempQueue(new PooledTempQueue(getQueueSession()));
    }
    logger.info("Done - Initializing " + getPoolSize() + " TemporaryQueues and adding them to the pool.");
  }
}

