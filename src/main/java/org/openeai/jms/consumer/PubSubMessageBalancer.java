/* *********************************************************************
 This file is part of the OpenEAI Application Foundation or
 OpenEAI Message Object API created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to build commercial integration software or to implement
 integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.consumer;

import org.openeai.OpenEaiObject;
import org.openeai.config.DbConnectionPoolConfig;
import org.openeai.dbpool.EnterpriseConnectionPool;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class is used by all PubSubConsumers to balance multiple instances of sync consumers.  Since
 * PubSub consumers all consume every message delivered to a topic (unlike PointToPointConsumers consuming from a Queue)
 * they need a way to determine if another consumer is already processing the message.  This is accomplished
 * via this class and a property on the JMS Message (MESSAGE_ID) which populated when the message is
 * published from the source by the OpenEAI foundation APIs.
 * <p>
 * This MESSAGE_ID is unique among all messages published and is maintained automatically
 * by the OpenEAI message production foundation (PubSubProducers and JMS Message Objects).
 * <p>
 * This particular Balancer uses a pre-defined database repository to store the MESSAGE_IDs
 * and to determine if another consumer is processing or has processed the message.
 * <p>
 *
 * @author Tod Jackson (tod@openeai.org)
 * @author Steve Wheat (steve@openeai.org)
 * @version 3.0  - 28 January 2003
 */
public class PubSubMessageBalancer extends OpenEaiObject {

    private EnterpriseConnectionPool m_connectionPool;

    /**
     * The T_PROCESSED_MESSAGE table can get very large and the query to check if a message is processed
     * can perform poorly.  This setting can disable the query step to test the assertion that it is causing
     * large queue depths on the broker.
     * For one release (#17), the query step was disabled but is being put back
     * to the old behaviour so that there are no surprises if someone grabs the latest version.
     */
    private boolean queryOnInsertFailure = false;

    /*
     * This constructor exists so that organizations can re-implement this class
     */
    /*
    public PubSubMessageBalancer(Properties connProps) throws IOException {
        setProperties(connProps);    // save incase we have to re-initialize the conn pool

        try {
            m_connectionPool = new EnterpriseConnectionPool("MessageBalancerConnectionPool", connProps);
        } catch (Exception e) {
            String errMessage = "Error initializing 'MessageBalancer' DB Connection pool.  Exception: " + e.getMessage();
            throw new IOException(errMessage);
        }
    }
    */

    /**
     * This is the constructor used by PubSubConsumers when they initialize themselves.
     * They pass the DbConnectionPoolConfig object associated to the Consumer to this
     * constructor and then the balancer instantiates an EnterpriseConnectionPool
     * that will be used to determine if another consumer is processing the message
     * or not.
     * <p>
     *
     * @param dbConfig DbConnectionPoolConfig used to instantiate the EnterpriseConnectionPool associated
     *                 to this Balancer.
     **/
    public PubSubMessageBalancer(DbConnectionPoolConfig dbConfig) throws IOException {
        try {
            m_connectionPool = new EnterpriseConnectionPool(dbConfig.getName(), dbConfig.getProperties());
        } catch (Exception e) {
            String errMessage = "Error initializing 'MessageBalancer' DB Connection pool via DbConnectionPoolConfig."
                    + "  Exception: " + e.getMessage();
            throw new IOException(errMessage);
        }
    }

    /**
     * This is the method called by the PubSubConsumer to determine if it's safe
     * for this consumer to process the message it consumed.  This method tries
     * to insert the message id into the database store, if the insert succeeds
     * it means no other consumer is processing the message.  If the insert fails, it may query
     * for the message id and if the message id is found on the query, it means another
     * consumer is already processing the message.
     * <p>
     *
     * @param messageId String the message id being checked that was pulled from the JMS message
     *                  consumed by the consumer.
     * @return true if the consumer can process the message, false if another consumer
     * is already processing the message.
     * @throws SQLException if errors occur querying for the id after a failed insert.
     *                      NOTE:  if any failures occur, the consumer will process the message because at that
     *                      point it doesn't know if another consumer's processing the message so it must
     *                      for safety sake.
     * @deprecated use canConsumerProcess(String appId, String messageId) instead.
     */
    public final synchronized boolean canConsumerProcess(String messageId) throws SQLException {
        if (messageId == null) {
            return true;
        }

        java.sql.Connection conn = null;
        try {
            conn = m_connectionPool.getConnection();
            conn.setAutoCommit(true);

            insertId(conn, messageId);
            return true;  // we can process it
        } catch (SQLException e) {
            // couldn't insert - may be because another consumer is already processing the message
            // or it could be some other kind of SQL error.  we can check by trying to retrieve
            // the message but that query can be slow and causes the broker queue depth to increase
            // so the query step has been made optional.
            if (!queryOnInsertFailure) {
                logger.info("Assuming message id " + messageId + " is already handled"
                        + " (SQL State:" + e.getSQLState() + "; Error Code:" + e.getErrorCode() + "; Message:" + e.getMessage() + ")");
                return false;
            }

            String id = retrieveId(conn, messageId);
            if (id == null) {
                String errMessage = "Could not insert message id " + messageId + " and no one else is processing the message.";
                logger.fatal(errMessage, e);
                throw new SQLException(errMessage);
            } else {
                return false;  // someone else is processing it
            }
        }
    }

    /**
     * This is the method called by the PubSubConsumer to determine if it's safe
     * for this consumer to process the message it consumed.  This method tries
     * to insert the message id into the database store, if the insert succeeds
     * it means no other consumer is processing the message.  If the insert fails, it may query
     * for the message id and if the message id is found on the query, it means another
     * consumer is already processing the message.
     * <p>
     *
     * @param appId     String the application id that the message is being processed on
     *                  behalf of.  i.e., the application consuming the message.
     * @param messageId String the message id being checked that was pulled from the JMS message
     *                  consumed by the consumer.
     * @return true if the consumer can process the message, false if another consumer
     * is already processing the message.
     * @throws SQLException if errors occur querying for the id after a failed insert.
     *                      NOTE:  if any failures occur, the consumer will process the message because at that
     *                      point it doesn't know if another consumer's processing the message so it must
     *                      for safety sake.
     */
    public final synchronized boolean canConsumerProcess(String appId, String messageId) throws SQLException {
        if (messageId == null) {
            return true;
        }

        long start = 0;
        java.sql.Connection conn = null;
        try {
            conn = m_connectionPool.getConnection();
            conn.setAutoCommit(true);

            start = System.currentTimeMillis();

            insertId(conn, appId, messageId);
            return true;  // we can process it
        } catch (SQLException e) {
            long elapsed = System.currentTimeMillis() - start;

            // couldn't insert - may be because another consumer is already processing the message
            // or it could be some other kind of SQL error.  we can check by trying to retrieve
            // the message but that query can be slow and causes the broker queue depth to increase
            // so the query step has been made optional.
            if (!queryOnInsertFailure) {
                logger.info("Assuming message id " + messageId + " for application " + appId + " is already handled"
                        + " took " + elapsed + " ms "
                        + " (SQL State:" + e.getSQLState() + "; Error Code:" + e.getErrorCode() + "; Message:" + e.getMessage() + ")");
                return false;
            }

            String id = retrieveId(conn, appId, messageId);
            if (id == null) {
                String errMessage = "Could not insert message id " + messageId + " for application " + appId
                        + " and no one else is processing the message.";
                logger.fatal(errMessage, e);
                throw new SQLException(errMessage);
            } else {
                return false;  // someone else is processing it
            }
        }
    }

    /**
     * Insert a record of a processed message.
     *
     * @param conn  connection
     * @param msgId processed message ID
     * @throws SQLException on error
     */
    private void insertId(java.sql.Connection conn, String msgId) throws SQLException {
        long start = System.currentTimeMillis();
        try {
            String sql = "INSERT INTO T_PROCESSED_MESSAGE (PROCESSED_MESSAGE_ID, CREATE_DATE, CREATE_USER) VALUES (?,?,?)";

            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.clearParameters();
                stmt.setString(1, msgId);
                stmt.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
                stmt.setString(3, conn.getMetaData().getUserName());
                stmt.executeUpdate();
            }
        }
        finally {
            logger.info("Inserted message processing event for message id " + msgId
                    + " took " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    /**
     * Insert a record of a processed message.
     *
     * @param conn  connection
     * @param appId application ID
     * @param msgId processed message ID
     * @throws SQLException on error
     */
    private void insertId(java.sql.Connection conn, String appId, String msgId) throws SQLException {
        long start = System.currentTimeMillis();
        try {
            String sql = "INSERT INTO T_PROCESSED_MESSAGE (APPLICATION_ID, PROCESSED_MESSAGE_ID, CREATE_DATE, CREATE_USER) VALUES (?,?,?,?)";

            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.clearParameters();
                stmt.setString(1, appId);
                stmt.setString(2, msgId);
                stmt.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
                stmt.setString(4, conn.getMetaData().getUserName());
                stmt.executeUpdate();
            }
        }
        finally {
            logger.info("Inserted message processing event for message id " + msgId + " for application " + appId
                    + " took " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    /**
     * Find a processed message by the ID.
     *
     * @param conn  connection
     * @param msgId processed message ID
     * @return the processed message ID
     * @throws SQLException on error
     */
    private String retrieveId(java.sql.Connection conn, String msgId) throws SQLException {
        long start = System.currentTimeMillis();
        String retId = null;

        try {
            PreparedStatement stmt = null;
            String sql = "SELECT PROCESSED_MESSAGE_ID FROM T_PROCESSED_MESSAGE WHERE PROCESSED_MESSAGE_ID=?";

            try {
                stmt = conn.prepareStatement(sql);
                stmt.clearParameters();
                stmt.setString(1, msgId);
                ResultSet results = stmt.executeQuery();
                while (results.next()) {
                    retId = results.getString(1);
                }

                return retId;
            } catch (SQLException e) {
                String errMessage = "Error retrieving message id " + msgId + " from database"
                        + " (SQL State:" + e.getSQLState() + "; Error Code:" + e.getErrorCode() + "; Message:" + e.getMessage() + ")";
                logger.fatal(errMessage, e);
                throw e;
            } catch (Exception e) {
                String errMessage = "Error retrieving message id " + msgId + " from database"
                        + " (Message:" + e.getMessage() + ")";
                logger.fatal(errMessage, e);
                throw new SQLException(errMessage, e);
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }
        finally {
            logger.info("Selected message processing event for message id " + msgId
                    + " and found " + retId
                    + " took " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    /**
     * Find a processed message by the IDs.
     *
     * @param conn  connection
     * @param appId application ID
     * @param msgId processed message ID
     * @return the processed message ID
     * @throws SQLException on error
     */
    private String retrieveId(java.sql.Connection conn, String appId, String msgId) throws SQLException {
        long start = System.currentTimeMillis();
        String retId = null;
        try {
            PreparedStatement stmt = null;
            String sql = "SELECT PROCESSED_MESSAGE_ID FROM T_PROCESSED_MESSAGE WHERE APPLICATION_ID = ? AND PROCESSED_MESSAGE_ID = ?";

            try {
                stmt = conn.prepareStatement(sql);
                stmt.clearParameters();
                stmt.setString(1, appId);
                stmt.setString(2, msgId);
                ResultSet results = stmt.executeQuery();
                while (results.next()) {
                    retId = results.getString(1);
                }

                return retId;
            } catch (SQLException e) {
                String errMessage = "Error retrieving message id " + msgId + " for application " + appId + " from database"
                        + " (SQL State:" + e.getSQLState() + "; Error Code:" + e.getErrorCode() + "; Message:" + e.getMessage() + ")";
                logger.fatal(errMessage, e);
                throw e;
            } catch (Exception e) {
                String errMessage = "Error retrieving message id " + msgId + " for application " + appId + " from database"
                        + " (Message:" + e.getMessage() + ")";
                logger.fatal(errMessage, e);
                throw new SQLException(errMessage, e);
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }
        finally {
            logger.info("Selected message processing event for message id " + msgId + " for application " + appId
                    + " and found " + retId
                    + " took " + (System.currentTimeMillis() - start) + " ms");
        }
    }
}

