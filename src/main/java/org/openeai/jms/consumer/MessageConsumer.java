/*******************************************************************************
 $Source$
 $Revision: 3832 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.consumer;

import java.io.*;

import java.lang.reflect.*;

import java.util.*;

// Java Messaging Service
import javax.jms.*;

// JDOM...
import org.jdom.Document;
import org.jdom.Element;

// JNDI Stuff
import javax.naming.*;
import javax.naming.directory.*;

import org.openeai.*;
import org.openeai.config.*;
import org.openeai.jms.consumer.commands.*;
import org.openeai.loggingutils.*;
import org.openeai.threadpool.*;
import org.openeai.xml.*;
import org.openeai.jms.producer.MessageProducer;

/**
 * The ancestor class for PubSub and PointToPoint consumers.  These are the counterparts
 * to OpenEAI MessageProducers.  These classes (PubSub and PointToPoint Consumers) consume
 * and potentially respond to messages sent by OpenEAI MessageProducers (PointToPoint and PubSub
 * Producers)
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.config.ConsumerConfig
 * @see org.openeai.jms.producer.MessageProducer
 */
public abstract class MessageConsumer extends OpenEaiObject {

  protected HashMap m_messages = new HashMap(5, 0.75f);
  protected String m_userName;
  protected String m_password;
  protected HashMap m_commandConfigs = new HashMap(5, 0.75f);
  private String m_providerUrl;
  private String m_initCtxFactory;
  private String m_securityPrincipal = "";
  private String m_securityCredentials = "";
  private boolean m_performMonitorOperation = true;

  protected static String STOPPED = "Stopped";
  protected static String STARTED = "Started";
  protected static String DISCONNECTED = "Disconnected";
  protected static String CONNECTED = "Connected";
  protected static String NOT_INITIALIZED = "Not Initialized";
  protected static String INITIALIZED = "Initialized";

  private String m_consumerStatus = STOPPED;
  private String m_initializationStatus = NOT_INITIALIZED;

  private String m_connectionFactoryName = null;
  private String m_destinationName = null;
  private ConsumerConfig m_config = null;

  protected ThreadPool m_threadPool = null;
  protected int m_numThreads = 1; // Default
  protected int _maxThreadPoolShutdownWaitTime = 15000; // default
  protected boolean m_startOnInitialization = false; // default
  private boolean m_transacted = false; // default
  private String m_consumerName = "";
  private String m_instanceName = "";

  // Shutdown Hook helpers
  private boolean m_canConsume = true; // default
  protected Vector m_inProcessMessages = null;
  //there is already a debug property for this
  //protected boolean m_verbose;

  // Generic Error handling document...
  private boolean m_validate = false;
  private Document m_genericErrorDoc = null;

  private String m_defaultCommandName = 
    ""; // The command this consumer executes if no commands are passed in.
  private String m_absoluteCommandName = 
    null; // The command this consumer executes regardless of what's passed in.

  public MessageConsumer() {
  }

  /**
   * Sets the maximum amount of time (in milliseconds) that the consumer will
   * wait for the ThreadPool to empty when the consumer's shutdown hook is
   * triggered.  This is to avoid situations where a command or commands get
   * themselves stuck in a state where they will never return.  Without a
   * hard timeout like this, the application would never be allowed to completely
   * shutdown because it would wait on the commands to complete indefinitely.
   * <P>
   * The default value for this is 15000 (15 seconds).
   * @param maxWaitTime
   */
  public final void setMaximumThreadPoolShutdownWaitTime(int maxWaitTime) {
    _maxThreadPoolShutdownWaitTime = maxWaitTime;
  }

  /**
   * Returns the maximum amount of time the consumer will wait for the thread
   * pool to empty itself.
   *
   * @return the maximum time the consumer will wait for the thread pool to empty.
   */
  public final int getMaximumThreadPoolShutdownWaitTime() {
    return _maxThreadPoolShutdownWaitTime;
  }

  /**
  * Sets the name of the Default command that should be executed if a Message
  * consumed does not have a COMMAND_NAME JMS Property associated to it.  This
  * is determined as the consumer initializes the commands associated to it
  * during consumer intiailization.  Note:  this is not applicable if a command
  * is designated as the "absolute" command.
  * <P>
  * @param name String name of the default command as specified in the Command's
  * configuration object in the gateway's deployment document.
  * @see org.openeai.config.CommandConfig#isDefault()
  **/
  public final void setDefaultCommandName(String name) {
    m_defaultCommandName = name;
  }

  /**
  * Returns the name of the Default command that should be executed if a Message
  * consumed does not have a COMMAND_NAME JMS Property associated to it.  This
  * is determined as the consumer initializes the commands associated to it
  * during consumer intiailization.  Note:  this is not applicable if a command
  * is designated as the "absolute" command.
  * <P>
  * @return String name of the default command as specified in the Command's
  * configuration object in the gateway's deployment document.
  * @see org.openeai.config.CommandConfig#isDefault()
  **/
  public final String getDefaultCommandName() {
    return m_defaultCommandName;
  }

  /**
  * Sets the name of the Absolute command that should be executed by this
  * consumer NO MATTER what the COMMAND_NAME JMS Property might be.  This
  * means no matter what message the consumer consumes, it will execute the
  * command that has this name.  This is determined as the consumer
  * initializes the commands associated to it during consumer intiailization.
  * <P>
  * @param name String name of the absolute command as specified in the Command's
  * configuration object in the gateway's deployment document.
  * @see org.openeai.config.CommandConfig#isAbsolute()
  **/
  public final void setAbsoluteCommandName(String name) {
    m_absoluteCommandName = name;
  }

  /**
  * Returns the name of the Absolute command that should be executed by this
  * consumer NO MATTER what the COMMAND_NAME JMS Property might be.  This
  * means no matter what message the consumer consumes, it will execute the
  * command that has this name.  This is determined as the consumer
  * initializes the commands associated to it during consumer intiailization.
  * <P>
  * @return String name of the absolute command as specified in the Command's
  * configuration object in the gateway's deployment document.
  * @see org.openeai.config.CommandConfig#isAbsolute()
  **/
  public final String getAbsoluteCommandName() {
    return m_absoluteCommandName;
  }

  /**
  * Sets the name of this consumer as specified in the Consumer's configuration
  * information.
  * <P>
  * @param name String name of the consumer.
  **/
  public final void setConsumerName(String name) {
    m_consumerName = name;
  }

  /**
  * Returns the name of this consumer as specified in the Consumer's configuration
  * information.
  * <P>
  * @return String name of the consumer.
  **/
  public final String getConsumerName() {
    return m_consumerName;
  }

  /**
  * Returns the instance name of this consumer.  This is used when establishing
  * durable subscriptions.
  * <P>
  * @return String the consumer's instance name.
  **/
  public final String getInstanceName() {
    return m_instanceName;
  }

  /**
  * Sets the instance name of this consumer.  This is used when establishing
  * durable subscriptions.  This is specified in the gateway's property file
  * (NOT the gateway deployment XML document) and is automatically set by
  * AppConfig as it starts the consumer.
  * <P>
  * @param instanceName String the consumer's instance name.
  **/
  public final void setInstanceName(String instanceName) {
    m_instanceName = instanceName;
  }

  /**
  * Sets the LDAP security principal this consumer should use when connecting
  * to the directory server to retrieve JMS administered objects
  * (ConnectionFactories and Destinations).  This is specified in the Consumer's
  * configuration element (ConsumerConfig).
  * <P>
  * @param principal String the LDAP principal
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setSecurityPrincipal(String principal) {
    m_securityPrincipal = principal;
  }

  /**
  * Returns the LDAP security principal this consumer should use when connecting
  * to the directory server to retrieve JMS administered objects
  * (ConnectionFactories and Destinations).  This is specified in the Consumer's
  * configuration element (ConsumerConfig).
  * <P>
  * @return String the LDAP principal
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getSecurityPrincipal() {
    return m_securityPrincipal;
  }

  /**
  * Sets the LDAP security credentials (password) this consumer should use when connecting
  * to the directory server to retrieve JMS administered objects
  * (ConnectionFactories and Destinations).  This is specified in the Consumer's
  * configuration element (ConsumerConfig).
  * <P>
  * @param credentials String the LDAP credentials
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setSecurityCredentials(String credentials) {
    m_securityCredentials = credentials;
  }

  /**
  * Returns the LDAP security credentials (password) this consumer should use when connecting
  * to the directory server to retrieve JMS administered objects
  * (ConnectionFactories and Destinations).  This is specified in the Consumer's
  * configuration element (ConsumerConfig).
  * <P>
  * @return String the LDAP credentials
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getSecurityCredentials() {
    return m_securityCredentials;
  }

  //TODO: Move this to PointToPointConsumer?

  /**
  * Sets the generic response document that this consumer should use to reply to
  * incomming requests if it has problems executing the command associated to the
  * request consumed.
  * <P>
  * @param doc org.jdom.Document
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setGenericErrorDoc(Document doc) {
    m_genericErrorDoc = doc;
  }
  //TODO: Move this to PointToPointConsumer?

  /**
  * Returns the generic response document that this consumer should use to reply to
  * incomming requests if it has problems executing the command associated to the
  * request consumed.
  * <P>
  * @return org.jdom.Document
  * @see org.openeai.config.ConsumerConfig
  **/
  public final Document getGenericErrorDoc() {
    return m_genericErrorDoc;
  }

  /**
  * Returns the current status of the Consumer.  Consumers can be in several different
  * status as they are used.  Most common, the statuses are either STOPPED or STARTED.
  * <P>
  * @return String the status of the consumer.
  **/
  public final String getConsumerStatus() {
    return m_consumerStatus;
  }

  /**
  * Sets the current status of the Consumer.  Consumers can be in several different
  * status as they are used.  Most common, the statuses are either STOPPED or STARTED.
  * The status is set by the consumer as it initializes itself and when it is shutdown.
  * <P>
  * @param status String the status of the consumer.
  **/
  public final void setConsumerStatus(String status) {
    m_consumerStatus = status;
  }

  /**
  * Returns a boolean that indicates whether or not the consumer is started.  This
  * is determined based on the status of the consumer (STARTED or STOPPED).
  * <P>
  * @return boolean true=consumer is started, false=consumer is not started.
  **/
  public final boolean isStarted() {
    if (getConsumerStatus().equals(STARTED)) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
	 * Puts the consumer in a state that will prohibit it from consuming any more messages.
   * Both PubSub and PointToPoint consumers check to see if consumption has been stopped
   * each time they consume a message.  If consumption has been stopped, they do not exit
   * out of their 'onMessage' method.  This in affect stops message consumption for the consumer.
   * Then the shutdown hook facility looks for any pending messages that need to be executed
   * that might have been saved if this situation occurred and it processes the pending
   * messages.  This does not disconnect the consumer from the broker.
	 *
	 */
  public final void stopConsumption() {
    setCanConsume(false);
  }

  private void setCanConsume(boolean canConsume) {
    m_canConsume = canConsume;
  }

  /**
	 * This is the method each consumer calls whenever they consume a message to see if they can continue
   * to consume messages.  If this returns false, they do not exit their 'onMessage' method affectively
   * stopping message consumption for the consumer.
   *<P>
   * Both PubSub and PointToPoint consumers check to see if consumption has been stopped
   * each time they consume a message.
	 *
	 */
  public final boolean consumptionStopped() {
    if (m_canConsume) {
      return false;
    }
    else {
      return true;
    }
  }

  /**
  * Sets the JMS property indicating whether or not this consumer operates in
  * transacted mode or not.  This is specified in the ConsumerConfig Element
  * in the deployment document for the gateway being started.
  * <P>
  * See JMS Specification for details regarding JMS and transactions.
  * <P>
  * @param trans boolean true means the consumer is transacted false means it is not.
  **/
  public final void setTransacted(boolean trans) {
    m_transacted = trans;
  }

  /**
  * Returns the JMS property indicating whether or not this consumer operates in
  * transacted mode or not.  This is specified in the ConsumerConfig Element
  * in the deployment document for the gateway being started.
  * <P>
  * See JMS Specification for details regarding JMS and transactions.
  * <P>
  * @return boolean true means the consumer is transacted false means it is not.
  **/
  public final boolean getTransacted() {
    return m_transacted;
  }

  private final void setValidation(boolean validate) {
    m_validate = validate;
  }

  private final boolean getValidation() {
    return m_validate;
  }
  
  /**
  * Returns the Consumer's perform monitor operation property.
  * <P>
  * Specifies whether or not to perform the monitor operation.
  * <P>
  * @return boolean
  **/
  public final boolean getPerformMonitorOperation() {
    return m_performMonitorOperation;
  }

  /**
  * Sets the Consumer's perform monitor operation property.
  * <P>
  * Specifies whether or not to perform the monitor operation.
  * <P>
  * @param boolean
  **/
  public final void setPerformMonitorOperation(boolean b) {
    m_performMonitorOperation = b;
  }

  /**
  * Adds a CommandConfig Java object to the list of Commands associated to this
  * consumer.  This CommandConfig object will be used to initialize the Command
  * associated to it.  Then these Commands will be stored with the consumer and
  * executed based on Messages consumed by the Consumer.
  * <P>
  * @param name String name of the ConsumerCommand that will be initialized with the CommandConfig object
  * @param cConfig CommandConfig the CommandConfig object that holds initialization information
  * for the Command that will be instantiated and executed by the Consumer.
  * @see org.openeai.config.CommandConfig
  * @see org.openeai.jms.consumer.commands.ConsumerCommand
  * @see org.openeai.jms.consumer.commands.RequestCommand
  * @see org.openeai.jms.consumer.commands.RequestCommandImpl
  * @see org.openeai.jms.consumer.commands.SyncCommand
  * @see org.openeai.jms.consumer.commands.SyncCommandImpl
  **/
  public final void addCommandConfig(String name, CommandConfig cConfig) {
    logger.debug("Adding a CommandConfig named: " + name);
    m_commandConfigs.put(name, cConfig);
  }

  /**
  * Returns a CommandConfig Java object from the list of ConsumerCommands associated to this
  * consumer by name.  This CommandConfig object will be used to initialize the ConsumerCommand
  * associated to it.  Then these ConsumerCommands will be stored with the consumer and
  * executed based on Messages consumed by the Consumer.
  * <P>
  * @param name String name of the Command that will be initialized with the CommandConfig object
  * @return CommandConfig the CommandConfig object stored in the HashMap of CommandConfigs
  * that matches the name passed in.  Returns null if a CommandConfig cannot be found
  * matching the name passed in.
  * @see org.openeai.config.CommandConfig
  * @see org.openeai.jms.consumer.commands.ConsumerCommand
  * @see org.openeai.jms.consumer.commands.RequestCommand
  * @see org.openeai.jms.consumer.commands.RequestCommandImpl
  * @see org.openeai.jms.consumer.commands.SyncCommand
  * @see org.openeai.jms.consumer.commands.SyncCommandImpl
  **/
  public final CommandConfig getCommandConfig(String name) {
    logger.debug("Looking for a CommandConfig named: " + name);
    return (CommandConfig)m_commandConfigs.get(name);
  }

  /**
  * Sets the HashMap of CommandConfigs to the value passed in.  This is a HashMap
  * of configured CommandConfig objects that will be used to initialize ConsumerCommand
  * objects.
  * <P>
  * @param cConfigs HashMap
  **/
  public final void setCommandConfigs(HashMap cConfigs) {
    logger.debug("Adding " + cConfigs.size() + " command configs.");
    Set k = cConfigs.keySet();
    Iterator it = k.iterator();
    m_commandConfigs = cConfigs;
  }

  /**
  * Returns a boolean indicating whether or not this consumer should be started
  * (connected to the broker and ready to consume messages) when it is initialized.
  * This is specified in the ConsumerConfig Element (startOnInitialization Attribute)
  * of the deployment document for the gateway being started.
  * <P>
  * @return boolean
  * @see org.openeai.config.ConsumerConfig
  **/
  public final boolean getStartOnInitialization() {
    return m_startOnInitialization;
  }

  /**
  * Sets a boolean indicating whether or not this consumer should be started
  * (connected to the broker and ready to consume messages) when it is initialized.
  * This is specified in the ConsumerConfig Element (startOnInitialization Attribute)
  * of the deployment document for the gateway being started.
  * <P>
  * @return boolean
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setStartOnInitialization(boolean start) {
    m_startOnInitialization = start;
  }

  /**
  * Sets the ConsumerConfig object associated to this Consumer.  This object
  * holds all the configuration information as specified in the Deployment
  * document that this consumer needs to initialize itself.
  * <P>
  * @param config ConsumerConfig the ConsumerConfig Java object that has been built
  * from contents of the deployment document for the gateway being started.
  **/
  public final void setConfig(ConsumerConfig config) {
    m_config = config;
  }

  /**
  * Returns the ConsumerConfig object associated to this Consumer.  This object
  * holds all the configuration information as specified in the Deployment
  * document that this consumer needs to initialize itself.
  * <P>
  * @return ConsumerConfig the ConsumerConfig Java object that has been built
  * from contents of the deployment document for the gateway being started.
  **/
  public final ConsumerConfig getConfig() {
    return m_config;
  }

  /**
  * Sets the destination name (Queue or Topic) that this Consumer should connect
  * to and consume from.
  * <P>
  * See JMS Specification for a description of Destinations.
  * <P>
  * @param name String the destination (Queue or Topic) name.
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setDestinationName(String name) {
    m_destinationName = name;
  }

  /**
  * Returns the destination name (Queue or Topic) that this Consumer should connect
  * to and consume from.
  * <P>
  * See JMS Specification for a description of Destinations.
  * <P>
  * @return String the destination (Queue or Topic) name.
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getDestinationName() {
    return m_destinationName;
  }

  /**
  * Sets the connection factory name (QueueConnectionFactory or TopicConnectionFactory)
  * that this Consumer should retrieve and use to initialize its connection to the
  * broker with.
  * <P>
  * See JMS Specification for a description of Connection Factories.
  * <P>
  * @param name String name of the Connection Factory.
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setConnectionFactoryName(String name) {
    m_connectionFactoryName = name;
  }

  /**
  * Returns the connection factory name (QueueConnectionFactory or TopicConnectionFactory)
  * that this Consumer should retrieve and use to initialize its connection to the
  * broker with.
  * <P>
  * See JMS Specification for a description of Connection Factories.
  * <P>
  * @return String name of the Connection Factory.
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getConnectionFactoryName() {
    return m_connectionFactoryName;
  }

  /**
  * Returns the initialization status of the consumer.  Since a Consumer can be
  * initialized but not started, there is a distinction between a consumer's
  * "status" and its "initialization status".  Initialization statuses can be
  * either INITIALIZED or NOT_INITIALIZED.  This status is set during the initialization
  * of the consumer and is used primarily by the consumer to determine what state
  * it's in.
  * <P>
  * @return String the consumer's initialization status.
  **/
  public final String getInitializationStatus() {
    return m_initializationStatus;
  }

  /**
  * Sets the initialization status of the consumer.  Since a Consumer can be
  * initialized but not started, there is a distinction between a consumer's
  * "status" and its "initialization status".  Initialization statuses can be
  * either INITIALIZED or NOT_INITIALIZED.  This status is set during the initialization
  * of the consumer and is used primarily by the consumer to determine what state
  * it's in.
  * <P>
  * @return String the consumer's initialization status.
  **/
  public final void setInitializationStatus(String status) {
    m_initializationStatus = status;
  }

  /**
  * Sets the ThreadPool object that will be used by this Consumer to execute
  * ConsumerCommands in.  When a Consumer receives a message from a destination
  * it determines what ConsumerCommand should be executed via the OpenEAI JMS
  * "COMMAND_NAME" then it adds a job to this ThreadPool which is a call to that
  * Command's execute method.
  * <P>
  * This ThreadPool is configured in the ConsumerConfig
  * Element of the gateway's deployment document and is passed to the Consumer
  * as part of the ConsumerConfig Java object.
  * <P>
  * @param tPool org.openeai.threadpool.ThreadPool
  * @see org.openeai.threadpool.ThreadPool
  * @see org.openeai.config.ConsumerConfig
  * @see org.openeai.config.ThreadPoolConfig
  **/
  public final void setThreadPool(ThreadPool tPool) {
    m_threadPool = tPool;
  }

  /**
  * Returns the ThreadPool object that will be used by this Consumer to execute
  * ConsumerCommands in.  When a Consumer receives a message from a destination
  * it determines what ConsumerCommand should be executed via the OpenEAI JMS
  * "COMMAND_NAME" then it adds a job to this ThreadPool which is a call to that
  * Command's execute method.
  * <P>
  * This ThreadPool is configured in the ConsumerConfig
  * Element of the gateway's deployment document and is passed to the Consumer
  * as part of the ConsumerConfig Java object.
  * <P>
  * @return org.openeai.threadpool.ThreadPool
  * @see org.openeai.threadpool.ThreadPool
  * @see org.openeai.config.ConsumerConfig
  * @see org.openeai.config.ThreadPoolConfig
  **/
  public final ThreadPool getThreadPool() {
    return m_threadPool;
  }

  /**
  * Sets the broker user name that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Consumer and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ConsumerConfig
  * Element in the deployment document.
  * <P>
  * @param in String broker user name
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setUserName(String in) {
    m_userName = in;
  }

  /**
  * Returns the broker user name that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Consumer and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ConsumerConfig
  * Element in the deployment document.
  * <P>
  * @return String broker user name
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getUserName() {
    return m_userName;
  }

  /**
  * Sets the broker user password that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Consumer and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ConsumerConfig
  * Element in the deployment document.
  * <P>
  * @param in String broker user name
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setPassword(String in) {
    m_password = in;
  }

  /**
  * Returns the broker user password that should be used when establishing a connection
  * with the broker.  Typically, this is specified in the ConnectionFactory used
  * by this Consumer and it is not needed here.  However, if you don't want to use
  * the ConnectionFactory for this you can do it here by specifying it in the ConsumerConfig
  * Element in the deployment document.
  * <P>
  * @return String broker user name
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getPassword() {
    return m_password;
  }

  /**
  * Sets the location where the administered objects can be retrieved from.
  * To maintain broker independance, this should reference a location in a
  * Directory server.
  * <P>
  * @param in String the URL that resolves to the location of the JMS Administered Objects
  * (ConnectionFactories and Destinations).
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setProviderUrl(String in) {
    m_providerUrl = in;
  }

  /**
  * Returns the location where the administered objects can be retrieved from.
  * To maintain broker independance, this should reference a location in a
  * Directory server.
  * <P>
  * @return String the URL that resolves to the location of the JMS Administered Objects
  * (ConnectionFactories and Destinations).
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getProviderUrl() {
    return m_providerUrl;
  }

  /**
  * Sets the class name of the JNDI context factory that will be used to retrieve the JMS
  * administered objects from a Directory Server or wherever the administered
  * objects are stored.
  * <P>
  * Typically, this is com.sun.jndi.ldap.LdapCtxFactory.
  * <P>
  * @param in String initial context factory class name.
  * @see org.openeai.config.ConsumerConfig
  **/
  public final void setInitialContextFactory(String in) {
    m_initCtxFactory = in;
  }

  /**
  * Returns the class name of the JNDI context factory that will be used to retrieve the JMS
  * administered objects from a Directory Server or wherever the administered
  * objects are stored.
  * <P>
  * Typically, this is com.sun.jndi.ldap.LdapCtxFactory.
  * <P>
  * @return String initial context factory class name.
  * @see org.openeai.config.ConsumerConfig
  **/
  public final String getInitialContextFactory() {
    return m_initCtxFactory;
  }

  /**
  * Establishes an InitialContext to be used by the calling application to
  * retrieve ConnectionFactories and Destination objects via JNDI.
  * @return        DirContext the InitialContext object established
  *
  **/
  protected DirContext getInitialContext() throws NamingException {
    DirContext ic = null;
    Hashtable env = new Hashtable(5, 0.75f);

    boolean useSsl = false;
    String tempProviderUrl = getProviderUrl();
    if (getProviderUrl().indexOf("ldaps:") != -1) {
      useSsl = true;
      // Replace the ldaps with ldap
      tempProviderUrl = 
          "ldap:" + getProviderUrl().substring(getProviderUrl().indexOf(":") + 
                                               1);
      //      setProviderUrl(newProviderUrl);
      logger.debug("ProviderUrl was changed to " + getProviderUrl());
    }

    logger.debug("Getting initial context");
    env.put(Context.INITIAL_CONTEXT_FACTORY, getInitialContextFactory());
    logger.debug("Set initCtxFactory to " + getInitialContextFactory());
    env.put(Context.PROVIDER_URL, tempProviderUrl);
    logger.debug("Set providerUrl to " + tempProviderUrl);

    if (useSsl) {
      // Bind via ssl
      logger.debug("Setting SECURITY_PROTOCOL to ssl");
      env.put(Context.SECURITY_PROTOCOL, "ssl");
    }

    // Authentication stuff if it's there?
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    env.put(Context.SECURITY_PRINCIPAL, getSecurityPrincipal());
    env.put(Context.SECURITY_CREDENTIALS, getSecurityCredentials());

    logger.info("Creating a new InitialContext object (ie - connecting to the DirectoryServer to retrieve Administered Object)...");
    logger.debug("Environment: " + env);
    Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
    ic = new InitialDirContext(env);
    logger.info("Created the InitialContext object...");
    return ic;
  }

  /**
  * Returns a SyncCommand from the HashMap of SyncCommands supported by this Consumer.
  * Since both PubSub and PointToPoint Consumers may handle sync commands, this
  * is specified in this ancestor class.  This method looks for the SyncCommand
  * with a name matching the name passed in and returns it.  This will be an
  * initialized command that is ready to be executed.  The consumer will call this
  * method when it consumes a message and determines that there is no expected reply.
  * <P>
  * It will use the "COMMAND_NAME" JMS Property from the message to
  * retrieve that SyncCommand from its list.
  * <P>
  * @param commandName String the name of the SyncCommand to find (COMMAND_NAME property on the JMS Message)
  * @return SyncCommand the SyncCommand that matches the name passed in and should
  * be executed.
  * @throws IOException if a SyncCommand with the specified name cannot be found.
  **/
  protected SyncCommand getSyncCommand(String commandName) throws IOException {
    if (commandName == null) {
      commandName = "";
    }
    if (m_messages.containsKey(commandName.toLowerCase())) {
      return (SyncCommand)m_messages.get(commandName.toLowerCase());
    }
    else {
      throw new IOException("Invalid message identifier " + commandName);
    }
  }

  /**
  * Calls the initSyncCommands() method that will retrieve all SyncCommands from
  * the ConsumerConfig object passed to the constructor and initialize those commands
  * making them available for use.
  * <P>
  * This method is extended by the PointToPoint Consumer because it also needs to
  * initialize RequestCommands.
  * <P>
  * @throws JMSException if errors occur initializing the SyncCommands.
  * @see MessageConsumer#initSyncCommands()
  * @see PointToPointConsumer#initializeConsumer()
  **/
  public void initializeConsumer() throws JMSException {
    try {
      initSyncCommands();
      setInitializationStatus(INITIALIZED);
    }
    catch (Exception e) {
      logger.fatal(e.getMessage(), e);
      throw new JMSException(e.getMessage());
    }
  }

  /**
  * Adds an initialized SyncCommand object to the list of SyncCommands that might be executed by
  * this Consumer.  Uses the className and CommandConfig object associated to the commandName paramater
  * passed in to instantiate the command.  Then, it adds that initialized command to this Consumer's HashMap of SyncCommands
  * that might be executed by either a PointToPoint or PubSub Consumer.  Therefore, the
  * command itself is only intantiated and initialized once, then, it's exeucte method
  * is called whenever this consumer determines that a message it consumed is to be
  * processed by that command.
  * <P>
  * It is here that the determination is made wether or not the command is the "default"
  * or "absolute" command based on information found in the CommandConfig object
  * associated to the Command.
  * <P>
  * @param commandName String name of the command
  * @param className String class name of the command that should be instantiated and
  * initialized as specified in the CommandConfig Element/Java object
  * @see org.openeai.config.CommandConfig
  * @see org.openeai.config.ConsumerConfig
  * @see org.openeai.jms.consumer.commands.ConsumerCommand
  * @see org.openeai.jms.consumer.commands.RequestCommand
  * @see org.openeai.jms.consumer.commands.RequestCommandImpl
  * @see org.openeai.jms.consumer.commands.SyncCommand
  * @see org.openeai.jms.consumer.commands.SyncCommandImpl
  **/
  public final void addSyncCommand(String commandName, 
                                   String className) throws JMSException {
    try {
      logger.info("Initializing Sync Command: " + commandName + "->" + 
                  className + " for gateway: " + getAppName());
      CommandConfig theCommandConfig = getCommandConfig(commandName);
      if (theCommandConfig.isDefault()) {
        logger.info(commandName + " is the default SyncCommand for the " + 
                    getConsumerName() + " consumer.");
        setDefaultCommandName(commandName);
      }
      if (theCommandConfig.isAbsolute()) {
        logger.info(commandName + " is the 'absolute' SyncCommand for the " + 
                    getConsumerName() + " consumer.");
        setAbsoluteCommandName(commandName);
      }
      Class[] parms = { theCommandConfig.getClass() };
      java.lang.Class obj = java.lang.Class.forName(className);
      try {
        Constructor c = obj.getConstructor(parms);
        Object[] o = { theCommandConfig };
        try {
          m_messages.put(commandName.toLowerCase(), c.newInstance(o));
        }
        catch (Exception e) {
          logger.fatal(e.getMessage(), e);
          throw new JMSException(e.getMessage());
        }
      }
      catch (Exception ne) {
        logger.fatal(ne.getMessage(), ne);
        throw new JMSException(ne.getMessage());
      }
    }
    catch (Exception e) {
      logger.fatal(e.getMessage(), e);
      throw new JMSException(e.getMessage());
    }
  }

  /**
  * This method should process any pending jobs that are currently in progress
  * and then disconnect this Consumer from the broker.  It is implemented
  * by PointToPoint and PubSub consumers.
  **/
  public abstract void stop();

  /**
  * Iterates through all SyncCommands that were listed in the CommandConfig objects
  * associated to this Consumer in its deployment document and instantiates those
  * SyncCommands by calling the addSyncCommand method.
  * <P>
  * @throws JMSException if errors occur initializing the SyncCommands
  * @see MessageConsumer#addSyncCommand(String, String)
  **/
  protected void initSyncCommands() throws JMSException {
    /*
			Instantiate and add each SyncCommand implementation
			specified in the properties file to the messages hash map
		*/
    logger.debug("Initializing messages...");
    Enumeration syncCommands = getProperties().propertyNames();
    while (syncCommands.hasMoreElements()) {
      String keyName = (String)syncCommands.nextElement();
      if (keyName.toLowerCase().indexOf("synccommand") != -1) {
        String className = getProperties().getProperty(keyName);
        String commandName = keyName.substring(keyName.indexOf(".") + 1);
        addSyncCommand(commandName, className);
      }
    }
  }

  /**
  * This is the class that is used to execute the SyncCommand associated to a
  * message consumed by the Consumer.  When a message is delivered to the Destination
  * onwhich the consumer is connected and there is no expected reply,
  * it will instantiate this class passing a message number and the actual message
  * it consumed and add this classes run method to the ThreadPool (if the ThreadPool
  * is in use).  If the ThreadPool is not in use, the consumer will simply call
  * this classes run method and will block until that command's execution is complete.
  * <P>
  * By adding this "job" to the ThreadPool, the consumer does not wait for the
  * command to complete execution before consuming another message.  Therefore,
  * you can affect how many "jobs" may be in progress by configuring the ThreadPool
  * associated to this Consumer.
  * <P>
  * Since both PubSub and PointToPoint Consumers may handle SyncCommands, this class
  * is defined at this ancestor level.  A PointToPointConsumer also contains a RequestTransaction
  * that executes RequestCommands if the incomming request expects a reply.
  * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see org.openeai.config.ThreadPoolConfig
  * @see org.openeai.threadpool.ThreadPool
  * @see PubSubConsumer.MyTopicListener#onMessage(Message)
  * @see PointToPointConsumer.MyQueueListener#onMessage(Message)
  * @see PointToPointConsumer.RequestTransaction
  **/
  protected class MessageTransaction implements java.lang.Runnable {
    private Message theMessage = null;
    private int m_msgNumber = 0;

    public MessageTransaction(int messageNumber, Message inMsg) {
      theMessage = inMsg;
      m_msgNumber = messageNumber;
    }

    /**
    * Calls the handleSync method passing the message number and Message consumed
    * by the consumer.  We break this out so both PubSub and PointToPoint consumers
    * can inherit the handleSync functionality.
    * <P>
    * @see MessageConsumer#handleSync(int, Message)
    **/
    public void run() {
      try {
        logger.info(getConsumerName() + " - Handling message " + m_msgNumber + 
                    " in MessageTransaction thread.");
        handleSync(m_msgNumber, theMessage);
        logger.info(getConsumerName() + " - Done with MessageTransaction " + 
                    m_msgNumber);
      }
      catch (Exception e) {
        logger.fatal(e.getMessage(), e);
      }
    }
  }

  /**
  * Default SyncCommand execution routine.  This method is called by the
  * MessageTransaction's run method and is used to
  * actually execute the SyncCommand associated to the message consumed.
  * <P>
  * The method will look for a JMS String property called "COMMAND_NAME".  If
  * the property is found, it will execute the SyncCommand associated to that name.
  * This property is automatically set by the OpenEAI Message Object API (MOA) foundation
  * when a message is sent using an organization's MOA implementation (a business object).
  * <P>
  * If an "Absolute" command has been associated to the consumer, it will execute
  * that command implementation NO MATTER what.
  * <P>
  * If no COMMAND_NAME is found on the Message passed in and there has been a "Default"
  * command associated to the consumer, it will execute that command implementation.
  * <P>
  * If no COMMAND_NAME property exists and there has been no "Default" or "Absolute"
  * command associated to this consumer, an error will occur.
  * <P>
  * If a COMMAND_NAME property DOES exist but it doesn't map to a known command implementation
  * and no "Absolute" command has been specified, an error will occur.
  * <P>
  * @param messageNumber int a message number managed by the consumer (the number of messages consumed by the consumer).
  * @param aMessage Message the JMS Message consumed by the consumer.
  * @throws JMSException if errors occur executing the Command.  Note, this should be a very
  * rare occurrence because Commands are generally responsible for handling their own errors.
  * The most common place where an exception might be thrown by a command is if it
  * had problems retrieving the data from the Message passed to it.  Otherwise, the Command
  * should either publish a Sync-Error or return an Error to the requesting application depending
  * on the type of command being executed (RequestCommand vs. SyncCommand)
  **/
  protected void handleSync(int messageNumber, 
                            Message aMessage) throws JMSException {
    // Get message name from Properties
    // - this is what will be mapped to an actual IncomingMessage object
    //   in the "getMessage" method
    String msgName = aMessage.getStringProperty(MessageProducer.COMMAND_NAME);

    // backward compatibility - need to check for MESSAGE_NAME
    if (msgName == null || msgName.length() == 0) {
      msgName = aMessage.getStringProperty(MessageProducer.MESSAGE_NAME);
    }

    logger.debug("Message Name is " + msgName);

    // If the message coming in doesn't have a COMMAND_NAME, then
    // we'll use the default command associated with this consumer.
    // If the consumer doesn't have a default command associated with
    // it, the IOException will be thrown.
    if (msgName == null || msgName.length() == 0) {
      logger.info("No COMMAND_NAME passed in, using 'Default' SyncCommand if one has been defined.");
      msgName = getDefaultCommandName();
    }

    // if the consumer has an "absolute" command name specified, it will be executed
    // no matter what's passed in.
    if (getAbsoluteCommandName() != null && 
        getAbsoluteCommandName().length() > 0) {
      msgName = getAbsoluteCommandName();
      logger.info("This consumer only handles one command.  The 'absolute' command is: " + 
                  msgName);
    }

    SyncCommand msg = null;
    try {
      msg = getSyncCommand(msgName);
    }
    catch (IOException e) {
      logger.warn("Could not handle message " + msgName + ".  Since command " + 
                  "doesn't return a result, this might be okay...");
      return;
    }
    try {
      msg.execute(messageNumber, aMessage);
    }
    catch (CommandException e) {
      String errMessage = 
        "[" + getConsumerName() + "] Exception occurred executing the command " + 
        msgName + " which is implemented in " + msg.getClass().getName() + 
        " Exception: " + e.getMessage();
      logger.fatal(errMessage);
      throw new JMSException(errMessage);
    }
  }

  private void initializeProperties(Properties props) throws IOException {
    try {
      String LOGTAG = "[MessageConsumer.initializeProperties] ";	
    	
      // Properties instance variable
      setProperties(props);

      // Mail Service properties
      setMailHost(props.getProperty("mailHost"));
      setFromAddr(props.getProperty("fromAddr"));
      setToAddr(props.getProperty("toAddr"));
      if (getMailHost() != null && getFromAddr() != null && 
          getToAddr() != null) {
        setMailService(new MailService(getMailHost(), getFromAddr(), 
                                       getToAddr())); // For emailing
      }

      setDebug(new Boolean(props.getProperty("debug", 
                                             "false")).booleanValue());
      setStartOnInitialization(new Boolean(props.getProperty("startOnInitialization", 
                                                             "false")).booleanValue());

      // JMS Properties
      setConnectionFactoryName(props.getProperty("ConnectionFactoryName"));
      setDestinationName(props.getProperty("DestinationName"));
      setInitialContextFactory(props.getProperty("InitialContextFactory", ""));
      setProviderUrl(props.getProperty("ProviderURL", ""));
      setSecurityPrincipal(props.getProperty("SecurityPrincipal", ""));
      setSecurityCredentials(props.getProperty("SecurityCredentials", ""));
      setUserName(props.getProperty("Username", ""));
      setPassword(props.getProperty("Password", ""));
      setConsumerName(props.getProperty("name", ""));
      setInstanceName(props.getProperty("instanceName", ""));
      setTransacted(new Boolean(props.getProperty("transacted", 
                                                  "false")).booleanValue());
      setValidation(new Boolean(props.getProperty("validate", 
                                                  "false")).booleanValue());
      setMaximumThreadPoolShutdownWaitTime(new Integer(props.getProperty("maximumThreadPoolShutdownWaitTime", 
                                                                         "15000")).intValue());
      
      setPerformMonitorOperation(new Boolean(props.getProperty("performMonitorOperation", 
              "true")).booleanValue());
      logger.info(LOGTAG + "performMonitorOperation is: " + getPerformMonitorOperation());

      try {
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        setGenericErrorDoc(xmlReader.initializeDocument(props.getProperty("DocumentURI", 
                                                                          ""), 
                                                        getValidation()));
      }
      catch (XmlDocumentReaderException e) {
        logger.fatal(e.getMessage(), e);
      }
    }
    catch (Exception a) {
      throw new IOException("Could not initialize MessageConsumer properties " + 
                            a.getMessage());
    }
  }

  /**
  * Takes the Properties object contained within the ConsumerConfig object passed
  * to the constructor of this consumer and sets all the appropriate instance variables
  * on this consumer during initialization.
  * <P>
  * @param props Properties the Java properties object retrieved from the ConsumerConfig
  * object built from the ConsumerConfig Element in the gateway's deployment document.
  * @throws IOException if errors occur populating the instance variables from the
  * Properties object.
  **/
  protected void init(Properties props) throws IOException {
    initializeProperties(props);
  }
}
