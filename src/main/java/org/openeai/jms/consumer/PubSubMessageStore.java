/*******************************************************************************
 $Source$
 $Revision: 1472 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.consumer;

import org.openeai.dbpool.*;
import org.openeai.*;
import org.openeai.moa.objects.resources.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import javax.jms.*;

/**
 * A Class class.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class PubSubMessageStore extends OpenEaiObject {

	private TopicSession m_topicSession = null;
  private EnterpriseConnectionPool m_connectionPool = null;
  private Vector m_storedMessages = new Vector();   // The entire message
  private Vector m_storedMessageIds = new Vector();
  private String m_outputDir = "";
  private boolean m_guaranteedOrderOfProcessing = false;
  private boolean m_storeMonitorRunning = false;
  private boolean m_storeCheckPointRunning = false;
  private final String m_available = "available";
  private final String m_unavailable = "unavailable";
  private final String m_removed = "removed";

  /**
   * Constructor
   */
  public PubSubMessageStore(Properties props) throws IOException {
    setProperties(props);    // save incase we have to re-initialize the conn pool

    setOutputDir(props.getProperty("storeOutputDirectory",""));
  	setGuaranteedOrderOfProcessing(new Boolean(props.getProperty("guaranteedOrderOfProcessing", "false")).booleanValue());

    /*
    try {
      m_connectionPool = new EnterpriseConnectionPool("MessageStoreConnectionPool", props);
    }
    catch (Exception e) {
      String errMessage = "Error initializing 'MessageStore' DB Connection pool.  Exception: " + e.getMessage();
      logger.warn(errMessage);
//      throw new IOException(errMessage);
    }
    */

    // Start the store monitor if it isn't already running.
    if (m_storeMonitorRunning == false) {
      MonitorStore monitorStore = new MonitorStore(30000);
      new Thread(monitorStore).start();
      m_storeMonitorRunning = true;
    }

    // Start the store checkpoint thread if it isn't already running.
    if (m_storeCheckPointRunning == false) {
      StoreCheckPoint storeCheckPoint = new StoreCheckPoint(30000);
      new Thread(storeCheckPoint).start();
      m_storeCheckPointRunning = true;
    }
  }

  public TopicSession getTopicSession() {
    return m_topicSession;
  }
  public void setTopicSession(TopicSession session) {
    m_topicSession = session;
  }

  public void setGuaranteedOrderOfProcessing(boolean gop) {
    m_guaranteedOrderOfProcessing = gop;
  }
  public boolean getGuaranteedOrderOfProcessing() {
    return m_guaranteedOrderOfProcessing;
  }

  private String getOutputDir() {
    return m_outputDir;
  }
  private void setOutputDir(String outputDir) {
    m_outputDir = outputDir;
  }

  public Vector getStoredMessages() {
    return m_storedMessages;
  }
  public void setStoredMessages(Vector messages) {
    m_storedMessages = messages;
  }

  public Vector getStoredMessageIds() {
    return m_storedMessageIds;
  }
  public void setStoredMessageIds(Vector messages) {
    m_storedMessageIds = messages;
  }

  public void addStoredMessage(StoredMessage sMessage) {
    if (sMessage.getPredecessor() != null) {
      // we need to persit it because it can't be processed until
      // the predecessor has been processed.
    }
    getStoredMessages().add(sMessage);                  // Add the entire message to the store
    getStoredMessageIds().add(sMessage.getMessageId()); // Add the message id to the store which points to a stored message
  }
  public StoredMessage getStoredMessage() throws SQLException {
    // returns the next message that's ready to be processed.

    // Check the "in-memory" store
    for (int i=0; i<getStoredMessages().size(); i++) {
      StoredMessage sMsg = (StoredMessage)getStoredMessages().get(i);
      if (sMsg.getStatus().equalsIgnoreCase(m_available)) {

        sMsg.setStatus(m_unavailable);
        return sMsg;

        /*
        // If guaranteed order of processing is turned off,
        // don't worry about checking anything, just return the first
        // message in the store.
        if (getGuaranteedOrderOfProcessing() == false) {
          return sMsg;
        }

        // If it has no predecessors, just return it because it can be processed.
        if (sMsg.getPredecessor() == null) {
  //        String msgId = sMsg.getMessageId();
  //        getStoredMessages().removeElementAt(i);
  //        getStoredMessageIds().contains(
          return sMsg;
        }

        // If it has a predecessor, we'll need to check the status of its predecessor
        // and only return it if it's predecessor has been processed.
        if (predecessorExists(sMsg.getPredecessor())) {
  //        String msgId = sMsg.getMessageId();
  //        getStoredMessages().removeElementAt(i);
          return sMsg;
        }
        */
      }
    }

    // retrieve a certain number of "StoredMessage" from the "persistent" store and
    // add them to the "in-memory" store.
    buildStoreFromPersistentRepository(100);
    if (getStoredMessages().size() > 0) {
      return getStoredMessage();
    }


    return null;
  }
  public void removeStoredMessage(StoredMessage sMsg) {
    // put sMsg in m_storedMessages in place of the one that's there with the same message id.
    for (int i=0; i<getStoredMessages().size(); i++) {
      StoredMessage storedMsg = (StoredMessage)getStoredMessages().get(i);
      if (storedMsg.getMessageId().equals(sMsg.getMessageId())) {
        storedMsg.setStatus(m_removed);
      }
    }
  }
  public void persistStore() throws JMSException, SQLException {
    // Take the current in memory store and persist it to some
    // external place, removing the "StoredMessages" from memory (m_storedMessages)
    // as they are persisted.

    for (int i=0; i<getStoredMessages().size(); i++) {
      StoredMessage storedMsg = (StoredMessage)getStoredMessages().get(i);
      if (storedMsg.getStatus().equalsIgnoreCase(m_removed) == false) {
        String msgId = storedMsg.getMessageId();
        TextMessage msg = (TextMessage)storedMsg.getMessage();
		  	StringBuffer sBuf = new StringBuffer();
        Enumeration propNames = msg.getPropertyNames();
        while (propNames.hasMoreElements()) {
          String propName = (String)propNames.nextElement();
          String propValue = msg.getStringProperty(propName);
          if (propValue != null) {
            // it's a property we care about.
            sBuf.append("JMS_Property:" + propName + "=" + propValue + "\n");
          }
        }
        String msgText = msg.getText();
        sBuf.append(msgText);
        String fileName = getOutputDir() + "/" + msgId + ".txt";
        try {
          logger.info("[PubSubMessageStore] persisting StoredMessage to " + fileName);
          FileOutputStream fos = new FileOutputStream(fileName);
          fos.write(new String(sBuf).getBytes());
          getStoredMessages().removeElementAt(i);
        }
        catch (Exception e) {
          String errorMessage = "Error persisting StoredMessage to disk (FileName: " +
            fileName + ").  Exception: " + e.getMessage();
          logger.fatal(errorMessage);
          // Continue on to the next one...
  //        throw new JMSException(errorMessage);
        }
      }
    }
    /*
    logger.info("Writing message to file: " + msgDumpDir + "/" + fileName);
    try {
 			xmlOut.output(doc, new FileOutputStream(msgDumpDir + "/" + fileName));
    }
    catch (Exception e) {
      logger.fatal("Error dumping message to a file.");
      logger.fatal(e.getMessage(), e);
    }
    */
  }

  private void buildStoreFromPersistentRepository(int numberOfMessages) {
    String fileSpec = getOutputDir();
    File m_file = new File(fileSpec);
    File[] m_fileList = m_file.listFiles();
    logger.info("There are " + m_fileList.length + " files.");
    for (int i=0; i<m_fileList.length; i++) {
      File aFile = m_fileList[i];
      if (aFile.isFile()) {
        try {
          Properties jmsProps = new Properties();
          StringBuffer sBuf = new StringBuffer();
          BufferedReader br = new BufferedReader(new FileReader(aFile));
          while (br.ready()) {
            String currentLine = br.readLine().trim();
            if (currentLine != null && currentLine.length() > 0) {
              if (currentLine.indexOf("JMS_Property:") != -1) {
                String propName = currentLine.substring(currentLine.indexOf(":") + 1, currentLine.indexOf("=") - 1);
                String propValue = currentLine.substring(currentLine.indexOf("=") + 1);
                jmsProps.put(propName, propValue);
              }
              else {
                sBuf.append(currentLine);
              }
            }
          }
          br.close();

          TextMessage tMsg = getTopicSession().createTextMessage();
          tMsg.setText(new String(sBuf));
          Enumeration jmsEnum = jmsProps.propertyNames();
          while (jmsEnum.hasMoreElements()) {
            String propName = (String)jmsEnum.nextElement();
            String propValue = jmsProps.getProperty(propName);
            if (propValue != null) {
              tMsg.setStringProperty(propName, propValue);
            }
          }

          /*
          FileInputStream fis = new FileInputStream(aFile);
          byte[] fBuf = new byte[fis.available()];
          fis.read(fBuf);
          fis.close();
          String storedMessageAsFile = new String(fBuf);
          */
        }
        catch (JMSException jmse) {
          logger.fatal("Error Creating JMS TextMessage to store persisted file " +
            aFile.getPath() + ".  Exception: " + jmse.getMessage());
        }
        catch (Exception e) {
          logger.fatal("Error reading file " + aFile.getPath() + ".  Exception: " + e.getMessage());
        }
      }
    }
  }

  public boolean predecessorExists(String msgSeries) throws SQLException {
    return true;
    /*
		PreparedStatement getStmt = null;
		String getString = "SELECT MESSAGE_SERIES " +
											 "FROM T_PROCESSED_MESSAGE WHERE MESSAGE_SERIES=?";

		ResultSet results=null;
		try {
  		java.sql.Connection conn = m_connectionPool.getConnection();
 			getStmt = conn.prepareStatement(getString);
			getStmt.clearParameters();
			getStmt.setString(1, msgSeries);
			results = getStmt.executeQuery();
      if (results.next()) {
        getStmt.close();
        return true;
      }
      getStmt.close();
		}
		catch (Exception e) {
      if (getStmt != null) {
        getStmt.close();
      }
      String errMessage = "Error retrieving Message Series from database.  Exception: " + e.getMessage();
			throw new SQLException(errMessage);
		}
    return false;
    */
  }

	private class StoreCheckPoint implements java.lang.Runnable {
    private int m_sleepInterval = 30000;   // thirty seconds
		public StoreCheckPoint(int sleepInterval) {
      m_sleepInterval = sleepInterval;
		}

    public void run() {
      // sleep for m_sleepInterval
      // wake up and persist all StoredMessages if
      // a certain threshold has been reached.
      boolean stayAlive = true;
      while(stayAlive) {
        try {
          Thread.sleep(m_sleepInterval);
        }
        catch (Exception e) {
          logger.fatal("Error sleeping...");
        }

        // wake up and persist...
        boolean keepPersisting = true;
        while (keepPersisting) {
          try {
            persistStore();
            keepPersisting = false;
          }
          catch(Exception e) {
            logger.fatal("Failed to completely persist my store!!!  Exception: " + e.getMessage());
          }
        }
      }
    }
  }

	private class MonitorStore implements java.lang.Runnable {
    private int m_sleepInterval = 30000;   // thirty seconds
		public MonitorStore(int sleepInterval) {
      m_sleepInterval = sleepInterval;
		}

    public void run() {
      // sleep for m_sleepInterval
      // wake up, try to do something with the session
      // if an exception occurs, restart the consumer
      boolean stayAlive = true;
      while(stayAlive) {
        try {
          Thread.sleep(m_sleepInterval);
        }
        catch (Exception e) {
          logger.fatal("Error sleeping...");
        }

        // wake up and check for any stored message that have been marked as
        // unavailable for a certain period of time.  If it has, mark it
        // as available again.
        try {
          for (int i=0; i<getStoredMessages().size(); i++) {
            StoredMessage sMsg = (StoredMessage)getStoredMessages().get(i);
            if (sMsg.getStatus().equalsIgnoreCase(m_unavailable)) {
              if (sMsg.getStatusChangeTime() - System.currentTimeMillis() > 20000) {
                sMsg.setStatus(m_available);
              }
            }
          }

          // Purge any StoredMessages that have been marked as "removed"
          for (int i=0; i<getStoredMessages().size(); i++) {
            StoredMessage sMsg = (StoredMessage)getStoredMessages().get(i);
            if (sMsg.getStatus().equalsIgnoreCase(m_removed)) {
              String msgId = sMsg.getMessageId();
              getStoredMessages().removeElementAt(i);
              int index = getStoredMessageIds().indexOf(msgId);
              if (index != -1) {
                getStoredMessageIds().removeElementAt(index);
              }
            }
          }
        }
        catch(Exception e) {
          logger.fatal(".  Exception: " + e.getMessage());
        }
      }
    }

    private boolean purgeFromRepository(StoredMessage sMsg) {
      String fileName = getOutputDir() + "/" + sMsg.getMessageId() + ".txt";
      File sMsgFile = new File(fileName);
      return sMsgFile.delete();
    }
  }
}

