/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
// Java utilities
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Log4j
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
// OpenEAI foundation
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;

/**
 * An example object provider that maintains an in-memory store of stacks. It is
 * required that the moa and querySpecification object both have getXxxId()
 * method in order to use this class
 *
 * @author George Wang (gw2100@gmail.com)
 * @author Steve Wheat (swheat@emory.edu)
 *
 */
public class ExampleGenerateProvider<M, R> extends AbstractGenerateProvider<M, R> {
    private static Logger logger = Logger.getLogger(ExampleGenerateProvider.class);
    private HashMap<String, M> idToMoa = new HashMap();
    private static String LOGTAG = "[ExampleCrudProvider] ";

    @Override
    public M generate(R r) throws ProviderException {
        // Class.forName(m.getClass().getName()).getConstructor(String.class).newInstance();
        M m = Provider.createTemplateInstance(this, 0);
        try {
            m = (M) appConfig.getObjectByType(m.getClass().getName());
        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " + "AppConfig. The exception is: " + ecoe.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, ecoe);
        }
        try {
            String id = null;
            if (getXxxId(r) == null || getXxxId(r).length() == 0) {
                id = String.valueOf(idToMoa.size());
            } else
                id = getXxxId(r);
            setXxxId(m, id);
            idToMoa.put(getXxxId(m), m);
        } catch (Exception e) {
            logger.error(e);
            throw new ProviderException("", e);
        }
        return m;
    }
}
