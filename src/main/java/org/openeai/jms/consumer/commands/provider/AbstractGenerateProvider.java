/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.util.List;

/**
 *
 * @author George Wang (gw2100@gmail.com)
 *
 */
public class AbstractGenerateProvider<M, R> extends AbstractProvider implements GenerateProvider<M, R> {

    @Override
    public M generate(R r) throws ProviderException {
        throw new UnsupportedOperationException();
    }

}
