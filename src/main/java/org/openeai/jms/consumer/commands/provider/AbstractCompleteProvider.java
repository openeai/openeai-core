/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.util.List;

/**
 *
 * @author George Wang (gw2100@gmail.com)
 *
 */
public class AbstractCompleteProvider<M, Q, R> extends AbstractProvider implements CompleteProvider<M, Q, R> {
    @Override
    public List<M> query(Q querySpec) throws ProviderException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void create(M m) throws ProviderException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(M m) throws ProviderException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(M m) throws ProviderException {
        throw new UnsupportedOperationException();
    }

    @Override
    public M generate(R r) throws ProviderException {
        throw new UnsupportedOperationException();
    }

}
