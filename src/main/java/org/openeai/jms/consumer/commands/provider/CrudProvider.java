/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

// Core Java
import java.util.List;

import org.openeai.config.AppConfig;

/**
 * <P>
 * 
 * 
 * @version 1.0 - 25 December 2016
 */
public interface CrudProvider<M, Q> extends Provider {

    /**
     * 
     * <P>
     * 
     * @param Q
     *            Specficiation, the query parameter.
     * @return List, a list of matching Stack objects.
     *         <P>
     * @throws ProviderException
     *             with details of the providing the list.
     */
    List<M> query(Q querySpec) throws ProviderException;

    /**
     * 
     * <P>
     * 
     * @param M,
     *            the generate parameter.
     * @return Stack, a generated Stack for the requisition.
     *         <P>
     * @throws ProviderException
     *             with details of the error generating the stack.
     */
    void create(M m) throws ProviderException;

    /**
     * 
     * <P>
     * 
     * @param M,
     *            the object to delete.
     *            <P>
     * @throws ProviderException
     *             with details of the error deleting the stack.
     */
    void delete(M m) throws ProviderException;
    void update(M m) throws ProviderException;
}