/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
// Java utilities
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Log4j
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
// OpenEAI foundation
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;

/**
 *
 * @author George Wang (gw2100@gmail.com)
 *
 */
public interface Provider {
    void init(AppConfig aConfig) throws ProviderException;
    /**
     * @param object
     *            instance of a class that is a subclass of a generic class
     * @param index
     *            index of the generic type that should be instantiated
     * @return new instance of T (created by calling the default constructor)
     * @throws RuntimeException
     *             if T has no accessible default constructor
     */
    @SuppressWarnings("unchecked")
    public static <T> T createTemplateInstance(Object object, int index) {
        ParameterizedType superClass = (ParameterizedType) object.getClass().getGenericSuperclass();
        Type type = superClass.getActualTypeArguments()[index];
        Class<T> instanceType;
        if (type instanceof ParameterizedType) {
            instanceType = (Class<T>) ((ParameterizedType) type).getRawType();
        } else {
            instanceType = (Class<T>) type;
        }
        try {
            return instanceType.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
