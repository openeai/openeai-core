/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

// Core Java
import java.util.List;

import org.openeai.config.AppConfig;

/**
 * <P>
 * 
 * 
 */
public interface CompleteProvider<M, Q, R> extends CrudProvider<M, Q>, GenerateProvider<M, R> {

}