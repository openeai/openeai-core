/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands;

import javax.jms.Message;

// Log4j
import org.apache.log4j.Logger;
import org.openeai.config.CommandConfig;
import org.openeai.jms.consumer.commands.provider.CrudProvider;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;

/**
 * 
 * If the Service provide Create, Update, Delete, Query operation, use this
 * Command. Required parameters:
 * 
 * M, messageObject; Q, queryObject; P, provider
 */

public class GenericCrudRequestCommand<M extends ActionableEnterpriseObject, Q extends XmlEnterpriseObject, P extends CrudProvider<M, Q>>
        extends GenericCrudRequestCommandBase<M, Q, P> implements RequestCommand {
    private static String LOGTAG = "[GenericCrudRequestCommand] ";
    private static Logger logger = Logger.getLogger(GenericCrudRequestCommand.class);
    public GenericCrudRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        return executeHelper(messageNumber, aMessage, false);
    }

}
