/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
// Java utilities
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Log4j
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
// OpenEAI foundation
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;

/**
 * An example object provider that maintains an in-memory store of stacks. It is
 * required that the moa and querySpecification object both have getXxxId()
 * method in order to use this class
 *
 * @author George Wang (gw2100@gmail.com)
 * @author Steve Wheat (swheat@emory.edu)
 *
 */
public class ExampleCrudProvider<M, Q> extends AbstractCrudProvider<M, Q> {
    private static Logger logger = Logger.getLogger(ExampleCrudProvider.class);
    protected HashMap<String, M> idToMoa = new HashMap();
    private static String LOGTAG = "[ExampleCrudProvider] ";

    @Override
    public List<M> query(Q querySpec) throws ProviderException {
        if (getXxxId(querySpec) == null || getXxxId(querySpec).equals("")) {
            String errMsg = "The id is null. The ExampleCrudProvider" + "presently only implements query by xxxId.";
            throw new ProviderException(errMsg);
        }
        List<M> moas = new ArrayList();
        if (idToMoa.get(getXxxId(querySpec)) == null)
            moas.addAll(idToMoa.values());
        else
            moas.add(idToMoa.get(getXxxId(querySpec)));
        return moas;
    }

    /**
     * @see StackProvider.java
     */
    @Override
    public void create(M m) throws ProviderException {
        try {
            if (getXxxId(m) == null || getXxxId(m).length() == 0) {
                setXxxId(m, String.valueOf(idToMoa.size()));
            }
            idToMoa.put(getXxxId(m), m);
        } catch (Exception e) {
            logger.error(e);
            throw new ProviderException("", e);
        }
    }

    /**
     * @see StackProvider.java
     */
    @Override
    public void update(M m) throws ProviderException {
        idToMoa.put(getXxxId(m), m);
        return;
    }

    @Override
    public void delete(M m) throws ProviderException {
        idToMoa.remove(getXxxId(m));
        return;
    }

}
