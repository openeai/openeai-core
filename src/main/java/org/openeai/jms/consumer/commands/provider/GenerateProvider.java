/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

/**
 * <P>
 * 
 */
public interface GenerateProvider<M, R> extends Provider {

    M generate(R r) throws ProviderException;
}