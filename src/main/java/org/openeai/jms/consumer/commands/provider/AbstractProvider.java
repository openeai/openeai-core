/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
// OpenEAI foundation
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;

/**
 *
 * @author George Wang (gw2100@gmail.com)
 *
 */
public class AbstractProvider extends OpenEaiObject implements Provider {
    private static Logger logger = Logger.getLogger(AbstractProvider.class);
    protected AppConfig appConfig;
    protected PropertyConfig pConfig;
    private static String LOGTAG = "[AbstractProvider] ";

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        logger.info(LOGTAG + "Initializing...");
        appConfig = aConfig;
        try {
            pConfig = (PropertyConfig) aConfig.getObject("ProviderProperties");
            setProperties(pConfig.getProperties());
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving a PropertyConfig object from " + "AppConfig: The exception is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, eoce);
        }
        logger.info(LOGTAG + pConfig.getProperties().toString());
        logger.info(LOGTAG + "Initialization complete.");
    }

    protected static String getXxxId(Object m) throws ProviderException {
        Method[] methods = m.getClass().getMethods();
        for (Method method : methods) {
            if (method.getParameterCount() == 0 && method.getName().endsWith("Id") && method.getReturnType().equals(String.class))
                try {
                    return (String) method.invoke(m, null);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    String errorMsg = "getXxxId() method not found";
                    logger.error(LOGTAG + errorMsg, e);
                    throw new ProviderException("getXxxId() method not found", e);
                }
        }
        throw new ProviderException("getXxxId() method not found");

    }
    protected static String setXxxId(Object m, String id) throws ProviderException {
        Method[] methods = m.getClass().getMethods();
        for (Method method : methods) {
            if (method.getParameterCount() == 1 && method.getName().endsWith("Id") && method.getReturnType().equals(Void.TYPE))
                try {
                    return (String) method.invoke(m, id);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    String errorMsg = "setXxxId() method not found";
                    logger.error(LOGTAG + errorMsg, e);
                    throw new ProviderException("setXxxId() method not found", e);
                }
        }
        throw new ProviderException("setXxxId() method not found");
    }

    public static String getDeployEnv() {
        // docUriBase.dev=https://dev-config.app.emory.edu/
        // docUriBase.qa=https://qa-config.app.emory.edu/
        // docUriBase.stage=https://staging-config.app.emory.edu/
        // docUriBase.prod=https://config.app.emory.edu
        String docUriBase = System.getProperty("docUriBase");
        if (docUriBase != null && docUriBase.length() > 0) {
            int indexSlashSlash = docUriBase.indexOf("//");
            int indexConfig = docUriBase.indexOf("config");
            if (indexSlashSlash > 0 && indexConfig > 0) {
                String env = docUriBase.substring(indexSlashSlash + 2, indexConfig);
                if (env.length() == 0)
                    return "prod";
                if (env.endsWith("-"))
                    return env.substring(0, env.indexOf("-"));
            }
        }
        return "";
    }
}
