/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands;

// Core Java
import java.util.ArrayList;
import java.util.List;

// Java Message Service
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

// Log4j
import org.apache.log4j.Logger;
// JDOM
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Authentication;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.transport.SyncService;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;
import org.openeai.jms.consumer.commands.provider.CompleteProvider;
import org.openeai.jms.consumer.commands.provider.GenerateProvider;
import org.openeai.jms.consumer.commands.provider.Provider;
import org.openeai.jms.consumer.commands.provider.ProviderException;

/**
 * 
 * If the service only need Generate operation, use this Command.
 * 
 * M, messageObject; R requisitionObject; P, provider
 * 
 * 
 */

public class GenericGenerateRequestCommand<M extends ActionableEnterpriseObject, R extends XmlEnterpriseObject, P extends GenerateProvider<M, R>>
        extends AbstractGenericRequestCommand implements RequestCommand {
    private static String LOGTAG = "[GenericGenerateRequestCommand] ";
    private static Logger logger = Logger.getLogger(GenericGenerateRequestCommand.class);
    protected GenerateProvider<M, R> m_provider = null;

    protected M m = newM();;
    protected String moaClassName;
    protected R requisition = newR();;
    protected String requisitionClassName;
    protected TestId testId = new TestId();
    protected Document m_generateResponseDoc = null;
    public GenericGenerateRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing... ");
        initProvider();
        initM();
        initR();
        initTestId();
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            m_generateResponseDoc = xmlReader.initializeDocument(getProperties().getProperty("generateResponseDocumentUri"),
                    getOutboundXmlValidation());
        } catch (XmlDocumentReaderException e) {
            String errMsg = "An error occurred initializing the primed reponse " + "document. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg, e);
            throw new InstantiationException(e.getMessage());
        }
        logger.info(LOGTAG + "instantiated successfully.");
    }

    protected void initTestId() throws InstantiationException {
        try {
            testId = (TestId) getAppConfig().getObjectByType(testId.getClass().getName());
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }
    protected void initR() throws InstantiationException {
        try {
            requisition = (R) getAppConfig().getObjectByType(requisition.getClass().getName());
            requisitionClassName = requisition.getClass().getSimpleName();
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }
    protected void initM() throws InstantiationException {
        try {
            m = (M) getAppConfig().getObjectByType(m.getClass().getName());
            moaClassName = m.getClass().getSimpleName();
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }

    protected void initProvider() throws InstantiationException {
        String className = getProperties().getProperty("providerClassName");
        if (className == null || className.equals("")) {
            String errMsg = "No providerClassName property " + "specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        logger.info(LOGTAG + "providerClassName is: " + className);

        try {
            logger.info(LOGTAG + "Getting class for name: " + className);
            Class providerClass = Class.forName(className);
            logger.info(LOGTAG + "providerClass =" + providerClass);
            m_provider = (P) providerClass.newInstance();
            logger.info(LOGTAG + "Initializing PProvider: " + m_provider.getClass().getName());
            m_provider.init(getAppConfig());
            logger.info(LOGTAG + "PProvider initialized.");
            setProvider(m_provider);
        } catch (ClassNotFoundException | ProviderException | IllegalAccessException e) {
            logger.fatal(LOGTAG, e);
            throw new InstantiationException(e.getMessage());
        }
    }
    M newM() {
        return Provider.createTemplateInstance(this, 0);
    }
    R newR() {
        return Provider.createTemplateInstance(this, 1);
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        long startTime = System.currentTimeMillis();
        Document localGenerateResponseDoc = (Document) m_generateResponseDoc.clone();
        M m = null;
        R requisition = null;
        TestId testId = null;
        try {
            m = (M) this.m.clone();
            requisition = (R) this.requisition.clone();
            testId = (TestId) this.testId.clone();
        } catch (CloneNotSupportedException e) {
            throw new CommandException(e);
        }

        Document localResponseDoc = (Document) getResponseDocument().clone();
        Document inDoc = null;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
        } catch (Exception e) {
            throw new CommandException(e);
        }
        if (getVerbose())
            logger.info("Message sent in is: \n" + getMessageBody(inDoc));
        TextMessage msg = (TextMessage) aMessage;
        try {
            msg.clearBody();
        } catch (JMSException jmse) {
            throw new CommandException(jmse);
        }
        Element eControlArea = getControlArea(inDoc.getRootElement());
        String msgAction = eControlArea.getAttribute("messageAction").getValue();
        String msgObject = eControlArea.getAttribute("messageObject").getValue();
        if (!msgObject.equalsIgnoreCase(moaClassName)) {
            return returnWithError(localResponseDoc, inDoc, msg, eControlArea, msgObject, moaClassName);
        }

        /// validation
        Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
        String authUserId = eAuthUserId.getValue();
        if (authUserId.equalsIgnoreCase("TestSuiteApplication")) {
            authUserId = "testsuiteapp@emory.edu/127.0.0.1";
        }
        if (!validateAuthUserId(authUserId)) {
            logger.fatal(LOGTAG
                    + ("Invalid AuthUserId. The value '" + authUserId + "' is not valid. The expected format is user@domain/ip number."));
            logger.fatal(LOGTAG + "Message sent in is: \n" + getMessageBody(inDoc));
            ArrayList errors = new ArrayList();
            errors.add(buildError(errorType, "GenericCrudRequestCommand-1001",
                    "Invalid AuthUserId. The value '" + authUserId + "' is not valid. The expected format is user@domain/ip number."));
            String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }
        Element eTestId = inDoc.getRootElement().getChild("ControlAreaRequest").getChild("Sender").getChild("TestId");
        if (eTestId != null)
            try {
                testId.buildObjectFromInput(eTestId);
            } catch (EnterpriseLayoutException e) {
                return returnWithError(localResponseDoc, inDoc, msg, eControlArea, e);
            }
        //// GENERATE
        if (msgAction.equalsIgnoreCase("Generate")) {
            logger.info(LOGTAG + "Handling a.Generate-Request message.");
            Element eGenerateObject = inDoc.getRootElement().getChild("DataArea").getChild(requisitionClassName);
            if (eGenerateObject == null) {
                logger.error(LOGTAG + requisitionClassName + " cannot be found");
                return returnWithError(inDoc, inDoc, msg, eGenerateObject);
            }
            try {
                requisition.buildObjectFromInput(eGenerateObject);
                requisition.setTestId(testId);
            } catch (EnterpriseLayoutException ele) {
                return returnWithError(localResponseDoc, inDoc, msg, eControlArea, ele);
            }
            logger.info(LOGTAG + "Generating a moa " + moaClassName);
            try {
                long createStartTime = System.currentTimeMillis();
                CompleteProvider completeProvider = (CompleteProvider) getProvider();
                m = (M) completeProvider.generate(requisition);
                logger.info(LOGTAG + "Generated moa in " + (System.currentTimeMillis() - createStartTime) + " ms.");
            } catch (Throwable pe) {
                return returnWithError(localResponseDoc, inDoc, msg, eControlArea, pe);
            }
            logger.info(LOGTAG + "Generated moa " + moaClassName);

            logger.info(LOGTAG + "Publishing sync... ");
            try {
                MessageProducer producer = getProducerPool().getProducer();
                if (getVerbose())
                    logger.info(LOGTAG + "Publishing " + moaClassName + ".Create-Sync message...");
                Authentication auth = new Authentication();
                auth.setAuthUserId(authUserId);
                auth.setAuthUserSignature("none");
                m.setAuthentication(auth);
                m.createSync((SyncService) producer);
                try {
                    if (producer != null)
                        getProducerPool().releaseProducer(producer);
                } catch (Exception e) {
                    logger.warn(LOGTAG, e);
                }
                logger.info(LOGTAG + "Published  " + moaClassName + ".Create-Sync" + " message.");
            } catch (EnterpriseObjectSyncException | JMSException eose) {
                String errMsg = "An error occurred publishing the  " + moaClassName + ".Create-Sync"
                        + " message after generating a moa. The " + "exception is: " + eose.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new CommandException(errMsg, eose);
            }

            logger.info(LOGTAG + "Prepare response... ");
            // Prepare the response.
            if (localGenerateResponseDoc.getRootElement().getChild("DataArea") != null) {
                localGenerateResponseDoc.getRootElement().getChild("DataArea").removeContent();
            } else {
                localGenerateResponseDoc.getRootElement().addContent(new Element("DataArea"));
            }
            Element eMoa = null;
            try {
                eMoa = (Element) m.buildOutputFromObject();
            } catch (EnterpriseLayoutException ele) {
                logger.error(LOGTAG, ele);
                throw new CommandException(ele);
            }
            localGenerateResponseDoc.getRootElement().getChild("DataArea").addContent(eMoa);
            String replyContents = buildReplyDocument(eControlArea, localGenerateResponseDoc);
            logger.info(LOGTAG + "Create-Request command execution complete in " + (System.currentTimeMillis() - startTime) + " ms.");
            return getMessage(msg, replyContents);
        }
        return returnWitheUnsupportedMessageActionError(localResponseDoc, inDoc, msg, eControlArea, msgAction);
    }

    /**
     * @param PProvider
     *            , the moa provider
     *            <P>
     *            Sets the moa provider for this command.
     */
    protected void setProvider(GenerateProvider<M, R> provider) {
        m_provider = provider;
    }

    /**
     * @return PProvider, the moa provider
     *         <P>
     *         Gets the moa provider for this command.
     */
    protected GenerateProvider<M, R> getProvider() {
        return m_provider;
    }

}
