/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
// Log4j
import org.apache.log4j.*;

// JDOM
import org.jdom.Document;
import org.jdom.Element;
//OpenEAI foundation components
import org.openeai.OpenEaiObject;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.LoggerConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.*;
import org.openeai.jms.consumer.commands.provider.ProviderException;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

/**
 * This abstract command provides some error handling functions
 * 
 * @author George Wang
 * @version 1.0 - 2018
 */
public abstract class AbstractGenericRequestCommand extends AbstractRequestCommand implements RequestCommand {
    private static Logger logger = Logger.getLogger(AbstractGenericRequestCommand.class);
    private static String LOGTAG = "[AbstractGenericRequestCommand] ";
    protected String errorType = "application";
    public AbstractGenericRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing... " + AbstractGenericRequestCommand.class.getName());

        logger.info(LOGTAG + "instantiated successfully.");
    }

    @Override
    protected Message returnWitheUnsupportedMessageActionError(Document localResponseDoc, Document inDoc, TextMessage msg,
            Element eControlArea, String msgAction) throws CommandException {
        String errDesc = "Unsupported message action: " + msgAction + ". "
                + "This command only supports query, generate, update, and delete.";
        logger.fatal(LOGTAG + errDesc);
        logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
        ArrayList errors = new ArrayList();
        errors.add(buildError("application", "OpenEAI-1002", errDesc));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }

    protected Message returnWithError(Document localResponseDoc, Document inDoc, TextMessage msg, Element eControlArea, String msgObject,
            String moaClassName) throws CommandException {
        String errCode = "OpenEAI-1001";
        String errDesc = "Unsupported message object: " + msgObject + ". This command expects " + moaClassName;
        logger.warn(LOGTAG + errDesc);
        logger.warn(LOGTAG + "Message sent in is: \n" + getMessageBody(inDoc));
        ArrayList errors = new ArrayList();
        errors.add(buildError(errorType, errCode, errDesc));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }

    protected Message returnWithError(Document localResponseDoc, Document inDoc, TextMessage msg, Element eControlArea, Throwable pe)
            throws CommandException {
        logger.error(LOGTAG, pe);
        logger.warn(LOGTAG + ("An error occurred generating the moa. The " + "exception is: " + pe.getMessage()));
        logger.warn("Message sent in is: \n" + getMessageBody(inDoc));
        ArrayList errors = new ArrayList();
        errors.add(buildError(errorType, "GenericCrudRequestCommand-1003",
                "An error occurred generating the moa. The " + "exception is: " + pe.getMessage()));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }

    protected Message returnWithError(Document localResponseDoc, Document inDoc, TextMessage msg, Element eControlArea)
            throws CommandException {
        String errDesc = "Invalid element found in the Delete-Request " + "message. This command expects a moa";
        logger.warn(LOGTAG + errDesc);
        logger.warn("Message sent in is: \n" + getMessageBody(inDoc));
        ArrayList errors = new ArrayList();
        errors.add(buildError(errorType, "OpenEAI-1015", errDesc));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }
}