/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands;

// Core Java
import java.util.ArrayList;
import java.util.List;

// Java Message Service
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

// Log4j
import org.apache.log4j.Logger;
// JDOM
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Authentication;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.transport.SyncService;

import org.openeai.jms.consumer.commands.provider.CrudProvider;
import org.openeai.jms.consumer.commands.provider.Provider;
import org.openeai.jms.consumer.commands.provider.ProviderException;

/**
 * M: messageObject Q: querySpecificationObject
 * 
 */

public abstract class GenericCrudRequestCommandBase<M extends ActionableEnterpriseObject, Q extends XmlEnterpriseObject, P extends CrudProvider<M, Q>>
        extends AbstractGenericRequestCommand implements RequestCommand {
    private static String LOGTAG = "[GenericCrudRequestCommand] ";
    private static Logger logger = Logger.getLogger(GenericCrudRequestCommandBase.class);
    protected P m_provider = null;
    protected String errorType = "application";
    protected M m = newM();
    protected String moaClassName;
    protected Q querySpec = newQ();
    protected String querySpecClassName;
    protected TestId testId = new TestId();
    public GenericCrudRequestCommandBase(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing... ");
        initProvider();
        initM();
        initQ();
        initTestId();
        logger.info(LOGTAG + "instantiated successfully.");
    }
    private void initTestId() throws InstantiationException {
        try {
            testId = (TestId) getAppConfig().getObjectByType(testId.getClass().getName());
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }
    private void initQ() throws InstantiationException {
        try {
            querySpec = (Q) getAppConfig().getObjectByType(querySpec.getClass().getName());
            querySpecClassName = querySpec.getClass().getSimpleName();
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }
    private void initM() throws InstantiationException {
        try {
            m = (M) getAppConfig().getObjectByType(m.getClass().getName());
            moaClassName = m.getClass().getSimpleName();
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }
    private void initProvider() throws InstantiationException {
        String className = getProperties().getProperty("providerClassName");
        if (className == null || className.equals("")) {
            String errMsg = "No providerClassName property specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        logger.info(LOGTAG + "providerClassName is: " + className);
        try {
            logger.info(LOGTAG + "Getting class for name: " + className);
            Class providerClass = Class.forName(className);
            logger.info(LOGTAG + "providerClass =" + providerClass);
            m_provider = (P) providerClass.newInstance();
            logger.info(LOGTAG + "Initializing PProvider: " + m_provider.getClass().getName());
            m_provider.init(getAppConfig());
            logger.info(LOGTAG + "PProvider initialized.");
            setProvider(m_provider);
        } catch (ClassNotFoundException | ProviderException | IllegalAccessException e) {
            logger.fatal(LOGTAG, e);
            throw new InstantiationException(e.getMessage());
        }
    }
    M newM() {
        return Provider.createTemplateInstance(this, 0);
    }
    Q newQ() {
        return Provider.createTemplateInstance(this, 1);
    }
    /**
     * @param int,
     *            the number of the message processed by the consumer
     * @param Message
     *            , the message for the command to process
     * @throws CommandException
     *             , with details of the error processing the message
     *             <P>
     *             This method makes a local copy of the response and provide
     *             documents to use in the reply to the request. Then it
     *             converts the JMS message to an XML document, retrieves the
     *             text portion of the message, clears the message body in
     *             preparation for the reply, gets the ControlArea from the XML
     *             document, and verifies that message object of the message is
     *             a VirtualPrivateCloud and the action is a query, generate,
     *             update, or delete. Then this method uses the configured
     *             PProvider to perform each operation.
     */
    @Override
    public abstract Message execute(int messageNumber, Message aMessage) throws CommandException;

    protected Message executeHelper(int messageNumber, Message aMessage, boolean processGenerate) throws CommandException {
        long startTime = System.currentTimeMillis();
        MessageProducer producer = null;
        try {
            M m = null;
            Q querySpec = null;
            TestId testId = null;
            try {
                m = (M) this.m.clone();
                querySpec = (Q) this.querySpec.clone();
                testId = (TestId) this.testId.clone();
            } catch (CloneNotSupportedException e) {
                throw new CommandException(e);
            }

            Document localResponseDoc = (Document) getResponseDocument().clone();
            Document localProvideDoc = (Document) getProvideDocument().clone();
            Document inDoc = null;
            try {
                inDoc = initializeInput(messageNumber, aMessage);
            } catch (Exception e) {
                throw new CommandException(e);
            }
            if (getVerbose())
                logger.info("Message sent in is: \n" + getMessageBody(inDoc));
            TextMessage msg = (TextMessage) aMessage;
            try {
                msg.clearBody();
            } catch (JMSException jmse) {
                throw new CommandException(jmse);
            }
            Element eControlArea = getControlArea(inDoc.getRootElement());
            String msgAction = eControlArea.getAttribute("messageAction").getValue();
            String msgObject = eControlArea.getAttribute("messageObject").getValue();
            if (!msgObject.equalsIgnoreCase(moaClassName)) {
                return returnWithError(localResponseDoc, inDoc, msg, eControlArea, msgObject, moaClassName);
            }

            /// validation
            String senderAppId = eControlArea.getChild("Sender").getChild("MessageId").getChild("SenderAppId").getValue();
            Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
            String authUserId = eAuthUserId.getValue();
            if (authUserId.equalsIgnoreCase("TestSuiteApplication")) {
                authUserId = "testsuiteapp@emory.edu/127.0.0.1";
            }
            if (!validateAuthUserId(authUserId)) {
                logger.fatal(LOGTAG + ("Invalid AuthUserId. The value '" + authUserId
                        + "' is not valid. The expected format is user@domain/ip number."));
                logger.fatal(LOGTAG + "Message sent in is: \n" + getMessageBody(inDoc));
                ArrayList errors = new ArrayList();
                errors.add(buildError(errorType, "GenericCrudRequestCommand-1001",
                        "Invalid AuthUserId. The value '" + authUserId + "' is not valid. The expected format is user@domain/ip number."));
                String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
                return getMessage(msg, replyContents);
            }
            Element eTestId = eControlArea.getChild("Sender").getChild("TestId");
            if (eTestId != null)
                try {
                    testId.buildObjectFromInput(eTestId);
                } catch (EnterpriseLayoutException e) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, e);
                }
            //// CREATE
            if (msgAction.equalsIgnoreCase("Create")) {
                logger.info(LOGTAG + "Handling a.Create-Request message.");
                Element eCreateObject = inDoc.getRootElement().getChild("DataArea").getChild("NewData").getChild(moaClassName);
                if (eCreateObject == null) {
                    logger.error(LOGTAG + moaClassName + " cannot be found");
                    return returnWithError(inDoc, inDoc, msg, eCreateObject);
                }
                try {
                    m.buildObjectFromInput(eCreateObject);
                    m.setTestId(testId);
                    m.getMessageId().setSenderAppId(senderAppId);
                } catch (EnterpriseLayoutException ele) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, ele);
                }
                logger.info(LOGTAG + "Creating a moa..." + moaClassName);
                try {
                    long createStartTime = System.currentTimeMillis();
                    getProvider().create(m);
                    logger.info(LOGTAG + "Create moa in " + (System.currentTimeMillis() - createStartTime) + " ms.");

                } catch (Throwable pe) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, pe);
                }
                logger.info(LOGTAG + "Created moa " + moaClassName);

                logger.info(LOGTAG + "Publishing sync... ");
                try {
                    producer = getProducerPool().getProducer();
                    if (getVerbose())
                        logger.info(LOGTAG + "Publishing " + moaClassName + ".Create-Sync message...");
                    Authentication auth = new Authentication();
                    auth.setAuthUserId(authUserId);
                    auth.setAuthUserSignature("none");
                    m.setAuthentication(auth);
                    m.createSync((SyncService) producer);
                    logger.info(LOGTAG + "Published  " + moaClassName + ".Create-Sync" + " message.");
                } catch (EnterpriseObjectSyncException | JMSException eose) {
                    String errMsg = "An error occurred publishing the  " + moaClassName + ".Create-Sync"
                            + " message after generating a moa. The " + "exception is: " + eose.getMessage();
                    logger.error(LOGTAG + errMsg);
                    throw new CommandException(errMsg, eose);
                }

                if (localResponseDoc.getRootElement().getChild("DataArea") != null) {
                    localResponseDoc.getRootElement().getChild("DataArea").removeContent();
                } else {
                    localResponseDoc.getRootElement().addContent(new Element("DataArea"));
                }
                logger.info(LOGTAG + "Create-Request command execution complete in " + (System.currentTimeMillis() - startTime) + " ms.");
                return getMessage(msg, buildReplyDocument(eControlArea, localResponseDoc));
            }

            //// QUERY
            if (msgAction.equalsIgnoreCase("Query")) {
                logger.info(LOGTAG + "Handling an Query-Request message.");
                Element eQuerySpec = (Element) inDoc.getRootElement().getChild("DataArea").getChildren().get(0);
                try {
                    querySpec.buildObjectFromInput(eQuerySpec);
                } catch (EnterpriseLayoutException ele) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, ele);
                }
                logger.info(LOGTAG + "Querying for the moa..." + moaClassName);
                List results = null;
                try {
                    long queryStartTime = System.currentTimeMillis();
                    results = getProvider().query(querySpec);
                    logger.info(
                            LOGTAG + "Queried for moa in " + moaClassName + " " + (System.currentTimeMillis() - queryStartTime) + "ms.");
                } catch (ProviderException pe) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, pe);
                }
                logger.info(LOGTAG + "Found " + (results == null ? 0 : results.size()) + " matching result(s).");

                // Prepare the response.
                localProvideDoc.getRootElement().getChild("DataArea").removeContent();
                if (results != null && results.size() > 0) {
                    ArrayList moast = new ArrayList();
                    for (int i = 0; i < results.size(); i++) {
                        Element eMoa = null;
                        try {
                            M moa = (M) results.get(i);
                            eMoa = (Element) moa.buildOutputFromObject();
                            moast.add(eMoa);
                            moa.setTestId(testId);
                            moa.getMessageId().setSenderAppId(senderAppId);
                        } catch (EnterpriseLayoutException ele) {
                            String errMsg = "An error occurred serializing " + "moa object to an XML element. " + "The exception is: "
                                    + ele.getMessage();
                            logger.error(LOGTAG + errMsg);
                            throw new CommandException(errMsg, ele);
                        }
                    }
                    localProvideDoc.getRootElement().getChild("DataArea").addContent(moast);
                }
                logger.info(LOGTAG + "Query-Request command execution complete in " + (System.currentTimeMillis() - startTime) + " ms.");
                return getMessage(msg, buildReplyDocument(eControlArea, localProvideDoc));
            }

            //// UPDATE
            if (msgAction.equalsIgnoreCase("Update")) {
                logger.info(LOGTAG + "Handling a.moa.Update-Request message.");
                Element emoaBase = inDoc.getRootElement().getChild("DataArea").getChild("BaselineData").getChild(moaClassName);

                Element emoa = inDoc.getRootElement().getChild("DataArea").getChild("NewData").getChild(moaClassName);
                if (emoa == null) {
                    logger.error(LOGTAG + moaClassName + " cannot be found");
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea);
                }
                try {
                    M base = (M) m.clone();
                    m.buildObjectFromInput(emoa);
                    base.buildObjectFromInput(emoaBase);
                    m.setBaseline(base);
                    m.setTestId(testId);
                    m.getMessageId().setSenderAppId(senderAppId);
                } catch (EnterpriseLayoutException | CloneNotSupportedException ele) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, ele);
                }
                logger.info(LOGTAG + "Updating a moa...");

                try {
                    long deleteStartTime = System.currentTimeMillis();
                    getProvider().update(m);
                    logger.info(LOGTAG + "Updated moa in " + (System.currentTimeMillis() - deleteStartTime) + " ms.");
                } catch (ProviderException pe) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, pe);
                }

                // Publish a Update-Sync Message
                try {
                    producer = getProducerPool().getProducer();
                    if (getVerbose())
                        logger.info(LOGTAG + "Publishing moa.Update-Sync message...");
                    m.updateSync((SyncService) producer);
                    logger.info(LOGTAG + "Published moa.Update-Sync" + " message.");
                } catch (EnterpriseObjectSyncException | JMSException eose) {
                    logger.error(LOGTAG, eose);
                    return returnWithError(inDoc, inDoc, msg, emoa, eose);
                }
                if (localResponseDoc.getRootElement().getChild("DataArea") != null) {
                    localResponseDoc.getRootElement().getChild("DataArea").removeContent();
                }
                String replyContents = buildReplyDocument(eControlArea, localResponseDoc);
                logger.info(LOGTAG + "Update-Request command execution complete in " + (System.currentTimeMillis() - startTime) + " ms.");
                return getMessage(msg, replyContents);
            }

            //// DELETE
            if (msgAction.equalsIgnoreCase("Delete")) {
                logger.info(LOGTAG + "Handling a Delete-Request message.");
                Element emoa = inDoc.getRootElement().getChild("DataArea").getChild("DeleteData").getChild(moaClassName);
                if (emoa == null) {
                    logger.error(LOGTAG + moaClassName + " cannot be found");
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea);
                }
                try {
                    m.buildObjectFromInput(emoa);
                    m.setTestId(testId);
                    m.getMessageId().setSenderAppId(senderAppId);
                } catch (EnterpriseLayoutException ele) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, ele);
                }
                logger.info(LOGTAG + "Deleting a moa...");

                try {
                    long deleteStartTime = System.currentTimeMillis();
                    getProvider().delete(m);
                    logger.info(LOGTAG + "Deleted moa in " + (System.currentTimeMillis() - deleteStartTime) + " ms.");
                } catch (ProviderException pe) {
                    return returnWithError(localResponseDoc, inDoc, msg, eControlArea, pe);
                }

                // Publish a Delete-Sync Message
                try {
                    producer = getProducerPool().getProducer();
                    if (getVerbose())
                        logger.info(LOGTAG + "Publishing moa.Delete-Sync message...");
                    m.deleteSync("delete", (SyncService) producer);
                    logger.info(LOGTAG + "Published moa.Delete-Sync" + " message.");
                } catch (EnterpriseObjectSyncException | JMSException eose) {
                    logger.error(LOGTAG, eose);
                    return returnWithError(inDoc, inDoc, msg, emoa, eose);
                }
                if (localResponseDoc.getRootElement().getChild("DataArea") != null) {
                    localResponseDoc.getRootElement().getChild("DataArea").removeContent();
                }
                String replyContents = buildReplyDocument(eControlArea, localResponseDoc);
                logger.info(LOGTAG + "Delete-Request command execution complete in " + (System.currentTimeMillis() - startTime) + " ms.");
                return getMessage(msg, replyContents);
            }
            if (processGenerate) {
                return processGenerate(localResponseDoc, inDoc, msg, eControlArea, msgAction, authUserId, startTime);
            }

            return returnWitheUnsupportedMessageActionError(localResponseDoc, inDoc, msg, eControlArea, msgAction);
        } finally {
            if (producer != null) {
                try {
                    getProducerPool().releaseProducer(producer);
                } catch (Exception e) {
                    logger.warn(e);
                }

            }
        }
    }

    protected Message processGenerate(Document localResponseDoc, Document inDoc, TextMessage msg, Element eControlArea, String msgAction,
            String authUserId, long startTime) throws CommandException {
        return returnWitheUnsupportedMessageActionError(localResponseDoc, inDoc, msg, eControlArea, msgAction);
    }
    /**
     * @param PProvider
     *            , the moa provider
     *            <P>
     *            Sets the moa provider for this command.
     */
    protected void setProvider(P provider) {
        m_provider = provider;
    }

    /**
     * @return PProvider, the moa provider
     *         <P>
     *         Gets the moa provider for this command.
     */
    protected P getProvider() {
        return m_provider;
    }

}
