/*******************************************************************************
 $Source$
 $Revision: 896 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.jms.consumer.commands;

import org.openeai.OpenEaiException;

/**
 * This Exception may be thrown by Commands when/if errors occur during the processing of the 'execute' method.
 * Generally, this exception should only ever be thrown if the Command has trouble turning the message body of 
 * the JMS Message passed to the execute method into an XML Document (org.jdom.Document).  Any other error encountered by the command should be handled
 * accordingly by either publishing a Sync-Error-Sync message or returning a Reply document with errors indicating
 * the nature of the problem.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class CommandException extends OpenEaiException {

  /**
   * Constructor
   */
  public CommandException() {
    super();
  }
  public CommandException(String msg) {
    super(msg);
  }
  public CommandException(String msg, Throwable rootCause) {
    super(msg, rootCause);
  }
  public CommandException(Throwable rootCause) {
    super(rootCause);
  }
}

