/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
// Log4j
import org.apache.log4j.*;

// JDOM
import org.jdom.Document;
import org.jdom.Element;
//OpenEAI foundation components
import org.openeai.OpenEaiObject;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.LoggerConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.*;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

public abstract class AbstractRequestCommand extends RequestCommandImpl implements RequestCommand {
    private static Logger logger = Logger.getLogger(AbstractRequestCommand.class);
    protected Document m_responseDoc = null; // the primed XML response document
    protected Document m_provideDoc = null; // the primed XML response document
    private static String LOGTAG = "[AbstractRequestCommand] ";
    protected boolean m_verbose = false;
    private ProducerPool m_producerPool = null;
    /**
     * @param CommandConfig
     * @throws InstantiationException
     *             <P>
     *             This constructor initializes the command using a
     *             CommandConfig object. It invokes the constructor of the
     *             ancestor, RequestCommandImpl, and then retrieves one
     *             PropertyConfig object from AppConfig by name and gets and
     *             sets the command properties using that PropertyConfig object.
     *             This means that this command must have one PropertyConfig
     *             object in its configuration named 'GeneralProperties'. This
     *             constructor also initializes the response document and
     *             provide document used in replies.
     */
    public AbstractRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing... " + AbstractRequestCommand.class.getName());

        // Initialize a command-specific logger if it exists.
        try {
            LoggerConfig lConfig = new LoggerConfig();
            lConfig = (LoggerConfig) getAppConfig().getObjectByType(lConfig.getClass().getName());
            PropertyConfigurator.configure(lConfig.getProperties());
        } catch (Exception e) {
        }

        // Get and set the general properties for this command.
        PropertyConfig pConfig = new PropertyConfig();
        try {
            pConfig = (PropertyConfig) getAppConfig().getObject("GeneralProperties");
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving a PropertyConfig object from " + "AppConfig: The exception is: " + eoce.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        setProperties(pConfig.getProperties());

        // Get a SyncService to use to publish sync messages.
        try {
            ProducerPool pool = (ProducerPool) getAppConfig().getObject("SyncPublisher");
            setProducerPool(pool);
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving a ProducerPool object " + "from AppConfig. The exception is: " + eoce.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Get the verbose property.
        String verbose = getProperties().getProperty("verbose", "false");
        setVerbose(Boolean.getBoolean(verbose));
        logger.info(LOGTAG + "property verbose: " + getVerbose());

        // Initialize response documents.
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            Document responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"),
                    getOutboundXmlValidation());
            if (responseDoc == null) {
                String errMsg = "Missing 'responseDocumentUri' " + "property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
            setResponseDocument(responseDoc);

            Document provideDoc = xmlReader.initializeDocument(getProperties().getProperty("provideDocumentUri"),
                    getOutboundXmlValidation());
            if (provideDoc == null) {
                String errMsg = "Missing 'provideDocumentUri' " + "property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
            setProvideDocument(provideDoc);

        } catch (XmlDocumentReaderException xdre) {
            String errMsg = "An error occurred initializing the primed reponse " + "document. The exception is: " + xdre.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(xdre.getMessage());
        }

        logger.info(LOGTAG + "instantiated successfully.");
    }

    protected Message returnWitheUnsupportedMessageActionError(Document localResponseDoc, Document inDoc, TextMessage msg,
            Element eControlArea, String msgAction) throws CommandException {
        String errDesc = "Unsupported message action: " + msgAction + ". "
                + "This command only supports query, generate, update, and delete.";
        logger.fatal(LOGTAG + errDesc);
        logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
        ArrayList errors = new ArrayList();
        errors.add(buildError("application", "OpenEAI-1002", errDesc));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }
    /**
     * @param boolean,
     *            the verbose parameter
     *            <P>
     *            Set a parameter to toggle verbose logging.
     */
    protected void setVerbose(boolean b) {
        m_verbose = b;
    }

    /**
     * @return boolean, the verbose parameter
     *         <P>
     *         Gets the value of the verbose logging parameter.
     */
    protected boolean getVerbose() {
        return m_verbose;
    }

    /**
     * @param Document,
     *            the response document
     *            <P>
     *            Set a primed XML response document the command will use to
     *            reply to the requests it handles.
     */
    protected void setResponseDocument(Document d) {
        m_responseDoc = d;
    }

    /**
     * @return Document, the response document
     *         <P>
     *         Gets the primed XML response document the command will use to
     *         reply to the requests it handles.
     */
    protected Document getResponseDocument() {
        return m_responseDoc;
    }

    /**
     * @param Document,
     *            the response document
     *            <P>
     *            Set a primed XML response document the command will use to
     *            reply to the requests it handles.
     */
    protected void setProvideDocument(Document d) {
        m_provideDoc = d;
    }

    /**
     * @return Document, the response document
     *         <P>
     *         Gets the primed XML response document the command will use to
     *         reply to the requests it handles.
     */
    protected Document getProvideDocument() {
        return m_provideDoc;
    }

    /**
     * @param ProducerPool
     *            , the producer pool for this command.
     *            <P>
     *            Sets the producer pool for this command.
     */
    protected void setProducerPool(ProducerPool producerPool) {
        m_producerPool = producerPool;
    }

    /**
     * @return ProducerPool, the producer pool for this command.
     *         <P>
     *         Gets the producer pool for this command.
     */
    protected ProducerPool getProducerPool() {
        return m_producerPool;
    }

    /**
     * @return String, a String containing the EPPN.
     *         <P>
     *         Parses the EPPN from the AuthUserId and returns it.
     */
    protected static String getEppnFromAuthUserId(String authUserId) {
        StringTokenizer st = new StringTokenizer(authUserId, "/");
        String eppn = st.nextToken();
        return eppn;
    }

    /**
     * @return String, a String containing the IP number.
     *         <P>
     *         Parses the EPPN from the AuthUserId and returns it.
     */
    protected static String getIpNumberFromAuthUserId(String authUserId) {
        StringTokenizer st = new StringTokenizer(authUserId, "/");
        st.nextToken();
        String ipNumber = st.nextToken();
        return ipNumber;
    }

    /**
     * @return boolean, a flag indicating whether or not the authUserId id is
     *         valid.
     *         <P>
     *         Validates the format of the AuthUserId to be eppn/ipNumber. More
     *         specifically, this is user@domain/ipnumber.
     */
    protected static boolean validateAuthUserId(String authUserId) {
        StringTokenizer st = new StringTokenizer(authUserId, "/");

        // If there are less than two tokens return false.
        if (st.countTokens() < 2) {
            logger.error(LOGTAG + "AuthUserId does not consist of two tokens.");
            return false;
        }
        String eppn = st.nextToken();
        GenericValidator gv = new GenericValidator();
        if (gv.isEmail(eppn) == false) {
            logger.error(LOGTAG + "EPPN is not a valid e-mail: " + eppn);
            return false;
        }

        String ip = st.nextToken();
        InetAddressValidator iav = new InetAddressValidator();
        if (iav.isValid(ip) == false) {
            logger.info(LOGTAG + "IP number is not valid: " + ip);
            return false;
        }
        return true;
    }

}