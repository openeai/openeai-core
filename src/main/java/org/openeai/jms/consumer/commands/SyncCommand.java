/*******************************************************************************
 $Source$
 $Revision: 957 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.jms.consumer.commands;

import javax.jms.Message;

/**
 * The interface implmented by all SyncCommands and specifies that they must implement
 * the 'execute' method.  These commands will be executed by
 * the PubSubConsumer when when it consumes a Sync Message..
 * <P>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see RequestCommand
 */
public interface SyncCommand {
	/**
   * Method used to implement business logic specific to a message or set of messages consumed
   * by a OpenEAI PubSubConsumer or PointToPointConsumer.  When a consumer consumes a message it determines which 
   * SyncCommand implementation to execute and calls this method passing the JMS Message.  The command
   * then retrieves the body of the message and converts that into an XML Document (org.jdom.Document).  Then
   * the command executes business logic appropriate to it based on the contents of the message.
   * <P>
   * NOTE:  The consumer only instantiates one instance of these commands (for each 
   * one supported by the consumer).  If the consumer is configured to be multi-threaded, 
   * it will be calling the execute method on a single instance of the command associated 
   * to the message it consumed.  Therefore, it is very important that the execute method 
   * be thread safe.
   * <P>
   * @param messageNumber, convenience parm that is maintained by the PointToPointConsumer and PubSubConsumer
   * that will execute the command.  This information can then be used in the Command to associate a 
   * specific line of execution back to the message number in the consumer.
   * @param aMessage JMS Message delivered to the PubSubConsumer.
   * <P>
   * @return void
	 * <P>
   * @throws CommandException.  Generally, this exception should only ever be thrown if the 
   * Command has trouble turning the message body of the JMS Message passed to the execute method into an XML Document (org.jdom.Document).  
   * Any other error encountered by the command should be handled accordingly by publishing a Sync-Error-Sync 
   * message with errors indicating the nature of the problem.
   * @see RequestCommand#execute(int, Message) RequestMessage.execute
	 */
  public void execute(int messageNumber, Message aMessage) throws CommandException;
  public void shutdown() throws CommandException;
}
