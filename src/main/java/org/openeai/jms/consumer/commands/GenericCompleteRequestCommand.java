/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

// Log4j
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.provider.CompleteProvider;
import org.openeai.jms.consumer.commands.provider.Provider;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Authentication;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.transport.SyncService;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

/**
 * 
 * If the Service provide all the operations: Create, Update, Delete, Query, and
 * Generate , use this Command. Required parameters:
 * 
 * M, messageObject; Q, queryObject; P, provider; and R, requisition object
 */

public class GenericCompleteRequestCommand<M extends ActionableEnterpriseObject, Q extends XmlEnterpriseObject, R extends XmlEnterpriseObject, P extends CompleteProvider<M, Q, R>>
        extends GenericCrudRequestCommandBase implements RequestCommand {
    private static String LOGTAG = "[GenericCompleteRequestCommand] ";
    private static Logger logger = Logger.getLogger(GenericCompleteRequestCommand.class);
    protected R requisition = newR();
    protected String requisitionClassName;
    protected Document m_generateResponseDoc = null;
    public GenericCompleteRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        initR();
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            m_generateResponseDoc = xmlReader.initializeDocument(getProperties().getProperty("generateResponseDocumentUri"),
                    getOutboundXmlValidation());
        } catch (XmlDocumentReaderException e) {
            String errMsg = "An error occurred initializing the primed reponse " + "document. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg, e);
            throw new InstantiationException(e.getMessage());
        }
    }
    R newR() {
        return Provider.createTemplateInstance(this, 2);
    }
    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        return executeHelper(messageNumber, aMessage, true);
    }
    protected void initR() throws InstantiationException {
        try {
            requisition = (R) getAppConfig().getObjectByType(requisition.getClass().getName());
            requisitionClassName = requisition.getClass().getSimpleName();
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }

    @Override
    protected Message processGenerate(Document localResponseDoc, Document inDoc, TextMessage msg, Element eControlArea, String msgAction,
            String authUserId, long startTime) throws CommandException {
        Document localGenerateResponseDoc = (Document) m_generateResponseDoc.clone();
        R requisition = null;
        M m = null;
        try {
            m = (M) this.m.clone();
            requisition = (R) this.requisition.clone();
            testId = (TestId) this.testId.clone();
        } catch (CloneNotSupportedException e) {
            throw new CommandException(e);
        }
        //// GENERATE
        if (msgAction.equalsIgnoreCase("Generate")) {
            logger.info(LOGTAG + "Handling a.Generate-Request message.");
            Element eGenerateObject = inDoc.getRootElement().getChild("DataArea").getChild(requisitionClassName);
            if (eGenerateObject == null) {
                logger.error(LOGTAG + requisitionClassName + " cannot be found");
                return returnWithError(inDoc, inDoc, msg, eGenerateObject);
            }
            try {
                requisition.buildObjectFromInput(eGenerateObject);
                requisition.setTestId(testId);
            } catch (EnterpriseLayoutException ele) {
                return returnWithError(localResponseDoc, inDoc, msg, eControlArea, ele);
            }
            logger.info(LOGTAG + "Generating a moa " + moaClassName);
            try {
                long createStartTime = System.currentTimeMillis();
                CompleteProvider completeProvider = (CompleteProvider) getProvider();
                m = (M) completeProvider.generate(requisition);
                logger.info(LOGTAG + "Generated moa in " + (System.currentTimeMillis() - createStartTime) + " ms.");
            } catch (Throwable pe) {
                return returnWithError(localResponseDoc, inDoc, msg, eControlArea, pe);
            }
            logger.info(LOGTAG + "Generated moa " + moaClassName);

            logger.info(LOGTAG + "Publishing sync... ");
            try {
                MessageProducer producer = getProducerPool().getProducer();
                if (getVerbose())
                    logger.info(LOGTAG + "Publishing " + moaClassName + ".Create-Sync message...");
                Authentication auth = new Authentication();
                auth.setAuthUserId(authUserId);
                auth.setAuthUserSignature("none");
                m.setAuthentication(auth);
                m.createSync((SyncService) producer);
                try {
                    if (producer != null)
                        getProducerPool().releaseProducer(producer);
                } catch (Exception e) {
                    logger.warn(LOGTAG, e);
                }
                logger.info(LOGTAG + "Published  " + moaClassName + ".Create-Sync" + " message.");
            } catch (EnterpriseObjectSyncException | JMSException eose) {
                String errMsg = "An error occurred publishing the  " + moaClassName + ".Create-Sync"
                        + " message after generating a moa. The " + "exception is: " + eose.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new CommandException(errMsg, eose);
            }

            logger.info(LOGTAG + "Prepare response... ");
            // Prepare the response.
            if (localGenerateResponseDoc.getRootElement().getChild("DataArea") != null) {
                localGenerateResponseDoc.getRootElement().getChild("DataArea").removeContent();
            } else {
                localGenerateResponseDoc.getRootElement().addContent(new Element("DataArea"));
            }
            Element eMoa = null;
            try {
                eMoa = (Element) m.buildOutputFromObject();
            } catch (EnterpriseLayoutException ele) {
                String errMsg = "An error occurred serializing a  ElasticIpAssignment object to an XML element. The exception is: "
                        + ele.getMessage();
                logger.error(LOGTAG + errMsg, ele);
                throw new CommandException(errMsg, ele);
            }
            localGenerateResponseDoc.getRootElement().getChild("DataArea").addContent(eMoa);
            String replyContents = buildReplyDocument(eControlArea, localGenerateResponseDoc);
            logger.info(LOGTAG + "Create-Request command execution complete in " + (System.currentTimeMillis() - startTime) + " ms.");
            return getMessage(msg, replyContents);
        }
        return returnWitheUnsupportedMessageActionError(localResponseDoc, inDoc, msg, eControlArea, msgAction);
    }

}
