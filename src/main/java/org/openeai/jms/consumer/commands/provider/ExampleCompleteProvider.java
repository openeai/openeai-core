/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package org.openeai.jms.consumer.commands.provider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
// Java utilities
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Log4j
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
// OpenEAI foundation
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;

/**
 *
 */
public class ExampleCompleteProvider<M, Q, R> extends ExampleCrudProvider<M, Q> implements CompleteProvider<M, Q, R> {

    private static Logger logger = Logger.getLogger(ExampleCompleteProvider.class);
    private static String LOGTAG = "[ExampleCompleteProvider] ";
    @Override
    public M generate(R r) throws ProviderException {
        M m = Provider.createTemplateInstance(this, 0);
        try {
            m = (M) appConfig.getObjectByType(m.getClass().getName());
        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " + "AppConfig. The exception is: " + ecoe.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, ecoe);
        }
        try {
            String id = null;
            if (getXxxId(r) == null || getXxxId(r).length() == 0) {
                id = String.valueOf(idToMoa.size());
            } else
                id = getXxxId(r);
            setXxxId(m, id);
            idToMoa.put(getXxxId(m), m);
        } catch (Exception e) {
            logger.error(e);
            throw new ProviderException("", e);
        }
        return m;
    }

}
