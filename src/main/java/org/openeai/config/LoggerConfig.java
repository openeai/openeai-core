/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.util.*;

import org.jdom.Element;

import org.openeai.xml.*;
import org.openeai.*;

/**
 * A LoggerConfig is a wrapper class that takes information stored in an OpenEAI
 * Deployment document (LoggerConfig Element) and stores it in a Java object.
 * Then the properties associated with the LoggerConfig are used to initialize
 * the static logger that all decendants of OpenEaiObject inherits.  If an object
 * isn't a subclass of OpenEaiObject then it can use these properties to initialize
 * a log4j Category object itself.  These properties are specified by Log4J.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the LoggerConfig
 * Element in the Deployment document.  NOTE:  The properties that need to be
 * specified are dictated by Log4J since that's the logging framework used
 * by OpenEAI.  The parameters listed below are for a typical OpenEAI application.
 * For more information regarding Log4J properties, consult that documentation.
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the LoggerConfig object.  This is how the object will be 
 * stored in AppConfig and should be unique.  This allows developers to 
 * conveniently categorize Loggers for an application if they wish to
 * use multiple loggers configured differently.  Generally, the logger inherited
 * by all OpenEaiObject decendants sufices.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Property</TD>
 * <TD>yes (at least one)</TD>
 * <TD>This is a pair of Elements that tells the LoggerConfig object what 
 * Java Properties object to build that will be used to initialize the Log4J Category.  
 * It consists of PropertyName and a PropertyValue
 * Elements that are stored in the Java Properties object.</TD>
 * </TR>
 *</TABLE>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class LoggerConfig
  extends EnterpriseConfigurationObjectImpl
  implements EnterpriseConfigurationObject {

  /**
   * Constructor
   */
  public LoggerConfig() {
    setType("LoggerConfig");
  }

  public LoggerConfig(String configDocUrl, String loggerName) throws EnterpriseConfigurationObjectException {
    setType("LoggerConfig");
    XmlDocumentReader xmlReader = new XmlDocumentReader();
    try {
      setConfigDoc(xmlReader.initializeDocument(configDocUrl, getValidation()));
    }
    catch (XmlDocumentReaderException e) {
      logger.fatal(e.getMessage(), e);
    }
    setName(loggerName);
    init();
  }

  public LoggerConfig(Element configElement) throws EnterpriseConfigurationObjectException {
    setType("LoggerConfig");
    init(configElement);
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the static 'logger' being initialized.
   * When AppConfig detects that it's configuring the 'logger' it builds this config object and
   * passes the properties from it to the initializeLog4j method in OpenEaiObject.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the EnterpriseConnectionPool being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    logger.debug("Element returned: ");
    logger.debug("  - " + configElement.getName());
    logger.debug("  - " + configElement.getAttribute("name").getValue());
    java.util.List props = configElement.getChildren();
    logger.debug(configElement.getAttribute("name").getValue() +
      " has " + props.size() + " children.");
    for (int i=0; i<props.size(); i++) {
      Element aProp = (Element)props.get(i);
      if (aProp.getName().equals("Property")) {
        addProperty(aProp.getChild("PropertyName").getText(), aProp.getChild("PropertyValue").getText());
      }
    }
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the logger being configured
   * then it calls the init(Element) method which actually initializes the LoggerConfig
   * with the information found in the configuration element.
   *
	 */
  private void init() throws EnterpriseConfigurationObjectException {
    Element rootElement = getConfigDoc().getRootElement();
    logger.debug("RootElement is: " + rootElement.getName());
    // Find the element specified by loggerName in the document
    Element configElement = getConfigElementByAttributeValue(getName(), "name");
    init(configElement);
  }
}

