/*******************************************************************************
 $Source$
 $Revision: 3648 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.io.Serializable;

/**
 * This object represents a single field in the EnterpriseObjects documents.
 * It wraps the information found in that document for a given field in to a
 * Java object.  These objects are then stored in the EnterpriseFields java object
 * and are used to apply various business rules to values being set on specific
 * instance variables within an organization's Message Object API.  This is accomplished through
 * the EnterpriseFormatter object which contains, Scrubbers, Translators etc. 
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see EnterpriseFields
  * @see EnterpriseFormatter
 */
public class Field extends Object implements Serializable {

  private EnterpriseFormatter m_formatter = null;
  private String m_fieldName = "";
  private String m_parentObjectName = "";
  private boolean m_isKey = false;

  /**
   * Constructor
   */
  public Field() {
  }

  /**
   * Returns a flag indicating if this field is part of the parent object's key information.
   *<P>
   * @return boolean true=the field is a part of the key, false=the key is not a part of the key
   */
  public boolean isKey() {
    return m_isKey;
  }
  /**
   * Sets the flag that indicates if this field is a part of the parent object's key information.
   * This is called as the EnterpriseFields object constructs these Field objects as they're 
   * found in the EnterpriseObjects XML Documents.
   *<P>
   * @param isKey boolean true=the field is a part of the key, false=the key is not a part of the key
   */
  public void setIsKey(boolean isKey) {
    m_isKey = isKey;
  }

  /**
   * Sets the name of the field.  This information is retrieved from the EnterpriseObjects XML Document
   * as the EnterpriseField object builds itself with information found in that document.
   *<P>
   * @param name String the name of the field as specified in the EnterpriseObjects XML Document.
   */
  public void setFieldName(String name) {
    m_fieldName = name;
  }
  /**
   * Returns the name of the field.
   *<P>
   * @return String the field's name.
   */
  public String getFieldName() {
    return m_fieldName;
  }

  /**
   * Sets the name of the Field's parent object.  For example, the Gender field is a child of the BasicPerson
   * object.
   *<P>
   * @param name String the name of the parent object as specified in the EnterpriseObjects XML documents.
   */
  public void setParentObjectName(String name) {
    m_parentObjectName = name;
  }
  /**
   * Returns the name of the Field's parent object.
   *<P>
   * @return String the parent object's name.
   */
  public String getParentObjectName() {
    return m_parentObjectName;
  }

  /**
   * Returns the EnterpriseFormatter associated to this Field.
   *<P>
   * @return EnterpriseFormatter
   * @see EnterpriseFormatter
   */
  public EnterpriseFormatter getFormatter() {
    return m_formatter;
  }
  /**
   * Sets the EnterpriseFormatter associated to this Field.  EnterpriseFields constructs and 
   * configures these EnterpriseFormatter for each Field listed in the EnterpriseObjects XML documents.
   *<P>
   * @param formatter Enterpriseformatter
   * @see EnterpriseFields
   * @see EnterpriseFormatter
   */
  public void setFormatter(EnterpriseFormatter formatter) {
    m_formatter = formatter;
  }

  /**
   * Convenience method that uses the EnterpriseFormatter associated to this field 
   * to determine if this Field is a required field.
   *<P>
   * @return boolean true=field is required, false=field is not required.
   */
  public boolean isRequired() {
    return getFormatter().getRequired();
  }
}

