/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.util.*;

import org.jdom.Element;
import org.jdom.Attribute;

/**
 * A ThreadPoolConfig is a wrapper class that takes information stored in an 
 * OpenEAI Deployment document (ThreadPoolConfig Element) and stores it in a Java object.
 * Then the configuration object is passed to the constructor of the OpenEAI ThreadPool objects
 * and they are able to configure themselves with the information found in the
 * config object.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the ThreadPoolConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to ThreadPoolConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) ThreadPool objects being configured should use
 * the same configuration information.  Therefore, many of the ThreadPool configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the ThreadPool.  Should be unique.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>maxThreads</TD>
 * <TD>yes</TD>
 * <TD>Maximum number of threads that may be in progress at the same time.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>minThreads</TD>
 * <TD>yes</TD>
 * <TD></TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>maxIdleTime</TD>
 * <TD>yes</TD>
 * <TD></TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>checkBeforeProcessing (true | false)</TD>
 * <TD>no (default is false)</TD>
 * <TD>Indicates whether or not this ThreadPool should verify it has "available" 
 * threads before accepting an incomming job to process.  If this flag is set to true, the Thread 
 * Pool will throw a ThreadPoolException if an application attempts to add
 * a new job when there are no available threads.  If this is set to false, 
 * the ThreadPool will add the job to the pool but won't process it until
 * an available thread exists.  Therefore, the "pending" job would remain in memory
 * until the ThreadPool has an available thread to process it.  This flag is 
 * most commonly used by Consumer ThreadPools.  Especially, PubSubConsumers so 
 * they will not keep adding jobs to memory if for some
 * reason all threads are in progress.  This is a mechanism to minimize the risk
 * of losing things currently in memory and to force balancing between multiple 
 * instances.  It is also a mechanism that can 
 * be used in PointToPointConsumers to force balancing between multiple instances
 * of a gateway.</TD>
 * </TR>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.threadpool.ThreadPoolImpl
 */
public class ThreadPoolConfig
extends EnterpriseConfigurationObjectImpl
implements EnterpriseConfigurationObject {

//  private String m_name = "";

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
	public ThreadPoolConfig() {
		setType("ThreadPoolConfig");
	}
	public ThreadPoolConfig(Element configElement) throws EnterpriseConfigurationObjectException {
		setType("ThreadPoolConfig");
		init(configElement);
	}


/*
  public void setName(String name) {
    m_name = name;
  }
  public String getName() {
    return m_name;
  }
*/

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the ThreadPool being initialized.
   * Then it sets various instance variables and properties on itself which will
   * be used by the ThreadPool when AppConfig instantiates it passing this configuration object.
   * The ThreadPool will then use the configuration java object to initialize itself.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the ThreadPool being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
	 */
	public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    String tPoolName = configElement.getAttribute("name").getValue();
    setName(tPoolName);

    // For now, everything's specified as an attribute so this should
    // take care of everything.
    java.util.List attrs = configElement.getAttributes();
		for (int i=0; i<attrs.size(); i++) {
			Attribute attr = (Attribute)attrs.get(i);
			String key = attr.getName();
			String value = attr.getValue();
			logger.debug("Adding " + key + " - " + value);
			addProperty(key, value);
		}

    // Should be none, for now as everything is specified as attributes.
		java.util.List props = configElement.getChildren();
		for (int i=0; i<props.size(); i++) {
			Element aProp = (Element)props.get(i);
			String key = aProp.getName();
			String value = aProp.getText();
			logger.debug("Adding " + key + " - " + value);
			addProperty(key, value);
		}
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the ThreadPool being configured
   * then it calls the init(Element) method which actually initializes the ThreadPoolConfig
   * with the information found in the configuration element.
   *
	 */
	private void init() throws EnterpriseConfigurationObjectException {
		Element rootElement = getConfigDoc().getRootElement();
		logger.debug("RootElement is: " + rootElement.getName());
		logger.debug("Looking for ThreadPoolConfig named: " + getName());
		// Find the element specified by threadPoolName in the document
		Element configElement = getConfigElementByAttributeValue(getName(), "name");
		init(configElement);
	}
}

