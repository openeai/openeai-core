/*******************************************************************************
 $Source$
 $Revision: 3648 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.config;

import java.io.Serializable;
import java.util.*;

import org.openeai.*;
import org.openeai.scrubbers.*;

/**
 * This class is used to store and verify the expected format of a particular field.
 * It's an instance variable in the Field object and contains formatting rules
 * specific to that field as specified in the EnterpriseObjects document.
 *<P>
 * For the purpose of this object, formatting includes the following which is all specified
 * in the EnterpriseObjects document for all objects:
 * <ul>
 * <li>Data length verficiation
 * <li>Data type verification
 * <li>Data requiredness verification.  DTDs and Schemas specify that an Element or Attribute is required
 * but they don't require there to be any actual data in that element.  This verification ensures that level
 * of requiredness.
 * <li>Data scrubbing (e.g. converting the case of the data passed in to a uniform format like "mixed case").  Fields
 * can have multiple scrubbers associated to them and those scrubbers can be executed in a specified order.
 * <li>Data translations.  This means converting supplied data to an "enterprise value" if needed.  For example,
 * different applications have different versions of what the value of "Gender" may be.  Some applications say
 * it can be "M" for "Male" while another application may say it can be "1" for "Male".  This translation component
 * allows both of those application values to be translated to the ONE Enterprise Value "Male".  If the field has
 * a translation associated to it and the data passed in can't be translated, an exception will occur.
 * </ul>
 *<P>
 * These "formatting rules" are applied when the setter method of an Enterprise Object
 * is called.  For example, when the BasicPerson.setGender("M"); method is called, the
 * BasicPerson object uses its EnterpriseFields object to turn that supplied value into an 
 * "Enterprise Value".  The EnterpriseFields object uses a Field's Formatter to perform that logic.
 *<P>
 * Another example would be a situtation where an application calls the Name.setLastName("JACKSON"); method
 * When that method is called, it too uses its EnterpriseFields object to format that data.  Since that 
 * field doesn't have any Translations associated with it, the Formatter will verify that the data is of
 * an appropriate length (maximum or exact), verify that it's of the appropriate type (char, numeric etc.) 
 * and run any scrubbers that may be associated to that field on the data.  Consequently, the application value
 * of "JACKSON" may be converted to the Enterprise Value of "Jackson" in the end.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see EnterpriseFields
  * @see Field
  * @see EnterpriseTranslator
  * @see EnterpriseScrubber
 */
public class EnterpriseFormatter extends OpenEaiObject  implements Serializable {

	private String m_datatype = "";
	private boolean m_required = false;
	private int m_length = 0;
	private String m_lengthType = "";
	private String m_mask = "";
	private EnterpriseTranslator m_translator = null;

	// Need to have a Vector of scrubbers here.
	private Vector m_scrubbers = null;

	/**
	 * Constructor
	 */
	public EnterpriseFormatter() {
	}

  /**
   * Sets the EnterpriseTranslator object associated to the Formatter for the Field
   * if one should exist.
   *
   * @param translator EnterpriseTranslator the translator to be associated to the Field
   * @see EnterpriseTranslator
   * @see Field
   */
	public void setTranslator(EnterpriseTranslator translator) {
		m_translator = translator;
	}
  /**
   * Returns the EnterpriseTranslator object associated to the Formatter for the Field.
   * If a field doesn't have any translations associated to it, this will be null.
   *
   * @return EnterpriseTranslator the translator associated to the Field or null if none exists
   * @see EnterpriseTranslator
   * @see Field
   */
	public EnterpriseTranslator getTranslator() {
		return m_translator;
	}

  /**
   * Sets the list of scrubber implementations to that of the Vector passed in.
   * 
   * @param scrubber Vector the list of scrubber implmentations as specified in the EnterpriseObjects
   * document for the object.
   */
	public void setScrubbers(Vector scrubber) {
		m_scrubbers = scrubber;
	}
  /**
   * Returns the list of all scrubbers associated to the field.
   *
   * @return Vector
   */
	public Vector getScrubbers() {
		if (m_scrubbers == null) {
			m_scrubbers = new Vector();
		}
		return m_scrubbers;
	}
  /**
   * This method returns a scrubber from a specified location in the list of scrubbers
   * associated to the Field.  This is called by the EnterpriseFields object when an
   * application value passed to a field's setter method is converted to an enterprise
   * value.
   *
   * @param index int the index from which to pull the scrubber.
   * @return the scrubber implementation found at the index passed in.
   */
	public EnterpriseScrubber getScrubber(int index) {
		return(EnterpriseScrubber)getScrubbers().get(index);
	}
  /**
   * This method is used as the EnterprsieFields object builds a Field's EnterpriseFormatter
   * to add an EnterpriseScrubber implmentation that's specified in the EnterpriseObjects document
   * to the Field's Formatter.
   *
   * @param scrubber EnterpriseScrubber an EnterpriseScrubber implementation.
   */
	public void addScrubber(EnterpriseScrubber scrubber) {
		getScrubbers().add(scrubber);
	}
  /**
   * This method is also used as the EnterprsieFields object builds a Field's EnterpriseFormatter
   * to add an EnterpriseScrubber implmentation that's specified in the EnterpriseObjects document
   * to the Field's Formatter.  The difference between this and the addScrubber method is that this
   * method inserts the scrubber implementation at a specified location in the list of scrubbers
   * for the field.  This scrubber order is specified in the EnterpiseObjects document.
   *
   * @param index int the index at which the scrubber implmentation should be inserted.
   * @param scrubber EnterpriseScrubber an EnterpriseScrubber implementation.
   */
	public void insertScrubber(int index, EnterpriseScrubber scrubber) {
		getScrubbers().insertElementAt(scrubber, index);
	}

	public void setMask(String mask) {
		m_mask = mask;
	}
	public String getMask() {
		return m_mask;
	}

  /**
   * Sets the allowable datatype for the Field.  Allowable datatypes are:
   * <ul>
   * <li>char
   * <li>numeric
   * <li>alphanumeric
   * <li>any
   *<P>
   * @param type String the datatype as specified in the EnterpriseObjects document for this Field.
   */
	public void setDatatype(String type) {
		m_datatype = type;
	}
  /**
   * Returns the allowable datatype for the Field.  Allowable datatypes are:
   * <ul>
   * <li>char
   * <li>numeric
   * <li>alphanumeric
   * <li>any
   *<P>
   * @return String the datatype as specified in the EnterpriseObjects document for this Field.
   */
	public String getDatatype() {
		return m_datatype;
	}

  /**
   * Sets the boolean flag indicating whether or not this Field is required.  Note, 'required' means
   * there must be data in the field for it to be valid.
   *
   * @param required boolean true means the field is required, false means it's not.
   */
	public void setRequired(boolean required) {
		m_required = required;
	}
  /**
   * Returns the boolean flag indicating whether or not this Field is required.  Note, 'required' means
   * there must be data in the field for it to be valid.
   *
   * @return boolean true means the field is required, false means it's not.
   */
	public boolean getRequired() {
		return m_required;
	}

  /**
   * Sets the allowable length for the Field as specified in the EnterpriseObject document.  
   * Note, there are two types of lengths that can be associated to a Field (exact or maximum).
   *
   * @param length int the allowable length for the Field
   * @see EnterpriseFormatter#setLengthType(String) setLengthType
   */
	public void setLength(int length) {
		m_length = length;
	}
  /**
   * Returns the allowable length for the Field as specified in the EnterpriseObject document.  
   * Note, there are two types of lengths that can be associated to a Field (exact or maximum).
   *
   * @return int the allowable length for the Field
   * @see EnterpriseFormatter#setLengthType(String) setLengthType
   */
	public int getLength() {
		return m_length;
	}

  /**
   * Sets the length type associated to the Field as specified in the EnterpriseObjects document.
   * Allowable length types are: exact and maximum.
   *<P>
   * A length type of "exact" means the data supplied for the Field being populated must be EXACTLY n bytes long.
   * A length type of "maximum" means the data supplied for the Field being populated may be UP-TO n bytes long.
   *<P>
   * @param type String the length type ("exact" or "maximum").
   */
	public void setLengthType(String type) {
		m_lengthType = type;
	}
  /**
   * Returns the length type associated to the Field as specified in the EnterpriseObjects document.
   * Allowable length types are: "exact" and "maximum".
   *<P>
   * A length type of "exact" means the data supplied for the Field being populated must be EXACTLY n bytes long.
   * A length type of "maximum" means the data supplied for the Field being populated may be UP-TO n bytes long.
   *<P>
   * @return String the length type ("exact" or "maximum").
   */
	public String getLengthType() {
		return m_lengthType;
	}

  /**
   * This method is called by the EnterpriseFields object when a Field's data is being converted to 
   * the Enterprise Value.  This method applies all the rules as specified in the description of this class
   * to the data supplied for the field.  If any rule is broken and the value cannot be translated to an
   * enterprise value, an exception is thrown indicating the formatting rule that was broken.
   *<P>
   * @param value String the application value being formatted (aka - the supplied value).
   * @return String the value after it has been formatted.  This may be the same value as was passed in
   * or it may be different depending on how the Field's formatting rules are specified.  For example, if
   * there are scrubbers associated to the Field, the value will most likely be changed.  Additionally, if 
   * the field has a translator associated to it, the supplied value may be translated from an application value (code) 
   * to that value's corresponding enterprise value.
   * @throws InvalidFormatException if errors occur while applying the fomrmatting rules.
   */
	public String format(String value) throws InvalidFormatException {
		if (value == null || value.length() == 0) {
			if (getRequired()) {
				// if it's required, it's an error condition.
				throw new InvalidFormatException("Blank or Null value not allowed.");
			}
			else {
				// No further processing is needed...
				return value;
			}
		}

		if (getLengthType().equals("exact")) {
			// If lengthType is "exact" verify length of value matches max length exactly
			if (value.length() != getLength()) {
        String errMsg = "Length of field value is " + value.length() +
          ".  Length of field must be " + getLength() + " exactly.";
        logger.fatal("Format Error: " + errMsg);
				throw new InvalidFormatException(errMsg);
			}
		}
		else {
			// Else verify that length of the value is not more than max length
			if (value.length() > getLength() && getLength() != 0) {
        String errMsg = "Length of field value is " + value.length() +
          ".  Maximum length for this field is " + getLength();
        logger.fatal("Format Error: " + errMsg);
				throw new InvalidFormatException(errMsg);
			}
		}

		// Datatype checks.
		String dataType = getDatatype();
		if (dataType.equals("char")) {
			// value must be all characters
		}
		else if (dataType.equals("numeric")) {
			// value must be all numeric
      // try converting to a double
      try {
        Double.valueOf(value);
      }
      catch (NumberFormatException nfe) {
        String errMsg = "Format Error:  Datatype for this field must be 'numeric'.  " +
          "Field value is " + value + "  Exception: " + nfe.getMessage();
        logger.fatal(errMsg);
        throw new InvalidFormatException(errMsg, nfe);
      }
		}
		else if (dataType.equals("alphanumeric")) {
			// value must be a valid character or number
		}
		else {
			// there is no mask or it's "any" which means the same thing
		}

		return value;
	}
}

