/*******************************************************************************
 $Source$
 $Revision: 885 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.util.*;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Attribute;

import org.openeai.*;
import org.openeai.xml.*;
import org.openeai.layouts.*;

/**
 * A MessageObjectConfig is a wrapper class that takes information stored in an 
 * OpenEAI Deployment document (MessageObjectConfig Element) and stores it in a Java object.
 * Then the configuration object is passed to the constructor of an 
 * organization's Enterprise Message Objects and they are able to configure 
 * themselves with the information found in the config object.  
 * <P>
 * These configuration objects are used to configure both JmsEnterpriseObjects as
 * well as XmlEnterpriseObjects.  Certain parameters are required for both types
 * of objects and some are only required for JmsEnterpriseObjects.  Where this is
 * true, it will be noted.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the MessageObjectConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to MessageObjectConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) message objects being configured should use
 * the same configuration information.  Therefore, many of the MessageObject configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the message object.  This will be the key by which the object is stored in AppConfig
 * and will most likely be used to retrieve the object out of AppConfig.  Therefore
 * it should be unique.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>deferInitialization (true | false)</TD>
 * <TD>no (default is false)</TD>
 * <TD>This parameter tells the message object if it should initialize its EnterpriseFields,
 * Layout Manager(s) and Primed XML documents when the object is first initialized (when the 
 * application is started) or wait until the first time the object is used.  This can 
 * mean significant reduction in the time required to start an application because these
 * operations are relatively expensive especially when an application requires a large number
 * of message objects.  If this flag is set to false, the message objects will initialize
 * these components when they are first needed instead of when the application starts.  The
 * risk of deferring intialization is that the resources needed to initialize these objects may not
 * be available when they are initialized later and this would only be known at that time.  
 * If they're initialized when the application first starts, the user will know 
 * immediately if everything starts okay.  Once an object's EnterpriseFields, Layout Managers
 * and primed XML documents are fully initialized, they will not be intialized again for
 * the life of the application.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>xmlDocumentValidation (true | false)</TD>
 * <TD>no*</TD>
 * <TD>This parameter tells the message object if it should perform XML validation
 * when producing requests (create, update, delete, generate and query) or publishing
 * sync (createSync, updateSync, deleteSync etc.).  If this flag is true, the
 * message object will validate the XML document created from the contents of the
 * object prior to performing the message action associated to the method being called.  NOTE:
 * This parameter is only used by JmsEnterpriseObjects.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>translationType (all | application)</TD>
 * <TD>no*</TD>
 * <TD>This parameter tells the message object what type of application->enterprise
 * data translations will be performed.  If 'application' is chosen, the MessageObject
 * will only store translations associated to the current application in the MessageObject's
 * EnterpriseFields object.  This means, it will only translate values for a particular
 * application (that application's "application values").  If 'all' is chosen, 
 * the MessageObject will store translation mappings for all applications 
 * in its EnterpriseFields object.  This means it will perform translations for 
 * all applications.  Generally, this is only ever set to all in the case of an
 * infrastructure type application/gateway where that application will be processing
 * messages and performing translations for all applications.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>SenderAppId</TD>
 * <TD>no*</TD>
 * <TD>This parameter provides the message object with information needed when 
 * it produces/publishes messages related to the application sending the message.
 * This information is stored in the object and dynamically placed in the messages
 * produced by this message object.  It is part of the ControlArea... for all
 * messages as specified by the OpenEAI Message Protocol.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Authentication</TD>
 * <TD>no*</TD>
 * <TD>This parameter provides the message object with information needed when 
 * it produces/publishes messages related to the user associated to the application 
 * sending the message.  This information is stored in the object and dynamically 
 * placed in the messages produced by this message object.  
 * It is part of the ControlArea... for all messages as specified by 
 * the OpenEAI Message Protocol.  NOTE:
 * This parameter is only used by JmsEnterpriseObjects.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>CommandName</TD>
 * <TD>yes</TD>
 * <TD>This parameter provides the message object with information needed when 
 * it produces/publishes messages related to the COMMAND_NAME JMS Property that
 * will be put on the JMS Message ultimately produced by the message object.  This
 * COMMAND_NAME property is then used by Consumers when they consume a message to
 * determine which Request or Sync Command implementation they should execute.  This
 * is how the message being sent by the application/gateway is correlated to the 
 * business logic contained within the Commands executed by the consumers.    NOTE:
 * This parameter is only used by JmsEnterpriseObjects.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>PrimedXmlDocuments</TD>
 * <TD>no</TD>
 * <TD>This parameter provides the message object with information needed to 
 * store in memory a pre-parsed "primed" document related to the actions
 * that will be performed by that object.  Primed documents only need to be listed
 * here if the object will be performing that action.  This primed document is used
 * when building the XML Enterprise message from the contents of the object
 * as a starting point (for structure only).  The dynamic portions of the document
 * are replaced by these configuration paramaters along with the content of the 
 * object.  It is parsed during initialization so the object
 * doesn't have to parse the document each time it sends a message.  NOTE:
 * This parameter is only used by JmsEnterpriseObjects.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ObjectClass</TD>
 * <TD>yes</TD>
 * <TD>Name of the Java object that will be instantiated with this Config class 
 * (a JmsEnterpriseObject or XmlEnterpriseObject)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>EnterpriseObjectDocument</TD>
 * <TD>no*</TD>
 * <TD>Provides the object with information needed to initialize it's EnterpriseFields
 * object.  Each object must have one of these so it can know the appropriate rules
 * for the object as values are put into the object.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>LayoutManager</TD>
 * <TD>no*</TD>
 * <TD>Provides the object with information needed to initialize it's Layout manager
 * objects.  Each object must have one layout manager of type "xml" associated to it.
 * Objects can also have additional layout managers associated to it of different types.
 * These layout managers are what allow an object to build itself from and serialize
 * itself to different formats.  They are all implementations of 
 * org.openeai.layouts.EnterpriseLayoutManager and can be switched between during
 * the course of the application's lifetime.</TD>
 * </TR>
 * </TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see EnterpriseFields
 * @see org.openeai.layouts.EnterpriseLayoutManager
 * @see org.openeai.moa.objects.resources.MessageId
 * @see org.openeai.moa.objects.resources.Authentication
 */
public class MessageObjectConfig
  extends EnterpriseConfigurationObjectImpl
  implements EnterpriseConfigurationObject {

  private EnterpriseFields m_fields = null;
  private String m_translationType = "";

  private HashMap m_inputLayoutManagers = new HashMap();
  private HashMap m_outputLayoutManagers = new HashMap();
  private EnterpriseLayoutManager m_inputLayoutManager = null;    // First one in list.
  private EnterpriseLayoutManager m_outputLayoutManager = null;   // First one in list.

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public MessageObjectConfig() {
    setType("MessageObjectConfig");
  }

  public MessageObjectConfig(String configDocUrl, String objectName) throws EnterpriseConfigurationObjectException {
    setType("MessageObjectConfig");
    XmlDocumentReader xmlReader = new XmlDocumentReader();
    try {
      setConfigDoc(xmlReader.initializeDocument(configDocUrl, getValidation()));
    }
    catch (XmlDocumentReaderException e) {
      logger.fatal(e.getMessage(), e);
    }
    setName(objectName);
    init();
  }

  public MessageObjectConfig(Element configElement) throws EnterpriseConfigurationObjectException {
    setType("MessageObjectConfig");
    init(configElement);
  }

  /**
   * Sets the translation type that should be associated to the business object
   * being initialized with this configuration object.
   * <P>
   * @param translationType String the translation type (application or all)
   * @see org.openeai.config.EnterpriseFields
   */
  public void setTranslationType(String translationType) {
    m_translationType = translationType;
  }
  /**
   * Returns the translation type that should be associated to the business object
   * being initialized with this configuration object.
   * <P>
   * @return String the translation type (application or all)
   * @see org.openeai.config.EnterpriseFields
   */
  public String getTranslationType() {
    return m_translationType;
  }

  /**
   * Sets the EnterpriseFields object on this configuration object that will be
   * ultimately used by the acutal business object that gets initialized with this
   * configuration object.
   *<P>
   * @param fields EnterpriseFields
   * @see org.openeai.config.EnterpriseFields
   */
  public void setEnterpriseFields(EnterpriseFields fields) {
    if (fields == null) {
      m_fields = new EnterpriseFields();
    }
    else {
      m_fields = fields;
    }
  }
  /**
   * Returns the EnterpriseFields object associated to this configuration object
   * that will in turn be set on the business object being initialized using this
   * configuration object.
   * <P>
   * @return EnterpriseFields
   * @see org.openeai.config.EnterpriseFields
   */
  public EnterpriseFields getEnterpriseFields() {
    return m_fields;
  }

  /**
   * Sets the currently active input layout manager to the one passed in.
   *<P>
   * @param layout EnterpriseLayoutManager a layout manager implementation
   */
  public void setInputLayoutManager(EnterpriseLayoutManager layout) {
    m_inputLayoutManager = layout;
  }
  /**
   * Returns the currently active input layout manager.
   *<P>
   * @return EnterpriseLayoutManager a layout manager implementation
   */
  public EnterpriseLayoutManager getInputLayoutManager() {
    return m_inputLayoutManager;
  }

  /**
   * Sets the currently active output layout manager to the one passed in.
   *<P>
   * @param layout EnterpriseLayoutManager a layout manager implementation
   */
  public void setOutputLayoutManager(EnterpriseLayoutManager layout) {
    m_outputLayoutManager = layout;
  }
  /**
   * Returns the currently active output layout manager.
   *<P>
   * @return EnterpriseLayoutManager a layout manager implementation
   */
  public EnterpriseLayoutManager getOutputLayoutManager() {
    return m_outputLayoutManager;
  }

  /**
   * This method returns the HashMap of all input layout managers that will be associated
   * to the XmlEnterpriseObject that ultimately gets initialized using this MessageObjectConfig
   * object.  These input layout managers are specified in the EnterpriseObjects XML documents
   * and this class stores the information found there until the business object actually being
   * initialized is instantiated and configured by AppConfig.
   *<P>
   * Input layout managers are used to build an object from an input and are implementations
   * of the EnterpriseLayoutManager interface.
   *<P>
   * @return HashMap
   * @see org.openeai.moa.XmlEnterpriseObject
   * @see org.openeai.layouts.EnterpriseLayoutManager
   */
  public HashMap getInputLayoutManagers() {
    return m_inputLayoutManagers;
  }
  /**
   * This method sets the HashMap of input layout managers that will be associated to 
   * the message object being configured using this configuration object.
   *<P>
   * Input layout managers are used to build an object from an input and are implementations
   * of the EnterpriseLayoutManager interface.
   *<P>
   * @see org.openeai.moa.XmlEnterpriseObject
   * @see org.openeai.layouts.EnterpriseLayoutManager
   * @param iManagers HashMap
   */
  public void setInputLayoutManagers(HashMap iManagers) {
    m_inputLayoutManagers = iManagers;
  }
  /**
   * This method adds an input layout manager to the HashMap of input layout managers
   * associated to the object that will be initialized using this configuration object.
   * This method is called as the MessageObjectConfig object reads the Layout Manager information
   * from the EnterpriseObjects XML documents.
   *<P>
   * Input layout managers are used to build an object from an input and are implementations
   * of the EnterpriseLayoutManager interface.
   *<P>
   * @param type String the type of layout manager.  This is important because it allows a developer
   * to associate multiple layout managers to an object and then switch between layout managers
   * at runtime if an object needs to be built from more than one type of input.  Note, all 
   * objects that extend XmlEnterpriseObject must have an input layout manager of type "xml".
   * @param iLayout EnterpriseLayoutManager an EnterpriseLayoutManager implementation that may be used
   * by the object to build itself from a certain type of input (xml, extract, etc.).
   */
  public void addInputLayoutManager(String type, EnterpriseLayoutManager iLayout) {
    m_inputLayoutManagers.put(type, iLayout);
  }
  /**
   * This method returns the input layout manager that matches the type passed in
   * or null if that type does not exist in the HashMap of input layout managers associated
   * to this configuration object.
   *<P>
   * @return EnterpriseLayoutManager the implementation that is of type "type" that will be 
   * used to build the object from inputs of that type or null if none exist matching the specified
   * type.
   * @param type String the type of input layout manager being requested.
   */
  public EnterpriseLayoutManager getInputLayoutManager(String type) {
    if (m_inputLayoutManagers.containsKey(type)) {
      return (EnterpriseLayoutManager)m_inputLayoutManagers.get(type);
    }
    return null;
  }

  /**
   * This method returns the HashMap of all output layout managers that will be associated
   * to the XmlEnterpriseObject that ultimately gets initialized using this MessageObjectConfig
   * object.  These output layout managers are specified in the EnterpriseObjects XML documents
   * and this class stores the information found there until the business object actually being
   * initialized is instantiated and configured by AppConfig.
   *<P>
   * Output layout managers are used to serialized an object to a particular output and are implementations
   * of the EnterpriseLayoutManager interface.
   *<P>
   * @return HashMap
   * @see org.openeai.moa.XmlEnterpriseObject
   * @see org.openeai.layouts.EnterpriseLayoutManager
   */
  public HashMap getOutputLayoutManagers() {
    return m_outputLayoutManagers;
  }
  /**
   * This method sets the HashMap of output layout managers that will be associated to 
   * the message object being configured using this configuration object.
   *<P>
   * Output layout managers are used to serialized an object to a particular output and are implementations
   * of the EnterpriseLayoutManager interface.
   *<P>
   * @see org.openeai.moa.XmlEnterpriseObject
   * @see org.openeai.layouts.EnterpriseLayoutManager
   * @param iManagers HashMap
   */
  public void setOutputLayoutManagers(HashMap iManagers) {
    m_outputLayoutManagers = iManagers;
  }
  /**
   * This method adds an output layout manager to the HashMap of output layout managers
   * associated to the object that will be initialized using this configuration object.
   * This method is called as the MessageObjectConfig object reads the Layout Manager information
   * from the EnterpriseObjects XML documents.
   *<P>
   * Output layout managers are used to serialized an object to a particular output and are implementations
   * of the EnterpriseLayoutManager interface.
   *<P>
   * @param type String the type of layout manager.  This is important because it allows a developer
   * to associate multiple layout managers to an object and then switch between layout managers
   * at runtime if an object needs to be serialized to more than one type of output.  Note, all 
   * objects that extend XmlEnterpriseObject must have an output layout manager of type "xml".
   * @param oLayout EnterpriseLayoutManager an EnterpriseLayoutManager implementation that may be used
   * by the object to serialize itself to a certain type of output (xml, extract, etc.).
   */
  public void addOutputLayoutManager(String type, EnterpriseLayoutManager oLayout) {
    m_outputLayoutManagers.put(type, oLayout);
  }
  /**
   * This method returns the output layout manager that matches the type passed in
   * or null if that type does not exist in the HashMap of output layout managers associated
   * to this configuration object.
   *<P>
   * @return EnterpriseLayoutManager the implementation that is of type "type" that will be 
   * used to serialize the object to outputs of that type or null if none exist matching the specified
   * type.
   * @param type String the type of output layout manager being requested.
   */
  public EnterpriseLayoutManager getOutputLayoutManager(String type) {
    if (m_outputLayoutManagers.containsKey(type)) {
      return (EnterpriseLayoutManager)m_outputLayoutManagers.get(type);
    }
    return null;
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the MessageObject being initialized.
   * Then it sets various instance variables and properties on itself which will
   * be used by the MessageObject when AppConfig instantiates it passing this configuration object.
   * The MessageObject will then use the configuration java object to initialize itself.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the MessageObject being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    logger.debug("Element returned: ");
    logger.debug("  - " + configElement.getName());
    String configObjectName = configElement.getAttribute("name").getValue();
    logger.debug("  - " + configObjectName);
    String transType = configElement.getAttribute("translationType").getValue();
    setTranslationType(transType);

    addProperty("xmlDocumentValidation", configElement.getAttribute("xmlDocumentValidation").getValue());

    boolean deferInitialization = false;
    Attribute aDefer = configElement.getAttribute("deferInitialization");
    if (aDefer != null) {
      deferInitialization = new Boolean(aDefer.getValue()).booleanValue();
    }

    Document enterpriseObjectsDoc = null;
    String messageObjectClassName = configElement.getChild("ObjectClass").getText();
    String messageObjectName = messageObjectClassName.substring(messageObjectClassName.lastIndexOf('.') + 1);
    String eoDocUri = null;

    java.util.List props = configElement.getChildren();
    boolean keepTrying = true;
    while(keepTrying) {
    int propsSize = props.size();
    if (propsSize == 0) {
      keepTrying = false;
    }
    for (int i=0; i<propsSize; i++) {
      Element aProp = (Element)props.get(0);
      props.remove(0);
      if (aProp.getName().equals("Authentication")) {
        // UserId, Signature
        java.util.List auths = aProp.getChildren();
        for (int j=0; j<auths.size(); j++) {
          Element anAuthChild = (Element)auths.get(j);
          String key = anAuthChild.getName();
          String value = anAuthChild.getText();
          logger.debug("Adding " + key + " - " + value);
          addProperty(key, value);
        }
      }
      else if (aProp.getName().equals("PrimedXmlDocuments")) {
        java.util.List docs = aProp.getChildren();
        for (int k=0; k<docs.size(); k++) {
          Element aDoc = (Element)docs.get(k);
          String key, value;
          key = aDoc.getAttribute("type").getValue() + "-" + aDoc.getName();
          value = aDoc.getText();
          logger.debug("Adding " + key + " - " + value);
          addProperty(key, value);
        }
      }
      // NEW ENTERPRISEFIELDS CONFIGURATION START - 8/8
      else if (aProp.getName().equals("EnterpriseObjectDocument")) {
        eoDocUri = aProp.getAttribute("docUri").getValue();

        // new 7/30/2002, changed config document to allow multiple EnterpriseObject.xml
        // documents.  This way, we can break them apart and make each "chunk" smaller
        // this should decrease the time necessary to build an object and make AppConfig
        // initilialization quicker.
        if (enterpriseObjectsDoc == null) {
          if (eoDocUri != null) {
            try {

              //TODO: check for deferInitialization flag.  If True, don't parse the document yet.
              if (deferInitialization == false) {
                logger.debug("EF initialization is NOT deferred, parsing EO docs...");
                XmlDocumentReader xmlReader = new XmlDocumentReader();
                enterpriseObjectsDoc =
                  xmlReader.initializeDocument(eoDocUri,getValidation());
                logger.debug("DONE parsing EO docs.");
              }
              else {
                logger.debug("EnterpriseFields initialization has been deferred (not parsing EO doc).");
              }
                
            }
            catch (XmlDocumentReaderException e) {
              logger.fatal(e.getMessage(), e);
              String msg = "Exception occurred initializing EnterpriseFields object.  Exception: " + e.getMessage();
              logger.fatal(msg);
              throw new EnterpriseConfigurationObjectException(msg, e);
            }
          }
        }
        if (getEnterpriseFields() == null) {
          EnterpriseFields fields = new EnterpriseFields();
          fields.setEnterpriseObjectsUri(eoDocUri);
          fields.setIgnoreMissingFields(new Boolean(aProp.getAttribute("ignoreMissingFields").getValue()).booleanValue());
          fields.setIgnoreValidation(new Boolean(aProp.getAttribute("ignoreValidation").getValue()).booleanValue());
          logger.debug("[MessageObjectConfig] ignoreValidation is " + fields.ignoreValidation());
          fields.setTranslationType(getTranslationType());
          fields.setAppName(getAppName());
//          ObjectDefinitions.APP_NAME = getAppName();
          try {

            //TODO:  check for deferInitialization flag.  If true, don't do this yet.
            if (deferInitialization == false) {
              logger.debug("EnterpriseFields initialization is NOT deferred, initializing EF...");
              fields.init(enterpriseObjectsDoc);
              logger.debug("DONE with EnterpriseFields initialization.");
            }
            else {
              logger.debug("EnterpriseFields initialization has been deferred (not initializing EnterpriseFields).");
            }
            
          }
          catch (EnterpriseFieldException e) {
            String msg = "Exception occurred initializing EnterpriseFields object.  Exception: " + e.getMessage();
            logger.fatal(msg);
            throw new EnterpriseConfigurationObjectException(msg, e);
          }
          setEnterpriseFields(fields);
        }
      }
      // NEW ENTERPRISEFIELDS CONFIGURATION END - 8/8

      // NEW LAYOUT MANAGER CONFIGURATION START - 8/8
      else if (aProp.getName().equals("LayoutManager")) {
        // Build all of the layout manager objects...
        logger.debug("MessageObjectName is " + messageObjectName);

        //TODO:  may have to check deferInitialization flag here also...
//        if (enterpriseObjectsDoc == null && deferInitialization == false) {
        if (eoDocUri == null) {
          String msg = "[MessageObjectConfig] Cannot initialize LayoutManagers until the EnterpriseObjectDocument has been initialized, putting it back on the list";
          logger.warn(msg);
          props.add(aProp);
          continue;
        }
        
        logger.debug("Initializing layout managers...");
        java.util.List lInputLayouts = aProp.getChildren("InputLayout");
        boolean hasXmlLayoutManager = false;
        for (int im=0; im<lInputLayouts.size(); im++) {
          Element eInLayout = (Element)lInputLayouts.get(im);
          String inputLayoutType = eInLayout.getAttribute("type").getValue();
          String inputLayoutName = "";
          if (inputLayoutType.equals("xml")) {
            hasXmlLayoutManager = true;
            Attribute aInLayoutName = eInLayout.getAttribute("name");
            if (aInLayoutName != null) {
              inputLayoutName = aInLayoutName.getValue();
            }
            else {
          		String className = getClass().getName();
          		inputLayoutName = className.substring(className.lastIndexOf('.') + 1);	// Our based element in the XML passed in
            }
          }
          else {
            Attribute aInLayoutName = eInLayout.getAttribute("name");
            if (aInLayoutName != null) {
              inputLayoutName = aInLayoutName.getValue();
            }
            else {
              String msg = "[MessageObjectConfig] Non-XML input layouts must have a name associated with them.";
              logger.fatal(msg);
              throw new EnterpriseConfigurationObjectException(msg);
            }
          }
          String inLayoutClass = eInLayout.getChild("ObjectClass").getText();
          try {
            logger.debug("Instantiating input layout manager: " + inLayoutClass + " for " + inputLayoutName);
            java.lang.Class inLayoutObj = java.lang.Class.forName(inLayoutClass);

            EnterpriseLayoutManager ainElm = (EnterpriseLayoutManager)inLayoutObj.newInstance();

            //TODO: check deferInitialization flag.
//            ainElm.init(messageObjectName, inputLayoutName, enterpriseObjectsDoc);
            if (deferInitialization == false) {
              logger.debug("Layout manager initialization is NOT deferred, initializing input layout manager...");
              ainElm.init(inputLayoutName, enterpriseObjectsDoc);
              logger.debug("DONE initializing input layout manager...");
            }
            else {
              logger.debug("Layout manager initialization has been deferred (input)");
              ainElm.setLayoutManagerName(inputLayoutName);
              ainElm.setEnterpriseObjectsUri(eoDocUri);
            }
            
            addInputLayoutManager(inputLayoutType, ainElm);
            // Set the default input layout manager to the first one in the list.
            if (im == 0) {
              setInputLayoutManager(ainElm);
            }
          }
          catch (Exception e) {
            String msg = "Error initializing Input Layout Manager.  Exception: " + e.getMessage();
            logger.fatal(msg, e);
            throw new EnterpriseConfigurationObjectException(msg, e);
          }
        }
        if (hasXmlLayoutManager == false) {
          // throw an exception
          String msg = "[MessageObjectConfig] Missing XML Input Layout manager.";
          logger.fatal(msg);
          throw new EnterpriseConfigurationObjectException(msg);
        }

        logger.debug("MessageObjectName is " + messageObjectName);
        java.util.List lOutputLayouts = aProp.getChildren("OutputLayout");
        hasXmlLayoutManager = false;
        for (int om=0; om<lOutputLayouts.size(); om++) {
          Element eOutLayout = (Element)lOutputLayouts.get(om);
          String outputLayoutType = eOutLayout.getAttribute("type").getValue();
          String outputLayoutName = "";
          if (outputLayoutType.equals("xml")) {
            hasXmlLayoutManager = true;
            Attribute aOutLayoutName = eOutLayout.getAttribute("name");
            if (aOutLayoutName != null) {
              outputLayoutName = aOutLayoutName.getValue();
            }
            else {
          		String className = getClass().getName();
          		outputLayoutName =className.substring(className.lastIndexOf('.') + 1);	// Our based element in the XML passed in
            }
          }
          else {
            Attribute aOutLayoutName = eOutLayout.getAttribute("name");
            if (aOutLayoutName != null) {
              outputLayoutName = aOutLayoutName.getValue();
            }
            else {
              String msg = "[MessageObjectConfig] Non-XML output layouts must have a name associated with them.";
              logger.fatal(msg);
              throw new EnterpriseConfigurationObjectException(msg);
            }
          }

          String outLayoutClass = eOutLayout.getChild("ObjectClass").getText();
          try {
            logger.debug("Instantiating output layout manager: " + outLayoutClass + " for " + outputLayoutName);
            java.lang.Class outLayoutObj = java.lang.Class.forName(outLayoutClass);

            EnterpriseLayoutManager aoutElm = (EnterpriseLayoutManager)outLayoutObj.newInstance();            

            //TODO: check deferInitialization flag.
//            aoutElm.init(messageObjectName, outputLayoutName, enterpriseObjectsDoc);
            if (deferInitialization == false) {
              logger.debug("Layout manager initialization is NOT deferred, initializing output layout manager...");
              aoutElm.init(outputLayoutName, enterpriseObjectsDoc);
              logger.debug("DONE initializing output layout manager...");
            }
            else {
              logger.debug("Layout manager initialization has been deferred (output)");
              aoutElm.setLayoutManagerName(outputLayoutName);
              aoutElm.setEnterpriseObjectsUri(eoDocUri);
            }
            
            addOutputLayoutManager(outputLayoutType, aoutElm);
            // Set the default output layout manager to the first one in the list.
            if (om == 0) {
              setOutputLayoutManager(aoutElm);
            }
          }
          catch (Exception e) {
            String msg = "Error Initializing Output Layout Manager.  Exception:  " + e.getMessage();
            logger.fatal(msg);
            throw new EnterpriseConfigurationObjectException(msg, e);
          }
        }
        if (hasXmlLayoutManager == false) {
          // throw an exception
          String msg = "[MessageObjectConfig] Missing XML Output Layout manager.";
          logger.fatal(msg);
          throw new EnterpriseConfigurationObjectException(msg);
        }
      }
      // NEW LAYOUT MANAGER CONFIGURATION END - 8/8
      else {
        // SenderAppId, CommandName (requestName)
        String key = aProp.getName();
        String value = aProp.getText();
        logger.debug("Adding " + key + " - " + value);
        addProperty(key, value);
      }
    } // for
    } // while
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the MessageObject being configured
   * then it calls the init(Element) method which actually initializes the MessageObjectConfig
   * with the information found in the configuration element.
   *
	 */
  private void init() throws EnterpriseConfigurationObjectException {
    Element rootElement = getConfigDoc().getRootElement();
    logger.debug("RootElement is: " + rootElement.getName());
    logger.debug("Looking for MessageObjectConfig named: " + getName());

    // Find the element specified by objectName in the document
    Element configElement = getConfigElementByAttributeValue(getName(), "name");
    init(configElement);
  }
}

