/*******************************************************************************
 $Source$
 $Revision: 1264 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import org.jdom.Element;
import org.jdom.Attribute;
import java.util.ArrayList;
import java.util.HashMap;

import org.openeai.afa.*;
import org.openeai.loggingutils.MailService;

/**
 * A ScheduleConfig is a wrapper class that takes information stored in an 
 * OpenEAI Deployment document (Schedule Element) and stores it in a Java object.
 * Then the configuration object is passed to the constructor of the OpenEAI Schedule objects
 * and they are able to configure themselves with the information found in the
 * config object.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the Schedule
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to Schedule elements.
 * Many Elements and attributes are required at the container level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) Schedule objects being configured should use
 * the same configuration information.  Therefore, many of the Schedule configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the Schedule being configured.  This should be unique.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>isImmediate (true | false)</TD>
 * <TD>no*</TD>
 * <TD>This flag indicates weather or not the ScheduledCommands associated to 
 * this Schedule should be executed immediately when the ScheduledApp awakes 
 * from its sleepInterval rather than using a "RunTime" to determine when the 
 * Schedule should be processed.  If this is true, when the ScheduledApp wakes
 * up, it will execute all ScheduledCommands associated to this Schedule.  If
 * it is false, the ScheduledApp will determine when/if the ScheduledCommands
 * associated to this Schedule should be executed based on the Runtime associated
 * to the Schedule.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>runMechanism (Thread | SubProcess)</TD>
 * <TD>no*</TD>
 * <TD>This parameter indicates how the ScheduledCommands associated to this 
 * Schedule should be executed.  If "Thread" is specified, the ScheduledCommands
 * will be executed by adding them to the ThreadPool associated to the ScheduledApp.
 * NOTE: Currently, only a runMechansim of "Thread" is supported.  In the future,
 * if "SubProcess" is specified a new VM will be started and the ScheduledCommand
 * will be executed in that SubProcess.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ObjectClass</TD>
 * <TD>yes</TD>
 * <TD>Name of the Java object that will be instantiated with this Config class 
 * (currently, org.openeai.afa.Schedule)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>RunTime</TD>
 * <TD>no</TD>
 * <TD>If the 'isImmediate' parameter is false, this Element specifies on what day(s) and
 * at what time(s) the ScheduledCommands associated to this Schedule should be executed.  
 * If isImmediate is true, this information is ignored.  Days are specified as follows:
 * <ul>
 * <li>Daily (7 days a week)
 * <li>Weekdays (Monday thru Friday)
 * <li>Weekends (Saturday and Sunday)
 * <li>Sunday
 * <li>Monday
 * <li>Tuesday
 * <li>Wednesday
 * <li>Thursday
 * <li>Friday
 * <li>Saturday
 * </ul>
 * Times are specified as the Hour and Minute of the Day that the ScheduledCommands
 * associated to the Schedule should be executed.  It is possible to specify that 
 * a particular Schedule should be executed at multiple times within a given day.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Commands</TD>
 * <TD>yes</TD>
 * <TD>Provides configuration information for the ScheduledCommands associated 
 * to this Schedule.  The ScheduledCommands use these CommandConfig objects to initialize
 * themselves so they're ready to be executed when this Schedule is met.</TD>
 * </TR>
 * </TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.afa.Schedule
 * @see org.openeai.afa.ScheduleRuntime
 * @see org.openeai.afa.ScheduledCommand
 * @see CommandConfig
 * @see ScheduledAppConfig
 * @see MailServiceConfig
 */
public class ScheduleConfig
extends EnterpriseConfigurationObjectImpl
implements EnterpriseConfigurationObject {

	private String m_name = "";
	private String m_className = "";
  private Element m_defaultParms = null;
  private HashMap m_commandConfigs = new HashMap();
  private boolean m_immediate = false;
  private ArrayList m_runtimes = new ArrayList();
  private MailService m_mailService = null;
  private static String MAIL_SERVICE_CONFIG = "MailServiceConfig";
  private static String RUN_TIME = "RunTime";
  private static String DAY = "Day";
  private static String NAME = "name";
  

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public ScheduleConfig() {
		setType("ScheduleConfig");
  }

	public ScheduleConfig(Element configElement) throws EnterpriseConfigurationObjectException {
		setType("ScheduleConfig");
		init(configElement);
	}

	/**
   * Sets the default paramaters associated to this Schedule as specified in the "Schedules" Configuration
   * Element (the container) in the application's config document.
   *<P>
   * These default parameters are built from the "container" level configuration component in the config
   * document (Schedules).  This is to allow "default" configuration information to be specified for all Schedules
   * that exist within this container.  If needed a particular Schedule can override these default parameters.  Otherwise
   * the default parameters are used.  This way, duplicate configuration information does not have to be specified
   * for each Schedule.
   *
   * @param eDefaultParms Element
	 */
  public void setDefaultParms(Element eDefaultParms) {
    m_defaultParms = eDefaultParms;
  }
	/**
   * Returns the default paramaters associated to this Schedule as specified in the "Schedules" Configuration
   * Element (the container) in the application's config document.
   *<P>
   * These default parameters are built from the "container" level configuration component in the config
   * document (Schedules).  This is to allow "default" configuration information to be specified for all Schedules
   * that exist within this container.  If needed a particular Schedule can override these default parameters.  Otherwise
   * the default parameters are used.  This way, duplicate configuration information does not have to be specified
   * for each Schedule.
   *
   * @return Element
	 */
  public Element getDefaultParms() {
    return m_defaultParms;
  }

  /**
   * This method sets the name that will be associated to the Schedule as specified in the deployment document.
   * @param name String the Schedule's name.
   */
	public void setName(String name) {
		m_name = name;
	}
  /**
   * This method returns the name that will be associated to the Schedule as specified in the deployment document.
   * @return String the Schedule's name.
   */
	public String getName() {
		return m_name;
	}

  /**
   * Ths method sets the class name that will be the Schedule implementation.  Generally, this will be org.openeai.afa.Schedule.
   * However, by allowing the flexibility of specifying the Schedule implementation, organizations 
   * could develop a different implementation of a Schedule that behaves differently if necessary.
   *<P>
   * @param className String the class name of the Schedule implementation.
   */
	public void setClassName(String className) {
		m_className = className;
	}
  /**
   * Ths method returns the class name that will be the Schedule implementation.  Generally, this will be org.openeai.afa.Schedule.
   * However, by allowing the flexibility of specifying the Schedule implementation, organizations 
   * could develop a different implementation of a Schedule that behaves differently if necessary.
   *<P>
   * @return String the class name of the Schedule implementation.
   */
	public String getClassName() {
		return m_className;
	}

  /**
   * This method returns a list of all CommandConfig objects associated to this Schedule.  These configuration
   * objects will be used to initialize the actual ScheduledCommands that this Schedule must execute when the
   * Schedule is met.
   * <P>
   * @return HashMap
   * @see org.openeai.afa.Schedule
   * @see org.openeai.afa.ScheduledCommand
   */
  public HashMap getCommandConfigs() {
    return m_commandConfigs;
  }
  
  /**
   * This method sets the MailService object that will be associated to the 
   * Schedule as specified in the deployment document.  This MailService object 
   * will be used by the ScheduleApp foundation to notify someone if a 'daemon' 
   * type application (ScheduledCommand) throws an exception during execution.
   * @param m the mail service object to associate to this schedule.
   */
  public void setMailService(MailService m) {
    m_mailService = m;
  }
  /**
   * This method returns the MailService object that will be associated to the 
   * Schedule as specified in the deployment document.  This MailService object 
   * will be used by the ScheduleApp foundation to notify someone if a 'daemon' 
   * type application (ScheduledCommand) throws an exception during execution.
   * @return MailService the mail service object.
   */
  public MailService getMailService() {
    return m_mailService;
  }
  
  /**
   * This method adds a ScheduledCommand's configuration object to the list of CommandConfigs that this Schedule
   * needs to initialize all the ScheduledCommands that it must execute.  This method is called as the ScheduleConfig
   * object reads through the deployment document and retrieves configuration information.
   * <P>
   * @param name String the name of the ScheduledCommand
   * @param cConfig CommandConfig the CommandConfig java object that wraps the configuration Element in the deployment document
   * @see org.openeai.afa.ScheduledCommand
   * @see CommandConfig
   * @see org.openeai.afa.Schedule
   */
  public void addCommandConfig(String name, CommandConfig cConfig) {
    m_commandConfigs.put(name, cConfig);
  }
  /**
   * Returns a CommandConfig object for the specified ScheduledCommand name.
   *<P>
   * @return CommandConfig
   * 
   */
  public CommandConfig getCommandConfig(String name) {
    return (CommandConfig)m_commandConfigs.get(name);
  }

  /**
   * Returns the list of ScheduleRuntime objects that will be associated to the Schedule being configured
   * with this configuration object.
   * <P>
   * @return ArrayList
   * @see org.openeai.afa.Schedule
   * @see org.openeai.afa.ScheduleRuntime
   */
  public ArrayList getScheduleRuntimes() {
    return m_runtimes;
  }
  /**
   * Adds a ScheduleRuntime object to the list as the configuraton object encounters them in 
   * the application's configuration document.
   *<P>
   * @param rTime ScheduleRuntime
   * @see org.openeai.afa.ScheduleRuntime
   * @see org.openeai.afa.Schedule
   */
  private void addScheduleRuntime(ScheduleRuntime rTime) {
    m_runtimes.add(rTime);
  }

  private void setImmediate(boolean immediate) {
    m_immediate = immediate;
  }
  /**
   * Returns a flag indicating whether or not the Schedule being configured will be an "immediate" schedule.
   *<P>
   * @return boolean 
   * @see org.openeai.afa.Schedule
   */
  public boolean isImmediate() {
    return m_immediate;
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the Schedule being initialized.
   * Then it sets various instance variables and properties on itself which will
   * be used by the Schedule when the ScheculedApp instantiates it passing this configuration object.
   * The Schedule will then use the configuration java object to initialize itself.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the Schedule being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
   * @see org.openeai.afa.Schedule
   * @see org.openeai.afa.ScheduledApp
	 */
	public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    String scheduleName = configElement.getAttribute(NAME).getValue();
    setName(scheduleName);

    logger.debug("Element returned: ");
    logger.debug("  - " + configElement.getName());
    logger.debug("  - " + scheduleName);

    Attribute aImmediate = configElement.getAttribute("isImmediate");
    if (aImmediate != null) {
      setImmediate(new Boolean(aImmediate.getValue()).booleanValue());
    }
    else {
      // Use default values...
      aImmediate = getDefaultParms().getAttribute("isImmediate");
      if (aImmediate != null) {
        setImmediate(new Boolean(aImmediate.getValue()).booleanValue());
      }
      else {
        setImmediate(false);
      }
    }

    // look for default Runtimes associated to all Schedules.  This will 
    // be overridden if the Schedule has its owne Runtime associated to it.
    Element eDefaultRuntimes = getDefaultParms().getChild(RUN_TIME);
    if (eDefaultRuntimes != null) {
      addRuntimes(eDefaultRuntimes);          
    }
    
    // look for default MailService to be used for all Scheduleds.  This will
    // be overridden if the Schedule has its own MailService associated to it.
    Element eDefaultMailService = getDefaultParms().getChild(MAIL_SERVICE_CONFIG);
    if (eDefaultMailService != null) {
      MailServiceConfig mcf = new MailServiceConfig();
      mcf.init(eDefaultMailService);
      try {
        setMailService(new MailService(mcf));
      }
      catch (Exception e) {
        logger.fatal("Error configuring MailService.  Exception: " + e.getMessage(), e);
        throw new EnterpriseConfigurationObjectException(e.getMessage(), e);        
      }
    }

    // Add all attributes as 'Properties'
    try {
      java.util.List attrs = configElement.getAttributes();
	  	for (int i=0; i<attrs.size(); i++) {
		  	Attribute attr = (Attribute)attrs.get(i);
			  String key = attr.getName();
  			String value = attr.getValue();
	  		logger.debug("ScheduleConfig, Adding " + key + " - " + value);
		  	addProperty(key, value);
  		}

      // Now the Elements.
      java.util.List props = configElement.getChildren();
      for (int i=0; i<props.size(); i++) {
        Element aProp = (Element)props.get(i);
        if (aProp.getName().equals("Commands")) {
          java.util.List defaultAttrs = aProp.getAttributes();
          java.util.List commandElements = aProp.getChildren();
          ArrayList commands = new ArrayList();
          Element eDefaultParms = new Element("DefaultParms");
          // Add default attributes
          for (int l=0; l<defaultAttrs.size(); l++) {
            org.jdom.Attribute anAttr = (org.jdom.Attribute)defaultAttrs.get(l);
            eDefaultParms.setAttribute((org.jdom.Attribute)anAttr.clone());
          }
          // Add default elements
          for (int k=0; k<commandElements.size(); k++) {
            Element eElem = (Element)commandElements.get(k);
            if (eElem.getName().equals("Command")) {
              commands.add(eElem);
            }
            else {
              eDefaultParms.addContent((Element)eElem.clone());
            }
          }
          // Instantiate and add a CommandConfig object for each Command element.
          // ...
          for (int j=0; j<commands.size(); j++) {
            Element eCommand = (Element)commands.get(j);
            CommandConfig aCommandConfig = new CommandConfig();
            aCommandConfig.setAppName(getAppName());
            aCommandConfig.setDefaultParms(eDefaultParms);
            aCommandConfig.init(eCommand);
            String commandType = aCommandConfig.getType();
            String commandClass = aCommandConfig.getClassName();
            String commandName = aCommandConfig.getName();
            String key = "";
            addProperty(commandName, commandClass);
            addCommandConfig(aCommandConfig.getName(), aCommandConfig);
          }
        }
        else if (aProp.getName().equals(RUN_TIME)) {
          // override any defaults and add the Runtimes specific to this Schedule.
          if (getScheduleRuntimes() != null) {
            getScheduleRuntimes().removeAll(getScheduleRuntimes());
          }
          addRuntimes(aProp);          
        }
        else if (aProp.getName().equals(MAIL_SERVICE_CONFIG)) {
          setMailService(null);
          MailServiceConfig mcf = new MailServiceConfig();
          mcf.init(aProp);
          setMailService(new MailService(mcf));
        }
        else {
          String key = aProp.getName();
          String value = aProp.getText();
          logger.debug("Adding " + key + " - " + value);
          addProperty(key, value);
        }
      }
    }
    catch (Exception e) {
      logger.fatal("Error configuring schedule " + getName() + ".  Exception: " + e.getMessage(), e);
      throw new EnterpriseConfigurationObjectException(e.getMessage(), e);
    }
  }

  private void addRuntimes(Element eRuntimes) {
    // we've got to deal with Weekdays, Weekends.
    // if the "Day@name" attribute is Weekends, we're going to add two ScheduleRuntimes (Saturday and Sunday)
    // if the "Day@name" attribute is Weekdays, we're going to add five ScheduleRuntimes (Monday - Friday)
    String dayName = eRuntimes.getChild(DAY).getAttribute(NAME).getValue();
    if (dayName.equalsIgnoreCase("Weekends")) {
      // Saturday and Sunday
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.SATURDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.SUNDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
    }
    else if (dayName.equalsIgnoreCase("Weekdays")) {
      // Monday thru Friday
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.MONDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.TUESDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.WEDNESDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.THURSDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.FRIDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
    }
    else if (dayName.equalsIgnoreCase("Daily")) {
      // Every day (Sunday - Saturday)
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.SUNDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.MONDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.TUESDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.WEDNESDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.THURSDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.FRIDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
      eRuntimes.getChild(DAY).getAttribute(NAME).setValue(Schedule.SATURDAY);
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
    }
    else {
      addScheduleRuntime(new ScheduleRuntime(eRuntimes));
    }
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the Schedule being configured
   * then it calls the init(Element) method which actually initializes the ScheduleConfig
   * with the information found in the configuration element.
   *
	 */
	private void init() throws EnterpriseConfigurationObjectException {
		Element rootElement = getConfigDoc().getRootElement();
		logger.debug("RootElement is: " + rootElement.getName());
		// Find the element specified by producerName in the document
		Element configElement = getConfigElementByAttributeValue(getName(), NAME);
		init(configElement);
	}
}

 
