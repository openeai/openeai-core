/*******************************************************************************
 $Source$
 $Revision: 964 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.util.Properties;

import org.jdom.Element;
import org.jdom.Attribute;

import org.openeai.*;
import org.openeai.xml.*;

/**
 * A PropertyConifg is a wrapper class that takes information stored in an 
 * OpenEAI Deployment document (PropertyConfig Element) and stores it in a Java object.
 * Then the configuration object can be retrieved from AppConfig and the Properties
 * object associated to the config object can be used within the application.
 * <P>
 * The PropertyConfig object is simply an object that reads the Properties contained
 * in the deployment document and creates a Java Properties object with those properties
 * in it.  Then an appication can retrieve that Properties object and use it just
 * like any other Java Properties object.
 *<P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the PropertyConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to PropertyConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) PropertyConfig objects being configured should use
 * the same configuration information.  Therefore, many of the Property configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the PropertyConfig object.  This is how the object will be 
 * stored in AppConfig and should be unique.  This allows developers to 
 * conveniently categorize properties for an application</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Property</TD>
 * <TD>yes (at least one)</TD>
 * <TD>This is a pair of Elements that tells the PropertyConfig object what 
 * Java Properties object to build.  It consists of PropertyName and a PropertyValue
 * Elements that are stored in the Java Properties object.  Then a developer can
 * retrieve that Properties object from this PropertyConfig object and 
 * obtain the actually property values by name.</TD>
 * </TR>
 *</TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see org.openeai.OpenEaiObject
 */
public class PropertyConfig
  extends EnterpriseConfigurationObjectImpl
  implements EnterpriseConfigurationObject {

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public PropertyConfig() {
    setType("PropertyConfig");
    setProperties(new Properties());
  }

  public PropertyConfig(String configDocUrl, String propertyName) throws EnterpriseConfigurationObjectException {
    setType("PropertyConfig");
    setProperties(new Properties());
    XmlDocumentReader xmlReader = new XmlDocumentReader();
    try {
      setConfigDoc(xmlReader.initializeDocument(configDocUrl, getValidation()));
    }
    catch (XmlDocumentReaderException e) {
      logger.fatal(e.getMessage(), e);
    }
    setName(propertyName);
    init();
  }

  public PropertyConfig(Element configElement) throws EnterpriseConfigurationObjectException {
    setType("PropertyConfig");
    setProperties(new Properties());
    init(configElement);
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and builds a Java properties object
   * from its contents.  This information can then be retreived by the application via the getProperties() method
   * after getting the property config object from AppConfig.
   *<P>
   * The Properties object that gets built is inherited from OpenEaiObject.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the PropertyConfig object being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
   * @see org.openeai.OpenEaiObject
   * @see EnterpriseConfigurationObjectImpl#init
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    if (configElement == null) {
      logger.fatal("Couldn't find PropertyConfig named: " + getName());
    }
    else {
      // look for and set the 'refresh' Attribute (done in EnterpriseConfigurationObjectImpl)
      super.init(configElement);
      java.util.List props = configElement.getChildren();
      logger.debug("There are " + props.size() + " properties to add.");
      for (int i=0; i<props.size(); i++) {
        Element aProp = (Element)props.get(i);
        if (aProp.getName().equals("Property")) {
          String propName = aProp.getChild("PropertyName").getText();
          String propValue = aProp.getChild("PropertyValue").getText();
          logger.debug("Adding property " + propName + " - " + propValue);
          addProperty(propName, propValue);
        }
      }
    }
    logger.debug("PropertyConfig Properties size is " + getProperties().size());
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the Properties object being configured
   * then it calls the init(Element) method which actually initializes the PropertyConfig
   * with the information found in the configuration element.
   *
	 */
  private void init() throws EnterpriseConfigurationObjectException {
    Element rootElement = getConfigDoc().getRootElement();
    // Find the element specified by propertyName in the document
    Element configElement = getConfigElementByAttributeValue(getName(), "name");
    init(configElement);
  }
}

