package org.openeai.config;

public interface SecretProvider {
	String replaceSecrets(String configDocAsString) throws SecretProviderException;
}
