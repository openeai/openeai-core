/*******************************************************************************
 $Source$
 $Revision: 885 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.config;

import org.openeai.OpenEaiException;

/**
 * This exception is thrown when a Configuration object encounters errors
 * initializing themselves with information found in the Enterprise configuration
 * document.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class EnterpriseConfigurationObjectException extends OpenEaiException {

  /**
   * Constructor
   */
  public EnterpriseConfigurationObjectException() {
    super();
  }
  public EnterpriseConfigurationObjectException(String msg) {
    super(msg);
  }
  public EnterpriseConfigurationObjectException(String msg, Throwable rootCause) {
    super(msg, rootCause);
  }
  public EnterpriseConfigurationObjectException(Throwable rootCause) {
    super(rootCause);
  }
}

 
