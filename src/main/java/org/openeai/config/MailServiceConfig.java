/*******************************************************************************
 $Source$
 $Revision: 1264 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import org.jdom.Element;
import org.jdom.Attribute;

import org.openeai.*;
import org.openeai.xml.*;

/**
 * A MailServiceConfig is a wrapper class that takes information stored in an 
 * OpenEAI Deployment document (MailServiceConfig Element) and stores it in a Java object.
 * Then the configuration object can be retrieved from AppConfig and the Properties
 * object associated to the config object can be used within the application.
 * <P>
 * The MailServiceConfig object is simply an object that reads the elements contained
 * in the deployment document and is used to instantiate a MailService object
 *<P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the MailServiceConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to MailServiceConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) MailServiceConfig objects being configured should use
 * the same configuration information.  Therefore, many of the MailServiceConfig 
 * elements are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the MailServiceConfig object.  This is how the object will be 
 * stored in AppConfig and should be unique.  This allows developers to 
 * conveniently categorize MailService objects for an application and retrieve 
 * them by name</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ObjectClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the Java object that will be instantiated with this Config class 
 * (for now, it's the org.openeai.loggingutils.MailService object)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>RecipientList</TD>
 * <TD>yes</TD>
 * <TD>A comma separated list of email addresses to which an email message will be sent.
 * This list can be overriden at runtime if needed.</TD>
 * </TR>
 * <TD>FromAddress</TD>
 * <TD>yes</TD>
 * <TD>The email address of the sending application.  This can be overriden at 
 * runtime if needed and doesn't have to be a "real" email address.</TD>
 * </TR>
 * <TD>MailHost</TD>
 * <TD>yes</TD>
 * <TD>The SMTP host that will be used to send the email message.  This can be 
 * overriden at runtime if needed.</TD>
 * </TR>
 * <TD>Subject</TD>
 * <TD>no</TD>
 * <TD>A default subject to include with the message.  Applications using this 
 * object will likely wish to override this default subject at runtime.</TD>
 * </TR>
 * <TD>MessageBody</TD>
 * <TD>no</TD>
 * <TD>A default message body to include with the message.  Applications using this 
 * object will likely wish to override this default MessageBody at runtime.  Currently, 
 * only "text" is supported in the body.</TD>
 * </TR>
 *</TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     4.0  - 26 April 2005
 * @see org.openeai.loggingutils.MailService
 */
public class MailServiceConfig
  extends EnterpriseConfigurationObjectImpl
  implements EnterpriseConfigurationObject {
  
	private String m_mailHost;
	private String m_recipientList;
	private String m_fromAddr;
	private String m_subject;
	private String m_msgBody;

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public MailServiceConfig() {
    setType("MailServiceConfig");
  }
  
/**
	 * Sets the mailHost variable to the data passed in
	 *
	 * @param mailHost mail host set by the calling client
	 *
	 * @return void
	 */
	public void setMailHost(String mailHost) {
		m_mailHost = mailHost;
	}
/**
	 * returns the value of the mail host variable
	 *
	 * @return  String.mail host
	 */
	public String getMailHost() {
		return m_mailHost;
	}

/**
	 * Sets the toAddr class variable to the data passed in
	 *
	 * @param addr String coma separated list of email addresses to send messages to set by the client
	 *
	 * @return void
	 *
	 * @exception AddressException
	 */
	public void setRecipientList(String addr) {
		m_recipientList = addr;
	}
/**
	 * Returns the to address variable
	 *
	 * @return String email addresses of the recipients of a message
	 */
	public String getRecipientList() {
		return m_recipientList;
	}

/**
	 * returns the from address variable
	 *
	 * @return String email address of the sender of the message
	 */
	public String getFromAddress() {
		return m_fromAddr;
	}
/**
	 * Sets the fromAddr class variable to the data passed in
	 *
	 * @param fromAddr Address that messages are coming from
	 *
	 * @return void
	 *
	 */
	public void setFromAddress(String fromAddr) {
		m_fromAddr = fromAddr;
	}

/**
	 * sets the subject variable to data passed in from the client
	 *
	 * @param subject subject of the message passed from the client
	 *
	 * @return void
	 */
	public void setSubject(String subject) {
		m_subject = subject;
	}

/**
	 * returns the subject of the current message
	 *
	 * @return String subject of the email message
	 */
	public String getSubject() {
		return m_subject;
	}

/**
	 * sets the msgBody variable to data passed from the client.
	 *
	 * @param msg String message to be sent
	 *
	 * @return void
	 */
	public void setMessageBody(String msg) {
		m_msgBody = msg;
	}
/**
	 * returns the message body of the current message
	 *
	 * @return String message text
	 */
	public String getMessageBody() {
		return m_msgBody;
	}

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and builds a Java properties object
   * from its contents.  This information can then be retreived by the application via the getProperties() method
   * after getting the property config object from AppConfig.
   *<P>
   * The Properties object that gets built is inherited from OpenEaiObject.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the PropertyConfig object being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
   * @see org.openeai.OpenEaiObject
   * @see EnterpriseConfigurationObjectImpl#init
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    if (configElement == null) {
      logger.fatal("Couldn't find MailServiceConfig element named: " + getName());
    }
    else {
      super.init(configElement);
      Element eRecipientList = configElement.getChild("RecipientList");
      if (eRecipientList != null) { setRecipientList(eRecipientList.getTextTrim()); }
      
      Element eFromAddress = configElement.getChild("FromAddress");      
      if (eFromAddress != null) { setFromAddress(eFromAddress.getTextTrim()); }

      Element eMailHost = configElement.getChild("MailHost");      
      if (eMailHost != null) { setMailHost(eMailHost.getTextTrim()); }
      
      Element eSubject = configElement.getChild("Subject");      
      if (eSubject != null) { setSubject(eSubject.getText()); }

      Element eMessageBody = configElement.getChild("MessageBody");
      if (eMessageBody != null) { setMessageBody(eMessageBody.getText()); }
    }
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the Properties object being configured
   * then it calls the init(Element) method which actually initializes the PropertyConfig
   * with the information found in the configuration element.
   *
	 */
  private void init() throws EnterpriseConfigurationObjectException {
    Element rootElement = getConfigDoc().getRootElement();
    // Find the element specified by propertyName in the document
    Element configElement = getConfigElementByAttributeValue(getName(), "name");
    init(configElement);
  }
}