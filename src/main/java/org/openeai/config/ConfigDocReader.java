package org.openeai.config;

import java.util.Properties;

import org.jdom.Document;

public interface ConfigDocReader {
	Document readConfigDocAsDocument(String docUri, String appId) throws ConfigDocReaderException;
	Document readConfigDocAsDocument(Properties props) throws ConfigDocReaderException;
	String readConfigDocAsString(String docUri, String appId) throws ConfigDocReaderException;
	String readConfigDocAsString(Properties props) throws ConfigDocReaderException;
}
