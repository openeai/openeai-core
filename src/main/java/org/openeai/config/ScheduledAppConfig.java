/*******************************************************************************
 $Source$
 $Revision: 885 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.util.HashMap;
import org.jdom.Attribute;
import org.jdom.Element;
import java.util.ArrayList;
import org.openeai.afa.ScheduleIdStore;

/**
 * A ScheduledAppConfig is a wrapper class that takes information stored in an 
 * OpenEAI Deployment document (ScheduleAppConfig Element) and stores it in a Java object.
 * Then the configuration object is passed to the constructor of the OpenEAI ScheduledApps
 * and they are able to configure themselves with the information found in the
 * config object.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the ScheduledAppConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to ScheduledAppConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) ScheduledAppConfig objects being configured should use
 * the same configuration information.  Therefore, many of the ScheduledApp configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the ScheduledApp being configured.  This should be unique.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>type (daemon | application | triggered)</TD>
 * <TD>no*</TD>
 * <TD>This parameter indicates what type of Scheduled application is being
 * started.  Currently, there are three types:
 * <ul>
 * <li>daemon - indicates that the scheduled application should sleep for the 
 * specified interval, wake up, and if any Schedules should be executed, execute
 * the ScheduledCommands associated to them.  Then it should go back to sleep for the 
 * specified interval.  It should continue this process indefinitely or until 
 * it's manually shut down.  This allows an organization to use the flexible 
 * OpenEAI scheduling foundation to execute various business logic on 
 * different days and at different times (ScheduleRuntime) all within the same running process.
 * <li>application - indicates that the scheduled application should sleep for 
 * the specified interval, wake up and execute the ScheduledCommands for all 
 * Schedules associated to this Scheduled App.  Then it should exit when the 
 * Schedules have been executed.  This would be a "typical" batch type 
 * application that starts, performs it's business logic and then exits.  
 * The reason this is a critical type of application is
 * that it allows an organization to build many different applications that perform
 * different business logic (ScheduledCommands) and run them all with the same
 * start script.  Instead of having to deploy a specific start script of some sort
 * for each application developed.
 * <li>triggered - behaves just like an "application" but instead of automatically
 * exiting upon completion, it will wait until it recieves a shutdown hook from
 * the operating system.
 * </ul></TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>scheduleCheckInterval</TD>
 * <TD>no*</TD>
 * <TD>Time in milliseconds that the ScheduledApp should sleep between checking
 * Schedules.  If the Scheduled App is of type 'daemon' this is the amount of time
 * between checking Schedules.  If the app is of type 'application' or 'triggered'
 * this is the initial amount of time the Scheduled App will sleep prior to executing
 * the Schedules for the application.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ObjectClass</TD>
 * <TD>yes</TD>
 * <TD>Name of the Java object that will be instantiated with this Config class 
 * (currently, org.openeai.afa.ScheduledApp)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ScheduleIdRepository</TD>
 * <TD>no</TD>
 * <TD>This elements allows you to specify where you would like the ScheduledApp to maintain 
 * the list of Schedules it's processed.  This is a persistent repository it uses to survive 
 * being shutdown.  When a 'daemon' Scheduled App executes a Schedule that has 'RunTimes' associated 
 * to it, it keeps a record of that execution so it won't execute it again if the application 
 * is restarted.  This is all built into the Scheduled app foundation and is only applicable to 
 * Scheduled applications that are of type 'daemon' and have Schedules with 'Runtimes' associated 
 * to them.
 * <P>
 * By default, the file system is used to store this list of executed schedules.  This 
 * repository is implemented in the org.openeai.afa.FileScheduleIdStore class.  If no ScheduleIdRepository 
 * is specified, this repository will be used and the ScheduleIds will be written to a 
 * subdirectory called 'ScheduleIds' that is relevant to the location of execution.  
 * <P>
 * Another useful repository implementation is the org.openeai.afa.DbScheduleIdStore 
 * class which uses a simple database structure to store the ids associated 
 * to executed schedules.  This is very useful if you need to run multiple instances of a Scheduled 
 * Application on multiple hosts and they don't all have access to the same file system resources.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ThreadPoolConfig</TD>
 * <TD>no*</TD>
 * <TD>Contains information that the ScheduledApp uses to initialize a ThreadPool object
 * that it uses to execute ScheduledCommands associated to Schedules when they are met.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Schedules</TD>
 * <TD>yes</TD>
 * <TD>Contains configuration information related to all the Schedules that this 
 * ScheduledApp will at some time execute.  A ScheduledApp may execute ScheduledCommands
 * associated to many different Schedules in the same running process.  See ScheduleConfig and the Schedule
 * object for more information about this item.</TD>
 * </TR>
 * </TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see ScheduleConfig
 * @see CommandConfig
 * @see org.openeai.afa.ScheduledApp
 * @see org.openeai.afa.Schedule
 * @see org.openeai.afa.ScheduledCommand
 * @see org.openeai.afa.ScheduleRuntime
 * @see org.openeai.afa.FileScheduleIdStore
 * @see org.openeai.afa.DbScheduleIdStore
 */
public class ScheduledAppConfig
  extends EnterpriseConfigurationObjectImpl
  implements EnterpriseConfigurationObject {

  private HashMap m_scheduleConfigs = new HashMap();
  private ThreadPoolConfig m_tConfig = null;
  private ScheduleIdStore m_scheduleIdStore = null;

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public ScheduledAppConfig() {
    setType("ScheduledAppConfig");
  }

  public ScheduledAppConfig(Element configElement) throws EnterpriseConfigurationObjectException {
    setType("ScheduledAppConfig");
    init(configElement);
  }

  /**
   * Returns a HashMap containing all Schedule configuration objects that will be used
   * to intialize the Schedules associated to this ScheduledApp.
   *<P>
   * @return HashMap
   */
  public HashMap getScheduleConfigs() {
    return m_scheduleConfigs;
  }
  /**
   * Adds a Schedule configuration object to the HashMap of ScheduleConfigs contained
   * within this ScheduledApp.
   *<P>
   * @param name String the name of the Schedule that will be configured (also the key in the HashMap)
   * @param cConfig ScheduleConfig the Schedule's configuration object that will be used to initialize
   * the Schedule object.
   * @see org.openeai.afa.Schedule
   * @see ScheduleConfig
   */
  public void addScheduleConfig(String name, ScheduleConfig cConfig) {
    m_scheduleConfigs.put(name, cConfig);
  }
  /**
   * Returns a ScheduleConfig object for the name specified.
   *<P>
   * @return ScheduleConfig
   * @param name String the name of the schedule
   */
  public ScheduleConfig getScheduleConfig(String name) {
    return (ScheduleConfig)m_scheduleConfigs.get(name);
  }

  /**
   * Sets the ThreadPool configuration object that will be used to initialize the ThreadPool
   * associated to the ScheduledApp.
   *<P>
   * @param tConfig ThreadPoolConfig
   * @see org.openeai.afa.ScheduledApp
   * @see org.openeai.threadpool.ThreadPool
   */
  public void setThreadPoolConfig(ThreadPoolConfig tConfig) {
    m_tConfig = tConfig;
  }
  /**
   * Returns the ThreadPool configuration object that will be used to initialize the ThreadPool
   * associated to the ScheduledApp.
   *<P>
   * @return ThreadPoolConfig
   * @see org.openeai.afa.ScheduledApp
   * @see org.openeai.threadpool.ThreadPool
   */
  public ThreadPoolConfig getThreadPoolConfig() {
    return m_tConfig;
  }

  public void setScheduleIdStore(ScheduleIdStore store) {
    m_scheduleIdStore = store;
  }
  public ScheduleIdStore getScheduleIdStore() {
    return m_scheduleIdStore;
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the ScheduledApp being initialized.
   * Then it sets various instance variables and properties on itself which will
   * be used by the ScheduledApp when AppConfig instantiates it passing this configuration object.
   * The ScheduledApp will then use the configuration java object to initialize itself.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the ScheduledApp being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    logger.debug("In ScheduledAppConfig's init method...");

    String outterAppName = getAppName();
    String scheduledAppName = configElement.getAttribute("name").getValue();
    setAppName(scheduledAppName);

    logger.debug("Element returned: ");
    logger.debug("  - " + configElement.getName());
    logger.debug("  - " + scheduledAppName);

    // Add all attributes as 'Properties'
    try {
      java.util.List attrs = configElement.getAttributes();
	  	for (int i=0; i<attrs.size(); i++) {
		  	Attribute attr = (Attribute)attrs.get(i);
			  String key = attr.getName();
  			String value = attr.getValue();
	  		logger.debug("ScheduledAppConfig, Adding " + key + " - " + value);
		  	addProperty(key, value);
  		}

      // Now the Elements.
      java.util.List props = configElement.getChildren();
      for (int i=0; i<props.size(); i++) {
        Element aProp = (Element)props.get(i);
        if (aProp.getName().equals("Schedules")) {
          java.util.List defaultAttrs = aProp.getAttributes();
          java.util.List scheduleElements = aProp.getChildren();
          ArrayList schedules = new ArrayList();
          Element eDefaultParms = new Element("DefaultParms");
          // Add default attributes
          for (int l=0; l<defaultAttrs.size(); l++) {
            org.jdom.Attribute anAttr = (org.jdom.Attribute)defaultAttrs.get(l);
            eDefaultParms.setAttribute((org.jdom.Attribute)anAttr.clone());
          }
          // Add default elements
          for (int k=0; k<scheduleElements.size(); k++) {
            Element eElem = (Element)scheduleElements.get(k);
            if (eElem.getName().equals("Schedule")) {
              logger.debug("ScheduleAppConfig, found a Schedule element.");
              schedules.add(eElem);
            }
            else {
              eDefaultParms.addContent((Element)eElem.clone());
            }
          }
          // Instantiate and add a ScheduleConfig object for each Schedule element.
          // ...
          for (int j=0; j<schedules.size(); j++) {
            Element eSchedule = (Element)schedules.get(j);
            ScheduleConfig aScheduleConfig = new ScheduleConfig();
            aScheduleConfig.setAppName(outterAppName);
            aScheduleConfig.setDefaultParms(eDefaultParms);
            aScheduleConfig.init(eSchedule);
            String scheduleClass = aScheduleConfig.getClassName();
            String scheduleName = aScheduleConfig.getName();
            logger.info("ScheduledAppConfig, adding ScheduleConfig, " + scheduleName);
            addProperty(scheduleName, scheduleClass);
            addScheduleConfig(aScheduleConfig.getName(), aScheduleConfig);
          }
        }
        else if (aProp.getName().equals("ThreadPoolConfig")) {
          setThreadPoolConfig(new ThreadPoolConfig(aProp));
        }
        else if (aProp.getName().equals("ScheduleIdRepository")) {
          String className = aProp.getChild("ObjectClass").getText();
          Element eConfig = aProp.getChild("Configuration");
          AppConfig aConfig = new AppConfig();
          aConfig.setName(getAppName());
          aConfig.init(eConfig);
          java.lang.Class storeClass = java.lang.Class.forName(className);
          ScheduleIdStore store = (ScheduleIdStore)storeClass.newInstance();
          store.init(aConfig);
          setScheduleIdStore(store);
        }
        else {
          String key = aProp.getName();
          String value = aProp.getText();
          logger.debug("Adding " + key + " - " + value);
          addProperty(key, value);
        }
      }
    }
    catch (Exception e) {
      logger.fatal("Error configuring scheduled app " + getAppName() + ".  Exception: " + e.getMessage(), e);
      throw new EnterpriseConfigurationObjectException(e.getMessage(), e);
    }
  }
  
	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the ScheduledApp being configured
   * then it calls the init(Element) method which actually initializes the ScheduledAppConfig
   * with the information found in the configuration element.
   *
	 */
  private void init() throws EnterpriseConfigurationObjectException {
    Element rootElement = getConfigDoc().getRootElement();
    logger.debug("RootElement is: " + rootElement.getName());
    // Find the element specified by producerName in the document
    Element configElement = getConfigElementByAttributeValue(getName(), "name");
    init(configElement);
  }
}

 
