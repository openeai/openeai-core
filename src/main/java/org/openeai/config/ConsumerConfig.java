/*******************************************************************************
 $Source$
 $Revision: 885 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.config;

import java.util.*;

import org.jdom.output.XMLOutputter;
import org.jdom.Attribute;
import org.jdom.Element;

import org.openeai.xml.*;

/**
 * A ConsumerConfig is a wrapper class that takes information stored in an
 * OpenEAI Deployment document (ConsumerConfig Element) and stores it in a Java object.
 * Then the configuration object is passed to the constructor of the OpenEAI consumers
 * and they are able to configure themselves with the information found in the
 * config object.
 * <P>
 * Both PubSub and PointToPoint consumers use the same configuration information 
 * to initialize themselves.  However, some items listed in a ConsumerConfig Element
 * are only used by PubSubConsumers and some are only used by PointToPointConsumers.
 * Where this is the case, it is noted.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the ConsumerConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to ConsumerConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) Consumers being configured should use
 * the same configuration information.  Therefore, many of the Consumer configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * Many of the parameters specified for a consumer are simply JMS parameters required
 * by the JMS Specification. 
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the Consumer.  This will be used in many logged messages as well as
 * when creating durable subscriptions (in the case of PubSubConsumers).  Therefore
 * it should be unique.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>startOnInitialization (true | false)</TD>
 * <TD>no (defaults to false)</TD>
 * <TD>Indicates whether or not this consumer should be "started" after it is 
 * initialized.  For the purpose of Consumers, started means it has established
 * it's connection to the Broker and is ready to receive messages.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>transacted (true | false)</TD>
 * <TD>no (defaults to false)</TD>
 * <TD>Indicates whether or not messages consumed by this consumer should be 
 * treated as "transacted" or not.  Transacted is defined by the JMS specification.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConnectionFactoryName</TD>
 * <TD>no*</TD>
 * <TD>The name of the JMS Administered object (TopicConnectionFactory or 
 * QueueConnectionFactory) that this Consumer will use
 * to initialize itself from a JMS perspective.  To maintain broker independance, 
 * this administered object should be stored in a Directory server.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>DestinationName</TD>
 * <TD>yes</TD>
 * <TD>The name of the JMS Administered object (Topic or Queue) that this Consumer 
 * will consume messages from.  To maintain broker independance, 
 * this administered object should be stored in a Directory server.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>InitialContextFactory</TD>
 * <TD>no*</TD>
 * <TD>The class name of the JNDI context factory that will be used to retrieve the JMS
 * administered objects from a Directory Server or wherever the administered
 * objects are stored.
 * <P>
 * Typically, this is "com.sun.jndi.ldap.LdapCtxFactory".</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ProviderURL</TD>
 * <TD>no*</TD>
 * <TD>Location where the administered objects can be retrieved from.  
 * To maintain broker independance, this should reference a location in a 
 * Directory server.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>SecurityPrincipal</TD>
 * <TD>no*</TD>
 * <TD>The user with which to create the InitialContext when retrieving the
 * Administered objects.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>SecurityCredentials</TD>
 * <TD>no*</TD>
 * <TD>The credentials associated to the user with which to create the 
 * InitialContext when retrieving the Administered objects.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Username</TD>
 * <TD>no*</TD>
 * <TD>Broker user with which to connect to the broker as.  Generally, this is 
 * stored within the ConnectionFactory administered object so it is not needed
 * here.  However, if it isn't stored or you wish to override what's stored
 * in the AO, you can specify that here.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Password</TD>
 * <TD>no*</TD>
 * <TD>Password associated to the broker user with which to connect to the broker 
 * as.  Generally, this is stored within the ConnectionFactory administered 
 * object so it is not needed here.  However, if it isn't stored or you wish 
 * to override what's stored in the AO, you can specify that here.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>Commands</TD>
 * <TD>yes</TD>
 * <TD>Lists the Commands that this Consumer executes.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ObjectClass</TD>
 * <TD>yes</TD>
 * <TD>Name of the Java object that will be instantiated with this Config class 
 * (currently, PointToPointConsumer or PubSubConsumer)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>GenericResponse</TD>
 * <TD>no*</TD>
 * <TD>Contains information that the Consumer uses to initialize a generic response
 * 'primed' document.  This is only relevant to PointToPointConsumers and is only
 * ever used if the Consumer has errors executing the Command associated to the 
 * JMS Message it consumes.  In all other cases, the Commands themselves are
 * responsible for returning the appropriate response to the calling application.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ThreadPoolConfig</TD>
 * <TD>no*</TD>
 * <TD>Contains information that the Consumer uses to initialize a ThreadPool object
 * that it uses to process incomming messages.  Depending on how this ThreadPool is
 * configured (checkBeforeProcessing), the consumer may pause message consumption 
 * if the ThreadPool is busy and only continue when it has available Threads 
 * that can process jobs.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>DbConnectionPoolConfig</TD>
 * <TD>no*</TD>
 * <TD>Contains information that PubSubConsumers use to initialize a EnterpriseConnectionPool object
 * that they use to verify another PubSubConsumer is not already processing the message.
 * This is called the PubSubMessageBalancer component.  Since all PubSubConsumers consume
 * each message delivered to a topic, there has to be a way to run muliple instances
 * of a PubSubConsumer and ensure that only one consumer processes the message.
 * This is the mechanism by which OpenEAI PubSubConsumers do this.  NOTE: this is only
 * relevant to PubSubConsumers.</TD>
 * </TR>
 * </TABLE>
 * @author      Tod Jackson (tod@openeai.org)
 * @author      Steve Wheat (steve@openeai.org)
 * @version     3.0  - 28 January 2003
 * @see CommandConfig
 * @see ThreadPoolConfig
 * @see DbConnectionPoolConfig
 * @see org.openeai.jms.consumer.PubSubMessageBalancer
 */
public class ConsumerConfig
  extends EnterpriseConfigurationObjectImpl
  implements EnterpriseConfigurationObject {

  private HashMap m_commandConfigs = new HashMap();
  private ThreadPoolConfig m_tConfig = null;
  private DbConnectionPoolConfig m_dbConfig = null;

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public ConsumerConfig() {
    setType("ConsumerConfig");
  }

  public ConsumerConfig(String configDocUrl, String consumerName) throws EnterpriseConfigurationObjectException {
    setType("ConsumerConfig");
    XmlDocumentReader xmlReader = new XmlDocumentReader();
    try {
      setConfigDoc(xmlReader.initializeDocument(configDocUrl, getValidation()));
    }
    catch (XmlDocumentReaderException e) {
      String msg = "Error Initializing config document at " + configDocUrl +
        "  Exception: " + e.getMessage();
      logger.fatal(msg);
      throw new EnterpriseConfigurationObjectException(msg, e);
    }
    setName(consumerName);
    init();
  }

  public ConsumerConfig(Element configElement) throws EnterpriseConfigurationObjectException {
    setType("ConsumerConfig");
    init(configElement);
  }

  /**
   * Returns the entire list (HashMap) of all CommandConfig objects assciated to this consumer.
   *
   * @return HashMap of all CommandConfig Java objects.
   */
  public HashMap getCommandConfigs() {
    return m_commandConfigs;
  }
  /**
   * Adds a CommandConfig Java object to the list (HashMap) of CommandConfigs that this consumer will execute.
   * As the ConsumerConfig reads and processes the consumer's Configuration Element it determines which commands
   * this consumer may execute.  For each one of those commands found, it instantiates and configures a CommandConfig
   * Java object associated to that Command's configuration.  Then, when the Consumer itself is initialized with 
   * this ConsumerConfig object, it will instantiate and initialize those commands that the consumer needs to know about.
   *
   * @param name String the name of the command being configured
   * @param cConfig CommandConfig the CommandConfig Java object that has been initialized with information found in the Command's Configuration
   * Element specified in the deployment document.
   * @see CommandConfig CommandConfig
   */
  public void addCommandConfig(String name, CommandConfig cConfig) {
    m_commandConfigs.put(name, cConfig);
  }
  /**
   * Returns a specific CommandConfig Java object by name that will be used to initialize an actual Command object
   * during Consumer initialization.
   *
   * @param name String name of the command being configured.
   */
  public CommandConfig getCommandConfig(String name) {
    return (CommandConfig)m_commandConfigs.get(name);
  }

  /**
   * Sets the ThreadPoolConfig Java object associated to this consumer.  All consumers may execute
   * their Commands in Threads by using the ThreadPool foundation object.  The Configuration Element
   * of a Consumer allows developers to specify how this threadpool should be configured.  The ThreadPoolConfig
   * Java object is a wrapper around that configuration Element.  When the ConsumerConfigs's init method is called by AppConfig
   * it retrieves this ThreadPool Configuration information and initializes the ThreadPoolConfig object with that information.
   * Then, when the Consumer itself is initialized, it can use that ThreadPoolConfig Java object to initialize the ThreadPool Java
   * object associated to the Consumer.
   *
   * @param tConfig ThreadPoolConfig the Java object that wraps the ThreadPoolConfig Element in the deployment document.
   * @see ThreadPoolConfig
   * @see org.openeai.threadpool.ThreadPoolImpl ThreadPoolImpl
   */
  public void setThreadPoolConfig(ThreadPoolConfig tConfig) {
    m_tConfig = tConfig;
  }
  /**
   * Returns the ThreadPoolConfig Java object associated to this consumer.  All consumers may execute
   * their Commands in Threads by using the ThreadPool foundation object.  The Configuration Element
   * of a Consumer allows developers to specify how this threadpool should be configured.  The ThreadPoolConfig
   * Java object is a wrapper around that configuration Element.  When the ConsumerConfigs's init method is called by AppConfig
   * it retrieves this ThreadPool Configuration information and initializes the ThreadPoolConfig object with that information.
   * Then, when the Consumer itself is initialized, it can use that ThreadPoolConfig Java object to initialize the ThreadPool Java
   * object associated to the Consumer.
   *
   * @return ThreadPoolConfig the Java object that wraps the ThreadPoolConfig Element in the deployment document.
   * @see ThreadPoolConfig
   * @see org.openeai.threadpool.ThreadPoolImpl ThreadPoolImpl
   */
  public ThreadPoolConfig getThreadPoolConfig() {
    return m_tConfig;
  }

  /**
   * Sets the DbConnectionPoolConfig Java object associated to this consumer.  PubSubConsumer's use a built in Message Balancer
   * foundation component that allows them to determine if another Consumer is already processing a particular message.  This balancer
   * is currently implemented by persisting unique information to a table in a database.  As PubSubConsumers consume messages,
   * they consult this store to determine if the message they've consumed is already being processed by another consumer.  This 
   * DbConnectionPoolConfig Java object wraps the DbConnectionPoolConfig Element found in the deployment document.  It is
   * associated to the PubSubConsumers and then when the consumers themselves are started, they use this Java object to 
   * initialized the EnterpriseConnectionPool foundation object associated to the consumer that will be used to balance PubSub messages.
   *
   * @param dbConfig DbConnectionPoolConfig to be associated to the PubSubMessageBalancer for this consumer.
   * @see DbConnectionPoolConfig
   * @see org.openeai.dbpool.EnterpriseConnectionPool EnterpriseConnectionPool
   * @see org.openeai.jms.consumer.PubSubMessageBalancer PubSubMessageBalancer
   */
  public void setDbConnectionPoolConfig(DbConnectionPoolConfig dbConfig) {
    m_dbConfig = dbConfig;
  }
  /**
   * Returns the DbConnectionPoolConfig Java object associated to this consumer.  PubSubConsumer's use a built in Message Balancer
   * foundation component that allows them to determine if another Consumer is already processing a particular message.  This balancer
   * is currently implemented by persisting unique information to a table in a database.  As PubSubConsumers consume messages,
   * they consult this store to determine if the message they've consumed is already being processed by another consumer.  This 
   * DbConnectionPoolConfig Java object wraps the DbConnectionPoolConfig Element found in the deployment document.  It is
   * associated to the PubSubConsumers and then when the consumers themselves are started, they use this Java object to 
   * initialized the EnterpriseConnectionPool foundation object associated to the consumer that will be used to balance PubSub messages.
   *
   * @return DbConnectionPoolConfig to be associated to the PubSubMessageBalancer for this consumer.
   * @see DbConnectionPoolConfig
   * @see org.openeai.dbpool.EnterpriseConnectionPool EnterpriseConnectionPool
   * @see org.openeai.jms.consumer.PubSubMessageBalancer PubSubMessageBalancer
   */
  public DbConnectionPoolConfig getDbConnectionPoolConfig() {
    return m_dbConfig;
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the Consumer (PubSub or PointToPoint) being initialized.
   * Then it sets various instance variables and properties on itself which will
   * be used by the Consumer when AppConfig instantiates it passing this configuration object.  The Consumer
   * will then use the configuration java object to initialize itself.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the Command being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
   * @see org.openeai.jms.consumer.PubSubConsumer PubSubConsumer
   * @see org.openeai.jms.consumer.PointToPointConsumer PointToPointConsumer
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    String consumerName = configElement.getAttribute("name").getValue();
    
    logger.debug("Element returned: ");
    logger.debug("  - " + configElement.getName());
    logger.debug("  - " + consumerName);

    /*
    String start = configElement.getAttribute("startOnInitialization").getValue();
    addProperty("startOnInitialization",start);
    addProperty("consumerName", consumerName);
    */

    // Add all attributes as 'Properties'
    try {
      java.util.List attrs = configElement.getAttributes();
	  	for (int i=0; i<attrs.size(); i++) {
		  	Attribute attr = (Attribute)attrs.get(i);
			  String key = attr.getName();
  			String value = attr.getValue();
	  		logger.debug("ConsumerConfig, Adding " + key + " - " + value);
		  	addProperty(key, value);
  		}

      // Now the Elements.
      java.util.List props = configElement.getChildren();
      for (int i=0; i<props.size(); i++) {
        Element aProp = (Element)props.get(i);
        if (aProp.getName().equals("Commands")) {
          java.util.List defaultAttrs = aProp.getAttributes();
          java.util.List commandElements = aProp.getChildren();
          ArrayList commands = new ArrayList();
          Element eDefaultParms = new Element("DefaultParms");
          // Add default attributes
          for (int l=0; l<defaultAttrs.size(); l++) {
            org.jdom.Attribute anAttr = (org.jdom.Attribute)defaultAttrs.get(l);
            eDefaultParms.setAttribute((org.jdom.Attribute)anAttr.clone());
          }
          // Add default elements
          for (int k=0; k<commandElements.size(); k++) {
            Element eElem = (Element)commandElements.get(k);
            if (eElem.getName().equals("Command")) {
              commands.add(eElem);
            }
            else {
              eDefaultParms.addContent((Element)eElem.clone());
            }
          }
//          java.util.List commands = aProp.getChildren();
          // Instantiate and add a CommandConfig object for each Command element.
          // ...
          for (int j=0; j<commands.size(); j++) {
            Element eCommand = (Element)commands.get(j);
//            CommandConfig aCommandConfig = new CommandConfig(eCommand);
            CommandConfig aCommandConfig = new CommandConfig();
            aCommandConfig.setAppName(getAppName());
            aCommandConfig.setDefaultParms(eDefaultParms);
            aCommandConfig.init(eCommand);
            String commandType = aCommandConfig.getType();
            String commandClass = aCommandConfig.getClassName();
            String commandName = aCommandConfig.getName();
            String key = "";
            if (commandType.equals("requestMessage")) {
              key = "RequestCommand." + commandName;
              String value = commandClass;
              logger.debug("Adding " + key + " - " + value);
              addProperty(key, value);
            }
            else if (commandType.equals("syncMessage")) {
              key = "SyncCommand." + commandName;
              String value = commandClass;
              logger.debug("Adding " + key + " - " + value);
              addProperty(key, value);
            }
            addCommandConfig(aCommandConfig.getName(), aCommandConfig);
          }
        }
        else if (aProp.getName().equals("GenericResponse")) {
          String docUri = aProp.getChild("DocumentURI").getText();
          String validate = aProp.getAttribute("validate").getValue();
          addProperty("DocumentURI", docUri);
          addProperty("validate", validate);
        }
        // General properties are not used by Consumers
        /*
        else if (aProp.getName().equals("Property")) {
          String propName = aProp.getChild("PropertyName").getText();
          String propValue = aProp.getChild("PropertyValue").getText();
          logger.debug("Adding Property " + propName + " - " + propValue);
          addProperty(propName, propValue);
        }
        */
        else if (aProp.getName().equals("ThreadPoolConfig")) {
          setThreadPoolConfig(new ThreadPoolConfig(aProp));
        }
        else if (aProp.getName().equals("DbConnectionPoolConfig")) {
          setDbConnectionPoolConfig(new DbConnectionPoolConfig(aProp));
        }
        else {
          String key = aProp.getName();
          String value = aProp.getText();
          logger.debug("Adding " + key + " - " + value);
          addProperty(key, value);
        }
      }
    }
    catch (Exception e) {
      logger.fatal("Error configuring consumer.  Exception: " + e.getMessage(), e);
      throw new EnterpriseConfigurationObjectException(e.getMessage(), e);
    }
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the Consumer being configured
   * then it calls the init(Element) method which actually initializes the ConsumerConfig
   * with the information found in the configuration element.
	 *
	 */
  private void init() throws EnterpriseConfigurationObjectException {
    Element rootElement = getConfigDoc().getRootElement();
    logger.debug("RootElement is: " + rootElement.getName());
    // Find the element specified by producerName in the document
    Element configElement = getConfigElementByAttributeValue(getName(), "name");
    init(configElement);
  }
}

