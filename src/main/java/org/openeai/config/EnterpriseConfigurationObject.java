/*******************************************************************************
 $Source$
 $Revision: 966 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.config;

import java.util.Properties;
import org.jdom.Element;

/**
 * The interface that all 'configuration' objects implement.
 * The reason we add this layer to our configuration strategy is
 * so all our foundation components don't have to be 'xml aware'.  This way we can
 * abstract that requirement and if we ever decide to use anything besides or in
 * addition to XML for configuration information, we just have to change this layer
 * and not our framework components per se.  The foundation objects are configured
 * using these 'configuration' Java objects, not XML.
 *<P>
 * Configuration objects are used like Java Properties objects except for the fact
 * that by using XML to specify the configuration of a certain type of object, we
 * can constrain that configuration and provide a meaningful, relatively simple way to 
 * configure our applications and more specifically, the objects that those applications
 * use.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public interface EnterpriseConfigurationObject {
/**
 * This method is called by the other init method.  It does the actual work of initializing
 * the configuration object with information contained in the Element that was passed to the other
 * method after that method does some housekeeping stuff with the element.
 * It might not be necessary to make this public....
 */
//  public void init() throws EnterpriseConfigurationObjectException;
/**
 * This method is called by AppConfig as it reads through the deployment document and 
 * parses Configuration information for each object that will be held by AppConfig.  The Element
 * that is passed to this init method is the 'configuration' element for the type of object
 * being initialized.  For example, the configuration element passed in might be a MessageObjectConfig
 * element or a ProducerConfig element.  The MessageObjectConfig and ProducerConfig Java objects that 
 * are implementations of this interface would then look for the appropriate configuration information
 * contained within that element and that is associated to that type of object and initialize itself
 * with it.  Then, AppConfig will construct a MessageObject or Producer using the MessageObjectConfig and
 * ProducerConfig java objects respectively.
 * <P>
 * @param  configElement Element The Element that is passed to this init method is the 'configuration' element for the type of object
 * being initialized pulled out of the deployment document by AppConfig.
 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException;
/**
 * This method returns the properties object associated to the configuration object implementation.
 * Many configuration parameters are simply turned into a Java Properties object for use by
 * the objects that will be configured using the implementation.
 * <P>
 * @return      Properties the properties object associated to the Configuration implementation
 * that might contain simple property like information depending on the implementation.  It is entirely
 * up to the configuration object implementation and the object that will be configured using that implementation
 * as to what goes into a Properties object and what does not.
 */
  public Properties getProperties();
/**
 * This method sets the application name to which this configuration object (and ultimately the 
 * object being configured) is associated.
 * Since all applications are basically a collection of objects that are used within that application
 * the application name can be associated to all objects used by that application.  This can be
 * useful information in an enterprise utilizing these foundational objects within there applications.
 * <P>
 * @param  name String the name of the application to which this object is associated.
 */
  public void setAppName(String name);
/**
 * This method returns the application name to which this configuration object (and ultimately the 
 * object being configured) is associated.
 * Since all applications are basically a collection of objects that are used within that application
 * the application name can be associated to all objects used by that application.  This can be
 * useful information in an enterprise utilizing these foundational objects within there applications.
 * <P>
 * @return      String the name of the application to which this object is associated.
 */
  public String getAppName();

  
  public boolean refresh();
}

 
