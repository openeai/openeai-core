/*******************************************************************************
 $Source$
 $Revision: 1472 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.config;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Attribute;

import org.openeai.xml.*;
import org.openeai.*;

/**
 * The parent class for all of our 'configuration' objects.
 * The reason we add this layer to our configuration strategy is
 * so all our framework components don't have to be 'xml aware'.  This way we can
 * abstract that requirement and if we ever decide to use anything besides or in
 * addition to XML for configuration information, we just have to change this layer
 * and not our framework components per se.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public abstract class EnterpriseConfigurationObjectImpl extends OpenEaiObject {

	private boolean m_validate = false;
	private boolean m_refresh = false;
	private Document m_configDoc = null;
	private String m_name = "";
	private String m_type = "";
	private String m_appName = "";
//	private java.util.List m_configTypes = initializeConfigTypes();
	private int matchCall = 0;

	/**
	 * Constructor
	 */
	public EnterpriseConfigurationObjectImpl() {
	}

	/**
	 * Provides base initialization.  Currently, this method only looks for the 'refresh' 
   * attribute that may be associated to all configuration elements.  If subclasses 
   * call super.init(configElement) they'll set that refresh property on themselves 
   * if it's present.  Then, they can provide the specific implemenation for themselves 
   * appropriately.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the configuration object being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
	 */
  public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    Attribute aRefresh = configElement.getAttribute("refresh");
    if (aRefresh != null) {
      boolean refresh = new Boolean(aRefresh.getValue()).booleanValue();
      setRefresh(refresh);
    }

    Attribute aName = configElement.getAttribute("name");
    if (aName != null) {
      setName(aName.getValue());
    }
  }
  
  /**
   * Sets the object name associated to the configuration object being built.  Specifically,
   * this is the "name" attribute that all configuration Elements have as defined in 
   * the Deployment.dtd document.  This allows AppConfig to store the pre-configured
   * objects that get initialized using these configuration objects by name and application
   * developers to retrieve them by name.
   *
   * @param name String the name of the configuration object (and thus the actual foundation object) specified
   * in the "name" attribute associated to each configuration Element in the deployment document.
   */
	public void setName(String name) {
		if (name == null) {
			m_name = "";
		}
		else {
			m_name = name;
		}
	}
  /**
   * Returns the object name associated to the configuration object being built.  Specifically,
   * this is the "name" attribute that all configuration Elements have as defined in 
   * the Deployment.dtd document.  This allows AppConfig to store the pre-configured
   * objects that get initialized using these configuration objects by name and application
   * developers to retrieve them by name.
   *
   * @return String the name of the configuration object (and thus the actual foundation object) specified
   * in the "name" attribute associated to each configuration Element in the deployment document.
   */
	public String getName() {
		return m_name;
	}

  /**
   * Sets the Application name associated to this configuration object (and the actual object that will
   * be initialized with this configuration object).
   *
   * @param name String the name of the application to which this object will belong.
   */
	public final void setAppName(String name) {
		if (name == null) {
			m_appName = "";
		}
		else {
			m_appName = name;
		}
	}
  /**
   * Returns the Application name associated to this configuration object (and the actual object that will
   * be initialized with this configuration object).
   *
   * @return String the name of the application to which this object will belong.
   */
	public final String getAppName() {
		return m_appName;
	}

  /**
   * Sets the type of Configuration object that this is.  Types so far include:
   * <ul>
   * <li>MessageObjectConfig
   * <li>CommandConfig
   * <li>ConsumerConfig
   * <li>ProducerConfig
   * <li>DbConnectionPoolConfig
   * <li>AppConfig
   * <li>PropertyConfig
   * <li>ThreadPoolConfig
   * </ul>
   *<P>
   * @param type.
   */
	public void setType(String type) {
		if (type == null) {
			m_type = "";
		}
		else {
			m_type = type;
		}
	}
  /**
   * Returns the type of Configuration object that this is.  Types so far include:
   * <ul>
   * <li>MessageObjectConfig
   * <li>CommandConfig
   * <li>ConsumerConfig
   * <li>ProducerConfig
   * <li>DbConnectionPoolConfig
   * <li>AppConfig
   * <li>PropertyConfig
   * <li>ThreadPoolConfig
   * </ul>
   *<P>
   * @return String type.
   */
	public String getType() {
		return m_type;
	}

  public final void setRefresh(boolean refresh) {
    m_refresh = refresh;
  }
  public boolean refresh() {
    return m_refresh;
  }
  
  /**
   * This method, along with 'getValidation' are used to determine if the XML configuration document
   * used to configure an application will be validated or not.  There are currently no uses
   * of this method therefore all deployment/configuration XML document validation is always false.
   * These methods may be removed altogether in the future because at this point there is really
   * no way to turn validation on for these deployment/configuration documents.  Additionally,
   * there may not be any reason to do so.  Validation is typically performed when the document
   * is being filled out.  There are times when it is necessary to allow an invalid document
   * to exist for testing purposes etc.
   *
   * @param validate boolean true to turn XML validation on false to turn it off.
   */
	public final void setValidation(boolean validate) {
		m_validate = validate;
	}
  /**
   * Returns a boolean indicating whether or not XML document validation for the deployment documents
   * is turned on or not.  Currently, XML validation is always 'false'.
   *
   * @return boolean true to turn XML validation on false to turn it off.
   */
	public final boolean getValidation() {
		return m_validate;
	}

  /**
   * Sets the deployment/configuration document that is associated to this Configuration object.  This
   * is determined when AppConifg is initialized.  Then it parses that document and calls this method
   * so the object will have a reference to the document it needs to configure itself.
   *<P>
   * @param configDoc Document JDOM Document.   
   */
	public final void setConfigDoc(Document configDoc) {
		m_configDoc = configDoc;
	}
  /**
   * Returns the deployment/configuration document that is associated to this Configuration object.  This
   * is determined when AppConifg is initialized.  This is what the object uses to determine how it needs
   * to be configured.
   *<P>
   * @return Document JDOM Document.   
   */
	public final Document getConfigDoc() {
		return m_configDoc;
	}

  /**
   * Adds a general property (name/value pair) to this object's inherited Properties object.
   * Many Configuration objects use general properties in addition to specific instance variables
   * in the object to specify the configuration informatin for an object.  It's up to the implementor
   * of the EnterpriseConfigurationObject (i.e. ProducerConfig) and the foundation component (i.e. PointToPointProducer) 
   * to determine what will be general properties and what will be specified via specific instance variables
   * and the corresponding getter/setter methods.
   *<P>
   * Generally, "simple" configuration elements should be considered for use as a general properties and complex
   * configuration elements that have an associated Java object should be specific getter/setter methods in the configuration object.
   *<P>
   * @param key String the property name
   * @param value String the property value
   */
	public final void addProperty(String key, String value) {
		getProperties().put(key, value);
	}

	public final Element getElementByAttributeValue(Element e, String attrValue) {
		XmlElementLocator locator = new XmlElementLocator();
		return locator.getElementByAttributeValue(e, attrValue);
	}

	public final Element getConfigElementByAttributeValue(String value, String attributeName) {
		// Find the element "name" in the current document that matches the type
		// of "configType"
		Element root = getConfigDoc().getRootElement();
		// Get all MessagingComponents.
		// For each component, get its configuration Element.
		// For each configuration element, check its type.
		// If its type matches the type of this object, get the name associated to the configuration element.
		// If the name matches this object's name, then return that element.
		logger.debug("Looking for value: " + value);
		logger.debug("Looking for attribute named: " + attributeName);
//		java.util.List mComponents = root.getChild("MessagingComponents").getChildren();
		java.util.List mComponents = root.getChildren();
		for (int i=0; i<mComponents.size(); i++) {
			Element e = (Element)mComponents.get(i);
			java.util.List mComponentItems = e.getChildren();
			for (int h=0; h<mComponentItems.size(); h++) {
				Element eMComponent = (Element)mComponentItems.get(h);
				Element eConfig = eMComponent.getChild("Configuration");
				java.util.List lConfigs = eConfig.getChildren();
				for (int j=0; j<lConfigs.size(); j++) {
					Element eConfigGroup = (Element)lConfigs.get(j);
					java.util.List lConfigItems = eConfigGroup.getChildren();
					for (int k=0; k<lConfigItems.size(); k++) {
						Element eConfigItem = (Element)lConfigItems.get(k);
						if (eConfigItem.getName().equals(getType())) {
							if (eConfigItem.getAttribute(attributeName).getValue().equals(getName())) {
								// we have winner!
								return eConfigItem;
							}
						}
						else {
							// Here's where we need to go all the way down for each configuration item
							// and see if it contains a config object matching the one we're looking for.
							Element e1 = matchConfigByAttribute(eConfigItem, attributeName);
							if (e1 != null) {
								if (e1.getName().equals(getType())) {
									if (e1.getAttribute(attributeName).getValue().equals(getName())) {
										// we have winner!
										return e1;
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	private Element matchConfigByAttribute(Element e, String attributeName) {
		matchCall++;
		if (e.getContentSize()>0) {
			java.util.List ec = e.getChildren();
			for (int i=0; i<ec.size(); i++) {
				Element e1 = (Element)ec.get(i);
				if (e1.getName().equals(getType())) {
					if (e1.getAttribute(attributeName).getValue().equals(getName())) {
						return e1;
					}
					else {
						e1 = matchConfigByAttribute(e1,attributeName);
						if (e1 != null) {
							if (e1.getName().equals(getType())) {
								if (e1.getAttribute(attributeName).getValue().equals(getName())) {
									return e1;
								}
							}
						}
					}
				}
				else {
					e1 = matchConfigByAttribute(e1, attributeName);
					if (e1 != null) {
						if (e1.getName().equals(getType())) {
							if (e1.getAttribute(attributeName).getValue().equals(getName())) {
								return e1;
							}
						}
					}
				}
			}
		}
		return null;
	}

  /*
	private boolean isConfigElement(Element e) {
		if (m_configTypes.contains(e.getName())) {
			return true;
		}
		else {
			return false;
		}
	}
  */

  /*
	private ArrayList initializeConfigTypes() {
		ArrayList v = new ArrayList();
		v.add("AppConfig");
		v.add("ProducerConfig");
		v.add("LoggerConfig");
		v.add("ConsumerConfig");
		v.add("CommandConfig");
		v.add("MessageObjectConfig");
		v.add("PropertyConfig");
		return v;
	}
  */
}

