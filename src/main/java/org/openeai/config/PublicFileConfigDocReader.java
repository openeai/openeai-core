package org.openeai.config;

import java.util.Properties;

import org.jdom.Document;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

public class PublicFileConfigDocReader implements ConfigDocReader {

	@Override
	public Document readConfigDocAsDocument(String docUri, String appId) throws ConfigDocReaderException {
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
			return xmlReader.initializeDocument(docUri, false);
		} 
        catch (XmlDocumentReaderException e) {
			e.printStackTrace();
            throw new ConfigDocReaderException(e.getMessage(), e);
		}
	}

	@Override
	public Document readConfigDocAsDocument(Properties props) throws ConfigDocReaderException {
        String providerUrl = props.getProperty("providerUrl");
        if (providerUrl == null) {
            throw new ConfigDocReaderException("Null 'providerUrl' not allowed.");
        }
        
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
			return xmlReader.initializeDocument(providerUrl, false);
		} 
        catch (XmlDocumentReaderException e) {
			e.printStackTrace();
            throw new ConfigDocReaderException(e.getMessage(), e);
		}
	}

	@Override
	public String readConfigDocAsString(String docUri, String appId) throws ConfigDocReaderException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String readConfigDocAsString(Properties props) throws ConfigDocReaderException {
		// TODO Auto-generated method stub
		return null;
	}
}
