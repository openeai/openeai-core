/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.util.*;
import org.openeai.*;

/**
 * This object maintains a list of mappings from application values to enterprise values
 * by object/field and application name.  This information is contained in the
 * EnterpriseObjects document and the EnterpriseMapping object is an instance
 * variable in EnterpriseTranslator.
 * <P>
 * The EnterpriseMapping object is built on a field by field basis as the EnterpriseFields
 * object builds itself with contents found in the EnterpriseObjects documents.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see EnterpriseTranslator
  * @see EnterpriseFields
 */
public class EnterpriseMapping extends OpenEaiObject {

  private String m_fieldName = "";
  private String m_enterpriseValue = "";
//  private String m_appName = "";
  private HashMap m_applicationValues = new HashMap();

  /**
   * Constructor
   */
  public EnterpriseMapping() {
  }

  /**
   * This method returns a HashMap containing all Application specific values related to 
   * this particular field.
   *
   * @return HashMap
   */
  public final HashMap getApplicationValues() {
    return m_applicationValues;
  }
  /**
   * This method sets the HashMap of application specific values related to this field.
   */
  public final void setApplicationValues(HashMap values) {
    if (values == null) {
      m_applicationValues = new HashMap();
    }
    else {
      m_applicationValues = values;
    }
  }
  /**
   * The EnterpriseFields object uses this method to add an application value (by appName) to 
   * the HashMap of all application values for this field.
   *<P>
   * @param appName String the name of the application that this value applies to (this is the key in the HashMap)
   * @param value String the actual application value for this the application and the field associated to this mapping object.
   */
  public final void addApplicationValue(String appName, String value) {
    m_applicationValues.put(appName, value);
  }

  /**
   * This method returns the enterprise value associated to this Mapping.  Note, there can only be
   * one Enterprise Value for a given field.
   * <P>
   * @return String the enterprise value associated to this field.
   */
  public final String getEnterpriseValue() {
    return m_enterpriseValue;
  }
  /**
   * This method sets the enterprise value associated to this mapping.  This is called when
   * the EnterpriseFields object builds the Translator/Mapping for the field from information
   * found in the EnterpriseObjects document.
   *<P>
   * @param value String the enterprise value as specified in the EnterpriseObjects document
   */
  public final void setEnterpriseValue(String value) {
    if (value == null) {
      m_enterpriseValue = "";
    }
    else {
      m_enterpriseValue = value;
    }
  }

  /**
   * This method returns the name of the field associated to this mapping.  This corresponds
   * to the name of the field as it is specified in the enterprise objects document.  Note,
   * the field name associated to the EnterpriseMapping object is a combination of the parent object
   * and the field itself (eg. - BasicPerson/Gender).  This is to ensure uniqueness when storing
   * field names since different parent objects could have the same field names within them.
   *<P>
   * @return String the name of the field.
   */
  public final String getFieldName() {
    return m_fieldName;
  }
  /**
   * This method sets the name of the field associated to this mapping.  This corresponds
   * to the name of the field as it is specified in the enterprise objects document.    Note,
   * the field name associated to the EnterpriseMapping object is a combination of the parent object
   * and the field itself (eg. - BasicPerson/Gender).  This is to ensure uniqueness when storing
   * field names since different parent objects could have the same field names within them.  This
   * method is called as the EnterpriseFields object builds the translator/mapping objects
   * based on the information found in the EnterpriseObjects XML document.
   *<P>
   * @param name String the name of the field.
   */
  public final void setFieldName(String name) {
    if (name == null) {
      m_fieldName = "";
    }
    else {
      m_fieldName = name;
    }
  }

  /**
   * This method returns the name of the application associated to this mapping.
   */
  /*
  public final String getAppName() {
    return m_appName;
  }
  public final void setAppName(String name) {
    if (name == null) {
      m_appName = "";
    }
    else {
      m_appName = name;
    }
  }
  */
}

