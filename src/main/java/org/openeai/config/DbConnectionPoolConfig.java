/*******************************************************************************
 $Source$
 $Revision: 743 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
***********************************************************************/

package org.openeai.config;

import org.jdom.Element;
import org.jdom.Attribute;

/**
 * A DbConnectionPoolConfig is a wrapper class that takes information stored in 
 * an OpenEAI Deployment document (DbConnectionPoolConfig Element) and stores it in a Java object.
 * Then the configuration object is passed to the constructor of our EnterpriseConnectionPools
 * and they are able to configure themselves with the information found in the
 * config object.
 * <P>
 * These are the configuration parameters specified by the DbConnectionPoolConfig
 * Element in the Deployment document.  NOTE:  Like all other OpenEAI configuration
 * objects, there is a "container" level associated to DbConnectionPoolConfig objects.
 * Many Elements and attributes are required at that level and may be optionally
 * overridden at this level.  This is to avoid having to enter redundant information
 * in the Deployment document if all (or most) EnterpriseConnectionPool objects
 * being configured should use the same configuration information.  
 * Therefore, many of the DbConnectionPool configuration
 * parameters are optional at this level but required at the "container" level.  Where
 * this is the case, it will be indicated by an "*".
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters specified by the DbConnectionPoolConfig
 * Element in the Deployment document.
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ConfigClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the configuration class that wraps the config Element (this class)</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>ObjectClass</TD>
 * <TD>no*</TD>
 * <TD>Name of the Java object that will be instantiated with this Config class</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>name</TD>
 * <TD>yes</TD>
 * <TD>Name of the pool</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbDriverName</TD>
 * <TD>yes</TD>
 * <TD>JDBC Database driver name</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbConnectString</TD>
 * <TD>yes</TD>
 * <TD>JDBC URL that is used to connect to the database</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbConnectUserId</TD>
 * <TD>yes</TD>
 * <TD>User id to connect as</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbConnectPassword</TD>
 * <TD>yes</TD>
 * <TD>password associated to the user id</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbPoolSize</TD>
 * <TD>yes</TD>
 * <TD>Initial size of the pool</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbPoolMaxSize</TD>
 * <TD>no</TD>
 * <TD>Maximum number of connections that may be created by this pool.  
 * if this parameter is left out or if zero is specified, the pool
 * will not restrict how large the pool may get at a given time.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>dbVerificationString</TD>
 * <TD>no</TD>
 * <TD>Some SQL string that can be used to test a connection prior to returning
 * it from the pool.  This should be a highly efficient statement.  If this
 * is not specified, ONLY the Connection.isClosed() method will be used.
 * If the dbDriverName is an Oracle driver a default verification string 
 * will be used in addition to the Connection.isClosed</TD>
 * </TR>
 * </TABLE>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
 */
public class DbConnectionPoolConfig
extends EnterpriseConfigurationObjectImpl
implements EnterpriseConfigurationObject {

  private String m_name = "";

	/**
	 * This is the constructor used by AppConfig to instantiate the config object.
   * Then, AppConfig calls this object's init(Element) method passing the configuration
   * element it retrieved from the XML configuration document which this object uses
   * to configure itself.  After this object has initialized itself,
   * it will be used to instantiate and initialize the framework object
   * (MessageObject, Producers, Consumers, ThreadPools etc.)
   * with the properties it's been initialized with.
	 */
  public DbConnectionPoolConfig() {
		setType("DbConnectionPoolConfig");
  }
	public DbConnectionPoolConfig(Element configElement) throws EnterpriseConfigurationObjectException {
		setType("DbConnectionPoolConfig");
		init(configElement);
	}


	/**
	 * Sets the name asociated to the DbConnectionPoolConfig as specified in the deployment document.  Specifically,
   * this is the "name" attribute associated to the DbConnectionPoolConfig Element.  This will be the
   * name by which the DbConnectionPool Java object is known.
   *
   * @param name String the name found in the DbConnectionPoolConfig@name attribute.
	 */
  public void setName(String name) {
    m_name = name;
  }
	/**
	 * Returns the name asociated to the DbConnectionPoolConfig as specified in the deployment document.  Specifically,
   * this is the "name" attribute associated to the DbConnectionPoolConfig Element.  This will be the
   * name by which the DbConnectionPool Java object is known.
   *
   * @return String the name found in the DbConnectionPoolConfig@name attribute.
	 */
  public String getName() {
    return m_name;
  }

	/**
	 * Implements the init(Element) method that all EnterpriseConfiguration objects must implement.
   * This init method takes the Configuration element passed in and pulls out configuration information
   * specific to the EnterpriseConnectionPool Java object being initialized.
   * Then it sets various instance variables and properties on itself which will
   * be used by the EnterpriseConnectionPool Java object when AppConfig instantiates it passing this configuration object.
   * The DbConnectionPool object will then use this configuration java object to initialize itself.
   *
   * @param configElement Element the configuration element that AppConfig has pulled from the configuration document
   * relevant to the EnterpriseConnectionPool being configured.  Or, the element that was found in the init() method.
   * @throws EnterpriseConfigurationObjectException if errors occur processing the configuration Element.
   * @see org.openeai.dbpool.EnterpriseConnectionPool
	 */
	public void init(Element configElement) throws EnterpriseConfigurationObjectException {
    String dbPoolName = configElement.getAttribute("name").getValue();
    setName(dbPoolName);

    // For now, everything's specified as an attribute so this should
    // take care of everything.
    java.util.List attrs = configElement.getAttributes();
		for (int i=0; i<attrs.size(); i++) {
			Attribute attr = (Attribute)attrs.get(i);
			String key = attr.getName();
			String value = attr.getValue();
			logger.debug("Adding " + key + " - " + value);
			addProperty(key, value);
		}

    // Should be none, for now as everything is specified as attributes.
		java.util.List props = configElement.getChildren();
		for (int i=0; i<props.size(); i++) {
			Element aProp = (Element)props.get(i);
			String key = aProp.getName();
			String value = aProp.getText();
			logger.debug("Adding " + key + " - " + value);
			addProperty(key, value);
		}
  }

	/**
	 * Implements the init() method that all EnterpriseConfiguration objects must implement.
   * This init method retreives the root element of the confuration document and
   * then finds the specific configuration element associated to the EnterpriseConnectionPool being configured
   * then it calls the init(Element) method which actually initializes the DbConnectionPoolConfig
   * with the information found in the configuration element.
   *
	 */
	private void init() throws EnterpriseConfigurationObjectException {
		Element rootElement = getConfigDoc().getRootElement();
		logger.debug("RootElement is: " + rootElement.getName());
		logger.debug("Looking for DbConnectionPoolConfig named: " + getName());
		// Find the element specified by threadPoolName in the document
		Element configElement = getConfigElementByAttributeValue(getName(), "name");
		init(configElement);
	}
}

