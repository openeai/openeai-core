package org.openeai.config;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.jdom.Document;
import org.openeai.OpenEaiObject;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

public class ConfigDocProviderDefaultImpl extends OpenEaiObject implements ConfigDocProvider {
    private static String CONFIG_DOC_READER_JVM_PROP = "configDocReaderClassName";
    private static String SECRET_PROVIDER_JVM_PROP = "secretProviderClassName";

    private String configDocReaderClassName=null;
    private ConfigDocReader configDocReader=null;
    private String secretProviderClassName=null;
    private SecretProvider secretProvider=null;

    // 1. look at configDocReaderClassName JVM param to get the configdocreader
    //	  and instantiate it (if exists)  e.g., S3ConfigDocReader
    // 2. call configdocreader.readConfigDocAsString to get the string
    //    representation of the config doc
    // 3. look at secretProviderClassName JVM param to get the secretprovider
    //	  and instantiate it (if exists)  e.g., SecretsManagerSecretProvider
    // 4. call secretprovider.replaceSecrets to get string representation of config doc
    // 5. convert the modified config doc to an XmlDocument
    // 6. return the XmlDocument to AppConfig

    private String getConfigDocReaderClassName() {
    	if (configDocReaderClassName != null) {
    		return configDocReaderClassName;
    	}
		configDocReaderClassName = System.getProperty(CONFIG_DOC_READER_JVM_PROP);
		if (configDocReaderClassName == null) {
			configDocReaderClassName = System.getenv(CONFIG_DOC_READER_JVM_PROP);
		}
        logger.info("configDocReaderClassName [JVM]: " + configDocReaderClassName);
		return configDocReaderClassName;
    }
    private boolean isConfigDocReaderEnabled() {
		if (getConfigDocReaderClassName() != null) {
			return true;
		}
    	return false;
    }
    private ConfigDocReader initializeConfigDocReader(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
    	if (className == null) {
    		return null;
    	}
    	if (configDocReader != null) {
    		return configDocReader;
    	}
        java.lang.Class<?> obj;
		obj = java.lang.Class.forName(className);
        ConfigDocReader reader = (ConfigDocReader) obj.newInstance();
        return reader;
    }

    private String getSecretProviderClassName() {
    	if (secretProviderClassName != null) {
    		return secretProviderClassName;
    	}
    	secretProviderClassName = System.getProperty(SECRET_PROVIDER_JVM_PROP);
		if (secretProviderClassName == null) {
			secretProviderClassName = System.getenv(SECRET_PROVIDER_JVM_PROP);
		}
        logger.info("secretProviderClassName [JVM]: " + secretProviderClassName);
		return secretProviderClassName;
    }
    private boolean isSecretProviderEnabled() {
		if (getSecretProviderClassName() != null) {
			return true;
		}
    	return false;
    }
    private SecretProvider initializeSecretProvider(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
    	if (className == null) {
    		return null;
    	}
    	if (secretProvider != null) {
    		return secretProvider;
    	}
        java.lang.Class<?> obj;
		obj = java.lang.Class.forName(className);
		SecretProvider provider = (SecretProvider) obj.newInstance();
        return provider;
    }

    @Override
    public Document getConfigDoc(String docUri, String appId) throws ConfigDocProviderException {
    	if (isConfigDocReaderEnabled()) {
    		try {
				ConfigDocReader reader = 
					initializeConfigDocReader(getConfigDocReaderClassName());
	            if (isSecretProviderEnabled()) {
	            	// use secret provider to replace patterns in the config doc
	            	// with actual secret values from wherever it gets them from
	            	SecretProvider sp = 
	            		initializeSecretProvider(getSecretProviderClassName());
	            	String newConfigDocString = 
	            		sp.replaceSecrets(reader.
	            			readConfigDocAsString(docUri, appId));
	            	InputStream is = 
		            		new ByteArrayInputStream(newConfigDocString.getBytes());
	            	XmlDocumentReader xmlReader = new XmlDocumentReader();
	            	return xmlReader.initializeDocument(is, false);
	            }
	            else {
	            	// no secret provider, just return the config doc
	            	return reader.readConfigDocAsDocument(docUri, appId);
	            }
			} 
    		catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new ConfigDocProviderException(e.getMessage(), e);
			} 
    		catch (InstantiationException e) {
				e.printStackTrace();
				throw new ConfigDocProviderException(e.getMessage(), e);
			} 
    		catch (IllegalAccessException e) {
				e.printStackTrace();
				throw new ConfigDocProviderException(e.getMessage(), e);
			} catch (XmlDocumentReaderException e) {
				e.printStackTrace();
				throw new ConfigDocProviderException(e.getMessage(), e);
			} catch (ConfigDocReaderException e) {
				e.printStackTrace();
				throw new ConfigDocProviderException(e.getMessage(), e);
			} catch (SecretProviderException e) {
				e.printStackTrace();
				throw new ConfigDocProviderException(e.getMessage(), e);
			}
    	}
    	else {
    		// use the default reader (PublicFileConfigDocReader)
    		ConfigDocReader reader = new PublicFileConfigDocReader();
            if (isSecretProviderEnabled()) {
            	// use secret provider to replace patterns in the config doc
            	// with actual secret values from wherever it gets them from
				try {
					SecretProvider sp = initializeSecretProvider(getSecretProviderClassName());
	            	String newConfigDocString = 
	                		sp.replaceSecrets(reader.
	                			readConfigDocAsString(docUri, appId));
	            	InputStream is = 
		            		new ByteArrayInputStream(newConfigDocString.getBytes());
                	XmlDocumentReader xmlReader = new XmlDocumentReader();
                	return xmlReader.initializeDocument(is, false);
				} 
				catch (ClassNotFoundException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				} 
				catch (InstantiationException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				} 
				catch (IllegalAccessException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				} catch (XmlDocumentReaderException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				} catch (ConfigDocReaderException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				} catch (SecretProviderException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				}
            }
            else {
            	// no secret provider, just return the config doc
            	try {
					return reader.readConfigDocAsDocument(docUri, appId);
				} catch (ConfigDocReaderException e) {
					e.printStackTrace();
					throw new ConfigDocProviderException(e.getMessage(), e);
				}
            }
    	}
    }

    @Override
    public Document getConfigDoc(Properties props) throws ConfigDocProviderException {
        String appId = props.getProperty("messageComponentName");
        if (appId == null) {
            throw new ConfigDocProviderException("Null 'messageComponentName' not allowed.");
        }
        String docUri = props.getProperty("providerUrl");
        if (docUri == null) {
            throw new ConfigDocProviderException("Null 'providerUrl' not allowed.");
        }

        return getConfigDoc(docUri, appId);
    }
}
