/*******************************************************************************
 $Source$
 $Revision: 3648 $
*******************************************************************************/

/**********************************************************************
This file is part of the OpenEAI Application Foundation or
OpenEAI Message Object API created by Tod Jackson
(tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
the University of Illinois Urbana-Champaign.

Copyright (C) 2002 The OpenEAI Software Foundation

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

For specific licensing details and examples of how this software
can be used to build commercial integration software or to implement
integrations for your enterprise, visit http://www.OpenEai.org/licensing.
*/

package org.openeai.config;

import java.io.Serializable;
import java.util.*;
import org.openeai.*;

/**
 * This object is used to perform translations from application values to Enterprise
 * values and vice versa.  It contains a Hashtable of EnterpriseMapping objects and
 * uses those mappings to translate the values.  The information in the Mappings is
 * specified in the EnterpriseObjects document and are associated to a specific field.
 *<P>
 * This object is built by the EnterpriseFields object when that object is initialized
 * and it reads through the EnterpriseObjects XML Document and finds a Field that has
 * a Translation associated with it.
 *<P>
 * For example, the 'Enterprise Values' as specified in a hypothetical EnterpriseObject document
 * for the Gender Element in the BasicPerson object
 * are 'Male' and 'Female'.  This means that those are the only allowable values that can
 * be set on the BasicPerson.gender instance variable.  Data is set on the gender field via
 * the BasicPerson.setGender(String) method.  When this data is passed through the BasicPerson's
 * EnterpriseFields object in that setter method, it attempts to translate the value being passed in to one of these
 * enterprise values if possible.  If the data passed in is not equal to or cannot be translated
 * to an enterprise value, an exception is thrown.
 * <P>
  * @author      Tod Jackson (tod@openeai.org)
  * @author      Steve Wheat (steve@openeai.org)
  * @version     3.0  - 28 January 2003
  * @see Field
  * @see EnterpriseMapping
  * @see EnterpriseFormatter
  * @see EnterpriseFields
 */
public class EnterpriseTranslator extends OpenEaiObject  implements Serializable {

  private Hashtable m_mappings = new Hashtable();

	/**
	 * Constructor
	 */
	public EnterpriseTranslator() {
	}

  /**
   * Returns a Hashtable of EnterpriseMapping objects associated to this translator.
   * @return Hashtable the mappings associated to this translator.
   */
	public final Hashtable getMappings() {
		return m_mappings;
	}
  private java.util.List getMapsForField(String objectFieldName) throws EnterpriseTranslationException {
    if (m_mappings.containsKey(objectFieldName)) {
  		return (java.util.List)m_mappings.get(objectFieldName);
    }
    else {
      String errMsg = "Can't find mapping for " + objectFieldName;
      logger.fatal(errMsg);
      throw new EnterpriseTranslationException(errMsg);
    }
  }
  /**
   * Adds a list of EnterpriseMapping objects to this translator to the Hashtable of mappings.
   * This the key of this Hashtable is the concatenation of the parent object plus the field name (for uniqueness).
   * For example, the BasicPerson/Gender field contains a list of mappings.  The key for that list of mappings
   * would be "BasicPerson/Gender".  This way, if Gender is ever used in another object besides BasicPerson,
   * we can distinguish between the two.
   */
	public final void addMapping(String objectFieldName, java.util.List vMapsForField)
    throws EnterpriseTranslationException {

    if (m_mappings.containsKey(objectFieldName)) {
      String errMsg = "Can't add mapping for " + objectFieldName + " because one already exists.";
      logger.fatal(errMsg);
      throw new EnterpriseTranslationException(errMsg);
    }
    else {
  		m_mappings.put(objectFieldName, vMapsForField);
    }
	}

  /**
   * This method converts the value passed in to the enterprise value and returns that value for the field specified.  It
   * uses it's list of EnterpriseMappings to check and see if it can find an application specific value that matches 
   * the value passed in.  If it does, it returns the enterprise value associated to the mapping that contains that application
   * value.  If the value passed in is empty and the field is not required and there is no mapping that accounts for
   * an empty application value, the method will return an empty string.  All other values passed in must result in 
   * a successful mapping from applicaiton to enterprise value or an exception will be thrown.
   *<P>
   * This method is called by the EnterpriseFields.getEnterpriseValue method.
   *<P>
   * @return String the enterprise value
   * @param objectFieldName String the parent/child field name (eg. - BasicPerson/Gender) that is the key in the list of EnterpriseMappings
   * @param fieldName String the name of the child field (not used for anything currently)
   * @param value String the application specific value being mapped to an enterprise value
   * @param isRequired boolean a flag indicating whether or not this field is required.  true=is required, false=is not required
   * @throws EnterpriseTranslationException if the application value cannot be successfully mapped to an enterprise value.
   * @see EnterpriseMapping
   * @see EnterpriseFields#getEnterpriseValue(String, String, String)
   */
	public final String convertToEnterpriseValue(String objectFieldName, String fieldName, String value, boolean isRequired)
	throws EnterpriseTranslationException {

		logger.debug("Translating field: " + objectFieldName);
		boolean foundValue = false;
		boolean foundField = false;
		String enterpriseValue = "";

    // New way
    Hashtable maps = getMappings();

    //debug
    /*
    if (fieldName.equalsIgnoreCase("ethnicity")) {
      Enumeration enum = maps.keys();
      logger.info("Keys that exist in 'maps':");
      String key = null;
      while (enum.hasMoreElements()) {
        key = (String)enum.nextElement();
        logger.info("Key: " + key);
      }
      logger.info("'maps' size " + maps.size());
      logger.info("'maps.containsKey(key)' ==> " + maps.containsKey(key));
      logger.info("'maps.containsKey(objectFieldName)' ==> " + maps.containsKey(objectFieldName));
      Vector vtMapsForField = (Vector)maps.get(key);
      logger.info("'mapsForField' size " + vtMapsForField.size());
    }
    */
    //end debug

    if (maps.containsKey(objectFieldName)) {
      java.util.List vMapsForField = (java.util.List)maps.get(objectFieldName);
      mapTest: for (int i=0; i<vMapsForField.size(); i++) {
  			EnterpriseMapping aMap = (EnterpriseMapping)vMapsForField.get(i);
	  		foundField = true;
		  	HashMap values = aMap.getApplicationValues();
			  enterpriseValue = aMap.getEnterpriseValue().trim();
        if (value.trim().equalsIgnoreCase(enterpriseValue)) {
          foundValue = true;
          break mapTest;
        }
        else if (values.containsKey(value.trim())) {
				  foundValue = true;
          break mapTest;
  			}
      }
    }

		if (foundField) {
			if (foundValue == false) {
        if (isRequired == true || value.trim().length() > 0) {
          String errMsg = "'" + value +
            "' is not a valid application value for " + objectFieldName;
          logger.fatal("Translation Error: " + errMsg);
  				throw new EnterpriseTranslationException(errMsg);
        }
        else {
          enterpriseValue = value;
        }
			}
		}
		else {
			// If we didn't find a mapping for the current field, we'll just return
			// the value they passed in as the enterprise value.
			// This means, there is no translation associated with the field.
			logger.debug("Couldn't find a mapping for field " + objectFieldName);
			enterpriseValue = value;
		}
		logger.debug("Translated " + objectFieldName + " from '" + value + "' to '" + enterpriseValue + "'");
		return enterpriseValue;
	}

  /**
   * This method converts the enterprise value passed in to the corresponding application specific value and 
   * returns that value for the field specified.  It uses it's list of EnterpriseMappings to check and see if 
   * it can find an enterprise value that matches the value passed in.  If it does, it then attempts to find
   * an application specific value that the enterprise value should be mapped to for the application name 
   * passed in.  If no application specific value can be located, it will simply return the enterprise value
   * passed in.
   * <P>
   * Note, applications may have multiple application specific values that get translated to one
   * enterprise value but that enterprise value can only be converted back to ONE application value.  Therefore,
   * when these mappings are specified in the EnterpriseObjects documents, application specific values that will
   * be used for "reverse translations" must be indicated as such by specifying the "preferred" attribute with a value
   * of "true".  This way, if there is more than one application value associated to a field for the same application
   * the foundation will know which application value should be used when a reverse translation is performed.
   *<P>
   * This method is called by the EnterpriseFields.getApplicationValue method.
   *<P>
   * @return String the application specific value that maps to the enterprise value passed in or simply
   * the enterprise value passed in if no "reverse" translations exist for the field.
   * @param appName String the name of the application for which the enterprise value is being converted.
   * @param objectFieldName String the parent/child field name (eg. - BasicPerson/Gender) that is the key in the list of EnterpriseMappings
   * @param fieldName String the name of the child field (not used for anything currently)
   * @param enterpriseValue String the enterprise value being mapped to an application specific value
   * @throws EnterpriseTranslationException if an error occurs attempting to map the enterprise value to an application value.
   * @see EnterpriseMapping
   * @see EnterpriseFields#getApplicationValue(String, String, String, String)
   */
	public final String convertToAppValue(String appName, String objectFieldName, String fieldName, String enterpriseValue)
	throws EnterpriseTranslationException {

		// Get the map matching "fieldName"
		// Loop through the contents of the map until we find an application
		// matching appName.  If found, return that application's value for this field
		// If not found, return the enterprise value.

		logger.debug("[EnterpriseTranslator] Getting app value for " + objectFieldName + " for application " +
      appName + " enterprise value: " + enterpriseValue);

		boolean foundValue = false;
		boolean foundField = false;
		String appValue = "";

    Hashtable maps = getMappings();
    if (maps.containsKey(objectFieldName)) {
      java.util.List vMapsForField = (java.util.List)maps.get(objectFieldName);
      if (vMapsForField.size() == 0) {
        appValue = enterpriseValue;
      }
      mapTest: for (int i=0; i<vMapsForField.size(); i++) {
  			EnterpriseMapping aMap = (EnterpriseMapping)vMapsForField.get(i);
	  		foundField = true;
		  	String entValue = aMap.getEnterpriseValue();
			  if (entValue.equals(enterpriseValue)) {
  				HashMap values = aMap.getApplicationValues();
	  			if (values.containsValue(appName)) {
            Iterator it5 = values.keySet().iterator();
            while (it5.hasNext()) {
              String key = (String)it5.next();
              String a = (String)values.get(key);
              if (a.equalsIgnoreCase(appName)) {
                appValue = key;
    			  		logger.debug("Translated " + entValue + " to " + appValue + " for application " + appName);
                break mapTest;
              }
            }
				  }
          // 4/3/2002 fix
          else if (values.containsKey(enterpriseValue)) {
            appValue = enterpriseValue;
            logger.debug(objectFieldName + ":Value " + entValue + " already matches value for application " + appName);
            break mapTest;
          }
          // Added 4/22/2002
	  			else if (values.containsValue("Default")) {
		  			appValue = (String)values.get("Default");
            Iterator it5 = values.keySet().iterator();
            while (it5.hasNext()) {
              String key = (String)it5.next();
              String a = (String)values.get(key);
              if (a.equalsIgnoreCase("Default")) {
                appValue = key;
    			  		logger.debug("Translated " + entValue + " to " + appValue +
                  " for application " + appName + ".  Application value didn't exist, using 'Default' application value.");
                break mapTest;
              }
            }
				  }
  				else {
	  				logger.debug("The application " + appName +
		  									 " does not have a specific value for field " + fieldName +
			  								 " with an enterprise value of " + entValue);
				  	appValue = entValue;
            /*
            logger.debug("The Application values hashmap consists of:");
            Iterator it5 = values.keySet().iterator();
            while (it5.hasNext()) {
              String key = (String)it5.next();
              String a = (String)values.get(key);
              logger.debug("key/value -> " + key + "/" + a);
            }
            */

            break mapTest;
  				}
	  		}
      }
    }
    else {
      appValue = enterpriseValue;
    }

		return appValue;
	}

	public final String toString() {
		String retValue = "";
		return retValue;
	}
}

