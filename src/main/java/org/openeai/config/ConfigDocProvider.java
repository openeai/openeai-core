package org.openeai.config;

import java.util.Properties;

import org.jdom.Document;

public interface ConfigDocProvider {
	Document getConfigDoc(String docUri, String appId) throws ConfigDocProviderException;
	Document getConfigDoc(Properties props) throws ConfigDocProviderException;
	
	// 1. look at configDocReaderClassName JVM param to get the configdocreader 
	//	  and instantiate it (if exists)  e.g., S3ConfigDocReader
	// 2. call configdocreader.readConfigDocAsString to get the string 
	//    representation of the config doc
	// 3. look at secretProviderClassName JVM param to get the secretprovider 
	//	  and instantiate it (if exists)  e.g., SecretsManagerSecretProvider
	// 4. call secretprovider.replaceSecrets to get string representation of config doc
	// 5. convert the modified config doc to an XmlDocument
	// 6. return the XmlDocument to AppConfig
}
