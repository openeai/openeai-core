package org.openeai.jms.consumer.commands.provider;

//import static org.junit.jupiter.api.Assertions.*;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;

import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

//@Ignore
public class AbstractProviderTest {
    String docUriBase_dev = "https://dev-config.app.emory.edu/";
    String docUriBase_qa = "https://qa-config.app.emory.edu/";
    String docUriBase_stage = "https://staging-config.app.emory.edu/";
    String docUriBase_prod = "https://config.app.emory.edu/";


    @Test
    public void testDev() {
        System.setProperty("docUriBase", docUriBase_dev);
        assertEquals("dev", AbstractProvider.getDeployEnv());
    }
    @Test
    public void testQa() {
        System.setProperty("docUriBase", docUriBase_qa);
        assertEquals("qa", AbstractProvider.getDeployEnv());
    }
    @Test
    public void testStage() {
        System.setProperty("docUriBase", docUriBase_stage);
        assertEquals("staging", AbstractProvider.getDeployEnv());
    }
    @Test
    public void testProd() {
        System.setProperty("docUriBase", docUriBase_prod);
        assertEquals("prod", AbstractProvider.getDeployEnv());
    }
    @Test
    public void testNotSet() {
        System.setProperty("docUriBase", "");
        assertEquals("", AbstractProvider.getDeployEnv());
    }
}
