package org.openeai.xml;

import junit.framework.TestCase;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class XmlDocumentReaderTest extends TestCase {
    final String docUriBase = "https://bitbucket.org/openeai/openeai-core/raw/4d3c7641cc25fae9941ab58147c3ffa5ed200725/";
     String currentDir= Paths.get(".").toAbsolutePath().normalize().toString()+"/";
     String xmlFileRelativePath="message/releases/org/openeai/CoreMessaging/Generic/1.0/xml/Response-Reply.success1.xml";
    String xmlFileRelativePathTestOnly="message/releases/org/openeai/CoreMessaging/Generic/1.0/xml/Response-Reply.testOnly.xml";
     String testResourceFolder="src/test/resources/";
     String xmlFilePathFromWorkingDir=testResourceFolder+xmlFileRelativePath;
    @Test
    public void testBuildDocFromCompleteAddressForFile() throws IOException, XmlDocumentReaderException, JDOMException {
        SAXBuilder builder=new SAXBuilder(false);
        builder.setEntityResolver(new IgnoreDTDResolver());
        assertNotNull(XmlDocumentReader.buildDocFromCompleteAddress("file://"+currentDir+xmlFilePathFromWorkingDir,builder ));
    }

    @Test
    public void testInitializeForFullFileUri() throws IOException, XmlDocumentReaderException {
        //assertTrue(XmlDocumentReader.urlExists("file:///Users/gwang28/project/emoryoit/runtime/dev/"));
        //assertTrue(XmlDocumentReader.urlExists("/Users"));
        assertNotNull(new XmlDocumentReader().initializeDocument("file://"+currentDir+xmlFilePathFromWorkingDir, false));

    }


    @Test
    public void testInitializeHttps() throws IOException, XmlDocumentReaderException {
        assertNotNull(new XmlDocumentReader().initializeDocument(docUriBase+xmlFileRelativePath, false));
    }


    @Test
    public void testInitializeWithDocUriBaseHttps() throws IOException, XmlDocumentReaderException {
        System.setProperty("docUriBase", docUriBase);
        assertNotNull(new XmlDocumentReader().initializeDocument(xmlFileRelativePath, false));
    }


    @Test
    public void testInitializeWithDocUriBaseAbsolutePath() throws IOException, XmlDocumentReaderException {
        System.setProperty("docUriBase", currentDir);
        assertNotNull(new XmlDocumentReader().initializeDocument(xmlFilePathFromWorkingDir, false));
    }

    @Test
    public void testInitializeWithDocUriBaseRelativePath() throws IOException, XmlDocumentReaderException {
        System.setProperty("docUriBase", testResourceFolder);
        assertEquals(true, new File(testResourceFolder+xmlFileRelativePath).exists());
        assertNotNull(new XmlDocumentReader().initializeDocument(xmlFileRelativePath, false));
    }

    @Test
    public void testInitializeFromClasspath() throws IOException, XmlDocumentReaderException {
        assertEquals("",System.getProperty("docUriBase"));
        assertEquals(false, new File(xmlFileRelativePath).exists());
        assertNotNull(new XmlDocumentReader().initializeDocument(xmlFileRelativePath, false));
    }


    @Test
    public void testInitializeNotFromopeneaiClasspathFirstWhenopeneaiClasspathFirstIsNotSet() throws IOException, XmlDocumentReaderException {
        System.out.println("openeaiClasspathFirst System property="+System.getProperty(XmlDocumentReader.OPENEAI_CLASSPATH_FIRST));
        boolean isopeneaiClasspathFirst=new XmlDocumentReader().openeaiClasspathFirst;
        System.out.println("isopeneaiClasspathFirst="+isopeneaiClasspathFirst);
        assertEquals(false,isopeneaiClasspathFirst);
        //assertEquals("",System.getProperty("docUriBase"));
        assertEquals(false, new File(xmlFileRelativePath).exists());
        Document document=new XmlDocumentReader().initializeDocument(xmlFileRelativePath, false);
        assertNotNull(document);
        String senderAppId=document.getRootElement().getChild("ControlAreaReply").getChild("Sender").getChild("MessageId").getChild("SenderAppId").getText();
        assertEquals("fromFilePathSenderAppId",senderAppId);
    }
    /**
     * https://stackoverflow.com/questions/27155195/how-getclassloader-getresourceasstream-works-in-java
     * @throws IOException
     * @throws XmlDocumentReaderException
     */
    @Test
    public void testFilelFromClasspathExistst() throws IOException, XmlDocumentReaderException {
        System.out.println("xmlFileRelativePathTestOnly="+xmlFileRelativePathTestOnly);
        URL resource=getClass().getResource("/"+xmlFileRelativePathTestOnly);
        System.out.println("resource="+resource);
        assertNotNull(resource);
    }


    @Test
    public void testFilelFromClasspathExiststNoSlash() throws IOException, XmlDocumentReaderException {
        System.out.println("xmlFileRelativePathTestOnly="+xmlFileRelativePathTestOnly);
        URL resource=getClass().getClassLoader().getResource(xmlFileRelativePathTestOnly);
        System.out.println("resource="+resource);
        assertNotNull(resource);
    }
//    String serviceEOFilePath="configs/messaging/Environments/Examples/EnterpriseObjects/3.0/com/google/gcp/Services/1.0/ServiceEO.xml";
//
//    @Test
//    public void testFilelFromClasspathExiststForServiceEO() throws IOException, XmlDocumentReaderException {
//        System.out.println("xmlFileRelativePathTestOnly="+serviceEOFilePath);
//        URL resource=getClass().getResource("/"+serviceEOFilePath);
//        System.out.println("resource="+resource);
//        assertNotNull(resource);
//    }
//
//    @Test
//    public void testFilelFromClasspathExiststForServiceEONoSlash() throws IOException, XmlDocumentReaderException {
//        System.out.println("xmlFileRelativePathTestOnly="+serviceEOFilePath);
//        URL resource=getClass().getClassLoader().getResource(serviceEOFilePath);
//        System.out.println("resource="+resource);
//        assertNotNull(resource);
//    }



    @Test
    public void testInitializeromClasspathFirstWhenClasspathFirstIsSetTrue() throws IOException, XmlDocumentReaderException {
        System.setProperty("docUriBase", docUriBase);
        System.setProperty(XmlDocumentReader.OPENEAI_CLASSPATH_FIRST,"true");
        System.out.println("openeaiClasspathFirst:="+new XmlDocumentReader().openeaiClasspathFirst+",docUriBase="+docUriBase);
        assertTrue(new XmlDocumentReader().openeaiClasspathFirst);
        assertEquals(false, new File(xmlFileRelativePathTestOnly).exists());
        Document document=new XmlDocumentReader().initializeDocument(xmlFileRelativePathTestOnly, false);
        assertNotNull(document);
        String senderAppId=document.getRootElement().getChild("ControlAreaReply").getChild("Sender").getChild("MessageId").getChild("SenderAppId").getText();
        System.out.println("senderAppId="+senderAppId);
        assertEquals("fromClassPathSenderAppId",senderAppId);
    }
    @Override
    protected void setUp() throws Exception {
       System.setProperty(XmlDocumentReader.DOC_URI_BASE,"");
        System.setProperty(XmlDocumentReader.OPENEAI_CLASSPATH_FIRST,"");
    }

    @Override
    protected void tearDown() throws Exception {
        //System.setProperty("docUriBase", "");
    }
}